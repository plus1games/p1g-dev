<?php
/**
 * Template Name: P1G Search Template
 *
 * This is the template for P1G search result.  It is used when we search on home and get result on this page.
 */
get_header();
$tmpdata = get_option('templatic_settings');
global $posts,$htmlvar_name,$wp_query,$wpdb,$query;

/********************************************************************************************************************/
//Search content here
$term = $_GET['p1g_search_text'];

if($_GET['p1g_search_cat'] == 'Places') {
	
	$query = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='City' AND meta_value like '%".$term."%') OR post_title like '%".$term."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345)";
	
} else if($_GET['p1g_search_cat'] == 'Sport Groups') {
	
	$query = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='City' AND meta_value like '%".$term."%') OR post_title like '%".$term."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406)";
	
} else if($_GET['p1g_search_cat'] == 'Sporting Goods') {
	
	$query = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='City' AND meta_value like '%".$term."%') OR post_title like '%".$term."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400)";
	
} else if($_GET['p1g_search_cat'] == 'Games') {
	
	$query = $wpdb->prepare("SELECT DISTINCT id FROM wp_posts AS p 
				 JOIN wp_postmeta AS pm ON pm.post_id = p.id 
				 WHERE p.post_type = 'event' AND post_status = 'publish' AND ((meta_key='%s' AND meta_value like '%s') OR post_title like '%s') ORDER BY post_title", 'City','%'.$term .'%','%'.$term .'%'); 
	
}

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
	'post_type'      => $_GET['p1g_search_cat'] == 'Games' ? 'event' : 'listing',
	'posts_per_page' => -1,
	'post__in'       => $wpdb->get_col($query),
	'showposts'      => 10,
	'paged'          => $paged,
);
// The Query
$the_query = new WP_Query( $args );
/*echo "<pre>";
print_r($the_query);
echo "</pre>";	
die('My Debug');*/

/********************************************************************************************************************/

do_action('after_directory_header');

	/*do action for display the breadcrumb in between header and container. */
	do_action('directory_before_container_breadcrumb'); 
	
	do_action( 'before_content' );

 ?>
<div id="content" class="contentarea large-9 small-12 columns <?php directory_class();?>">
	
	<?php 
	do_action( 'open_content' );
	do_action('directory_inside_container_breadcrumb'); /*do action for display the breadcrumb  inside the container. */ 	
	?>
     
    <div class="view_type_wrap">
		<?php
		/* Hooks for category title */
		do_action('directory_before_categories_title'); /* html variables are coming from this action in this plugin */ ?>

		<h1 class="loop-title"><?php wp_title();echo ": ".$term; ?></h1>

		<?php do_action('directory_after_categories_title'); 
		/* Hooks for category title end */

		/* Hooks for category description */
		do_action('directory_before_categories_description'); 

		if ( category_description() ) : /* Show an optional category description */ ?>
		  <div class="archive-meta"><?php echo category_description(); ?></div>
		<?php endif; 

		do_action('directory_after_categories_description');
		/* Hooks for category description */
		
		do_action('directory_before_subcategory');
	 
		do_action('directory_subcategory');
			/* Loads the sidebar-before-content. */
			if(function_exists('supreme_sidebar_before_content'))
		   apply_filters('tmpl_before-content',supreme_sidebar_before_content() ); 
		   
		do_action('directory_after_subcategory');
	 ?>
    </div>
     <!--Start loop taxonomy page-->
   
	<?php do_action('directory_before_loop_taxonomy');?>
    
    <!--Start loop taxonomy page-->
    <section id="loop_listing_taxonomy" class="search_result_listing <?php if(isset($tmpdata['default_page_view']) && $tmpdata['default_page_view']=="gridview"){echo 'grid';}else{echo 'list';}?>" <?php if( is_plugin_active('Tevolution-Directory/directory.php') && isset($tmpdata['default_page_view']) && $tmpdata['default_page_view']=="mapview"){ echo 'style="display: none;"';}?>>
    <?php if ($the_query->have_posts()) : 
			while ($the_query->have_posts()) : $the_query->the_post(); ?>
				<?php do_action('directory_before_post_loop'); ?>
				 
				<article class="post  <?php templ_post_class();?>" >  
					
					<?php 
					/* Hook to display before image */	
					do_action('directory_before_category_page_image');
						
					/* Hook to Display Listing Image  */
					do_action('directory_category_page_image');
					 
					/* Hook to Display After Image  */						 
					do_action('directory_after_category_page_image'); 
					   
					/* Before Entry Div  */	
					do_action('directory_before_post_entry');?>
					
					<!-- Entry Start -->
					<div class="entry"> 
					   
						<?php  /* do action for before the post title.*/ 
						do_action('directory_before_post_title');         ?>
					   <div class="listing-wrapper">
						<!-- Entry title start -->
						<div class="entry-title">
					   
						<?php do_action('templ_post_title');                /* do action for display the single post title */?>
					   
						</div>
						
						<?php do_action('directory_after_post_title');          /* do action for after the post title.*/?>
					   
						<!-- Entry title end -->
						
						<!-- Entry details start -->
						<div class="entry-details">
						
						<?php  /* Hook to get Entry details - Like address,phone number or any static field  */  
						do_action('listing_post_info');   ?>
						
						</div>
						<!-- Entry details end -->
					   </div>
						<!--Start Post Content -->
						<?php /* Hook for before post content . */ 
					   
						do_action('directory_before_post_content'); 
						
						/* Hook to display post content . */ 
						do_action('templ_taxonomy_content');
					   
						/* Hook for after the post content. */
						do_action('directory_after_post_content'); 
						?>
						<!-- End Post Content -->
					   <?php 
					    /* Hook for before listing categories     */
						do_action('directory_before_taxonomies');
					    
						/* Display listing categories     */
					    do_action('templ_the_taxonomies'); 

						/* Hook to display the listing comments, add to favorite and pinpoint   */						
						do_action('directory_after_taxonomies');?>
					</div>
					<!-- Entry End -->
					<?php do_action('directory_after_post_entry');?>
				</article>
			<?php do_action('directory_after_post_loop');
          	endwhile;
			if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
				<nav class="prev-next-posts">
					<div class="prev-posts-link">
				  		<?php echo get_next_posts_link( 'Older Posts', $the_query->max_num_pages ); // display older posts link ?>
					</div>
					<div class="next-posts-link">
				  		<?php echo get_previous_posts_link( 'Newer Posts' ); // display newer posts link ?>
					</div>
			  	</nav>
			<?php } 
			wp_reset_postdata();
			wp_reset_query(); 
			/* Restore original Post Data */
		else:?>
          	<p class='nodata_msg'><?php _e( 'Apologies, but no results were found for the requested archive.', DIR_DOMAIN ); ?></p>              
        <?php endif; 
        
        /* pagination start */
		if($wp_query->max_num_pages !=1):?>
		<div id="listpagi">
			<div class="pagination pagination-position">
				<?php if(function_exists('pagenavi_plugin')) { pagenavi_plugin(); } ?>
			</div>
		</div>
		<?php endif; /* pagination end */ ?>
    </section>
    <?php 
	do_action('directory_after_loop_taxonomy');
	 
	if(function_exists('supreme_sidebar_after_content'))
		apply_filters('tmpl_after-content',supreme_sidebar_after_content());  /* after-content-sidebar - use remove filter to don't display it */
		
	do_action( 'close_content' );
	?>
      <!--End loop taxonomy page -->
</div>
<!--taxonomy  sidebar -->
<?php
do_action( 'after_content' );
$queried_object = get_queried_object();   
$term_id = $queried_object->term_id; 
$taxonomies = get_object_taxonomies( (object) array( 'post_type' => get_post_type() ,'public'   => true, '_builtin' => true ));
$children = get_term_children( 345,'listingcategory' );
array_push($children,'345');
$taxonomy_name = $taxonomies[0];
if(!in_array($term_id,$children)){
	/*echo "<style> 
			.ver-list-filter{
				display:none;
			}
		</style>";	*/		
}
if ( is_active_sidebar( $taxonomy_name.'_listing_sidebar') ) : ?>
	<aside id="sidebar-primary" class="sidebar large-3 small-12 columns">
		<?php dynamic_sidebar($taxonomy_name.'_listing_sidebar'); 
				dynamic_sidebar('sidebar-places'); 
			 
		 ?>
	</aside>
	
<?php 
elseif ( is_active_sidebar( 'primary-sidebar') ) : ?>
	<aside id="sidebar-primary" class="sidebar large-3 small-12 columns">
		<?php dynamic_sidebar('primary-sidebar'); ?>
	</aside>
<?php endif; ?>
<!--end taxonomy sidebar -->
<?php get_footer(); ?>