<?php
/**
 * Template Name: Outdoor Parks
 */
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
get_header();
global $wpdb,$EM_Location,$EM_Locations, $post;
//Indoor /Outdoor  Parks
$inoutdoor_locations="SELECT location_state , COUNT(location_name) as locations FROM wp_em_locations WHERE location_state !='' GROUP BY location_state ORDER BY location_state";
$result_inoutdoor=$wpdb->get_results($inoutdoor_locations);
foreach($result_inoutdoor as $in){
	$sql_cities="SELECT DISTINCT(location_town) FROM wp_em_locations WHERE location_state='".$in->location_state."' ORDER BY location_town";
	$result_cities=$wpdb->get_results($sql_cities);
	$indoorcount=0;
	$outdoorcount=0;
	foreach($result_cities as $rc){
		$sqllocationid="SELECT DISTINCT(location_id) as id FROM wp_em_locations WHERE location_town='".$rc->location_town."' AND location_state='".$in->location_state."'";
		$result_locationid=$wpdb->get_results($sqllocationid);
		foreach($result_locationid as $rl)	
		{
			$sqlfield="SELECT location_id ,field_id,field_indoor FROM wp_em_location_fields WHERE location_id='".$rl->id."'";
			$resultfield=$wpdb->get_results($sqlfield);
			$chkdoor=array();
			foreach($resultfield as $rv)
            {
                $chkdoor[] = $rv->field_indoor;
			}
			if(in_array('Indoor',$chkdoor)){
				$indoorcount++;
			}
			else{
				$outdoorcount++;
			}
		}
		
	}
	$indoor_count[]="['".$in->location_state."',".$indoorcount."]";
	$outdoor_count[]="['".$in->location_state."',".$outdoorcount."]";
}

 //print_r($state_count);
	$indoor_data=implode(',',$indoor_count);
	$outdoor_data=implode(',',$outdoor_count);

 
?>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    google.load('visualization', '1', { 'packages': ['geochart'] });
	//Outdoor Park
	google.setOnLoadCallback(drawMapoutdoor);
    function drawMapoutdoor() {
        var data = google.visualization.arrayToDataTable([
    ['Country', 'Number of Outdoor Locations'],
			<?php echo $outdoor_data; ?>
    /*['Alabama', 0],*/
    ]);
	    var options = {};
        options['region'] = 'US';
        options['resolution'] = 'provinces';
        options['colors'] = ['#b4e0a4', '#9bd686', '#82cc68', '#75b75d', '#5b8e48'];
		options['datalessRegionColor']=['#C1C1C1']
		options['legend'] = {'position': 'top'};
        var container = document.getElementById('map_outdoor');
        var geochart = new google.visualization.GeoChart(container);
		google.visualization.events.addListener(geochart, 'select', function() {
        var selection = geochart.getSelection()[0];
        var label = data.getValue(selection.row, 0);
		var action = 'outdoorpark'
		jQuery('#outdoorcontainer').text('Loading...');
		//alert (label);
        jQuery.ajax({
			   url    :'<?php echo get_template_directory_uri();?>/custom-ajax/parks-ajax.php',
			   data   :'state='+label+'&action='+action,
				   dataType:'html',
			   success:function(x){
					jQuery('#outdoorcontainer').html(x);
			   }
			 });
		});
        geochart.draw(data, options);
    };
  </script>
	<div id ='geomapleft' class='' style='width:68%;float:left;margin:20px;'>
		<div style=''>
			<h2 style="">USA Outdoor Parks Map </h2>
			<div id='map_outdoor' style="width: 98%; height: auto;float:left;max-width:550px;"></div>
			<div id='outdoorcontainer' style='clear:both;overflow:auto;'></div>
		</div><br/>
	</div>
	
	<?php
	echo "<div>";
	get_sidebar();
	echo "</div>";
	get_footer();
	?>