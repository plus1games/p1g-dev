<?php
/*
 * Template Name: Custom Contact us
 */

get_header(); ?>
<div id="map-canvas">
</div><div id="show_hide-map" ref="close">Hide the Map</div>
<style>
	#map-canvas{
	 height:350px;width:100%;margin-top:-75px;
	}
	#show_hide-map{
	cursor:pointer;
	float:right;background: #CDCDCD;padding:5px;width:100px;
	}
	#text-14{
     width:90%;
	}
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>
 var site_url='<?php  echo site_url(); ?>';
jQuery(document).ready( function($){
	$("#show_hide-map").click(function(){
	var attr_ref=	$(this).attr('ref');
		if(attr_ref == 'open'){
			  $(this).html('Hide the Map');
			  $(this).attr( "ref", "close" );
		}
		if(attr_ref == 'close'){
			  $(this).html('Show the Map');
			  $(this).attr( "ref", "open" );
		}
		$("#map-canvas").slideToggle("slow");
	});
});

function initialize() {
     var myLatlng = new google.maps.LatLng(41.613167,-93.735607);
     var icon_img1=site_url+"/wp-content/plugins/events-manager/images/gmap_marker_here2.png";
     var mapOptions = {
			zoom: 11,	
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
     }
	 var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	 var marker = new google.maps.Marker({
			  position: myLatlng,
			  map: map,
			  icon:icon_img1,
			  title: 'Plus1Games LLC.'
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>

<div class="center">
<div class="content alignleft">
<h2 style="text-indent:50px;"> Contact us now!</h2><?php  dynamic_sidebar( 'Contact Form' ) ; ?></div>
<div class="sidebar alignleft"><dl class="address">
	<dt>
		Plus1Games LLC.<br />
		2175 NW 86th St., Suite 6,<br />
		Clive, IA 50325
    </dt>
	  <dd><span>Telephone:</span> <a style='text-decoration:none;' href="callto:1-855-557-5871"><img src='http://www.aerius.co.nz/PicsHotel/aerius/Images/skype-call.jpg' >1-855-557-5871</a></dd>
	  <dd><span>FAX:</span> 1-855-557-5871</dd>
	  <dd><span>E-mail:</span> <a href="mailto:plus1game@gmail.com">plus1game@gmail.com</a></dd>
</dl>
<div class="contact-social">
<div><strong>Call us on:</strong> <br>
                        <a  href="callto:1-855-557-5871" hidefocus="true" style="outline: medium none;"><img width="79" height="25" alt="" src="http://www.iotisrealty.com/wp-content/themes/homequest-parent/images/social_skype.png"></a></div>
                                                            <div><strong>Follow on:</strong> <br>
                        <a href="https://twitter.com/plus1games" hidefocus="true" style="outline: medium none;"><img width="79" height="25" alt="" src="http://www.iotisrealty.com/wp-content/themes/homequest-parent/images/share_twitter.png"></a></div>
                                                            <div><strong>Join us on:</strong> <br>
                        <a href="https://www.facebook.com/plus1games" hidefocus="true" style="outline: medium none;"><img width="88" height="25" alt="" src="http://www.iotisrealty.com/wp-content/themes/homequest-parent/images/share_facebook.png"></a></div>
 <div class="clear"></div>
</div>
 <div class="clear"></div>
<!-- <div class="phone_banner"><div class="agent_phone">
<span>OR CALL US RIGHT NOW</span>
<p><strong style="color:#000000;size:14px;">1-855-557-5871</strong></p>
</div>
</div> --></div></div></div>
<?php get_footer(); ?>
