<?php
/**
 * Template Name: Blog-news
 */

get_header(); ?>
<style>
.newer a,.older a {
color:#ffffff;
}
</style>
<div class="center">
<div class="content alignleft" style='width:68%;float:left;margin:20px;'>
  <h1 class="sp-title"><?php the_title(); ?></h1>
  <?php
  $temp = $wp_query;
  //$wp_query= null;
  $paged = get_query_var( 'paged', 1 );
  $arg = array (
	'post_type'              => array( 'post' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'		=>'5',
	'nopaging'				=>false,
	'paged'					=>	$paged,
);
  $wp_query = new WP_Query($arg);
  //$wp_query = new WP_Query();
  //$wp_query->query('post_type=post&showposts=-1');
  ?>
 <?php if ( $wp_query->have_posts() ) {
	echo '<ul>';
	while ( $wp_query->have_posts() ) {
		$wp_query->the_post();?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
				<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<div class="post-meta">
					<div class="fleft">Posted in: <?php the_category(', ') ?> &nbsp;|&nbsp; <time datetime="<?php the_time('Y-m-d\TH:i'); ?>"><?php the_time('F j, Y'); ?> at <?php the_time() ?></time> , by <?php the_author_posts_link() ?></div>
				</div><!--.post-meta-->
			</header>
			<a href="<?php the_permalink() ?>"><figure class="featured-thumbnail"><?php the_post_thumbnail(); ?></figure></a>
				<div class="post-content">
					<div class="excerpt1"><?php the_excerpt(); ?></div>
					<a href="<?php the_permalink() ?>" class="button">Read more</a>
				</div>
			  <div class="postcommnt"><?php //comments_popup_link('No comments', '1 comment', '% comments', 'comments-link', 'Comments are closed'); ?></div>
		</article>
		<?php //echo '<li>' . get_the_title() . '</li>';
	}
	echo '</ul>';
} else {
	// no posts found

}
$max_page=$wp_query->max_num_pages;
?>
  
  <?php if ( $wp_query->max_num_pages > 1 ) : ?>
    <nav class="oldernewer">
<?php if ($paged !=$max_page){ ?>
      <div class="older button" style="color:#ffffff;">
        <?php next_posts_link('&laquo; Older Entries') ?>
      </div><!--.older-->
	  <?php } ?>
<?php if ($paged !=1){ 
 ?>
<div class="newer button"style="color:#ffffff;">
        <?php previous_posts_link('Newer Entries &raquo;') ?>
      </div><!--.newer-->
<?php }  ?>


          </nav><!--.oldernewer-->
  <?php endif; ?>
  
  <?php $wp_query = null; $wp_query = $temp;?>

</div><!--#content-->
<div><?php get_sidebar(); ?></div>
</div>
<?php get_footer(); ?>