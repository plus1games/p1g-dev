<?php
global $wpdb,$bp;
$userid= $bp->displayed_user->userdata->ID;
$size_array=  array('child_s'    =>'Child-S',
					'child_m'    =>'Child-M',
					'child_l'    =>'Child-L',
					'adult_s'    =>'Adult-S',
					'adult_m'    =>'Adult-M',
					'adult_l'    =>'Adult-L',
					'adult_xl'   =>'Adult-XL',
					'adult_xxl'  =>'Adult-XXL',
					'adult_xxxl' =>'Adult-XXXL');
$resultold=$wpdb->get_results("select* from wp_user_uniformdetail where user_id=$userid");
if(isset($_POST['uniform_save'])){	  
			 $temp=$_POST;
		     unset($temp['uniform_save']);	
			 unset($temp['sleeve']);
			 $temp['user_id']=$userid;	
			 $temp['type']='official';
			 $updateqry= "UPDATE wp_user_uniformdetail
			 SET type         = '{$temp[type]}',
			 size_pant        = '{$temp[size_pant]}',
			 size_shirt_long  = '{$temp[size_shirt_long]}',
			 size_shirt_short = '{$temp[size_shirt_short]}',
			 size_short       = '{$temp[size_short]}',
			 size_hat         = '{$temp[size_hat]}',
			 size_glove         = '{$temp[size_glove]}',
			 size_shoe        = '{$temp[size_shoe]}'
		     WHERE user_id='$userid'";
	  if(empty($resultold))
		  $wpdb->insert('wp_user_uniformdetail',$temp);         
	  else
		  $update=$wpdb->query($updateqry);       
 
}
$result=$wpdb->get_results("select* from wp_user_uniformdetail where user_id=$userid");	

?>
<script type="text/javascript">
jQuery(document).ready(function($){
	 $('#shortsleeve').click(function(){
		  $('#sleeveimg').attr('src','<?php echo get_template_directory_uri()?>/buddypress/images/shortsleeve.jpg');
		  $('#size_shirt_short').css('display','block');
		  $('#size_shirt_long').css('display','none');
	 });
	  $('#longsleeve').click(function(){
		 $('#sleeveimg').attr('src','<?php echo get_template_directory_uri()?>/buddypress/images/longsleeve.jpg');
		 $('#size_shirt_long').css('display','block');
		 $('#size_shirt_short').css('display','none');
	 }); 
});

</script>
<style>
.uniform_row_first{width:100%;float:left;height:30px;}
.uniform_item,.uniform_size{width:48%;float:left;font-size:20px;font-weight:bold;}
.radiosleeve{margin-top:20px;margin-left:20px;}
.uniform_row{float:left;width:100%;height:100px;}
.uniform_left{float:left;width:48%; height:100px; border-right:1px solid gray;border-bottom:1px solid gray;}
.uniform_icon{width:70px;height:70px;float:left;margin-top:15px;border:1px solid #cccccc;border-radius:3px;}
.uniform_icon img {width:70px;height:70px;}
#size_shirt_long, #size_shirt_short{margin-top:0px;}
.size_label{float:left;margin-left:10px;font-weight:bold;margin-top:40px;}
.uniform_left select{margin-left:20px;margin-top:40px;width:80%;}
#uniform_save{background:url("images/bg-button.gif") repeat-x scroll 0 0 #00398E;border:none;color:#ffffff;}
.uniform_icon img {
	margin-top:0px;
}
</style>

<form name="form_uniform" method="POST" action="">
<div id="uniform_outer">

<div class ="uniform_row_first">
<div class="uniform_item"><center>Item </center></div>
<div class="uniform_size"><center>Size</center> </div>
</div>

<div class ="uniform_row">
<div class="uniform_left"><div class="uniform_icon"><img src="<?php echo get_template_directory_uri()?>/buddypress/images/pants.jpg "></div><span class="size_label"> Pant Size</span></div>
<div class="uniform_left">
<select name="size_pant">
<option value="">select your size</option>
<?php foreach($size_array as $k=>$v){
 $selected = ($result[0]->size_pant == $k)?'selected="selected"':'';?>
 <option value="<?php echo $k ; ?>" <?php echo $selected ;?> > <?php echo $v ; ?></option>
<?php }?>
</select>
</div>
</div>

<div class ="uniform_row">
<div class="uniform_left"><div class="uniform_icon"><img id = "sleeveimg" src="<?php echo get_template_directory_uri()?>/buddypress/images/longsleeve.jpg "></div><span class="size_label"> Shirt Size</span></div>
<div class="uniform_left">
<p class="radiosleeve"> <input type="radio" name="sleeve" value="longsleeve" checked=true id="longsleeve"> Long Sleeve &nbsp;&nbsp;&nbsp; <input type="radio" name="sleeve" value="shortsleeve" id="shortsleeve"> Short Sleeve </p>
<select name="size_shirt_long" id="size_shirt_long">
<option value="">select your size</option>
<?php foreach($size_array as $k=>$v){
$selected = ($result[0]->size_shirt_long == $k)?'selected="selected"':'';?>
 <option value="<?php echo $k ; ?>"  <?php echo $selected ;?> ><?php echo $v ; ?></option>
<?php }?>
</select>
<select name="size_shirt_short" id="size_shirt_short" style="display:none;">
<option value="">select your size</option>
<?php foreach($size_array as $k=>$v){

$selected = ($result[0]->size_shirt_short == $k)?'selected="selected"':'';?>
 <option value="<?php echo $k ; ?>"  <?php echo $selected ;?> ><?php echo $v ; ?></option>
<?php }?>
</select>
</div>
</div>


<div class ="uniform_row">
<div class="uniform_left"><div class="uniform_icon"><img src="<?php echo get_template_directory_uri()?>/buddypress/images/shorts.jpg "></div><span class="size_label"> Short Size</span></div>
<div class="uniform_left">
<select name="size_short">
<option value="">select your size</option>
<?php foreach($size_array as $k=>$v){
$selected = ($result[0]->size_short == $k)?'selected="selected"':'';?>
 <option value="<?php echo $k ; ?>"  <?php echo $selected ;?> ><?php echo $v ; ?></option>
<?php }?>
</select>
</div>
</div>

<div class ="uniform_row">
<div class="uniform_left"><div class="uniform_icon"><img src="<?php echo get_template_directory_uri()?>/buddypress/images/hat.jpg "></div><span class="size_label"> Hat Size</span></div>
<div class="uniform_left">
<select name="size_hat">
<option value="">select your size</option>
<?php foreach($size_array as $k=>$v){

$selected = ($result[0]->size_hat == $k)?'selected="selected"':'';?>
 <option value="<?php echo $k ; ?>"  <?php echo $selected ;?> ><?php echo $v ; ?></option>
<?php }?>
</select>
</div>
</div>

<div class ="uniform_row">
<div class="uniform_left"><div class="uniform_icon"><img src="<?php echo get_template_directory_uri()?>/buddypress/images/gloves1.jpg "></div><span class="size_label"> Glove Size</span></div>
<div class="uniform_left">
<select name="size_glove">
<option value="">select your size</option>
<?php foreach($size_array as $k=>$v){

$selected = ($result[0]->size_glove == $k)?'selected="selected"':'';?>
 <option value="<?php echo $k ; ?>"  <?php echo $selected ;?> ><?php echo $v ; ?></option>
<?php }?>
</select>
</div>
</div>

<div class ="uniform_row">
<div class="uniform_left"><div class="uniform_icon"><img src="<?php echo get_template_directory_uri()?>/buddypress/images/shoes.jpg "></div><span class="size_label"> Shoe Size</span></div>
<div class="uniform_left">
<select name="size_shoe">
<option value="">select your size</option>
<?php foreach($size_array as $k=>$v){

$selected = ($result[0]->size_shoe == $k)?'selected="selected"':'';?>
 <option value="<?php echo $k ; ?>"  <?php echo $selected ;?> ><?php echo $v ; ?></option>
<?php }?>
</select>
</div>
</div>
<p style="font-style:italic;font-size:11px;text-align:right;margin-right:20px;">Unit(US/Metric)</p>

<input type="submit" class="button" value="Save Changes" name="uniform_save" id="uniform_save">
</div>

</form>