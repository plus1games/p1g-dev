<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
global $wpdb;

$sql = 'SELECT * FROM sporting_goods_data LIMIT 1000,100';
$result = $wpdb->get_results($sql);
//print_r($result);
$added = 0;
$skipped = 0;
$alreadyexist = 0;
if(!empty($result)){
	foreach($result as $v){
		$address = $v->address;
		$chkaddsql = "SELECT * FROM wp_posts as p JOIN `wp_postmeta` as pm ON p.ID = pm.post_id WHERE pm.`meta_key` LIKE 'address' AND pm.`meta_value` LIKE '%".$address.",United States%' AND p.post_title='Hibbett Sports'";
		$chkaddresult = $wpdb->get_results($chkaddsql);
		if(empty($chkaddresult)){
			$title = 'Hibbett Sports';			
			$city = $v->city;
			$state = $v->state;
			$phone = $v->phone;
			$time = $v->storetime;
			$addressinfo = explode('+',$v->url);
			$count = count($addressinfo);
			$zip = $addressinfo[$count-1];
			$zip = rtrim($zip,',');
			$fulladdress = $address.','.$zip;

			$v = str_replace(' ','+',convert_chars(addslashes(iconv('', 'utf-8',$fulladdress))));
			$newarr=array(
				  'query'=>$v,
				  'key'=>'AnL9vxUiTfsQRlm8lO_UOhr4WUasQBUQyf73-XiwEn-EO0D3XStYFZhTJpzwAckv'
				);
			$extended_part= http_build_query($newarr);
			$url = "http://dev.virtualearth.net/REST/v1/Locations?".$extended_part; 
			$ch = curl_init();
			  curl_setopt($ch, CURLOPT_URL, $url);
			   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				 curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			   $geocode = curl_exec($ch);
			  curl_close($ch); 
			$output= json_decode($geocode);
			echo '<pre>';
			print_r($output);
			$lat = $output->resourceSets[0]->resources[0]->point->coordinates[0];
			$long = $output->resourceSets[0]->resources[0]->point->coordinates[1];

			// create new post
			$now = current_time('mysql', 1);
			$new_post = array(
				'post_title'   => convert_chars($title),
				'post_content' => '',
				'post_status'  => 'publish',	
				'post_type'    => 'listing',
				'post_excerpt' => "",
				'post_author'  =>'1', 		
			 );

			$pkg_id = '33549';
			$pkg_type = '1';
			$user_pkg_select = '1';
			$cat_fields ='1';
			$zooming_factor ='13';
			$geo_latitude = $lat;
			$geo_longitude = $long;
			$map_view = 'Road Map';
			$contact_person = '';
			$email =  '';
			$website =  '';
			$twitter = '';
			$facebook = '';
			$youtube = '';
			$google = '';
			$authorized_representative = '';
			$paymentmethod ='Free';
			$payable_amount ='0';
			$paid_amount = '0';
			$package_select = '33549';
			$alive_days ='1825';
			$featured_h ='n';
			$featured_c ='n';
			$featured_type ='none';
			$remote_ip='';
			$address = $address.',United States';
			$meta_array=array('pkg_id'=>$pkg_id,'pkg_type'=>$pkg_type,'is_user_select_subscription_pkg'=>$user_pkg_select,'cat_fields'=>$cat_fields,'address'=>$address,'zooming_factor'=>$zooming_factor,'geo_latitude'=>$geo_latitude,'geo_longitude'=>$geo_longitude,'map_view'=>$map_view,'city'=>$city,'state'=>$state,'zip'=>$zip,'listing_logo'=>'','contact_person'=>$contact_person,'phone'=>$phone,'email'=>$email,'website'=>$website,'twitter'=>$twitter,'facebook'=>$facebook,'youtube'=>$youtube,'google'=>$google,'authorized_representative'=>$authorized_representative,'paymentmethod'=>$paymentmethod,'payable_amount'=>$payable_amount,'paid_amount'=>$paid_amount,'package_select'=>$package_select,'alive_days'=>$alive_days,'featured_h'=>$featured_h,'featured_c'=>$featured_c,'featured_type'=>$featured_type,'remote_ip'=>$remote_ip,'listing_timing'=>$time);

			$post_id = wp_insert_post($new_post);

			$wp_postcodes=$wpdb->prefix.'postcodes';
			$wpdb->insert($wp_postcodes, array("post_id" =>$post_id,
												"post_type" =>'listing',
												 "address" =>$address,
												"latitude" =>$lat,
												"longitude" =>$long,
			));
			$lastInsertId = $wpdb->insert_id;

			wp_set_object_terms($post_id,'sporting-goods', 'listingcategory');
			foreach($meta_array as $metakey=>$meta_value){
				add_post_meta($post_id, $metakey, $meta_value);
			}
			if($post_id){
				echo $post_id.'<br/>';
				$added++;
			}else{
				$skipped++;
			}
		}else{
			$alreadyexist++;
		}
	}
}
echo 'Added : '.$added.'<br/>';
echo 'Skipped : '.$skipped.'<br/>';
echo 'Already exists : '.$alreadyexist.'<br/>';
?>