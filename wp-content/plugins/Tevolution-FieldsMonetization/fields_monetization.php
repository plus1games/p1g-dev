<?php
/*
Plugin Name: Tevolution-Fields Monetization
Plugin URI: http://templatic.com/docs/tevolution-fields-monetization/
Description: Tevolution based add-on is helpful to monetize your custom fields for your submit pages. With this add-on you can limit the user input for the custom fields of the field types Text, Textarea and Text editor and can also limit the number of image uploads for a particular listing. To know more, refer <a href="http://templatic.com/docs/tevolution-fields-monetization">Fields Monetization guide</a>.
Version: 1.0.8
Author: Templatic
Author URI: http://templatic.com/
*/

define( 'FIELDS_MONETIZATION_VERSION', '1.0.8' );
define('FIELDS_MONETIZATION_SLUG','Tevolution-FieldsMonetization/fields_monetization.php');
define( 'FIELDS_MONETIZATION_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'FIELDS_MONETIZATION_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'FIELDS_MONETIZATION_PLUGIN_FILE', __FILE__ );
@define('TFM_DOMAIN','fields_monetization');

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

$locale = get_locale();
load_textdomain( TFM_DOMAIN, plugin_dir_path( __FILE__ ).'languages/'.$locale.'.mo' );
if (defined('WP_DEBUG') and WP_DEBUG == true){ error_reporting(E_ALL ^ E_NOTICE); } else { error_reporting(0); }
include(dirname(__FILE__)."/install.php");
register_activation_hook(__FILE__,'add_fields_monetization');
	if(!function_exists('add_fields_monetization')){
	function add_fields_monetization(){		
		update_option('fields_monetization_redirect_on_first_activation','Active');
		$tmpdata = get_option('templatic_settings');
		$tmpdata['templatic-category_custom_fields'] = 'Yes';
		update_option('templatic_settings',$tmpdata);
	}
}
if(strstr($_SERVER['REQUEST_URI'],'plugins.php')){
	require_once('wp-updates-plugin.php');
	new WP_Fields_Monetization_Updates( 'http://templatic.com/updates/api/index.php', plugin_basename(__FILE__) );
}
/*
 * Update fieldsmonetization_update_login plugin version after templatic member login
 */
add_action('wp_ajax_fieldsmonetization','fieldsmonetization_update_login');
function fieldsmonetization_update_login()
{
	check_ajax_referer( 'fieldsmonetization', '_ajax_nonce' );
	$plugin_dir = rtrim( plugin_dir_path(__FILE__), '/' );	
	require_once( $plugin_dir .  '/templatic_login.php' );	
	exit;
}

if(is_admin() && strstr($_SERVER['REQUEST_URI'],'/wp-admin/')){
	include_once(FIELDS_MONETIZATION_PLUGIN_DIR."/admin_functions.php");
}
/*
	Delete the redirection option from the database
*/
function fields_monetization_plugin_deactivate() { 
	delete_option('fields_monetization_redirect_on_first_activation');
}
add_action( 'admin_head', 'fields_monetization_function' );
add_action( 'wp_head', 'fields_monetization_function' );
/*
	Add style for back-end.
*/
function fields_monetization_function(){
	if(is_admin())
	{
		wp_enqueue_style('general-style', FIELDS_MONETIZATION_PLUGIN_URL.'style.css' );
	}
}
register_deactivation_hook(__FILE__, 'fields_monetization_plugin_deactivate');

/* 
	Returns all custom fields
*/
function get_post_custom_fields_templ_plugin_backend_monetization($post_types='',$category_id='',$taxonomy='') {
	global $wpdb,$post,$_wp_additional_image_sizes,$sitepress;
 	$tmpdata = get_option('templatic_settings');
	remove_all_actions('posts_where');
	
	if($post_types)
	{
		$args=
		array( 
		'post_type' => 'custom_fields',
		'posts_per_page' => -1	,
		'post_status' => array('publish'),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'post_type_'.$post_types.'',
				'value' => array('all',$post_types),
				'compare' => 'In',
				'type'=> 'text'
			),
			array(
				'key' => 'show_on_page',
				'value' =>  array('user_side','both_side'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			
			array(
				'key' => 'is_active',
				'value' =>  '1',
				'compare' => '='
			)
		),
		'tax_query' => array(
				'relation' => 'OR',
			array(
				'taxonomy' => $taxonomy,
				'field' => 'id',
				'terms' => $category_id,
				'operator'  => 'IN'
			),
			array(
				'taxonomy' => 'category',
				'field' => 'id',
				'terms' => 1,
				'operator'  => 'IN'
			)
			
		 ),
		 
		'meta_key' => 'sort_order',
		'orderby' => 'meta_value_num',
		'meta_value_num'=>'sort_order',
		'order' => 'ASC'
		);
		remove_filter('posts_where','custom_fields_filter_where');	
	}else
	{
	$args=
		array( 
		'post_type' => 'custom_fields',
		'posts_per_page' => -1	,
		'post_status' => array('publish'),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'show_on_page',
				'value' =>  array('user_side','both_side'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			array(
				'key' => 'is_active',
				'value' =>  '1',
				'compare' => '='
			)
		),
		 
		'meta_key' => 'sort_order',
		'orderby' => 'meta_value_num',
		'meta_value_num'=>'sort_order',
		'order' => 'ASC'
		);
		add_filter('posts_where','custom_fields_filter_where');
	}
	$post_query = null;
	$post_query = new WP_Query($args);	
	$post_meta_info = $post_query;	
	$return_arr = array();
	
	if($post_meta_info){
		while ($post_meta_info->have_posts()) : $post_meta_info->the_post();			
				if(get_post_meta($post->ID,"ctype",true)){
					$options = explode(',',get_post_meta($post->ID,"option_values",true));
				}
				$custom_fields = array(
						"id"		=> $post->ID,
						"name"		=> get_post_meta($post->ID,"htmlvar_name",true),
						"label" 	=> $post->post_title,
						"htmlvar_name" 	=> get_post_meta($post->ID,"htmlvar_name",true),
						"default" 	=> get_post_meta($post->ID,"default_value",true),
						"type" 		=> get_post_meta($post->ID,"ctype",true),
						"desc"      => $post->post_content,
						"option_title" => get_post_meta($post->ID,"option_title",true),
						"option_values" => get_post_meta($post->ID,"option_values",true),
						"is_require"  => get_post_meta($post->ID,"is_require",true),
						"is_active"  => get_post_meta($post->ID,"is_active",true),
						"show_on_listing"  => get_post_meta($post->ID,"show_on_listing",true),
						"show_on_detail"  => get_post_meta($post->ID,"show_on_detail",true),
						"validation_type"  => get_post_meta($post->ID,"validation_type",true),
						"style_class"  => get_post_meta($post->ID,"style_class",true),
						"extra_parameter"  => get_post_meta($post->ID,"extra_parameter",true),
						"show_in_email" =>get_post_meta($post->ID,"show_in_email",true),
						);
				if($options)
				{
					$custom_fields["options"]=$options;
				}
				
				$return_arr[$post->ID] = $custom_fields;
			
		endwhile;wp_reset_query();
	}
	return $return_arr;
}
/* 
	Fetch category as per price package selection from front end.
*/
function display_custom_post_field_plugin1($custom_metaboxes,$session_variable='',$post_type=''){
	$tmpdata = get_option('templatic_settings');
	echo '<div class="element wp-tab-panel price_package_custom_field">';
	echo '<label for="select_all_custom_field"><input type="checkbox" onclick="return selectall_custom_field();" id="select_all_custom_field" name="custom_fields[]">&nbsp;Select All</label>';
	$custom_field_array = array();
	if(!empty($custom_metaboxes))
	{
		
		
		foreach($custom_metaboxes as $heading=>$_custom_metaboxes)
		{		 
			foreach($_custom_metaboxes as $key=>$val) {
				$name = $val['name'];
				$site_title = $val['label'];
				$type = $val['type'];
				$htmlvar_name = $val['htmlvar_name'];			
				$cust_field_id = $val['id'];	
				$value = "";
				$checked = '';				
				if(isset($htmlvar_name)  && !in_array($cust_field_id,$custom_field_array))
				{
					if(is_array(get_post_meta($_REQUEST['package_id'],'custom_fields',true)) && in_array($cust_field_id,get_post_meta($_REQUEST['package_id'],'custom_fields',true)))
					{
						$checked = 'checked=checked';
					}
					if( $htmlvar_name == 'post_title' || $htmlvar_name == 'post_content' || $htmlvar_name == 'post_excerpt'  || $htmlvar_name == 'post_images' )
					{
						if($type=='text')
						{
							$type_text = __('Text',TFM_DOMAIN);
						}
						elseif($type=='texteditor')
						{
							$type_text = __('Text Editor',TFM_DOMAIN);
						}
						elseif($type=='textarea')
						{
							$type_text = __('Textarea',TFM_DOMAIN);
						}
						elseif($type=='image_uploader')
						{
							$type_text = __('Image Uploader',TFM_DOMAIN);
						}
						?>
							<div class="clearfix  prickage_text_width">
							<label><?php echo '<input name="custom_fields[]"  id="custom_fields" type="checkbox"  onclick="return false" checked="checked" value="'.$cust_field_id.'" /> '. $site_title." (".$type_text.")"; ?></label>
                            <?php 
							if($type!='image_uploader') {
								?>
							<label>
						<?php echo '<input size="70" PLACEHOLDER="'.__('Max char limit',TFM_DOMAIN).'" name="'.$htmlvar_name.'_character_limit"  id="'.$htmlvar_name.'_character_limit" type="text"  value="'.get_post_meta($_REQUEST['package_id'], $htmlvar_name.'_character_limit', true).'" />'; ?></label>
                        <?php } ?>
						</div>
						<?php
					}
					if($htmlvar_name!="category" && $htmlvar_name != 'post_title' && $htmlvar_name != 'post_content' && $htmlvar_name != 'post_excerpt' && $htmlvar_name != 'post_images' && $type != 'heading_type')
					{
						if($type=='text')
						{
							$type_text = __('Text',TFM_DOMAIN);
						}
						elseif($type=='image_uploader')
						{
							$type_text = __('Multi image uploader',TFM_DOMAIN);
						}
						elseif($type=='date')
						{
							$type_text = __('Date Picker',TFM_DOMAIN);
						}
						elseif($type=='multicheckbox')
						{
							$type_text = __('Multi Checkbox',TFM_DOMAIN);
						}
						elseif($type=='radio')
						{
							$type_text = __('Radio',TFM_DOMAIN);
						}
						elseif($type=='select')
						{
							$type_text = __('Select',TFM_DOMAIN);
						}
						elseif($type=='texteditor')
						{
							$type_text = __('Text Editor',TFM_DOMAIN);
						}
						elseif($type=='textarea')
						{
							$type_text = __('Textarea',TFM_DOMAIN);
						}
						elseif($type=='upload')
						{
							$type_text = __('File uploader',TFM_DOMAIN);
						}
						elseif($type=='geo_map')
						{
							$type_text = __('Geo Map',TFM_DOMAIN);
						}
						elseif($type=='upload')
						{
							$type_text = __('File uploader',TFM_DOMAIN);
						}
						elseif($type == 'multicity')
						{
							$type_text = __('Multi City',TFM_DOMAIN);
						}
						else
						{
							$type_text = apply_filters('new_field_label', $type);
						}
						if($htmlvar_name == 'map_view')
						{
							$site_title = __('Map View',TFM_DOMAIN);
						}
					?>
					<div class="clearfix  prickage_text_width">
						<label><?php echo '<input name="custom_fields[]"  id="custom_fields" type="checkbox" '.$checked.' value="'.$cust_field_id.'" /> '. $site_title." (".$type_text.")"; ?>
						
						<?php
						if($htmlvar_name!="category" && $htmlvar_name != 'post_title' && $htmlvar_name != 'post_content' && $htmlvar_name != 'post_excerpt' && $htmlvar_name != 'post_images' && ($type == 'text' || $type == 'textarea' || $type == 'texteditor' ) )
						{
						?>
						</label>
						<label>
						<?php  echo '<input size="70" PLACEHOLDER="'.__('Max char limit',TFM_DOMAIN).'" name="'.$htmlvar_name.'_character_limit"  id="'.$htmlvar_name.'_character_limit" type="text"  value="'.get_post_meta($_REQUEST['package_id'], $htmlvar_name.'_character_limit', true).'" />'; ?>
							
						<?php
						}
						?>
						</label>
						<div class="clearfix"></div>
					</div>
					<?php
					}
					$custom_field_array[] = $cust_field_id;
				}
				
			}
		}
	}
	echo "</div>";
}
/* 
	Fetch category as per price package selection from front end.
*/
function monetize_ajax_display_custom_post_field_plugin1($custom_metaboxes,$session_variable='',$post_type=''){
	$tmpdata = get_option('templatic_settings');
	$count_custom_field = 0;
	echo '<div class="element wp-tab-panel price_package_custom_field">';
	echo '<label for="select_all_custom_field"><input type="checkbox" onclick="return selectall_custom_field();" id="select_all_custom_field" name="custom_fields[]">&nbsp;Select All</label>';
	foreach($custom_metaboxes as $heading=>$_custom_metaboxes)
	{		 
		foreach($_custom_metaboxes as $key=>$val) {
			$count_custom_field++;
			$name = $val['name'];
			$site_title = $val['label'];
			$type = $val['type'];
			$htmlvar_name = $val['htmlvar_name'];			
			$cust_field_id = $val['id'];	
			$value = "";
			$checked = '';
			
			if(empty($custom_field_array)){ $custom_field_array = array(); }			
			if(!in_array(@$cust_field_id,@$custom_field_array))
			{
				if(get_post_meta($_REQUEST['package_id'],'custom_fields',true) !=''){ $metas = get_post_meta($_REQUEST['package_id'],'custom_fields',true); }else{
					$metas = array();
				}
				if(in_array($cust_field_id,$metas))
				{
					$checked = 'checked=checked';
				}
				if($htmlvar_name == 'post_title' || $htmlvar_name == 'post_content' || $htmlvar_name == 'post_excerpt' || $htmlvar_name == 'post_images' )
				{
					if($type=='text')
					{
						$type_text = __('Text',TFM_DOMAIN);
					}
					elseif($type=='texteditor')
					{
						$type_text = __('Text Editor',TFM_DOMAIN);
					}
					elseif($type=='textarea')
					{
						$type_text = __('Textarea',TFM_DOMAIN);
					}
					elseif($type=='image_uploader')
					{
						$type_text = __('Image Uploader',TFM_DOMAIN);
					}
					?>
                    	<div class="clearfix  prickage_text_width">
                    	<label><?php echo '<input name="custom_fields[]"  id="custom_fields" type="checkbox"  onclick="return false" checked="checked" value="'.$cust_field_id.'" /> '. $site_title." (".$type_text.")"; ?></label>
                        <?php 
							if($type!='image_uploader') {
							?>
                        <label>
					<?php  echo '<input size="70" PLACEHOLDER="'.__('Max char limit',TFM_DOMAIN).'" name="'.$htmlvar_name.'_character_limit"  id="'.$htmlvar_name.'_character_limit" type="text"  value="'.get_post_meta($_REQUEST['package_id'], $htmlvar_name.'_character_limit', true).'" />'; ?></label>
                     <?php } ?>
                    	</div>
                    <?php
				}
				if($htmlvar_name!="category" && $htmlvar_name != 'post_title' && $htmlvar_name != 'post_content' && $htmlvar_name != 'post_excerpt' && $htmlvar_name != 'post_images' && $type != 'heading_type')
				{
					if($type=='text')
					{
						$type_text = __('Text',TFM_DOMAIN);
					}
					elseif($type=='image_uploader')
					{
						$type_text = __('Multi image uploader',TFM_DOMAIN);
					}
					elseif($type=='date')
					{
						$type_text = __('Date Picker',TFM_DOMAIN);
					}
					elseif($type=='multicheckbox')
					{
						$type_text = __('Multi Checkbox',TFM_DOMAIN);
					}
					elseif($type=='radio')
					{
						$type_text = __('Radio',TFM_DOMAIN);
					}
					elseif($type=='select')
					{
						$type_text = __('Select',TFM_DOMAIN);
					}
					elseif($type=='texteditor')
					{
						$type_text = __('Text Editor',TFM_DOMAIN);
					}
					elseif($type=='textarea')
					{
						$type_text = __('Textarea',TFM_DOMAIN);
					}
					elseif($type=='upload')
					{
						$type_text = __('File uploader',TFM_DOMAIN);
					}
					elseif($type=='geo_map')
					{
						$type_text = __('Geo Map',TFM_DOMAIN);
					}
					elseif($type=='upload')
					{
						$type_text = __('File uploader',TFM_DOMAIN);
					}
					elseif($type == 'multicity')
					{
						$type_text = __('Multi City',TFM_DOMAIN);
					}
					if($htmlvar_name == 'map_view')
					{
						$site_title = __('Map View',TFM_DOMAIN);
					}
				?>
				<div class="clearfix  prickage_text_width">
					<label><?php echo '<input name="custom_fields[]"  id="custom_fields" type="checkbox" '.$checked.' value="'.$cust_field_id.'" /> '. $site_title." (".$type_text.")"; ?>
					
					<?php
					if($htmlvar_name!="category" && $htmlvar_name != 'post_title' && $htmlvar_name != 'post_content' && $htmlvar_name != 'post_excerpt' && $htmlvar_name != 'post_images' && ($type == 'text' || $type == 'textarea' || $type == 'texteditor' ) )
					{
					?>
					</label>
					<label>
					<?php  echo '<input size="70" PLACEHOLDER="'.__('Max char limit',TFM_DOMAIN).'" name="'.$htmlvar_name.'_character_limit"  id="'.$htmlvar_name.'_character_limit" type="text"  value="'.get_post_meta($_REQUEST['package_id'], $htmlvar_name.'_character_limit', true).'" />'; ?>
						
					<?php
					}
					?>
					</label>
					<div class="clearfix"></div>
				</div>
				<?php
					
				}
				$custom_field_array[] = $cust_field_id;
				
			}
			
		}
	}
	if($count_custom_field <= 0)
	{
		echo $count_custom_field = 'custom_field_not_found';
	}
	echo "</div>";
}
/* 
	Fetch custom fields as per selection filter .
*/
function custom_fields_filter_where($where)
{
	global $wpdb,$wp_query;
	foreach($_REQUEST['category'] as $key=>$val)
	{
		if($key == 0)
		{
			$arg = ' AND ';
		}
		else
		{
			$arg = ' OR ';
		}
		$where .= " $arg $wpdb->posts.ID in (select pm.post_id from $wpdb->postmeta pm where pm.meta_key ='field_category' and FIND_IN_SET( ".$val.", pm.meta_value ))";
	}
	
	return $where;
}

/* 
Name : show_custom_fields_shortcode
description : include shortcode page.
*/
add_action('init','show_custom_fields_shortcode');
function show_custom_fields_shortcode()
{
	if (is_plugin_active('Tevolution/templatic.php'))
	{
		include(dirname(__FILE__)."/shortcodes/custom_fields_functions.php");
	
	}
}
?>