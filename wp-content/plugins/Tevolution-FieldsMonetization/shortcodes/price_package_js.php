<?php ob_start(); ?>
<script type="text/javascript">
/*
Name :show_featuredprice
Description : Return the total prices and add the calculation in span.
*/
function monetize_show_featuredprice(pkid)
{
	
	if(pkid=="")
	  {
		document.getElementById("featured_h").innerHTML="";
		return;
	  }else{
		document.getElementById("process2").style.display ="block";
	  }
	  
	  jQuery.ajax({
			url:ajaxUrl,
			type:'POST',
			async: true,
			data:'action=tmpl_ajax_price&pkid='+pkid,
			success:function(results){
			
			document.getElementById('all_cat_price').value = 0;
			document.getElementById('cat_price').innerHTML = 0;
			if(document.getElementById("process2"))
				document.getElementById("process2").style.display ="none";
		
			var myString = results;
			
			var myStringArray = myString.split("###RAWR###");  /* split with ###RAWR### because return result is concated with ###RAWR###*/
			
			document.getElementById('alive_days').value = myStringArray[6];
					
			if(myStringArray[5] == 1)
			{
				if(document.getElementById('is_featured').style.display == "none")
				{
					document.getElementById('is_featured').style.display="";
				}
				if(document.getElementById('price_package_price_list'))
				{
					if(myStringArray[1] > 0 ||  myStringArray[0] > 0 || myStringArray[4] > 0)
					{
						if(document.getElementById('moderate_comment'))
						{
							if(parseFloat(myStringArray[8] ) > 0 || myStringArray[1] > 0 ||  myStringArray[0] > 0 || myStringArray[4] > 0){
								if(parseFloat(document.getElementById('all_cat_price').value) > 0)
								{
									document.getElementById('cat_price').style.display = "";
									document.getElementById('cat_price_id').style.display = "";
									document.getElementById('before_cat_price_id').style.display = "";
									document.getElementById('pakg_add').style.display = "";
								}
								else
								{
									document.getElementById('cat_price').style.display = "none";
									document.getElementById('cat_price_id').style.display = "none";
									document.getElementById('before_cat_price_id').style.display = "none";
									document.getElementById('pakg_add').style.display = "none";
								}
								if(myStringArray[4] > 0)
								{
									document.getElementById('before_pkg_price_id').style.display = "";
									document.getElementById('pkg_price').style.display = "";
									document.getElementById('pkg_price_id').style.display = "";
								}
								else
								{
									document.getElementById('before_pkg_price_id').style.display = "none";
									document.getElementById('pkg_price').style.display = "none";
									document.getElementById('pkg_price_id').style.display = "none";
									document.getElementById('pakg_add').style.display = "none";
								}
								if(document.getElementById("all_cat_price").value > 0)
								{
									document.getElementById('cat_price_total_price').style.display = "";
								}
								document.getElementById('price_package_price_list').style.display = "block";
								
							}
						}
						else
						{
							if(parseFloat(document.getElementById('all_cat_price').value) > 0)
							{
								document.getElementById('cat_price').style.display = "";
								document.getElementById('cat_price_id').style.display = "";
								document.getElementById('before_cat_price_id').style.display = "";
								document.getElementById('pakg_add').style.display = "";
							}
							else
							{
								document.getElementById('cat_price').style.display = "none";
								document.getElementById('cat_price_id').style.display = "none";
								document.getElementById('before_cat_price_id').style.display = "none";
								document.getElementById('pakg_add').style.display = "none";
							}
							if(myStringArray[4] > 0)
							{
								document.getElementById('before_pkg_price_id').style.display = "";
								document.getElementById('pkg_price').style.display = "";
								document.getElementById('pkg_price_id').style.display = "";
								document.getElementById('pakg_add').style.display = "";
							}
							else
							{
								document.getElementById('before_pkg_price_id').style.display = "none";
								document.getElementById('pkg_price').style.display = "none";
								document.getElementById('pkg_price_id').style.display = "none";
								document.getElementById('pakg_add').style.display = "none";
							}
							if(document.getElementById("all_cat_price").value > 0)
							{
								document.getElementById('cat_price_total_price').style.display = "";
							}
							document.getElementById('price_package_price_list').style.display = 'block';
						}
					}
					else
					{
						if(parseFloat(document.getElementById('all_cat_price').value) > 0)
						{
							document.getElementById('cat_price').style.display = "";
							document.getElementById('cat_price_id').style.display = "";
							document.getElementById('before_cat_price_id').style.display = "";
							document.getElementById('price_package_price_list').style.display = 'block';
							document.getElementById('cat_price_total_price').style.display = "";
							document.getElementById('pakg_add').style.display = "";
						}
						else
						{
							document.getElementById('cat_price').style.display = "none";
							document.getElementById('cat_price_id').style.display = "none";
							document.getElementById('before_cat_price_id').style.display = "none";
							document.getElementById('price_package_price_list').style.display = 'none';
							document.getElementById('cat_price_total_price').style.display = "none";
							document.getElementById('pakg_add').style.display = "none";
						}
					}
				}
				if(document.getElementById('moderate_comment'))
				{
					if(myStringArray[7] == 1 && (parseFloat(myStringArray[8]) > 0 )){
						document.getElementById('moderate_comment').style.display = "block";
					}
					else
					{
						document.getElementById('moderate_comment').style.display = "none";
					}
				}
				document.getElementById('featured_c').value = myStringArray[1];
				document.getElementById('featured_h').value = myStringArray[0];
				
				if(document.getElementById('author_can_moderate_comment'))
					document.getElementById('author_can_moderate_comment').value = myStringArray[8];
				
				var positionof = '<?php echo get_option('currency_pos'); ?>';
				var ftrhome_value = '';
				var ftrhome_currency_symbol = '';
				var ftrcat_value = '';
				var ftrcat_currency_symbol = '';
				if(myStringArray[0] <=0)
				{
					ftrhome_value = '<?php _e('Free',TFM_DOMAIN); ?>';
				}
				else
				{
					ftrhome_value = myStringArray[0];
					ftrhome_currency_symbol = '<?php echo tmpl_fetch_currency();?>';
				}
				if(myStringArray[1] <=0)
				{
					ftrcat_value = '<?php _e('Free',TFM_DOMAIN); ?>';
					ftrcat_currency_symbol = '';
				}
				else
				{
					ftrcat_value = myStringArray[1];
					ftrcat_currency_symbol = '<?php echo tmpl_fetch_currency();?>';
				}
				if(positionof == 1){ 
				
				document.getElementById('ftrhome').innerHTML = "("+ftrhome_currency_symbol+ftrhome_value+")";
				document.getElementById('ftrcat').innerHTML = "("+ftrcat_currency_symbol+ftrcat_value+")";
				if(document.getElementById('ftrcomnt'))
				{
					document.getElementById('ftrcomnt').innerHTML =  "(<?php echo tmpl_fetch_currency();?>"+myStringArray[8]+")";
				}
				}else if(positionof == 2){
				document.getElementById('ftrhome').innerHTML = "("+ftrhome_currency_symbol+' '+ftrhome_value+")";
				document.getElementById('ftrcat').innerHTML = "("+ftrcat_currency_symbol+' '+ftrcat_value+")";
				if(document.getElementById('ftrcomnt'))
				{
					document.getElementById('ftrcomnt').innerHTML = "(<?php echo tmpl_fetch_currency(get_option('currency_symbol'),'currency_symbol');?> "+myStringArray[8]+")";
				}
				}else if(positionof == 3){
				document.getElementById('ftrhome').innerHTML = "("+ftrhome_value+ftrhome_currency_symbol+")";
				document.getElementById('ftrcat').innerHTML = "("+ftrcat_value+ftrcat_currency_symbol+")";
				if(document.getElementById('ftrcomnt'))
				{
					document.getElementById('ftrcomnt').innerHTML = "("+myStringArray[8]+" <?php echo tmpl_fetch_currency();?>)";
				}
				}else{
				document.getElementById('ftrhome').innerHTML = "("+ftrhome_value+ftrhome_currency_symbol +")";
				document.getElementById('ftrcat').innerHTML = "("+ftrcat_value+' '+ftrcat_currency_symbol+")";
				if(document.getElementById('ftrcomnt'))
				{
					document.getElementById('ftrcomnt').innerHTML = "("+myStringArray[8]+" <?php echo tmpl_fetch_currency();?>)";
				}
				}
				
				document.getElementById('pkg_price').innerHTML = myStringArray[4];   
			}else
			{
				if(document.getElementById('moderate_comment'))
				{
					if(myStringArray[7] == 1 && ( parseFloat(myStringArray[8]) > 0 )){
						document.getElementById('moderate_comment').style.display = "";
						document.getElementById('ftrcomnt').innerHTML =  "("+myStringArray[8]+" <?php echo tmpl_fetch_currency();?>)";
						if(document.getElementById('author_can_moderate_comment'))
							document.getElementById('author_can_moderate_comment').value = myStringArray[8];
					}
					else
					{
						document.getElementById('moderate_comment').style.display = "none";
					}
				}
				if(document.getElementById('price_package_price_list'))
				{
					if(myStringArray[1] > 0 ||  myStringArray[0] > 0 || myStringArray[4] > 0 )
					{
						if(document.getElementById('moderate_comment'))
						{
							if(parseFloat(myStringArray[8]) > 0 || myStringArray[1] > 0 ||  myStringArray[0] > 0 || myStringArray[4] > 0 ){
							
								if(parseFloat(document.getElementById('all_cat_price').value) > 0)
								{
									document.getElementById('cat_price').style.display = "";
									document.getElementById('cat_price_id').style.display = "";
									document.getElementById('before_cat_price_id').style.display = "";
									document.getElementById('pakg_add').style.display = "";
								}
								else
								{
									document.getElementById('cat_price').style.display = "none";
									document.getElementById('cat_price_id').style.display = "none";
									document.getElementById('before_cat_price_id').style.display = "none";
									document.getElementById('pakg_add').style.display = "none";
								}
								
								if(myStringArray[4] > 0)
								{
									document.getElementById('before_pkg_price_id').style.display = "";
									document.getElementById('pkg_price').style.display = "";
									document.getElementById('pkg_price_id').style.display = "";
								}
								else
								{
									document.getElementById('before_pkg_price_id').style.display = "none";
									document.getElementById('pkg_price').style.display = "none";
									document.getElementById('pkg_price_id').style.display = "none";
								}
								if(document.getElementById("all_cat_price").value > 0)
								{
									document.getElementById('cat_price_total_price').style.display = "";
								}
								document.getElementById('price_package_price_list').style.display = "block";
							}
						}
						else
						{
							if(parseFloat(document.getElementById('all_cat_price').value) > 0)
							{
								document.getElementById('cat_price').style.display = "";
								document.getElementById('cat_price_id').style.display = "";
								document.getElementById('before_cat_price_id').style.display = "";
								document.getElementById('pakg_add').style.display = "";
								document.getElementById('cat_price_total_price').style.display = "";
							}
							else
							{
								document.getElementById('cat_price').style.display = "none";
								document.getElementById('cat_price_id').style.display = "none";
								document.getElementById('before_cat_price_id').style.display = "none";
								document.getElementById('pakg_add').style.display = "none";
								document.getElementById('cat_price_total_price').style.display = "none";
							}
							if(myStringArray[4] > 0)
							{
								document.getElementById('before_pkg_price_id').style.display = "";
								document.getElementById('pkg_price').style.display = "";
								document.getElementById('pkg_price_id').style.display = "";
							}
							else
							{
								document.getElementById('before_pkg_price_id').style.display = "none";
								document.getElementById('pkg_price').style.display = "none";
								document.getElementById('pkg_price_id').style.display = "none";
							}
							document.getElementById('price_package_price_list').style.display = 'block';
						}
					}
					else
					{
						if(parseFloat(document.getElementById('all_cat_price').value) > 0)
						{
							document.getElementById('cat_price').style.display = "";
							document.getElementById('cat_price_id').style.display = "";
							document.getElementById('before_cat_price_id').style.display = "";
							document.getElementById('price_package_price_list').style.display = 'block';
						}
						else
						{
							document.getElementById('cat_price').style.display = "none";
							document.getElementById('cat_price_id').style.display = "none";
							document.getElementById('before_cat_price_id').style.display = "none";
							document.getElementById('price_package_price_list').style.display = 'none';
						}
					}
				}
				document.getElementById('pkg_price').innerHTML = myStringArray[4];  
				document.getElementById('featured_c').value=0;
				document.getElementById('ftrcat').innerHTML	= '<?php _e('Free',TFM_DOMAIN); ?>';		
				document.getElementById('featured_h').value=0;
				document.getElementById('ftrhome').innerHTML = '<?php _e('Free',TFM_DOMAIN); ?>';
				document.getElementById('is_featured').style.display = "none"; 
				document.getElementById('total_price').value = parseFloat(myStringArray[0]) + parseFloat(myStringArray[1]) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
				document.getElementById('result_price').innerHTML = parseFloat(myStringArray[0]) + parseFloat(myStringArray[1]) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
					
			}		
			if((document.getElementById('featured_h').checked== true) && (document.getElementById('featured_c').checked== true))
			{			
						if(myStringArray[0]==""){myStringArray[0]=0}else{myStringArray[0]=myStringArray[0];}
						if(myStringArray[1]==""){myStringArray[1]=0}else{myStringArray[1]=myStringArray[1];}
						if((parseFloat(myStringArray[0]) + parseFloat(myStringArray[1]) ) > 0 &&  myStringArray[4] >0)
						{
							document.getElementById('pakg_price_add').style.display = '';
							document.getElementById('cat_price_total_price').style.display = '';
						}
						else
						{
							document.getElementById('pakg_price_add').style.display = 'none';
							document.getElementById('cat_price_total_price').style.display = 'none';
						}
						document.getElementById('feture_price').innerHTML = parseFloat(myStringArray[0]) + parseFloat(myStringArray[1]) ;
						
						document.getElementById('total_price').value = parseFloat(myStringArray[0]) + parseFloat(myStringArray[1]) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
						
						document.getElementById('result_price').innerHTML = parseFloat(myStringArray[0]) + parseFloat(myStringArray[1]) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
						
			}else if((document.getElementById('featured_h').checked == true) && (document.getElementById('featured_c').checked == false)){
						if(myStringArray[0]==""){myStringArray[0]=0}else{myStringArray[0]=myStringArray[0];}	
						if((parseFloat(myStringArray[0]) ) > 0 &&  myStringArray[4] >0)
						{
							document.getElementById('pakg_price_add').style.display = '';
							document.getElementById('cat_price_total_price').style.display = '';
						}
						else
						{
							document.getElementById('pakg_price_add').style.display = 'none';
							document.getElementById('cat_price_total_price').style.display = 'none';
						}
						document.getElementById('feture_price').innerHTML = parseFloat(myStringArray[0]);
						
						document.getElementById('total_price').value =parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
						
						document.getElementById('result_price').innerHTML = parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
			}else if((document.getElementById('featured_h').checked == false) && (document.getElementById('featured_c').checked == true)){
						if(myStringArray[1]==""){myStringArray[1]=0}else{myStringArray[1]=myStringArray[1];}
						if(parseFloat(myStringArray[1]) > 0 &&  myStringArray[4] >0)
						{
							document.getElementById('pakg_price_add').style.display = '';
							document.getElementById('cat_price_total_price').style.display = '';
						}
						else
						{
							document.getElementById('pakg_price_add').style.display = 'none';
							document.getElementById('cat_price_total_price').style.display = 'none';
						}
						document.getElementById('feture_price').innerHTML = parseFloat(myStringArray[1]);
						document.getElementById('total_price').value = parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
						
						document.getElementById('result_price').innerHTML =  parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
			}else if((document.getElementById('author_can_moderate_comment')) && (document.getElementById('author_can_moderate_comment').checked == false) && (document.getElementById('author_can_moderate_comment').checked == true)){
						if(myStringArray[8]==""){myStringArray[8]=0}else{myStringArray[8]=myStringArray[8];}
						var ftrcomnt = 0;
						if(document.getElementById('ftrcomnt'))
						{
							document.getElementById('ftrcomnt').innerHTML = parseFloat(myStringArray[8]);
							ftrcomnt = parseFloat(myStringArray[8]);
						}
						document.getElementById('total_price').value = parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(ftrcomnt) + parseFloat(myStringArray[8]);
						
						document.getElementById('result_price').innerHTML =  parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(ftrcomnt) + parseFloat(myStringArray[8]);
			}
			else{
				document.getElementById('total_price').value = parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
				
				document.getElementById('result_price').innerHTML =parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(myStringArray[4]);
			}
			if(document.getElementById('submit_coupon_code') && document.getElementById('total_price').value > 0)
			{
				document.getElementById('submit_coupon_code').style.display='';
			}
			else
			{
				if(document.getElementById('submit_coupon_code')){
					document.getElementById('submit_coupon_code').style.display='none';
				}
			}
			/*
			added the value of particualr price package of user can select maximum category
			*/
			if(document.getElementById('category_can_select') && document.getElementById('moderate_comment')){
					document.getElementById('category_can_select').value = myStringArray[9];
			}
			if(document.getElementById('category_can_select') ){
					document.getElementById('category_can_select').value = myStringArray[7];
			}
			/*
			added the value of particualr price package of user can select maximum category ends here
			*/
			show_monetize_category(pkid);	
			}
		});
	  
	  
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
		else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		xmlhttp.onreadystatechange=function()
	  {
	    if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
	  } 
	}	
	
	 
}
/*
Name :monetize_fetch_packages
Description : Return package details and category pricing( term_price for category ) USE THIE IF IN FUTURE WE are going to give categorywise pricing
*/
function monetize_fetch_packages(pkgid,form,pri)
{ 
	var total = 0;
	var t=0;
	
	//var c= form['category[]'];
	<?php $tmpdata = get_option('templatic_settings'); ?>
    var cat_display = '<?php echo $tmpdata['templatic-category_type']; ?>';
	var cat_wise_display = '<?php echo $tmpdata['templatic-category_custom_fields']; ?>';
	var dml = document.forms['submit_form'];
	var c = document.getElementsByName('category[]');
	//if(cat_wise_display == 'No')
	 {
		
		var cats = document.getElementById('all_cat').value;
		document.getElementById('all_cat').value = "";
		document.getElementById('all_cat_price').value = 0;
		document.getElementById('cat_price').innerHTML = 0;
	 }
	if(document.getElementById('all_cat_price').value <= 0)
	{
		if(document.getElementById('before_cat_price_id')){
			document.getElementById('before_cat_price_id').style.display = "none";
		
		document.getElementById('cat_price').style.display = "none";
		
		document.getElementById('cat_price_id').style.display = "none";
		
		document.getElementById('pakg_add').style.display = "none";
		
		document.getElementById('cat_price_total_price').style.display = "none";
		}
		if(document.getElementById('feture_price').innerHTML == 0 && document.getElementById('cat_price_total_price'))
			document.getElementById('cat_price_total_price').style.display = "none";
		else
		{
			document.getElementById('cat_price_total_price').style.display = "";
		}
	}
	var validation = false;
	var checkboxCount = 0;
	if(cat_display =='checkbox' || cat_display==''){
		
		for(var i=0;i<c.length;i++){
			c[i].checked?t++:null;
			if(c[i].checked)
			{
				var a = c[i].value.split(",");
				
				document.getElementById('all_cat').value += a[0]+"|";
				
				document.getElementById("category_error").style.display = 'none';
				
				document.getElementById("common_error").style.display = 'none';
				
				validation = true;
				
				checkboxCount++;
				
				document.getElementById('all_cat_price').value = parseFloat(document.getElementById('all_cat_price').value) + parseFloat(a[1]);
				
				document.getElementById('cat_price').innerHTML = parseFloat(document.getElementById('all_cat_price').value);
				
				if(document.getElementById('all_cat_price').value > 0)
				{
					
					if(document.getElementById('before_cat_price_id')){
					document.getElementById('before_cat_price_id').style.display = "";
					document.getElementById('price_package_price_list').style.display = "";

					if(document.getElementById('pkg_price').innerHTML != 0)
					{
						document.getElementById('pakg_add').style.display = "";
					
						document.getElementById('cat_price_total_price').style.display = "";
					}
					document.getElementById('cat_price_id').style.display = "";
					}
					document.getElementById('cat_price').style.display = "";
				
				}
				else
				{
					if(document.getElementById('before_cat_price_id')){
					document.getElementById('before_cat_price_id').style.display = "none";
					
					document.getElementById('cat_price').style.display = "none";
					
					document.getElementById('cat_price_id').style.display = "none";
					}
				}
			}
			
			if(document.getElementById('pkg_price'))
				{
					document.getElementById('total_price').value =  parseFloat(document.getElementById('all_cat_price').value) + parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('pkg_price').innerHTML);
					
					document.getElementById('result_price').innerHTML =  parseFloat(document.getElementById('all_cat_price').value) + parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('pkg_price').innerHTML);
				}else
				{
					document.getElementById('total_price').value =  parseFloat(document.getElementById('all_cat_price').value) ;
					
					document.getElementById('result_price').innerHTML =  parseFloat(document.getElementById('all_cat_price').value) ;
				}
		}
		if(!validation)
		{
			document.getElementById("category_error").style.display = '';
			document.getElementById("common_error").style.display = '';
		}
		if(checkboxCount > document.getElementById("category_can_select").value || checkboxCount == 0)
		{
			document.getElementById("category_error").style.display = '';
			document.getElementById("category_error").innerHTML = '';
		}
		else
		{
			document.getElementById("category_error").style.display = 'none';
		}
	}else{
		if(cat_display == 'select' ){var s = document.getElementById('select_category'); /* var is use for select box */
			if(s.options[s.selectedIndex].value){
					var a = s.options[s.selectedIndex].value.split(",");
					document.getElementById('all_cat').value += a[0]+"|";
					document.getElementById('all_cat_price').value = parseFloat(document.getElementById('all_cat_price').value) + parseFloat(a[1]);
					document.getElementById('cat_price').innerHTML = addCurrencyFormate(parseFloat(document.getElementById('all_cat_price').value).toFixed(2));
			}
			
			if(document.getElementById('all_cat_price').value > 0)
			{
				if(document.getElementById('before_cat_price_id')){
					document.getElementById('before_cat_price_id').style.display = "";
					document.getElementById('price_package_price_list').style.display = "";

					if(document.getElementById('pkg_price').innerHTML != 0)
					{
						document.getElementById('pakg_add').style.display = "";
					
						document.getElementById('cat_price_total_price').style.display = "";
					}
					document.getElementById('cat_price_id').style.display = "";
					}
					document.getElementById('cat_price').style.display = "";
			}
			else
			{
				if( document.getElementById('before_cat_price_id') ){
					document.getElementById('before_cat_price_id').style.display = "none";
				}
				if( document.getElementById('cat_price') ){
					document.getElementById('cat_price').style.display = "none";
				}
				if( document.getElementById('cat_price_id') ){				
					document.getElementById('cat_price_id').style.display = "none";
				}
			}
			document.getElementById('total_price').value = (parseFloat(document.getElementById('all_cat_price').value) + parseFloat(document.getElementById('feture_price').innerHTML.replace(',','')) + parseFloat(document.getElementById('pkg_price').innerHTML.replace(',',''))).toFixed(2);			
			document.getElementById('result_price').innerHTML = addCurrencyFormate((parseFloat(document.getElementById('all_cat_price').value) + parseFloat(document.getElementById('feture_price').innerHTML.replace(',','')) + parseFloat(document.getElementById('pkg_price').innerHTML.replace(',',''))).toFixed(2));
			
		}else if(cat_display == 'multiselectbox'){
			var s = document.getElementById('select_category'); /* var is use for select box */
			
			for(var i=0;i < s.options.length;i++){
				s.options[i].selected?t++:null;
				
				if(s.options[ i ].selected){
						var a = s.options[ i ].value.split(",");
						document.getElementById('all_cat').value += a[0]+"|";
						document.getElementById('all_cat_price').value = parseFloat(document.getElementById('all_cat_price').value) + parseFloat(a[1]);
						document.getElementById('cat_price').innerHTML = parseFloat(document.getElementById('all_cat_price').value);
				}
				document.getElementById('total_price').value =  parseFloat(document.getElementById('all_cat_price').value) + parseFloat(document.getElementById('feture_price').innerHTML) + parseFloat(document.getElementById('pkg_price').innerHTML);
				document.getElementById('result_price').innerHTML =  parseFloat(document.getElementById('all_cat_price').value) + parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('pkg_price').innerHTML);
			}
		}
	}
	var cats = document.getElementById('all_cat').value;
	var post_type = document.getElementById('cur_post_type').value ;
	var taxonomy = document.getElementById('cur_post_taxonomy').value ;
	/* Below code is for category wise packages */
	if(cat_wise_display == 'No')
	 {
		 if( document.getElementById("packages_checkbox"))
		{
			document.getElementById("packages_checkbox").innerHTML="";
		}
	    document.getElementById("process2").style.display ="";
	 
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
			else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
			xmlhttp.onreadystatechange=function()
		  {
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
			
			document.getElementById("packages_checkbox").innerHTML =xmlhttp.responseText;
			if(document.getElementById("process2"))
				document.getElementById("process2").style.display ="none";
			}
		  }
		  url="<?php echo FIELDS_MONETIZATION_PLUGIN_URL;?>/shortcodes/ajax_price.php?pckid="+cats+"&post_type="+post_type+"&taxonomy="+taxonomy
		  xmlhttp.open("GET",url,true);
		  xmlhttp.send();
	 }
}
/*
Name :monetize_templ_all_categories
Description : function return the result when all categories selected
*/
function monetize_templ_all_categories(cp_price) {
	var total = 0;
	var t=0;
	//var c= form['category[]'];
	var dml = document.forms['submit_form'];
	var c = dml.elements['category[]'];
	var selectall = dml.elements['selectall'];
	if(selectall.checked == false){
		cp_price = 0;
	} else {
		cp_price = cp_price;
	}
	if(cp_price > 0 || document.getElementById('pkg_price').innerHTML != 0)
	{
		document.getElementById('price_package_price_list').style.display = 'block';
		if(document.getElementById('before_cat_price_id').innerHTML != '')
			document.getElementById('before_cat_price_id').style.display = '';
		document.getElementById('cat_price').style.display = '';
		if(document.getElementById('cat_price_id').innerHTML != '')
			document.getElementById('cat_price_id').style.display = '';
		document.getElementById('pakg_add').style.display = '';
		document.getElementById('cat_price_total_price').style.display = '';
	}
	else
	{
		document.getElementById('price_package_price_list').style.display = 'none';
		document.getElementById('before_cat_price_id').style.display = 'none';
		document.getElementById('cat_price').style.display = 'none';
		document.getElementById('cat_price_id').style.display = 'none';
		document.getElementById('pakg_add').style.display = 'none';
		document.getElementById('cat_price_total_price').style.display = 'none';
	}
	var post_type = document.getElementById('cur_post_type').value ;
	var taxonomy = document.getElementById('cur_post_taxonomy').value ;
	var cats = document.getElementById('all_cat').value;
	document.getElementById('all_cat').value = "";
	document.getElementById('all_cat_price').value = 0;
	document.getElementById('cat_price').innerHTML = 0;
	
		for(var i=0 ;i < c.length;i++){
		c[i].checked?t++:null;
		if(c[i].checked){	
			var a = c[i].value.split(",");
			if(i ==  (c.length - 1) ){
				document.getElementById('all_cat').value += a[0];
			} else {
				document.getElementById('all_cat').value += a[0]+"|";
			}
		}
	}
	document.getElementById('all_cat_price').value = parseFloat(cp_price);
	document.getElementById('cat_price').innerHTML = parseFloat(document.getElementById('all_cat_price').value);
	document.getElementById('total_price').value =  parseFloat(document.getElementById('all_cat_price').value) + parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('pkg_price').innerHTML);
	document.getElementById('result_price').innerHTML =  parseFloat(document.getElementById('all_cat_price').value) + parseFloat(document.getElementById('feture_price').innerHTML) +  parseFloat(document.getElementById('pkg_price').innerHTML);
	
	var cats = document.getElementById('all_cat').value ;
	if( document.getElementById("packages_checkbox"))
	{
	  document.getElementById("packages_checkbox").innerHTML="";
	 }
	  document.getElementById("process2").style.display ="";
	
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
		else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		xmlhttp.onreadystatechange=function()
	  {
	    if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("packages_checkbox").innerHTML =xmlhttp.responseText;
			if(document.getElementById("process2"))
			document.getElementById("process2").style.display ="none";
		}
	  }
	  url="<?php echo FIELDS_MONETIZATION_PLUGIN_URL;?>/shortcodes/ajax_price.php?pckid="+cats+"&post_type="+post_type+"&taxonomy="+taxonomy
	  xmlhttp.open("GET",url,true);
	  xmlhttp.send();	
	}
	
function monetize_myfields(fid)
{
	document.getElementById(fid+'_hidden').value = document.getElementById(fid).value;
}
/*
Name :featured_list
Description : function return the result after user select feture listing type(check box)
*/
function monetize_featured_list(fid)
{
	
	<?php 
	if(is_plugin_active('thoughtful-comments/fv-thoughtful-comments.php')){
	?>
	
	if((document.getElementById('featured_h').checked== true) && (document.getElementById('featured_c').checked== true) && (document.getElementById('author_can_moderate_comment').checked == true))
	{
		document.getElementById('featured_type').value = 'both';
		document.getElementById('author_moderate').value = '1';
		var totprice = parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value) + parseFloat(document.getElementById('author_can_moderate_comment').value);
		var resprice = parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(document.getElementById('author_can_moderate_comment').value);
		document.getElementById('feture_price').innerHTML = totprice.toFixed(2);
		document.getElementById('result_price').innerHTML = resprice.toFixed(2);
		document.getElementById('total_price').value = (parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(document.getElementById('author_can_moderate_comment').value));
	
	}else if((document.getElementById('featured_h').checked == true) && (document.getElementById('featured_c').checked == false) && (document.getElementById('author_can_moderate_comment').checked == true)){
		document.getElementById('featured_type').value = 'h';
		document.getElementById('author_moderate').value = '1';
		document.getElementById('feture_price').innerHTML = (parseFloat(document.getElementById('featured_h').value) + parseFloat(document.getElementById('author_can_moderate_comment').value));
		
		document.getElementById('result_price').innerHTML = (parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(document.getElementById('author_can_moderate_comment').value));
		
		document.getElementById('total_price').value = (parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(document.getElementById('author_can_moderate_comment').value));
	}else if((document.getElementById('featured_h').checked == false) && (document.getElementById('featured_c').checked == true) && (document.getElementById('author_can_moderate_comment').checked == true)){
		document.getElementById('author_moderate').value = '1';
		document.getElementById('featured_type').value = 'c';
		document.getElementById('feture_price').innerHTML = parseFloat(parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('author_can_moderate_comment').value));
		
		document.getElementById('result_price').innerHTML = (parseFloat(document.getElementById('featured_c').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(document.getElementById('author_can_moderate_comment').value));
		
		document.getElementById('total_price').value = (parseFloat(document.getElementById('featured_c').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(document.getElementById('author_can_moderate_comment').value));
		
	}
	if((document.getElementById('featured_h').checked== true) && (document.getElementById('featured_c').checked== true) && (document.getElementById('author_can_moderate_comment').checked == false))
	{
		document.getElementById('author_moderate').value = '0';
		document.getElementById('featured_type').value = 'both';
		var totprice = parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value) ;
		var resprice = parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		document.getElementById('feture_price').innerHTML = totprice.toFixed(2);
		document.getElementById('result_price').innerHTML = resprice.toFixed(2);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
	
	}else if((document.getElementById('featured_h').checked == true) && (document.getElementById('featured_c').checked == false) && (document.getElementById('author_can_moderate_comment').checked == false)){
		document.getElementById('author_moderate').value = '0';
		document.getElementById('featured_type').value = 'h';
		document.getElementById('feture_price').innerHTML = parseFloat(document.getElementById('featured_h').value);
		
		document.getElementById('result_price').innerHTML = parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
	}else if((document.getElementById('featured_h').checked == false) && (document.getElementById('featured_c').checked == true) && (document.getElementById('author_can_moderate_comment').checked == false)){
		document.getElementById('featured_type').value = 'c';
		document.getElementById('author_moderate').value = '0';
		document.getElementById('feture_price').innerHTML = parseFloat(document.getElementById('featured_c').value);
		
		document.getElementById('result_price').innerHTML = parseFloat(document.getElementById('featured_c').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('featured_c').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
	}
	else if((document.getElementById('featured_h').checked == false) && (document.getElementById('featured_c').checked == false) && (document.getElementById('author_can_moderate_comment').checked == true)){
		document.getElementById('author_moderate').value = '1';
		document.getElementById('feture_price').innerHTML =  parseFloat(document.getElementById('author_can_moderate_comment').value);
		document.getElementById('result_price').innerHTML =  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(document.getElementById('author_can_moderate_comment').value);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML) + parseFloat(document.getElementById('author_can_moderate_comment').value);
	}
	else if((document.getElementById('featured_h').checked == false) && (document.getElementById('featured_c').checked == false) && (document.getElementById('author_can_moderate_comment').checked == false)){
		document.getElementById('author_moderate').value = '0';
		document.getElementById('featured_type').value = 'n';
		document.getElementById('feture_price').innerHTML = '0';
		document.getElementById('result_price').innerHTML = parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
	}
	<?php
	}
	else
	{
	?>
		if((document.getElementById('featured_h').checked== true) && (document.getElementById('featured_c').checked== true))
	{
		document.getElementById('featured_type').value = 'both';
		var totprice = parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value);
		var resprice = parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		document.getElementById('feture_price').innerHTML = totprice.toFixed(2);
		document.getElementById('result_price').innerHTML = resprice.toFixed(2);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('featured_c').value) + parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
	
	}else if((document.getElementById('featured_h').checked == true) && (document.getElementById('featured_c').checked == false)){
		document.getElementById('featured_type').value = 'h';
		document.getElementById('feture_price').innerHTML = document.getElementById('featured_h').value;
		
		document.getElementById('result_price').innerHTML = parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('featured_h').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
	}else if((document.getElementById('featured_h').checked == false) && (document.getElementById('featured_c').checked == true)){
		document.getElementById('featured_type').value = 'c';
		document.getElementById('feture_price').innerHTML = document.getElementById('featured_c').value;
		
		document.getElementById('result_price').innerHTML = parseFloat(document.getElementById('featured_c').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('featured_c').value) +  parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
	}else if((document.getElementById('featured_h').checked == false) && (document.getElementById('featured_c').checked == false)){
		document.getElementById('featured_type').value = 'n';
		document.getElementById('feture_price').innerHTML = '0';
		document.getElementById('result_price').innerHTML = parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
	
	}else{
		document.getElementById('featured_type').value = 'n';
		document.getElementById('feture_price').innerHTML = '0';
		document.getElementById('result_price').innerHTML = parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
		
		document.getElementById('total_price').value = parseFloat(document.getElementById('pkg_price').innerHTML) +  parseFloat(document.getElementById('cat_price').innerHTML);
	}
	<?php
	}
	?>
	if(document.getElementById('submit_coupon_code') && document.getElementById('total_price').value > 0)
	{
		if(document.getElementById('submit_coupon_code')){
			document.getElementById('submit_coupon_code').style.display='';
			}
	}
	else
	{
		if(document.getElementById('submit_coupon_code')){
		document.getElementById('submit_coupon_code').style.display='none';
		}
	}
}
</script>
