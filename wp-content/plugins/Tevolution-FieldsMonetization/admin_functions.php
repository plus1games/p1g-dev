<?php
add_action('admin_init', 'fields_monetization_plugin_redirect');
/*
	Redirect on monetization of tevolution after activating plugin
*/
function fields_monetization_plugin_redirect()
{
	if (get_option('fields_monetization_redirect_on_first_activation') == 'Active' && is_plugin_active('Tevolution/templatic.php'))
	{
		update_option('fields_monetization_redirect_on_first_activation', 'Deactive');
		wp_redirect(site_url().'/wp-admin/admin.php?page=monetization&activate=fields_monetization');
	}
}

/*
	Fetch custom fields as per category select in price package in back end.
*/
add_action('show_custom_fields_monetization_edit','show_fields_monetization_data_edit',99);
function show_fields_monetization_data_edit()
{
	if(get_post_meta($_REQUEST['package_id'],'package_post_type',true) == 'all')
	{
		$post_type = '';
	}
	else
	{
		$sep = ',';
		$package_post_type = get_post_meta($_REQUEST['package_id'],'package_post_type',true);
		$pkg_post_type = explode(',',$package_post_type); 		
		$post_types = get_option('templatic_custom_post');
		$key = '';
		$taxonomy = '';
		foreach ($post_types as $key => $post_type) {	
			$slugs = $post_type['slugs'][0];
			if(in_array($key,$pkg_post_type)){
				$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $key,'public'   => true, '_builtin' => true ));
				if(is_plugin_active('woocommerce/woocommerce.php') && $post_type == 'product' ){
						$rate_taxonomies = $taxonomies[1];
				}else{
					$rate_taxonomies = $taxonomies[0];						
				}
				$taxonomy = $rate_taxonomies;
				$category = explode(",",get_post_meta($_REQUEST['package_id'],'category',true));
				$custom_metaboxes[] = get_post_custom_fields_templ_plugin_backend_monetization($key,$category,$taxonomy);
			}
		}
	}
	display_custom_post_field_plugin1($custom_metaboxes);
}
add_action('wp_ajax_show_custom_fields_monetization1','show_fields_monetization_data_callback',99);
/*
	Fetch custom fields as per category select in price package in back end.
*/
function show_fields_monetization_data_callback()
{
	if($_REQUEST['package_post_type'] == 'all')
	{
		$post_type = '';
	}
	else
	{
		
		$sep = ',';
		for($i=0;$i<count($_REQUEST['package_post_type']);$i++)
		{
			if($i == (count($_REQUEST['package_post_type']) - 1))
				$sep = '';
			$package_post_type =  explode(",",$_REQUEST['package_post_type'][$i]);
			$post_type = $package_post_type[0];
			$taxonomy = $package_post_type[1];
			$custom_metaboxes[] = get_post_custom_fields_templ_plugin_backend_monetization($post_type,$_REQUEST['category'],$taxonomy);
		}
		if(!empty($_REQUEST['category']))
		{
			echo monetize_ajax_display_custom_post_field_plugin1($custom_metaboxes);
		}exit;
	}
}
/* 
	Select all for option to select all custom fields .
*/
add_action('admin_footer','monetize_select_all_custom_fields');
function monetize_select_all_custom_fields()
{?>
	<script>
	function selectall_custom_field()
	{
		dml = document.forms['monetization'];
		chk = dml.elements['custom_fields[]'];
		len = dml.elements['custom_fields[]'].length;
		
		if(document.getElementById('select_all_custom_field').checked == true) { 
			for (i = 0; i < len; i++)
			chk[i].checked = true ;
		} else { 
			for (i = 0; i < len; i++)
			chk[i].checked = false ;
		}
	}
	</script>
<?php
}
/* 
	Save custom fields as per price package selection.
*/
add_action('save_price_package','save_custom_fields');
function save_custom_fields()
{
	global $last_postid;
	if($_REQUEST['package_post_type'] == 'all')
	{
		$post_type = '';
	}
	else
	{
		$sep = ',';
		for($i=0;$i<count($_REQUEST['package_post_type']);$i++)
		{
			if($i == (count($_REQUEST['package_post_type']) - 1))
				$sep = '';
			$package_post_type =  explode(",",$_REQUEST['package_post_type'][$i]);
			$post_type = $package_post_type[0];
			$taxonomy = $package_post_type[1];
			$custom_metaboxes[] = get_post_custom_fields_templ_plugin_backend_monetization($post_type,$_REQUEST['category'],$taxonomy);
			
		}
	}
	
	
	foreach($custom_metaboxes as $heading=>$_custom_metaboxes)
	{		 
		foreach($_custom_metaboxes as $key=>$val) {
			$name = $val['name'];
			$site_title = $val['label'];
			$type = $val['type'];
			$htmlvar_name = $val['htmlvar_name'];			
			$cust_field_id = $val['id'];	
			$value = "";
			
			if($htmlvar_name!="category" && $htmlvar_name != 'post_images' && $type != 'heading_type')
			{
				if($htmlvar_name!="category" && $htmlvar_name != 'post_images' && ($type == 'text' || $type == 'textarea' || $type == 'texteditor' )  )
				{
					update_post_meta($last_postid, $htmlvar_name.'_character_limit', $_REQUEST[$htmlvar_name.'_character_limit']);
				}
			}
		}
	}
	update_post_meta($last_postid, 'custom_fields', $_REQUEST['custom_fields']);
	update_post_meta($last_postid, 'max_image', $_REQUEST['max_image']);
	update_post_meta($last_postid, 'category_can_select', $_REQUEST['category_can_select']);//save the field where user can select maximum category.
}
?>