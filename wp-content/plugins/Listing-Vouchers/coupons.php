<?php
/*
Plugin Name: Directory - Listing Vouchers
Plugin URI: http://templatic.com/docs/listing-vouchers-documentation-guide/
Description: A Directory add-on which will allow you and listing authors to add coupons while adding a listing, your site visitors can download or print the coupon from your website. This will generate a new custom field named "Upload Voucher". You can see this custom field at Tevolution-> Custom Fields Setup. 
Version: 1.0.3
Author: Templatic
Author URI: http://templatic.com/
License: GPL2
*/
ob_start();
/* define Absolute folder path for the plugin */
define('TEVOLUTION_COUPON_FOLDER_PATH', plugin_dir_path( __FILE__ ));
define('T_LISTING_VOUCHER_VERSION','1.0.3');
@define('LISTING_VOUCHER_PLUGIN_NAME','Directory - Listing Vouchers');
define('T_LISTING_VOUCHER_SLUG','Listing-Vouchers/coupons.php');
/* Plugin Folder URL */
define( 'TEVOLUTION_COUPON_URL', plugin_dir_url( __FILE__ ) );

/* Define TFM_DOMAIN name */
define('CPN_DOMAIN','tevolution_coupons');

/* define temperory directory absolute path for coupons */
define('TEVOLUTION_COUPON_TEMP_DIR',get_template_directory().'/images/coupons/');

$wp_upload_dir = wp_upload_dir();
$cpn_folder_url = $wp_upload_dir['basedir'];
$destination = $cpn_folder_url.'/coupons/';

/* destination path for upload coupons */
define('TEVOLUTION_COUPON_MAIN_DIR',$destination);

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );	

/* all finctions file */
require_once(plugin_dir_path( __FILE__ ).'coupons-functions.php' );

if(is_plugin_active('Tevolution/templatic.php'))
{
	/* Include plugin main files. */
	add_action('init','tmpl_load_coupon_domain');
}

/* load text domain for translation files */
function tmpl_load_coupon_domain(){
	if(file_exists(TEVOLUTION_COUPON_FOLDER_PATH.'languages/'.get_locale().'.mo')){
		load_textdomain( CPN_DOMAIN,TEVOLUTION_COUPON_FOLDER_PATH.'languages/'.get_locale().'.mo' );
	}
	
}

function coupons_plugin_activate() {
	
	update_option('coupon_activation','Active');
	
	/* file for install required data during plugin installation */
}
/* for auto updates */
if(strstr($_SERVER['REQUEST_URI'],'plugins.php') || strstr($_SERVER['REQUEST_URI'],'update.php') ){
	require_once('wp-updates-plugin.php');
	new WPUpdatesDirectoryListingvoucher( 'http://templatic.com/updates/api/index.php', plugin_basename(__FILE__) );
}
/*
 * Install listing vouchers custom fields
 */
add_action("admin_init",'listing_vouchers_install',100);
/* add a voucher field during installation and assign it to activated post types */
function listing_vouchers_install(){
	include(TEVOLUTION_COUPON_FOLDER_PATH."install.php");
}
register_activation_hook( __FILE__, 'coupons_plugin_activate');

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ),'tevolution_coupon_action_links'  );
/* add settings link to plugin listing's page */
function tevolution_coupon_action_links($links){
	
	if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php'))
	{
		$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=monetization&action=add_package&tab=packages' ) . '">' . __( 'Settings', CPN_DOMAIN ) . '</a>',	
		);
	}
	else
	{
		$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=templatic_settings#submit_page_settings' ) . '">' . __( 'Settings', CPN_DOMAIN ) . '</a>',	
		);
	}
			
	
	return array_merge( $plugin_links, $links );
}

/* check if coupon is set and ready for download */
if(isset($_REQUEST['dcoupon']) && !empty($_REQUEST['dcoupon']))
{
	$coupon = $_REQUEST['dcoupon'];
	$file = TEVOLUTION_COUPON_MAIN_DIR.$coupon;

	downloadFile($file); /* downloads the coupon*/
}

/* check if coupon is set and ready for download on preview page */
if(isset($_REQUEST['pcoupon']) && !empty($_REQUEST['pcoupon']))
{
	$coupon = $_REQUEST['pcoupon'];
	$file = TEVOLUTION_COUPON_TEMP_DIR.$coupon;

	downloadFile($file); /* downloads the coupon*/
}

/* check if coupons session are set or not, and create array for coupons */
if(isset($_SESSION['file_info_coupon']) &&($_SESSION['file_info_coupon']=='' || empty($_SESSION['file_info_coupon'][0]))){
	$_SESSION['file_info_coupon']= explode(',',$_SESSION['custom_fields']['cpnarr']);
}

/* check if tevolution plugin is activaded or not,if not then populate notice in adminbar */
add_action('admin_notices','coupons_admin_notices');

/* for adding a new field type in custom fields */
add_action('cunstom_field_type','coupon_custom_field',10,1);

/* shows the coupons custon filed on submit form */
add_action('tevolution_custom_fieldtype','coupon_custom_field_on_submitform',1,3);

/* for styling and scripting */
add_action('wp_enqueue_scripts', 'process_styles_scripts');

add_action('wp_footer','tmpl_coupon_script');
add_action('admin_footer','tmpl_coupon_script');

/* script for print coupons */
function tmpl_coupon_script(){
	if(is_single()){
		/* add print script at footer */
		print_script();		
	}
	if(is_admin()){
		?>
		<script>
			jQuery(document).ready(function(){
				if(jQuery('#form_table #ctype').val() == 'coupon_uploader'){
					jQuery("#form_table #show_on_listing").removeAttr('checked');
					jQuery("#form_table #show_on_listing").attr('disabled', 'disabled');
				}else{
					jQuery("#form_table #show_on_listing").removeAttr('disabled');
				}

				jQuery('#form_table #ctype').bind('change',function(){
					if(jQuery(this).val() == 'coupon_uploader'){
						jQuery("#form_table #show_on_listing").removeAttr('checked');
						jQuery("#form_table #show_on_listing").attr('disabled', 'disabled');
					}else{
						jQuery("#form_table #show_on_listing").removeAttr('disabled');
					}
				});
			});
		</script>	
		<?php
	}
}
/* for adding a tab for coupons in listing details page */
add_action('dir_end_tabs','show_coupons_tab');

/* for adding a tab for coupons in event details page child theme */
/*add_action('show_listing_event','show_coupons_tab');*/

/* for adding a tab for coupons in event details page */
add_action('show_listing_event','show_coupons_tab');

/* for showing a coupons in details page */
add_action('listing_coupons','coupons_listing');

/* for showing a coupons in details page */
add_action('listing_extra_details','coupons_listing');

/* for showing a coupons in details page on mobile devices */
add_action('dir_end_mobile_tabs','coupons_listing_in_mobile_device');

/* for upload the coupons */
add_action('save_post','coupons_process',10,1);

/* for adding a tab for coupons in preview page */
add_action('dir_end_tabs_preview','show_coupons_tab_preview');

/* for coupons' preview in preview page */
add_action('listing_end_preview','show_coupon_preview');

/* for coupons' preview in preview page */
add_action('directory_preview_after_post_content','show_coupon_preview');

/* for coupons' preview in preview page */
add_action('eve_preview_after_post_content','show_coupon_preview_with_title');


/* for add a row at backend for number of coupons can upload  */
add_action('templ_submitform_new_row','numberof_coupons_allowed_submitform');

/* add a new row for allowed coupons option in packages */
add_action('fields_monetization_new_row','numberof_coupons_allowed_inpackages');

/* save value of allowed coupons */
add_action('save_price_package','save_numberof_coupons_allowed');

/* add new filed label in Custom Fields in packages */
add_filter('new_field_label','label_for_coupon',10,1);

/* for destroy session and delete coupons from teperory folder */
add_action('remove_session','destroy_coupons_from_tmp');

add_action('admin_init', 'coupon_plugin_redirect');
/*
	Redirect on plugin coupon settings
*/
function coupon_plugin_redirect()
{
	if (get_option('coupon_activation') == 'Active' && is_plugin_active('Tevolution/templatic.php'))
	{
		update_option('coupon_activation', 'Deactive');
		if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php'))
			{
				wp_redirect(site_url().'/wp-admin/admin.php?page=monetization&action=add_package&tab=packages#coupons_allowed');
			}
		else
			{
				wp_redirect(site_url().'/wp-admin/admin.php?page=templatic_settings#numberof_coupons_general_setting');
			}
		
	}
}

/* filter for field validation */
add_filter('submit_form_validation','validate_coupon_custom_field');

/* metabox for coupon uploader at backend */
add_action('admin_init','post_coupon_uploader');

function post_coupon_uploader()
{
	global $post,$post_type,$post_id,$wpdb;
	
	if(isset($_REQUEST['post_type']))
		$post_type = $_REQUEST['post_type'];
	else
		$post_type = $post_type;
		
		
	$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'coupons' and $wpdb->posts.post_type = 'custom_fields' and $wpdb->posts.post_status = 'publish'");
	if($post_content->ID != '')
	{
		$show_on_page = get_post_meta($post_content->ID,'show_on_page',true); 
		
		if(($show_on_page == 'both_side' || $show_on_page == 'admin_side') && in_array($post_type,apply_filters('tevolution_get_post_type',get_post_meta($post_content->ID,'post_type_'.$post_type))))
		{
			add_meta_box("coupon_iuploader",  __("Upload Vouchers",CPN_DOMAIN), "upload_coupons_img_pdf",$post_type, "normal","core", "default");
		}
	}
}


function upload_coupons_img_pdf()
{
	if(file_exists(TEVOLUTION_COUPON_FOLDER_PATH.'coupon_uploader.php'))
	{
		include_once(TEVOLUTION_COUPON_FOLDER_PATH.'coupon_uploader.php');
		coupons_listing(); /* shows all coupons if avaliable */
	}	

	
}

/* action for save coupons from backend */
add_action( 'save_post', 'coupons_save_meta_box_data' );

/* save coupons from back end */
function coupons_save_meta_box_data($post_id)
{
	if(isset($_REQUEST['cpnarr']) && $_REQUEST['cpnarr']!=""){	
			/*coupons_process($post_id);*/
	}
}
add_action('title_above_content','coupons_title');
function coupons_title()
{
	echo '<h2 class="cpn_ttl">';
	_e('Vouchers',CPN_DOMAIN);
	echo '</h2>';
}
/*
	update Listing Vouchers plugin version after templatic member login
 */
add_action('wp_ajax_directory_listing_voucher','directory_listing_voucher_update_login');
function directory_listing_voucher_update_login()
{
	check_ajax_referer( 'directory_listing_voucher', '_ajax_nonce' );
	$plugin_dir = rtrim( plugin_dir_path(__FILE__), '/' );	
	require_once( $plugin_dir .  '/templatic_login.php' );	
	exit;
}
?>