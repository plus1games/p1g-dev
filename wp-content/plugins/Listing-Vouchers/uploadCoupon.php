<?php
/*
 * uploadCoupon.php
 * Validate coupons.
 * Acceps file formate: pdf,png,jpg,gif
 * 
 */

require("../../../wp-load.php");
$structure = TEMPLATEPATH."/images/";
$tmpdata = get_option('templatic_settings');

/* gets the number from backend for how many coupons are allowed to upload */
if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php'))
{
	$num_of_allow_coupons = get_post_meta($_REQUEST['package_select'],'coupons_allowed',true);
	$num_of_coupons = ($num_of_allow_coupons == '' || $num_of_allow_coupons == 0) ? 10 : $num_of_allow_coupons;	
}
else
{
	$num_of_coupons = ($tmpdata['numberof_coupons'] == '' || $tmpdata['numberof_coupons'] == 0)? 10 : $tmpdata['numberof_coupons'];
}

/* check if folder 'coupons' is available or not. If not available then create a folder */
if(!is_dir($structure."coupons"))
{
	if (!mkdir($structure."coupons", 0777, true)) 
	 {
		die('Failed to create folders...');
	 }
}

/* temporary folder path for coupons */
$uploaddir = TEMPLATEPATH."/images/coupons/";
$nam = $_FILES['uploadcoupon']['name'];

$upload = '';
if($_FILES['uploadcoupon']['size'])
	{
		$file_size= $_FILES['uploadcoupon']['size'];
	}

$tmpdata = get_option('templatic_settings');
$limit_size =  $tmpdata['templatic_image_size'];

if(!$limit_size)
	 {
		$limit_size = 50;
		update_option('templatic_image_size',$limit_size);
	 }
 
if($file_size[0])
	{
		if(($file_size[0]/1024) >= $limit_size)
		 {
			echo 'LIMIT';
			exit;
		 }
	}

/* validation for how many coupons are allowed */
if(count($nam) > $num_of_coupons)
{
	echo json_encode(count($nam));
	die;
}

$extra_ext = array('.pdf','.PDF');
$extension_file = wp_parse_args( $extra_ext , $extension_file ); /* allowed file formats array*/

$total_coupons = count($nam);
$c = 1;

global $extension_file;
/*foreach($nam as $key=>$_nam)*/
{
	 $path_info = pathinfo($nam);
	 $file_extension = $path_info["extension"];
	 $finalName = basename($nam,".$file_extension").time().".".$file_extension;
	 $finalCropName = basename($nam,".$file_extension").time()."-80x80.".$file_extension;
	 $finalName=str_replace(' ','',$finalName);
	 $file = $uploaddir .$finalName ;
	 $file_ext= substr($file, -4, 4);		
	 if(in_array($file_ext,$extension_file))
	 {
		 if (move_uploaded_file($_FILES['uploadcoupon']['tmp_name'], $file))
		 {
				
				/* $filename should be the path to a file in the upload directory.*/
				$filename = $file;
				$upload[] = $finalName;
				echo json_encode($upload);
		 }
		 else
		 {
			echo "error";
		 }
	 }else
	 	echo 'error';
}exit;
?>