<?php
global $wp_query,$wpdb,$pagenow;
$listing_post_type=0;
$custom_post_type_listing = CUSTOM_POST_TYPE_LISTING;
$custom_cat_type_listing = CUSTOM_CATEGORY_TYPE_LISTING;
$custom_tag_type_listing = CUSTOM_TAG_TYPE_LISTING;
$custom_post_types_args = array();
$post_type_array = get_post_types($custom_post_types_args,'objects');
if(isset($_POST['reset_custom_fields']) && (isset($_POST['custom_reset']) && $_POST['custom_reset']==1))
{
	update_option('listing_vouchers_custom_fields_insert','none');
}
if(function_exists('tevolution_get_post_type'))
{
	$post_type_array = tevolution_get_post_type();
}
else
{
	$post_type_array = get_post_types($custom_post_types_args,'objects');
}
if(((isset($_REQUEST['ctab']) && $_REQUEST['ctab']=='custom_fields') || $_REQUEST['page']=='templatic_system_menu'|| $pagenow=='themes.php' || $pagenow=='plugins.php')  && get_option('listing_vouchers_custom_fields_insert') !='inserted' )
{
	if(get_option('listing_vouchers_custom_fields_insert') !='inserted'):
	update_option('listing_vouchers_custom_fields_insert','inserted');
	/* Insert Post Coupons into posts */
	$exclude_post_type = apply_filters('reset_exclude_post_types',array());
		$cus_pos_type = get_option("templatic_custom_post");
		$post_type_arr='post,';
		$heading_post_type_arr='post,';
		if($cus_pos_type && count($cus_pos_type) > 0)
		{
			foreach($cus_pos_type as $key=> $_cus_pos_type)
			{
				if(!empty($exclude_post_type)){
					if(!in_array($key,$exclude_post_type)){
						$post_type_arr .= $key.",";
					}
				}else{
					$post_type_arr .= $key.",";
				}
				$heading_post_type_arr .= $key.",";
			}
		}
		$post_type_arr = substr($post_type_arr,0,-1);
		$heading_post_type_arr = substr($heading_post_type_arr,0,-1);
	 $post_content = $wpdb->get_row("SELECT post_name,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'coupons' and $wpdb->posts.post_type = 'custom_fields' and $wpdb->posts.post_status = 'publish'");

	 if(count($post_content) == 0)
	 {
		$my_post = array(
			 'post_title' => 'Upload Voucher',
			 'post_content' => 'The voucher images you upload here will be displayed on the listing detail page with print and download options',
			 'post_status' => 'publish',
			 'post_author' => 1,
			 'post_name' => 'coupons',
			 'post_type' => "custom_fields",
			);
			
		$post_meta = array(
			'heading_type' => 'Label of Field',
			'listing_heading_type' => 'Label of Field',
			'post_type'=> $post_type_arr,
			'ctype'=>'coupon_uploader',
			'htmlvar_name'=>'post_coupons',
			'field_category' =>'all',
			'is_require' => '',
			'sort_order' => '18',
			'listing_sort_order' => '18',
			'is_active' => '1',
			'show_on_page' => 'both_side',
			'show_in_column' => '0',
			'show_on_listing' => '0',
			'is_edit' => 'true',
			'show_on_detail' => '1',
			'is_delete' => '0',
			'show_on_success' => '1',
			'is_search'=>'0',
			'show_in_email'  =>'1',
			'field_require_desc' => '',
			'validation_type' => ''
			);

			
		$post_id = wp_insert_post( $my_post );
		wp_set_post_terms($post_id,'1','category',true);
		if(is_plugin_active('sitepress-multilingual-cms/sitepress.php')){
			global $sitepress;
			$current_lang_code= ICL_LANGUAGE_CODE;
			$default_language = $sitepress->get_default_language();	
			/* Insert wpml  icl_translations table*/
			$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields',$post_id, $current_lang_code, $default_language );
			if(function_exists('wpml_insert_templ_post'))
				wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
		}
		/*wp_set_post_terms($post_id,'1','category',true);*/
		foreach($post_meta as $key=> $_post_meta)
		{
			add_post_meta($post_id, $key, $_post_meta);
		}
		$post_types=get_option('templatic_custom_post');
		$posttype='post,';
		foreach($post_types as $key=>$val){
			$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $key,'public'   => true, '_builtin' => true ));	
			$posttype.=$key.',';
			update_post_meta($post_id, 'post_type_'.$key,$key );
			update_post_meta($post_id, 'taxonomy_type_'.$taxonomies[0],$taxonomies[0] );
			update_post_meta($post_id, $key.'_sort_order',18);
			update_post_meta($post_id, $key.'_heading_type','Label of Field');
		}
 	 }else{
		$post_type=get_post_meta($post_content->ID, 'post_type',true );
		 	if(!strstr($post_type,'listing'))
				update_post_meta($post_content->ID, 'post_type',$post_type.',coupons' );
					
			update_post_meta($post_content->ID, 'post_type_listing','coupons' );
			update_post_meta($post_content->ID, 'taxonomy_type_listingcategory','listingcategory' );
	 }
	 else:
		
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'coupons' and $wpdb->posts.post_type = 'custom_fields' and $wpdb->posts.post_status = 'publish'");
		
		update_post_meta($post_content->ID, 'heading_type','Label of Field' );
		update_post_meta($post_content->ID, 'listing_heading_type','Label of Field' );
		
	 endif;	 
	 
}
?>