<?php
/*
Plugin Name: Custom Modifications
Plugin URI: 
Description: Custom modifications.
Version: 1.0.0
Author: BHagat
License: GPL2
*/
add_action('after_setup_theme','show_buddybar');
function show_buddybar(){	
	if(function_exists('bp_use_wp_admin_bar'))
	add_filter( 'bp_use_wp_admin_bar', false );
}

function bp_core_load_admin_bar_new() {
		remove_action( 'bp_adminbar_logo',  'bp_adminbar_logo'               );
		//remove_action( 'bp_adminbar_menus', 'bp_adminbar_login_menu',    2   );
		//remove_action( 'bp_adminbar_menus', 'bp_adminbar_account_menu',  4   );
		remove_action( 'bp_adminbar_menus', 'bp_adminbar_thisblog_menu', 6   );
		remove_action( 'bp_adminbar_menus', 'bp_adminbar_random_menu',   100 );
		//echo '<style>#wp-admin-bar, #wp-admin-bar .padder{background-color:#6B6B6B;background-image:none !important;}</style>';
}
add_action( 'wp_footer', 'bp_core_load_admin_bar_new', 1);
add_action( 'admin_footer', 'bp_core_load_admin_bar_new',1 );

function showradioimages(){ 
		$exthtml ='<input type="hidden" name="field_24" id="option_47" value="" type="radio"> <img id="m" src="'.get_template_directory_uri().'/images_new/malesign.png" height="50" width="50"><img id="f"  src="'.get_template_directory_uri().'/images_new/femalesign.png" height="50" width="50">';?>
		<script type="text/javascript">
		jQuery(document).ready(function($){
			if($( "#field_24" ).length ) {	
				  $( "#field_24" ).html('<?php echo $exthtml; ?>');
				  $("#m").click(function(){
					   $("input[name='field_24']").val('Male');
					   $("#m").css("box-shadow","1px 1px 11px #6495ED");
					   $("#f").css("box-shadow","none");	
				  });
				  $("#f").click(function(){
					   $("input[name='field_24']").val('Female');
					   $("#f").css("box-shadow","1px 1px 11px #6495ED");
					   $("#m").css("box-shadow","none");	
				  });
			  }
		});</script>
<?php }
add_action('wp_footer','showradioimages');
?>
