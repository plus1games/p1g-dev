<?php
/*$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb,$EM_Location, $post,$EM_Categories;
$categories1 = EM_Categories::get( apply_filters('em_content_categories_args', $args) );
foreach($categories1 as $v){
	$categories[$v->id]=$v->name;
}

function get_lat_long($extended_part){
		$url = "http://maps.google.com/maps/api/geocode/json?".$extended_part; 
		 $ch = curl_init();
		  curl_setopt($ch, CURLOPT_URL, $url);
		   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		     $response = curl_exec($ch);
		    curl_close($ch); 
		   $response = json_decode($response); 
		  $lat = $response->results[0]->geometry->location->lat;
		 $long = $response->results[0]->geometry->location->lng; 
		$result=array('lat'=>$lat,'long'=>$long);
	  return $result;
  }
  $csv_file = $get_path[0].'wp-content/excel/excel_new.csv';

  if ( ! is_file( $csv_file ) )
     exit('File not found.');
  if (($handle = fopen( $csv_file, "r")) !== FALSE)
  {
	  $i=0;
      while (($data = fgetcsv($handle, 0, ",")) !== FALSE)
      {
           if($i == 0){
			   for($n=13;$n<=32;$n++){
				 $heading[$n]=$data[$n];
			   }	
		   }
		  if($i != 0){ 
              
			  $ext_part=$data[1].$data[2].$data[3];
			  $newarr=array(
				 'address'=>$ext_part,
				 'sensor'=>'false'
				  );
			   $extended_part= http_build_query($newarr);
			   $lat_long=(object)get_lat_long($extended_part);
			   if($data[0] != '' && $lat_long->lat !=0  && $lat_long->long !=0){ 
				  $postdate=date('Y-m-d H:i:s',time());
				  $title=wp_specialchars($data[0]);
				  $post_arr=array(
					   'post_author'=>    1,			   
					   'post_title'=>	  $title,
					   'post_status'=>	  'publish',
					   'comment_status'=> 'closed',	
					   'post_type'=>	  'location'		  
				  );
				  $post_id = wp_insert_post( $post_arr );
				  
				  $insert_location_data=array(
					  'post_id'=>				$post_id,
					  'location_slug'=>			sanitize_title($title),
					  'location_name'=>			$title,
					  'location_owner'=>		1,
					  'blog_id'=>				0,
					  'location_address'=>		$data[1],
					  'location_town'=>			$data[2],
					  'location_state'=>		$data[3],
					  'location_postcode'=>		$data[4],
					  'location_country'=>		'US',
					  'location_latitude'=>		$lat_long->lat,
					  'location_longitude'=>	$lat_long->long,
					  'location_status'=>		1,
					  'location_website'=>		$data[5],
					  'location_phone'=>		$data[6]
					  );			  
					$EM_Location = new EM_Location($post_id,'post_id');
					$EM_Location->save_meta();
								
					update_post_meta($post_id, '_location_address', $data[1]);
					update_post_meta($post_id, '_location_town', $data[2]);
					update_post_meta($post_id, '_location_state', $data[3]);
					update_post_meta($post_id, 'state', $data[3]);
					update_post_meta($post_id, '_location_postcode', $data[4]);
					update_post_meta($post_id, '_location_country', 'US');
					update_post_meta($post_id, '_location_latitude', $lat_long->lat);
					update_post_meta($post_id, '_location_longitude', $lat_long->long);
					update_post_meta($post_id, '_location_website', $data[5]);
					update_post_meta($post_id, '_location_phone', $data[6]);
					update_post_meta($post_id, '_location_status', 1);	
					
					$wpdb->query("UPDATE ".EM_LOCATIONS_TABLE." SET location_owner='1',
					location_address='{$data[1]}',
					location_town='{$data[2]}',
					location_state='{$data[3]}',
					location_postcode='{$data[4]}',
					location_country='US',
					location_latitude='{$lat_long->lat}',
					location_longitude='{$lat_long->long}',
					location_website='{$data[5]}',
					location_phone='{$data[6]}',
					location_status=1 WHERE location_id='{$EM_Location->location_id}'");	
					
					$total_fields=0;
					for($n=13;$n<=32;$n++){				
						$key = array_search($heading[$n], $categories); 
						//echo $key.'|'.$heading[$n].'|'.$data[$n].'<br/>';
						if($key !='' && $data[$n] !=''){						
								for($field_number=1;$field_number<=$data[$n];$field_number++){
								  $insert_field_data=array(
										  'location_id'=>		   $EM_Location->location_id,
										  'post_id'=>			   $post_id,
										  'field_group_id'=>	   $EM_Location->location_id.'-'.$total_fields,
										  'field_category'=>	   $key,
										  'field_name'=>		   $heading[$n].$field_number,
										  'field_indoor'=>		   'Outdoor',
										  'field_public'=>		   'Public',
										  'field_location_status'=>1
								  );
								  $wpdb->insert('wp_em_location_fields', $insert_field_data);
								  $total_fields =$total_fields +1;
								}						   

						}
					}
			  }
	      }
		  $i++;
	  }
	  fclose($handle);
  }

  exit( $i-1 ." locations Inserted." );*/

?>