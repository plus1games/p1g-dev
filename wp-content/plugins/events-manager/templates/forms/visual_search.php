<?php
require( dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/wp-load.php' );
$events_count = EM_Events::count( apply_filters('em_content_events_args', $args) );
global $EM_Event, $post ,$wpdb, $current_user, $display_name;

    $event_level        =$_GET['level'];
    $selcatid			=$_GET['selcatid'];
    $event_gender		=$_GET['gender'];
    $event_age			=$_GET['selageid'];
    $event_door			=$_GET['selnewdoor'];
    $zipploc			=$_GET['zipploc'];
	$event_strtimg		=$_GET['evntstnew'];
	$evntstrtntwn		=$_GET['evntstrtntwn'];
	$eventcstmweek		=$_GET['eventcstmweek'];
	$eventcstmweekend	=$_GET['eventcstmweekend'];
	$evntsttdy			=$_GET['evntsttdy'];
	$evntenddte			=$_GET['evntenddte'];
    $radius             =$_GET['disct'];
    $dunit              =$_GET['dunit'];
	if($dunit == 'km')
	{
	  $radius = $radius*0.62;
	}  
	if($event_strtimg == "week"){
        $event_strtdte =$_GET['eventcstmweek'];
		$event_enddte =$_GET['eventcstmweekend'];	
	}else if($event_strtimg =="today"){
        $event_strtdte =$_GET['evntsttdy'];
		$event_enddte =$_GET['evntsttdy'];

	}else if($event_strtimg=="custom")
	{    
       $event_strtdte =$_GET['evntstrtntwn'];
	   $event_enddte =$_GET['evntenddte']; 
	}
	$tmprad=explode('|',$zipploc);				
	if($tmprad[0] !=''){
		    $date=date('Y-m-d H:i:s',time());
			$insertdata =array('zipcode'=>$tmprad[0],'type'=>'event','search_date'=>$date);			
			$insert_id =$wpdb->insert( 'zipsearch_record', $insertdata );

	}
if (class_exists('EM_Events')) {	
	$date1 = date("Y-m-d", strtotime(str_replace('-','/',$event_strtdte)));
	$date2 = date("Y-m-d", strtotime(str_replace('-','/',$event_enddte)));
	if($event_strtdte !='' && $event_enddte !=''){
		$scope = $date1.','.$date2;
	}else{
        $scope = 'future';
	}
	
	 echo EM_Events::output( array(
			'category'=>$selcatid,
			'event_level'=>$event_level,
			'event_sex'=>$event_gender,
			'event_age'=>$event_age,
			'event_door'=>$event_door,						
			'location_id'=>$loc_idd,	
            'zip_code' => $zipploc,		
		    'scope'=>$scope ,
		    'pagination'=>0,
			'limit'=>2000
			));
			
	if(is_user_logged_in()){	
	echo '<br/><a style="text-decoration:none;" target="_blank" href="'.site_url().'/members/'.wp_get_current_user()->data->user_login.'/events/my-events" > 	<button type="button">Add Event</button> </a>';
	}else{
		echo '<br/> <button id ="mustlogin" style="text-decoration:none;" type="button">Add Event </button> ';
      echo '<div id="loginmsg" style="display:none;padding:8px;padding-left:8px; border:1px solid red;background-color:#FFEBE8;color:#000;">You must login to add events.</div>';
	}

	 }	?>
	 <script>
	 jQuery("#mustlogin" ).click(function(){		
	           jQuery("#loginmsg" ).css("display","block");			   
	 });
	 </script>
	 
	