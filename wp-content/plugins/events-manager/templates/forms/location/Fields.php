<?php require( dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))) . '/wp-load.php' );

global $EM_Location, $post,$wpdb;
$required = '*';
$categories = EM_Categories::get( apply_filters('em_content_categories_args', $args) );	
$fieldsizearrval = array('1'=>'1','2'=>'2');
$fieldtypearrval = array('3'=>'3','4'=>'4');

//$args['limit'] = get_option('dbem_categories_default_limit');
//$args['page'] = (!empty($_REQUEST['page']) && is_numeric($_REQUEST['page']) )? //$_REQUEST['page'] : 1;
//echo "<pre>";
//echo $categories[66]->name;


?>
<style>
input[name=field_rate[]]{

}
</style>

<div id="em-location-Fields" >
<script>	
	jQuery(document).ready(function(){			
		jQuery("#addrow").click(function(index){
			var rowCount = jQuery('#locationfields tr').length;
			if(rowCount == 1){ 
				location.reload();
				return false;
			}
			//var clonedRow = jQuery("#locationfields tr:last").clone(); 		
			var clonedRow = jQuery('#locationfields tbody>tr:last').clone(true);	
			clonedRow.each(function () {
				        jQuery(this).find('input[name^=field_name]').attr('name','field_name[]').attr("readonly", false);
						 jQuery(this).find('select[name^=field_category]').attr('name','field_category[]').attr("readonly", false);
						  jQuery(this).find('select[name^=field_indoor]').attr('name','field_indoor[]').attr("readonly", false);
						   jQuery(this).find('select[name^=field_public]').attr('name','field_public[]').attr("readonly", false);
						   jQuery(this).find('input[name^=field_size]').attr('name','field_size[]').attr("readonly", false);
						  jQuery(this).find('input[name^=field_type]').attr('name','field_type[]').attr("readonly", false);
						 jQuery(this).find('input[name^=field_desc]').attr('name','field_desc[]').attr("readonly", false);
						jQuery(this).find('input[name^=field_rate]').attr('name','field_rate[]').attr("readonly", false);
						  
						
			});
			clonedRow.insertAfter('#locationfields tbody>tr:last'); 
		});
		jQuery("a.remove").click(function() {			
		    var ans=confirm("Are you sure you want to delete?");
			  if(ans){                  
				jQuery(this).closest("tr").remove(); // remove row
				  return false; 
		        }else{
		      return false;
		    }
		});


		jQuery("a.edit").click(function(){
		jQuery(this).parent().parent().find("td input,td select").removeAttr("readonly");
		return false;
		   });
		   jQuery("a.duplicate").click(function(){
			  var sel1 = jQuery(this).parent().parent().find('select[name^=field_cat]').val();
			   var sel2 = jQuery(this).parent().parent().find('select[name^=field_ind]').val();
				var sel3 = jQuery(this).parent().parent().find('select[name^=field_pub]').val();
			     var duplicate_row = jQuery(this).parent().parent().clone(true); 
				    duplicate_row.each(function () {
						jQuery(this).find('input[name^=field_name]').attr('name','field_name[]').attr("readonly", false);
						 jQuery(this).find('select[name^=field_category]').attr('name','field_category[]').attr("readonly", false);
						  jQuery(this).find('select[name^=field_indoor]').attr('name','field_indoor[]').attr("readonly", false);
						   jQuery(this).find('select[name^=field_public]').attr('name','field_public[]').attr("readonly", false);
						   jQuery(this).find('input[name^=field_size]').attr('name','field_size[]').attr("readonly", false);
						  jQuery(this).find('input[name^=field_type]').attr('name','field_type[]').attr("readonly", false);
						 jQuery(this).find('input[name^=field_desc]').attr('name','field_desc[]').attr("readonly", false);
						jQuery(this).find('input[name^=field_rate]').attr('name','field_rate[]').attr("readonly", false);
						  
					});
			       if(duplicate_row.insertAfter('#locationfields tbody>tr:last')){ 
			      jQuery('#locationfields tbody>tr:last').find('select[name^=field_cat]').val(sel1);
			     jQuery('#locationfields tbody>tr:last').find('select[name^=field_ind]').val(sel2);
			    jQuery('#locationfields tbody>tr:last').find('select[name^=field_pub]').val(sel3);
			}
			return false;
		   });
	});

</script>

<?php 
//echo "<pre>";

//print_r($EM_Location);

//die();
?>
<div style="width:100%; overflow:scroll;margin-top:20px;">
	<button id="addrow" type="button" name="addrow" value="Add Fields" >Add Field</button>
	
	<table  id="locationfields" border="1" width="100%">
	<tr height="26"style="font-weight:bold;background:#FFFBCC">
		<th>Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th> 
			<th>Field Category</th>
				<th>Field Name</th>
					<th>Indoor/Outdoor</th>
						 <th>Public-Private</th>
					<th>Description</th>
				<th>Size</th>
			<th>Type</th>
		<th>Rate/Hour</th>
	
		
		
	</tr>
	<?php if($EM_Location->location_id==''){ ?>
		<tr height="24">
		
		<td > <a class="edit" href="" ><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/edit_icon.jpg" title="edit"></a> &nbsp;<a  href="" class="remove"><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/delete_icon.jpg" title="delete"></a>
		<a  href="" class="duplicate"><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/copy_icon.jpg" title="duplicate"></a>
		</td>
		
		<td><!-- <input type="text" name="field_category[]" value=""> -->		
		<select name="field_category[]" >
		<?php foreach($categories as $category){			
		 echo '<option value="'.$category->id.'">'.$category->name.'</option>';
		}  ?>
		
		</select>
		</td>
		<td><input type="text" name="field_name[]" value="" ></td>
		<td><!-- <input type="text" name="field_indoor[]" value=""> -->
		<select name="field_indoor[]">
		<option value="Indoor">Indoor</option>
		<option value="Outdoor">Outdoor</option>
		</select>
		
		</td>
		<td>
		<select name="field_public[]" >
		<option value="Public">Public</option>
		<option value="Private">Private</option>
		<option value="Semi-Private">Semi-Private</option>
		<option value="Resort">Resort</option>
		<option value="Military">Military</option>
		</select>
		<!-- <input type="text" name="field_public[]" value=""> --></td>
		<td><input type="text" name="field_desc[]" value="" ></td>	
		<td><input type="text" name="field_size[]" value="" ></td>	
		<td><input type="text" name="field_type[]" value="" ></td>	
		<td><input type="text" name="field_rate[]" value="" ></td>
	</tr>
<?php } else{
$location_id_pre= $EM_Location->location_id;
 $quer2="SELECT * from wp_em_location_fields WHERE location_id='".$location_id_pre."'";
$res=$wpdb->get_results($quer2);
if(count($res)>=1){  
	foreach($res as $row){?>
		<tr height="24">
				<td><a class="edit" href="" ><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/edit_icon.jpg" title="edit"></a> &nbsp;<a  href="" class="remove"><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/delete_icon.jpg" title="delete"></a>
				<a  href="" class="duplicate"><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/copy_icon.jpg" title="duplicate"></a></td>	
				<td>
				<select name="field_category[<?php echo $row->field_id; ?>]" readonly="readonly">
		<?php foreach($categories as $category){?>
		 <option value="<?php echo $category->id ; ?>" <?php echo ($row->field_category == trim($category->id)) ? "selected='selected'"  : ''; ?>><?php echo $category->name ; ?></option>
		<?php }  ?>
				
				</td>
				<td><input type="text" name="field_name[<?php echo $row->field_id; ?>]" value="<?php echo $row->field_name;?>" readonly="readonly"></td>
        <td><select name="field_indoor[<?php echo $row->field_id; ?>]" readonly="readonly">
		<option value="Indoor" <?php echo ($row->field_indoor == 'Indoor') ? "selected='selected' " : ''; ?> >Indoor</option>
		<option value="Outdoor" <?php echo ($row->field_indoor == 'Outdoor') ? "selected='selected' " : ''; ?>>Outdoor</option>
		</select>
		
		</td>
		<td>
		<select name="field_public[<?php echo $row->field_id; ?>]"  readonly="readonly">
		<option value="Public" <?php echo ($row->field_public == 'Public') ? "selected='selected' " : ''; ?>>Public</option>
		<option value="Private" <?php echo ($row->field_public == 'Private') ? "selected='selected' " : ''; ?>>Private</option>
		<option value="Public" <?php echo ($row->field_public == 'Semi-Private') ? "selected='selected' " : ''; ?>>Semi-Private</option>
		<option value="Private" <?php echo ($row->field_public == 'Resort') ? "selected='selected' " : ''; ?>>Resort</option>
		<option value="Public" <?php echo ($row->field_public == 'Military') ? "selected='selected' " : ''; ?>>Military</option>
		
		</select>
		</td>
				
				<td><input type="text" name="field_desc[<?php echo $row->field_id; ?>]" value="<?php echo $row->field_desc;?>" readonly="readonly"></td>
				<td><input type="text" name="field_size[<?php echo $row->field_id; ?>]" value="<?php echo $row->field_size;?>" readonly="readonly"></td>
				<td><input type="text" name="field_type[<?php echo $row->field_id; ?>]" value="<?php echo $row->field_type;?>" readonly="readonly"></td>
				<td><input type="text"   name="field_rate[<?php echo $row->field_id; ?>]" value="<?php echo $row->field_rate;?>" readonly="readonly"></td>
			</tr>
<?php
	}
}
/*
  $num_rows=mysql_num_rows($res);
if($num_rows >= 1){
 while($row= mysql_fetch_assoc($res)){ ?>       
		<tr height="24">
				<td><a class="edit" href="" ><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/edit_icon.jpg" title="edit"></a> &nbsp;<a  href="" class="remove"><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/delete_icon.jpg" title="delete"></a>
				<a  href="" class="duplicate"><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/copy_icon.jpg" title="duplicate"></a></td>	
				<td>
				<select name="field_category[<?php echo $row['field_id']; ?>]" readonly="readonly">
		<?php foreach($categories as $category){?>
		 <option value="<?php echo $category->id ; ?>" <?php echo ($row['field_category'] == trim($category->id)) ? "selected='selected'"  : ''; ?>><?php echo $category->name ; ?></option>
		<?php }  ?>
				
				</td>
				<td><input type="text" name="field_name[<?php echo $row['field_id']; ?>]" value="<?php echo $row['field_name']?>" readonly="readonly"></td>
        <td><select name="field_indoor[<?php echo $row['field_id']; ?>]" readonly="readonly">
		<option value="Indoor" <?php echo ($row['field_indoor'] == 'Indoor') ? "selected='selected' " : ''; ?> >Indoor</option>
		<option value="Outdoor" <?php echo ($row['field_indoor'] == 'Outdoor') ? "selected='selected' " : ''; ?>>Outdoor</option>
		</select>
		
		</td>
		<td>
		<select name="field_public[<?php echo $row['field_id']; ?>]"  readonly="readonly">
		<option value="Public" <?php echo ($row['field_public'] == 'Public') ? "selected='selected' " : ''; ?>>Public</option>
		<option value="Private" <?php echo ($row['field_public'] == 'Private') ? "selected='selected' " : ''; ?>>Private</option>
		<option value="Public" <?php echo ($row['field_public'] == 'Semi-Private') ? "selected='selected' " : ''; ?>>Semi-Private</option>
		<option value="Private" <?php echo ($row['field_public'] == 'Resort') ? "selected='selected' " : ''; ?>>Resort</option>
		<option value="Public" <?php echo ($row['field_public'] == 'Military') ? "selected='selected' " : ''; ?>>Military</option>
		
		</select>
		</td>
				
				<td><input type="text" name="field_desc[<?php echo $row['field_id']; ?>]" value="<?php echo $row['field_desc']?>" readonly="readonly"></td>
				<td><input type="text" name="field_size[<?php echo $row['field_id']; ?>]" value="<?php echo $row['field_size']?>" readonly="readonly"></td>
				<td><input type="text" name="field_type[<?php echo $row['field_id']; ?>]" value="<?php echo $row['field_type']?>" readonly="readonly"></td>
				<td><input type="text"   name="field_rate[<?php echo $row['field_id']; ?>]" value="<?php echo $row['field_rate']?>" readonly="readonly"></td>
			</tr>
           <?php }
            
				 }*/else{?>
				

				  	<tr  height="24">
		<td> <a class="edit" href="" ><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/edit_icon.jpg" title="edit"></a> &nbsp;<a  href="" class="remove"><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/delete_icon.jpg" title="delete"></a>
		<a  href="" class="duplicate"><img src="<?php  echo content_url(); ?>/plugins/events-manager/images/copy_icon.jpg" title="duplicate"></a></td>
		<td>		
		<select name="field_category[]" >
		<?php foreach($categories as $category){
		 echo '<option value="'.$category->id.'">'.$category->name.'</option>';
		}  ?></td>
		<td><input type="text" name="field_name[]" value="" ></td>
		<td>
		<select name="field_indoor[]" >
		<option value="Indoor">Indoor</option>
		<option value="Outdoor">Outdoor</option>
		</select>
		
		</td>
		<td>
		<select name="field_public[]" >
		<option value="Public">Public</option>
		<option value="Private">Private</option>
		<option value="Semi-Private">Semi-Private</option>
		<option value="Resort">Resort</option>
		<option value="Military">Military</option>
		</select>
		</td>
		<td><input type="text" name="field_desc[]" value="" ></td>	
		<td><input type="text" name="field_size[]" value="" ></td>	
		<td><input type="text" name="field_type[]" value="" ></td>	
		<td><input type="text" name="field_rate[]" value="" ></td>
	</tr>
		<?php }
}
?>
	
	</table>
	</div>
	
</div>