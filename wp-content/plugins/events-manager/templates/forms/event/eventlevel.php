<?php
	if($EM_Event->event_id != ''){
		$querylevel="select * from wp_em_events WHERE event_id=".$EM_Event->event_id;	
		$rowlevel = $wpdb->get_results($querylevel,ARRAY_A);
	 	$selected =$rowlevel[0]['event_level'];
	} else {
		$selected='';
	}
?>
<script>
	jQuery(document).ready(function(){ 	

		jQuery('#Recreation').click(function(){
					
		 	var callurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/callback2.php";
			var level='Recreation';
			jQuery('#Recreation').css("box-shadow","1px 1px 11px #6495ED");
			jQuery('#Beginner').css("box-shadow","none");
			jQuery('#Intermediate').css("box-shadow","none");
			jQuery('#Advance').css("box-shadow","none");
			jQuery('#Competition').css("box-shadow","none");
			jQuery.ajax({					
				url: callurl,
				data: "data="+level,
				success:function(msg){
					jQuery("#showhidden").html(msg);
				}
			});
		});

		jQuery('#Beginner').click(function(){
					
		 	var callurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/callback2.php";
			var level='Beginner';
			jQuery('#Beginner').css("box-shadow","1px 1px 11px #6495ED");
			jQuery('#Recreation').css("box-shadow","none");
			jQuery('#Intermediate').css("box-shadow","none");
			jQuery('#Advance').css("box-shadow","none");
			jQuery('#Competition').css("box-shadow","none");
			jQuery.ajax({					
				url: callurl,
				data: "data="+level,
				success:function(msg){
					jQuery("#showhidden").html(msg);
				}
			});
		});

		jQuery('#Intermediate').click(function(){
			var callurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/callback2.php";
			var level='Intermediate';
			jQuery('#Intermediate').css("box-shadow","1px 1px 11px #6495ED");
			jQuery('#Recreation').css("box-shadow","none");
			jQuery('#Beginner').css("box-shadow","none");
			jQuery('#Advance').css("box-shadow","none");
			jQuery('#Competition').css("box-shadow","none");
			jQuery.ajax({					
				url: callurl,
				data: "data="+level,
				success:function(msg){
					jQuery("#showhidden").html(msg);
				}
			});
		});

		jQuery('#Advance').click(function(){
			var callurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/callback2.php";
			var level='Advance';
			jQuery('#Advance').css("box-shadow","1px 1px 11px #6495ED");
			jQuery('#Recreation').css("box-shadow","none");
			jQuery('#Beginner').css("box-shadow","none");
			jQuery('#Intermediate').css("box-shadow","none");
			jQuery('#Competition').css("box-shadow","none");
			jQuery.ajax({					
				url: callurl,
				data: "data="+level,
				success:function(msg){
					jQuery("#showhidden").html(msg);	
				}
			});
		});

		jQuery('#Competition').click(function(){
			var callurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/callback2.php";
			var level='Competition';
			jQuery('#Competition').css("box-shadow","1px 1px 11px #6495ED");
			jQuery('#Recreation').css("box-shadow","none");
			jQuery('#Beginner').css("box-shadow","none");
			jQuery('#Intermediate').css("box-shadow","none");
			jQuery('#Competition').css("box-shadow","none");
			jQuery.ajax({					
				url: callurl,
				data: "data="+level,
				success:function(msg){
					jQuery("#showhidden").html(msg);	
				}
			});
		});

		jQuery('#<?php echo $selected; ?>').click();
});
</script>

<div class="leveldiv" style="min-height:150px;">
	<div style="float:left;margin-right:31px;height:150px;" >
		<img id="Recreation" style="cursor:pointer;" src="<?php echo content_url(); ?>/plugins/events-manager/images/Recreation.png" title="Recreation" width="80" height="130"><br/>
		<center>Recreation</center>
	</div>

	<div style="float:left;margin-right:31px;height:150px;" >
		<img id="Beginner" style="cursor:pointer;" src="<?php echo content_url(); ?>/plugins/events-manager/images/Beginner.png" title="Beginner" width="80" height="130"><br/>
		<center>Beginner</center>
	</div>

	<div style="float:left;margin-right:31px;height:150px;" >
		<img id="Intermediate" style="cursor:pointer;" src="<?php echo content_url(); ?>/plugins/events-manager/images/Intermediate.png" title="Intermediate" width="80" height="130" ><br/>
		<center>Intermediate</center>
	</div>

	<div style="float:left;margin-right:31px;height:150px;" >
		<img id="Advance" style="cursor:pointer;" src="<?php echo content_url(); ?>/plugins/events-manager/images/Advance.png" title="Advance" width="80" height="130"><br/>
		<center>Advance</center>
	</div>

	<div style="float:left;margin-right:31px;height:150px;" >
		<img id="Competition" style="cursor:pointer;" src="<?php echo content_url(); ?>/plugins/events-manager/images/Competition.png" title="Competition" width="80" height="130"><br/>
		<center>Competition</center>
	</div>
</div>
<div id="showhidden"></div>