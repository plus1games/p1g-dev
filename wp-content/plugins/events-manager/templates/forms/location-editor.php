<?php
/* WARNING! This file may change in the near future as we intend to add features to the location editor. If at all possible, try making customizations using CSS, jQuery, or using our hooks and filters. - 2012-02-14 */
/* 
 * To ensure compatability, it is recommended you maintain class, id and form name attributes, unless you now what you're doing. 
 * You also must keep the _wpnonce hidden field in this form too.
 */

 global $EM_Location, $EM_Notices;
if( is_object($EM_Location) && !$EM_Location->can_manage('edit_locations','edit_others_locations') ){
	?>
	<div class="wrap"><h2><?php _e('Unauthorized Access','dbem'); ?></h2><p><?php echo sprintf(__('You do not have the rights to manage this %s.','dbem'),__('location','dbem')); ?></p></div>
	<?php
	return false;
}elseif( !is_object($EM_Location) ){
	$EM_Location = new EM_Location();
}
$required = "<i>(".__('required','dbem').")</i>";
echo $EM_Notices;

if(!empty($EM_Location->location_id)){
	$add_title="Edit Location";
}
else{
	$add_title="Add New Location";
}
?>

<div>
	<div id="add_location" class="add_location" ><?php echo $add_title ?>
	</div>
	<div id="location_links" class="location_links" >
		<ul>
			<li class ="active_li" id="1" rel="info">Location</li>
			<li rel="address" id="2">Address</li>
			<li rel="authorization" id="3">Contact Info</li>
			<li rel="acres" id="4">Details</li>
			<li rel="amenities" id="5">Amenities</li>
			<li rel="field" id="6">Field</li>
			<li rel="field_contact" id="7">Field Contact</li>
			<li rel="gallery" id="8">Gallery</li>
			<li rel="document" id="9">Documents</li>
			<li rel="save_location" id="10">Remove/Save Location</li>
		</ul>
	</div>
	<form enctype='multipart/form-data' id='location-form' method='post' action=''>
	<input type='hidden' name='action' value='location_save' />
	<input type='hidden' name='_wpnonce' value='<?php echo wp_create_nonce('location_save'); ?>' />
	<input type='hidden' name='location_id' value='<?php echo $EM_Location->location_id ?>'/>
		
	<?php //global $EM_Notices; //echo $EM_Notices; ?>
	<?php do_action('em_front_location_form_header'); ?>
	<?php if(get_option('dbem_attributes_enabled')) : ?>
		<?php
		$attributes = em_get_attributes(true); //lattributes only
		$has_depreciated = false;
		?>
		<?php if( count( $attributes['names'] ) > 0 ) : ?>
			<?php foreach( $attributes['names'] as $name) : ?>
			<div class="location-attributes">
				<label for="em_attributes[<?php echo $name ?>]"><?php echo $name ?></label>
				<?php if( count($attributes['values'][$name]) > 0 ): ?>
				<select name="em_attributes[<?php echo $name ?>]">
					<?php foreach($attributes['values'][$name] as $attribute_val): ?>
						<?php if( is_array($EM_Location->location_attributes) && array_key_exists($name, $EM_Location->location_attributes) && $EM_Location->location_attributes[$name]==$attribute_val ): ?>
							<option selected="selected"><?php echo $attribute_val; ?></option>
						<?php else: ?>
							<option><?php echo $attribute_val; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
				<?php else: ?>
				<input type="text" name="em_attributes[<?php echo $name ?>]" value="<?php echo array_key_exists($name, $EM_Location->location_attributes) ? htmlspecialchars($EM_Location->location_attributes[$name], ENT_QUOTES):''; ?>" />
				<?php endif; ?>
			</div>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>
	<div id="location_forms" >
		<div id="info" class="location_details">
			<h4>
				<?php _e ( 'Location Name', 'dbem' ); ?>
			</h4>
			<div class="inside">
				<input name='location_name' id='location-name' type='text' value='<?php echo htmlspecialchars($EM_Location->location_name, ENT_QUOTES); ?>' size='40'  />
				<br />
				<?php _e('The name of the location', 'dbem') ?>
			</div>
			<h4> 
				<?php _e ( 'Details', 'dbem' ); ?>
			</h4>
			<div class="inside">
				<?php if( get_option('dbem_events_form_editor') && function_exists('wp_editor') ): ?>
				<?php wp_editor($EM_Location->post_content, 'em-editor-content', array('textarea_name'=>'content') ); ?> 
				<?php else: ?>
				<textarea name="content" rows="10" style="width:100%"><?php echo $EM_Location->post_content; ?></textarea>
				<br />
				<?php _e ( 'Details about the location.', 'dbem' )?> <?php _e ( 'HTML Allowed.', 'dbem' )?>
				<?php endif; ?>
			</div>
			<br/><br/><br/><br/>
			<div class="continue" rel="2">Save & Continue</div>
		</div>
		<div id="address" class="location_details">
			<h4>
				<?php _e ( 'Location Address', 'dbem' ); ?>
			</h4>
			<div class="inside" >
				<?php em_locate_template('forms/location/where.php','dbem'); ?>
			</div>
			<div class="continue" rel="3">Save & Continue</div>
		</div>

		<div id="authorization" class="location_details">
			<h4>
				<?php _e ( 'Location Website/Phone', 'dbem' ); ?>
			</h4> 
			<div class="inside" >
				<?php em_locate_template('forms/location/website.php','dbem'); ?>
			</div>
			<div class="continue" rel="4">Save & Continue</div>
		</div>
			
		<div id="acres" class="location_details">
			<h4>
				<?php _e ( 'Location Acres', 'dbem' ); ?>
			</h4> 
			<div class="inside" >
				<?php em_locate_template('forms/location/acres.php','dbem'); ?>
			</div>
			<h4>
				<?php _e ( 'Location Public/Private', 'dbem' ); ?>
			</h4> 
			<div class="inside" >
				<?php em_locate_template('forms/location/public.php','dbem'); ?>
			</div>
			<h4>
				<?php _e ( 'Location Authorized Reresentative', 'dbem' ); ?>
			</h4> 
			<div class="inside" >
				<?php em_locate_template('forms/location/authorized.php','dbem'); ?>
			</div>
			<div class="continue" rel="5">Save & Continue</div>
		</div>
		<div id="amenities" class="location_details">
			<h4>
				<?php _e ( 'Location Amenities', 'dbem' ); ?>
			</h4>
			<div class="inside" >
				<?php em_locate_template('forms/location/animities.php','dbem'); ?>
			</div>
			<div class="continue" rel="6">Save & Continue</div>
		</div>
		<div id="field" class="location_details">
			<h4>
				<?php _e ( 'Location Fields', 'dbem' ); ?>
			</h4> 
			<?php em_locate_template('forms/location/Fields.php','dbem'); ?>
			<div class="continue" rel="7">Save & Continue</div>
		</div>
		<div id="field_contact" class="location_details">
			<h4>
				<?php _e ( 'Fields Contact', 'dbem' ); ?>
			</h4> 
			<?php em_locate_template('forms/location/fields_contact.php','dbem'); ?>
			<div class="continue" rel="8">Save & Continue</div>
		</div>
		<div id="gallery" class="location_details">
			<h4>
				<?php _e ( 'Location Gallery', 'dbem' ); ?>
			</h4>
			<div class="inside" >
				<?php em_locate_template('forms/location/gallery.php','dbem'); ?>
			</div>	
			<div class="continue" rel="9">Save & Continue</div>
		</div>
			
			
		<div id="document" class="location_details">
			<?php em_locate_template('forms/location/attached_document.php','dbem'); ?>
			<div class="continue" rel="10">Save & Continue</div>
			
		</div>
		<div id="save_location" class="location_details">
			<h4>
				<?php do_action('em_front_location_form_footer'); ?>

				<input type="hidden" name="location_id" value="<?php echo $EM_Location->location_id; ?>" />
				<input type="hidden" name="action" value="location_save" />
			
				<?php if( !empty($_REQUEST['redirect_to']) ): ?>
				<input type="hidden" name="redirect_to" value="<?php echo $_REQUEST['redirect_to']; ?>" />
				<?php endif; ?>
				<p class='submit'><input type='submit' class='button-primary' name='submit' value='<?php _e('Update location', 'dbem') ?>' /></p>
			</h4>
		</div>	
	</div>	
	</form>
</div>

<style>
#add_location{
	background-color:#284C94;
	color: #ffffff;
	font-size: 20px;
	font-weight: bold;
	text-shadow: 2px 1px 0 000000;
	padding:15px 0px 15px 20px;
}
#location_links{
	width:24%;
	float:left;
}
#location_forms{
	width:74%;
	float:right;
	padding:10px;
	background-color:#fff;
}

.location_details{
	display:none;
	position:relative;
	height:auto;
	min-height:598px;
}
#location_links ul li{
	color:#C3C3C3;
	font-size:17px;
	cursor:pointer;
	padding:20px 0 20px 15px;
	border:2px solid #F4F4F4;
	background:#fff;

}
#location_links ul li.active_li{ 
	color:black;
	background-color:#EEEEEE;
	position:relative;
} 
#location_links ul li.active_li:after , #location_links ul li.active_li:before { 
	left: 76%;
    position: absolute;
	top: 50%; 
	border: solid transparent; 
	content: " ";
  }
#location_links ul li.active_li:after {
	border-color: rgba(136, 183, 213, 0); 
	border-right-color: #fff; 
	border-width: 31px; 
	margin-top: -30px;
} 
.location_details h4{
	background:none;
	margin-left:25px !important;
	margin-right:10px;
	}
.location_details .inside{
	margin-left:25px;
	margin-right:10px;
}
.location_details .wp-editor-wrap{
	border:1px solid #F5F5F5;
	
}
.continue {
	background-color:#284C94 !important;
	color: #ffffff;font-size: 15px;
	text-shadow: 2px 1px 0 000000;
	padding:10px;
	width:110px;
	margin:20px;
	float:right;
	border-radius:5px;
	cursor:pointer;
	bottom:0px;
	right:0px;
	position:absolute;
}
#save_location input[type="submit"]{
	background:  #284C94 !important;
	border-radius: 5px;
    color: #fff;
    font-size: 14px;
    font-weight: bold;
    height: auto;
    padding: 14px;
    width: 140px;
}
body{
	color:#666;
}
</style>


<script type="text/javascript">

	jQuery(document).ready( function($){
		$('#info').css('display','block');
		$('#location_links ul li').click(function() {
		var rel_val   = $(this).attr('rel');
		$('.location_details').css('display','none');
		$(this).addClass('active_li').siblings().removeClass('active_li');
		$('#'+rel_val).css('display','block');
		});

		$('#location_forms .continue').click(function() {
			var button_rel=$(this).attr('rel');
			$('#location_links ul li#'+button_rel).trigger( "click" );

		});

	});
</script>

					<!-- 
	<?php //if( $EM_Location->can_manage('upload_event_images','upload_event_images') ): ?>
	<h4><?php //_e ( 'Location Image', 'dbem' ); ?></h4>
	<div class="inside" style="padding:10px;">
		<?php //if ($EM_Location->get_image_url() != '') : ?> 
			<img src='<?php //echo $EM_Location->get_image_url('medium'); ?>' alt='<?php //echo $EM_Location->location_name ?>'/>
		<?php //else : ?> 
			<?php //_e('No image uploaded for this location yet', 'dbem') ?>
		<?php //endif; ?>
		<br /><br />
		<label for='location_image'><?php //_e('Upload/change picture', 'dbem') ?></label> <input id='location-image' name='location_image' id='location_image' type='file' size='40' />
		<br />
		<label for='location_image_delete'><?php //_e('Delete Image?', 'dbem') ?></label> <input id='location-image-delete' name='location_image_delete' id='location_image_delete' type='checkbox' value='1' />
	</div>
	<?php //endif; ?> -->
	<!-- //////////////////////////////////////////////////// -->
