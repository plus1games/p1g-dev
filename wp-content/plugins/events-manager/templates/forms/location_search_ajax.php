<?php

require( dirname(dirname(dirname(dirname(dirname(dirname((__FILE__))))))). '/wp-load.php' );

global $EM_Event, $post ,$wpdb, $current_user, $display_name,$EM_Location;
       
	   $cat_id = $_GET['cat_id'];
	   $country_id =$_GET['country_id'];
	   $region_id =$_GET['region_id'];
	   $state_id =$_GET['state_id'];
	   $city_id =$_GET['city_id'];
	   $search_id =$_GET['search_id'];
	   $page_number =$_GET['page_number'];

$args['offset'] = ($args['page']-1)*$args['limit'];
echo EM_Locations::output( array('limit'=>'10','pagination'=>1,'country'=>$country_id ,'state'=>$state_id,'region'=>$region_id , 'town'=>$city_id,'category'=>$cat_id, 'search'=>$search_id,'is_Ajax'=>$_GET['is_Ajax'],'page'=>$page_number) );


if(is_user_logged_in()){	
	echo '<a style="text-decoration:none;" target="_blank" href= "'.site_url().'/members/'.wp_get_current_user()->data->user_login.'/events/my-locations" > 
	 <button type="button">Add Location</button> </a>';
	}else{
		echo '<br/> <button id ="mustlogin" style="text-decoration:none;" type="button">Add Event </button> ';
      echo '<div id="loginmsg" style="display:none;padding:8px;padding-left:8px; border:1px solid red;background-color:#FFEBE8;color:#000;">You must login to add locations.</div>';
	}

?>
<script>
	 jQuery("#mustlogin" ).click(function(){		
	           jQuery("#loginmsg" ).css("display","block");
	 });
	 </script>
<script type="text/javascript">
//<![CDATA[

jQuery(document).ready( function($){
		$(".page-numbers").click(function(){
		var hdn_page_number = $(this).attr('title');
		var cat_id =$('#hdn_cat_id').val();
		var country_id =$('#hdn_country_id').val();
		var region_id =$('#hdn_region_id').val();
	    var state_id =$('#hdn_state_id').val();
		var city_id =$('#hdn_city_id').val();
		var search_id = $('#hdn_search_id').val();
	 	var callurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/location_search_ajax.php";
		var data='cat_id='+cat_id+'&country_id='+country_id+'&region_id='+region_id+'&&state_id='+state_id+'&city_id='+city_id+'&search_id='+search_id+'&is_Ajax=1&page_number='+hdn_page_number;

        $('#loader').html('<img src="<?php echo content_url(); ?>/plugins/events-manager/includes/images/ajax-loader.gif"/>');
			$.ajax({
				url:callurl,
				data:data,
				success:function(msg){
					$('#loader').html('');
					$('#showcatlocation').html(msg);
				}
			}); 
 });
		

});
//]]>
</script>