<?php
/* 
 * Remember that this file is only used if you have chosen to override event pages with formats in your event settings!
 * You can also override the single event page completely in any case (e.g. at a level where you can control sidebars etc.), as described here - http://codex.wordpress.org/Post_Types#Template_Files
 * Your file would be named single-event.php
 */
/*
 * This page displays a single event, called during the em_content() if this is an event page.
 * You can override the default display settings pages by copying this file to yourthemefolder/plugins/events-manager/templates/ and modifying it however you need.
 * You can display events however you wish, there are a few variables made available to you:
 * 
 * $args - the args passed onto EM_Events::output() 
 */
 
global $EM_Event;
/* @var $EM_Event EM_Event */
echo $EM_Event->output_single();

?>
<style>
#show_hide_map {
    background: none repeat scroll 0 0 #CDCDCD;
    cursor: pointer;
    float: right;
    padding: 5px;
    width: 100px;
}
</style>
<script>

jQuery(document).ready( function($){
	$("#show_hide_map").click(function(){
	var attr_ref=	$(this).attr('ref');
	if(attr_ref == 'open'){
     $(this).html('Hide the Map');
	 $(this).attr( "ref", "close" );
	}
	if(attr_ref == 'close'){
     $(this).html('Show the Map');
	  $(this).attr( "ref", "open" );
	}
    $(".em-locations-map").slideToggle("slow");
	});
	
	});
	</script>