<?php
/**
 * Controller for the event views in BP (using mvc terms here)
 */
function bp_em_my_events_profile() {
	global $bp, $EM_Event;
	if( !is_object($EM_Event) && !empty($_REQUEST['event_id']) ){
		$EM_Event = new EM_Event($_REQUEST['event_id']);
	}
	
	do_action( 'bp_em_my_events_profile' );
	
	$template_title = 'bp_em_my_events_profile_title';
	$template_content = 'bp_em_my_events_profile_content';

	if( !empty($_GET['action']) ){
		switch($_GET['action']){
			case 'edit':
				$template_title = 'bp_em_my_events_profile_editor_title';
				break;
		}
	}

	add_action( 'bp_template_title', $template_title );
	add_action( 'bp_template_content', $template_content );
	

	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}
function bp_em_my_events_profile_title() {
	_e( 'Game Profile', 'dbem' );
}

function bp_em_my_events_profile_content() {
	em_locate_template('buddypress/my-game-profile.php', true);
}


function bp_em_my_events_profile_uniform() {
	global $bp, $EM_Event;
	if( !is_object($EM_Event) && !empty($_REQUEST['event_id']) ){
		$EM_Event = new EM_Event($_REQUEST['event_id']);
	}
	
	do_action( 'bp_em_my_events_profile_uniform' );
	
	$template_title = 'bp_em_my_events_profile_title_uniform';
	$template_content = 'bp_em_my_events_profile_content_uniform';

	if( !empty($_GET['action']) ){
		switch($_GET['action']){
			case 'edit':
				$template_title = 'bp_em_my_events_profile_editor_title';
				break;
		}
	}

	add_action( 'bp_template_title', $template_title );
	add_action( 'bp_template_content', $template_content );
	
	
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}


function bp_em_my_events_profile_title_uniform() {
	_e( 'Uniform', 'dbem' );
}

function bp_em_my_events_profile_content_uniform() {
	em_locate_template('buddypress/my-uniform.php', true);
}


function bp_em_my_events_profile_payment() {
	global $bp, $EM_Event;
	if( !is_object($EM_Event) && !empty($_REQUEST['event_id']) ){
		$EM_Event = new EM_Event($_REQUEST['event_id']);
	}
	
	do_action( 'bp_em_my_events_profile_payment' );
	
	$template_title = 'bp_em_my_events_profile_title_payment';
	$template_content = 'bp_em_my_events_profile_content_payment';

	if( !empty($_GET['action']) ){
		switch($_GET['action']){
			case 'edit':
				$template_title = 'bp_em_my_events_profile_editor_title';
				break;
		}
	}

	add_action( 'bp_template_title', $template_title );
	add_action( 'bp_template_content', $template_content );
	
	
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}


function bp_em_my_events_profile_title_payment() {
	_e( 'Payment', 'dbem' );
}

function bp_em_my_events_profile_content_payment() {
	em_locate_template('buddypress/my-payment.php', true);
}

function bp_em_my_events_profile_achievement() {
	global $bp, $EM_Event;
	if( !is_object($EM_Event) && !empty($_REQUEST['event_id']) ){
		$EM_Event = new EM_Event($_REQUEST['event_id']);
	}
	
	do_action( 'bp_em_my_events_profile_achievement' );
	
	$template_title = 'bp_em_my_events_profile_title_achievement';
	$template_content = 'bp_em_my_events_profile_content_achievement';

	if( !empty($_GET['action']) ){
		switch($_GET['action']){
			case 'edit':
				$template_title = 'bp_em_my_events_profile_editor_title';
				break;
		}
	}

	add_action( 'bp_template_title', $template_title );
	add_action( 'bp_template_content', $template_content );
	

	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}


function bp_em_my_events_profile_title_achievement() {
	_e( 'Achievement', 'dbem' );
}

function bp_em_my_events_profile_content_achievement() {
	em_locate_template('buddypress/my-achievement.php', true);
}

function bp_em_my_events_profile_official() {
	global $bp, $EM_Event;
	if( !is_object($EM_Event) && !empty($_REQUEST['event_id']) ){
		$EM_Event = new EM_Event($_REQUEST['event_id']);
	}
	
	do_action( 'bp_em_my_events_profile_official' );
	
	$template_title = 'bp_em_my_events_profile_title_official';
	$template_content = 'bp_em_my_events_profile_content_official';

	if( !empty($_GET['action']) ){
		switch($_GET['action']){
			case 'edit':
				$template_title = 'bp_em_my_events_profile_editor_title';
				break;
		}
	}

	add_action( 'bp_template_title', $template_title );
	add_action( 'bp_template_content', $template_content );
	
	
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}


function bp_em_my_events_profile_title_official() {
	_e( 'Official', 'dbem' );
}

function bp_em_my_events_profile_content_official() {
	em_locate_template('buddypress/my-official.php', true);
}

function bp_em_my_events_profile_social_network() {
	global $bp, $EM_Event;
	if( !is_object($EM_Event) && !empty($_REQUEST['event_id']) ){
		$EM_Event = new EM_Event($_REQUEST['event_id']);
	}
	
	do_action( 'bp_em_my_events_profile_social_network' );
	
	$template_title = 'bp_em_my_events_profile_title_social_network';
	$template_content = 'bp_em_my_events_profile_content_social_network';

	if( !empty($_GET['action']) ){
		switch($_GET['action']){
			case 'edit':
				$template_title = 'bp_em_my_events_profile_editor_title';
				break;
		}
	}

	add_action( 'bp_template_title', $template_title );
	add_action( 'bp_template_content', $template_content );
	
	
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}


function bp_em_my_events_profile_title_social_network() {
	_e( 'Social Network', 'dbem' );
}

function bp_em_my_events_profile_content_social_network() {
	em_locate_template('buddypress/my-social-network.php', true);
}


function bp_em_my_events_profile_editor_title() {
	global $EM_Event;
	if( is_object($EM_Event) ){
		if($EM_Event->is_recurring()){
			_e( "Reschedule Events", 'dbem' )." '{$EM_Event->event_name}'";
		}else{
			_e ( "Edit Event", 'dbem' ) . " '" . $EM_Event->event_name . "'";
		}
	}else{
		_e( 'Add Event', 'dbem' );
	}
}
?>