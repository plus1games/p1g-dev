  
function attachAutocomplete(element, callback , url){	 
	
	$('#'+element).catcomplete({
		source: function( request, response ) {
			//alert("Getting source data");
			//var matcher = new RegExp($.trim(request.term).replace(/[-[\]{}()*+?.,\\^$|#\s]/g, ""), "i" );
			var matcher = $.trim(request.term);					
			
			//If it looks like we're looking up a postcode, we want to use the address point service
			
			url = 'http://www.plus1games.com/wp-content/plugins/events-manager/templates/forms/category_location_search.php';
				$.ajax({
					url: url,
					type: 'GET',
					data: {'search':matcher},
					dataType: 'json',
					success: function(results) {
						//Add these to the array and return using the response call
						//Parsing data here
							response(results);
							
						} 
					
				});
			
		},
		selectFirst:true, 
		//highlight: false,
		autoFocus: true,
		//minChars:2,
		delay: 700,
		minLength: 1,

		select: function( event, ui ) {			
			callback(ui.item);
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

		},
		close: function() {
			//alert("Closing...");
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	});
	//alert("Attached auto complete!");
}
 $.widget( "custom.catcomplete", $.ui.autocomplete, {		
		_renderMenu: function( ul, items ) {
			//alert('s'+items.length);
			var self = this,
			currentCategory = "";
			$.each( items, function( index, item ) {
				//limit the result to 10
				//WHY DO THIS????
				//if (index < 10) {
					if ( item.category != currentCategory ) {
						ul.append( "<li class='ui-autocomplete-category'><strong>" + item.category + "</strong></li>" );
						currentCategory = item.category;
					}
                        
					if(item.source != 'noresult'){
                            
						item.imgsrc = baseUrl + 'img/arrow.png';
						if( item.street_number != '' || item.source == 'ub-api'){
							item.imgsrc = baseUrl + 'img/empty.png';				
						}
						var img = $("<img style='float:right;'>").attr({
							"src":item.imgsrc, 
							"alt":item.label
							});

						item.label= $.trim(item.label);
						if (item.label.length > 80) {
							item.label= item.label.substring(0,80) +" .. ";
						}
								
						var pre = item.element;
						var inputblock = "";
						if(item.source == 'google-geo-api'){
							if( item.street_number != '' && typeof(item.street_number)!='undefined' ){
								inputblock = inputblock+'<input type="text" class="li_street_input" id="'+pre+'street_'+item.id+'" size="5"  value="'+item.street_number+'"> ';				
							} else {
								inputblock = inputblock+'<input type="hidden" id="'+pre+'lat_'+item .id+'" value="'+item.latitude+'"><input type="hidden" id="'+pre+'lng_'+item .id+'" value="'+item.longitude+'"><input type="text" class="li_street_input" id="'+pre+'street_'+item .id+'" size="5"  style="border:1px orange solid;"><input type="hidden" id="'+pre+'street_name_'+item .id+'" value="'+item.route+'">';	
							}
						} else {
							if( item.street_number != '' && typeof(item.street_number)!= 'undefined' ){
								inputblock = inputblock+'<input type="hidden" class="li_street_input" id="'+pre+'street_'+item.id+'" size="5"  value="'+item.street_number+'"> ';				
							} else {
									
							}
						}

						var a = $("<a></a>").css({
							"text-overflow":"ellipsis",
							"white-space":"nowrap",
							"overflow":"hidden"
						}).append(inputblock).append(img).append(item.label);
						var li = $("<li></li>").data("item.autocomplete",item).append(a);
                               
						ul.append( li );
					}else{
                            
						var a = $("<a></a>").append(item.label);
						var li = $("<li></li>").data("item.autocomplete",item).append(a);
						ul.append( li );
						//$( '#originPOI' ).removeClass( "ui-autocomplete-loading" );
						//$( '#destinationPOI' ).removeClass( "ui-autocomplete-loading" );
					}
					//self._renderItem( ul, item );
				//}
			});
		}
	});




		 

