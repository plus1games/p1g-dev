<?php
function em_admin_parklogos(){
    global $post, $wpdb,$EM_Categories;
	add_option('city-matrix-time-interval');
	add_option('city-matrix-mail-address');
	echo "<h2>Upload Logo(s) for Cities:</h2>";
	$sql="SELECT DISTINCT state from zips ORDER BY state ASC ";
	//$sql="SELECT DISTINCT location_state from wp_em_locations WHERE location_state !='' ORDER BY location_state ASC ";
	$result=$wpdb->get_results($sql);
	/*$sqlcity ="SELECT DISTINCT location_town FROM wp_em_locations WHERE location_town !='' ORDER BY location_town ASC ";*/
	$sqlcity="SELECT DISTINCT `location_state` ,`location_town` FROM wp_em_locations WHERE `location_state` !='' ORDER BY `location_town` ASC"; 
	$resultscity = $wpdb->get_results($sqlcity);

	$sqlstate="SELECT DISTINCT `location_state` FROM wp_em_locations WHERE `location_state` !='' ORDER BY `location_state` ASC"; 
	$resultsstate = $wpdb->get_results($sqlstate);
	echo 'State: <select name="dropstate" id="dropstate">';
	foreach($result as $k => $v){       
	   echo '<option value="'.$v->state.'">'.$v->state.'</option>';	  
	}
	echo '</select>   City: <select name="dropcity" id="dropcity"></select>';?>
	<a href="javascript:void(0);" class="image_upload_button">Upload Image</a>	
	<br/><br/><br/>
	<h2>Email Address where reports are send:</h2>
	Email Address:<input type="text"  value="<?php echo get_option( 'city-matrix-mail-address'); ?>" name="citymat-mail-address" id="citymat-mail-address"/>	<span>Multiple addresses should be comma separated.</span><br/><br/>
	 <button  rel="error" class="wpmu-button"id="btn-save-citymat">Save</button><span class="indicatorsave" id="indicator-save"> </span>
	<h2>HTML reports for Cities:</h2>	
			<div id="citymat">
			   <p>			   
					<!--Time Interval:--><select style ="display:none" name="citymat-time-interval" id="citymat-time-interval">
					<option value="daily" <?php if(get_option( 'city-matrix-time-interval')=='daily') echo "selected='selected'";?>>Daily</option>
					<option value="weekly" <?php if(get_option( 'city-matrix-time-interval')=='weekly') echo "selected='selected'";?>>Weekly</option>
					<option value="monthly" <?php if(get_option( 'city-matrix-time-interval')=='monthly') echo "selected='selected'";?>>Monthly</option>
					</select>
					<!-- <br/><br/> -->
					Select City:<select name="citymat_city" id="citymat_city">
					<option value=""selected="selected">select city</option>
					 <?php foreach($resultscity as $v){
					 
						echo '<option value="'.$v->location_town.'||'.$v->location_state.'">'.$v->location_town.' ('.$v->location_state.')</option>';
	                 }?></select><br/><br/>
					 Select Park Type:<select name="citymat_parktype" id="citymat_parktype">
					 <option value=""selected="selected">All</option>
					 <option value="public">Public</option>
					 <option value="private">Private</option>
					 <option value="semiprivate">Semi-Private</option>
					 <option value="military">Military</option>
					 <option value="resort">Resort</option></select><br/><br/>
					&nbsp;&nbsp;<button  rel="citymat" class="wpmu-button"id="btn-mail-citymat">Run Report</button><span class="indicator" id="indicator-citymat"> </span>
					
				</p>
			</div>	

			<h2>HTML reports for States:</h2>	
			<div id="statemat">
			   <p>			   
					Select State:<select name="statemat_state" id="statemat_state">
					<option value=""selected="selected">select state</option>
					 <?php foreach($resultsstate as $s){
					 
						echo '<option value="'.$s->location_state.'">'.$s->location_state.'</option>';
	                 }?></select><br/><br/>
					 <?php
					 $categories = EM_Categories::get();
						?>
					 Select Game Category:<select name="game_category" id="game_category">
					<option value=""selected="selected">select game category</option>
					 <?php foreach($categories as $cv){
					 
						echo '<option value="'.$cv->id.'">'.$cv->name.'</option>';
	                 }?></select><br/><br/>
					 Select Park Type:<select name="citymat_parktype" id="citymat_parktype">
					 <option value=""selected="selected">All</option>
					 <option value="public">Public</option>
					 <option value="private">Private</option>
					 <option value="semiprivate">Semi-Private</option>
					 <option value="military">Military</option>
					 <option value="resort">Resort</option></select><br/><br/>
					&nbsp;&nbsp;<button  rel="statemat" class="wpmu-button"id="btn-mail-statemat">Run Report For State</button><span class="indicatorstate" id="indicator-statemat"> </span>
					
				</p>
			</div>	
	<style>
		span{
		   color:#999999;font-size:11px;font-style: italic;
		}
		input[type='text']{
           width:300px;
		}
		span.indicator{
		   color:green;
		}
		span.indicatorstate{
		   color:green;
		}
		span.indicatorsave{
		   color:green;
		}
			
	</style>		
	<script type="text/javascript"> 

	jQuery(document).ready(function(){
		jQuery('#dropstate').change(function(){
             var currentstate = jQuery(this).val();
             jQuery.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/admin/parklogs-ajax.php',
			   data   :'currentstate='+currentstate+'&action=dropdown' ,
			   success:function(x){
					jQuery('#dropcity').html(x);
			   }
			 });
	    }).change();
		jQuery('.image_upload_button').click(function() {
            tb_show('', 'media-upload.php?TB_iframe=true');
            window.send_to_editor = function(html) {				
                var imgurl = jQuery(html).attr('href');
				var state= jQuery('#dropstate').val();
				var city= jQuery('#dropcity').val();
                tb_remove();
				jQuery.ajax({
			    url    :'<?php echo plugins_url();?>/events-manager/admin/parklogs-ajax.php',
			    data   :'imgurl='+imgurl+'&state='+state+'&city='+city+'&action=upload' ,
			    success:function(x){				
			   }
			 });

            };
        return false;
        });
		jQuery("#btn-mail-citymat").click(function(){
			 var subtype        = jQuery(this).attr('rel');
			 var address        = jQuery('#citymat-mail-address').val();
			 var time_interval  = jQuery('#citymat-time-interval').val();
			 var city           = jQuery('#citymat_city').val();
			 var park_type      = jQuery('#citymat_parktype').val();
			 jQuery('#indicator-citymat').text('running...');
			 jQuery.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/admin/parklogs-ajax.php',
			   data:'action=htmlreport&address='+address+'&selcity='+city+'&selparktype='+park_type+'&subtype='+subtype ,
			   success:function(x){
				   if(x=='f')
					   jQuery('#indicator-citymat').text('No Report Generated:City is required.');
				   else
						jQuery('#indicator-citymat').text('Report Generated');
			   }
			 });
		});
		jQuery("#btn-mail-statemat").click(function(){
			 var subtype        = jQuery(this).attr('rel');
		     var address        = jQuery('#citymat-mail-address').val();
			 var time_interval  = jQuery('#citymat-time-interval').val();
			 var state           = jQuery('#statemat_state').val();
			 var category           = jQuery('#game_category').val();
			 var park_type      = jQuery('#citymat_parktype').val();
			 jQuery('#indicator-statemat').text('running...');
			 jQuery.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/admin/parklogs-ajax.php',
			   data:'action=htmlreport&address='+address+'&selstate='+state+'&selparktype='+park_type+'&subtype='+subtype+'&category='+category ,
			   success:function(x){
				   if(x=='f')
					   jQuery('#indicator-statemat').text('No Report Generated:State is required.');
				   else
						jQuery('#indicator-statemat').text('Report Generated');
			   }
			 });
		});
		jQuery("#btn-save-citymat").click(function(){
		    		 
			 var address       = jQuery('#citymat-mail-address').val();
			 var time_interval = jQuery('#citymat-time-interval').val();
			 
			 jQuery('#indicator-save').text('saving...');
			 jQuery.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/admin/parklogs-ajax.php',
			   data   :'action=savedata&address='+address+'&time_interval='+time_interval,
			   success:function(x){
					jQuery('#indicator-save').text('settings saved');
			   }
			 });
		});

		
	});
	</script>
	<?php }
	
?>
