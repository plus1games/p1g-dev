<?php
header('Content-type: application/json');
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;
if(isset($_REQUEST['name']) || $_REQUEST['name']!='') 
 {
	$args = array (
		'post_type'              => 'listing',
		'post_status'            => 'publish',
		's'                      => $_REQUEST['name'],
		'posts_per_page'		 => '-1',
		'tax_query' => array(array(
										'taxonomy' => 'listingcategory', 
										'field' => 'id',
										'terms' => 345, 
										'include_children' => true,
								)),
	);

	$query = new WP_Query( $args );
	$parks =array();
	$parks['result']= 'success';
	$parks['total_parks']=0;
	foreach($query as $key=>$val){
		if($key == 'posts'){
			$parks['parks']=array();
			$i=0;
			foreach($val as $k=>$v){
				
				$v = get_object_vars($v);
				if (stripos($v['post_title'],$_REQUEST['name']) !== false) {
					$parks['total_parks']= $i+1;
					$parks['parks'][$v['ID']]=$v;
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $v['ID'] ) );
					$parks['parks'][$v['ID']]['featured_image']=$image[0];
					$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
					$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
					$resultimages=$wpdb->get_results($sqlimage);
					$parks['parks'][$v['ID']]['images']=array();
					foreach($resultimages as $img){
						if($img->ID !=$featuredimg_id){
							$parks['parks'][$v['ID']]['images'][]=$img->guid;
						}
					}
					$parks['parks'][$v['ID']]['meta']=get_post_meta($v['ID']);
					$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
					foreach($terminfo as $t){
						$t = get_object_vars($t);
						$parks['parks'][$v['ID']]['term'][]=$t;
					}
					$i++;
				}
			}
		}
	}
	if(empty($parks['parks'])){
		$parks['parks']=array();
		$parks['parks']='No Park Found';
	}
echo json_encode($parks);
}
?>