<?php 
/**
 * Display function for the reporting page. 
 */
function em_admin_reporting_page(){
	/*
	global $post, $wpdb, $EM_Event,$EM_Location;
    $categories = EM_Categories::get( apply_filters('em_content_categories_args', $args) );
	
	$sqlstate="select DISTINCT location_state from ".$wpdb->prefix."em_locations where location_state != ''";
    $resultstate = $wpdb->get_results($sqlstate);

	$sqltown="select DISTINCT location_town from ".$wpdb->prefix."em_locations where location_town != ''";
    $resulttown = $wpdb->get_results($sqltown);
    
	?>	
	<script>
	jQuery(function($){
        $('.inside').hide();
		$('.hndle, .handlediv').click(function(){
			$(this).parents('.postbox').find('.inside').toggle();
		});

		$("#usersubmit").click(function(){
			$('#ucount').text('');
			var scope = $('#userscope').val();		 
			$.ajax({
				url		:'<?php echo plugins_url();?>/events-manager/admin/reporting-ajax.php',
				data	:'scope='+scope+'&raction=user',
				success :function(x){
					$('#ucount').text(x);
				}
			 });
		}).click();

		$("#locationsubmit").click(function(){
			 $('#lcount').text('');
			 var scope		= $('#scope').val();
			 var scopestate = $('#location_state').val();
			 var scopecity  = $('#location_town').val();
			 var scopegame  = $('#location_game').val();
			 $.ajax({
			   url:'<?php echo plugins_url();?>/events-manager/admin/reporting-ajax.php',
			   data:'scope='+scope+'&state='+scopestate+'&city='+scopecity+'&game='+scopegame+'&raction=location',
			   success:function(x){
					$('#lcount').text(x);
			   }
			 });
		}).click();

		$("#searchsubmit").click(function(){
			 $('#scount').text('');
			 var scope     = $('#searchscope').val();
			 var searchzip = $('#searchzip').val();			 
			 $.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/admin/reporting-ajax.php',
			   data   :'scope='+scope+'&searchzip='+searchzip+'&raction=search',
			   success:function(x){
					$('#scount').text(x);
			   }
			 });
		}).click();

	})</script>

	<div class="wrap">
		<div id="icon-events" class="icon32"><br /></div>
		<h2><?php _e('Events Manager Reporting','dbem'); ?></h2>
		<div class="postbox" ><div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle" style="padding:8px;"><span>User general information</span></h3><div class="inside">
		<h4>Number of Users Added:</h4>
		<div class="tablenav top">		
		<div class="alignleft actions">		
		<select name="userscope"id="userscope">
		     <option value=""selected="selected">select day</option>
			 <option value="today">Today</option>
             <option value="week">Within a Week</option>
			 <option value="month">Within a Month</option>
		</select>
	    <input type="submit" value="Filter" class="button" id="usersubmit" name=""></div>

		<div class="wpmu-button"id="ucount"> </div>
		<br class="clear">
		</div><br/></div></div>

		
		<div class="postbox" ><div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle" style="padding:8px;"><span>Location general information</span></h3><div class="inside">
		<h4>Number of Locations Added:</h4>
		<div class="tablenav top">		
		<div class="alignleft actions">
		<select name="location_state"id="location_state">
             <option value=""selected="selected">select state</option>
			 <?php foreach($resultstate as $v){
				echo '<option value="'.$v->location_state.'">'.$v->location_state.'</option>';
	         }  ?>
		</select>
		<select name="location_town"id="location_town">
             <option value=""selected="selected">select city</option>
			 <?php foreach($resulttown as $v){
			     echo '<option value="'.$v->location_town.'">'.$v->location_town.'</option>';
	         } ?>
		</select>
		<select name="location_game"id="location_game">
             <option value=""selected="selected">select game</option>
			 <?php foreach($categories as $v){
			     echo '<option value="'.$v->id.'">'.$v->name.'</option>';
	         } ?>
		</select>
		<select name="scope"id="scope">
		     <option value=""selected="selected">select day</option>
			 <option value="today">Today</option>
             <option value="week">Within a Week</option>
			 <option value="month">Within a Month</option>
		</select>
	    <input type="submit" value="Filter" class="button" id="locationsubmit" name=""></div>

		<div class="wpmu-button"id="lcount"> </div>
		<br class="clear">
		</div><br/></div></div>

		
		<div class="postbox" ><div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle" style="padding:8px;"><span>Search general information</span></h3><div class="inside">
		<h4>Number of times user searched in zipcode:</h4>
		<div class="tablenav top">		
		<div class="alignleft actions">
		<select name="searchzip"id="searchzip">
		     <option value="event"selected="selected">Events</option>
			 <option value="location">Locations</option>             
		</select>
		<select name="searchscope"id="searchscope">
		     <option value=""selected="selected">select day</option>
			 <option value="today">Today</option>
             <option value="week">Within a Week</option>
			 <option value="month">Within a Month</option>
		</select>
	    <input type="submit" value="Filter" class="button" id="searchsubmit" name=""></div>

		<div class="wpmu-button"id="scount"> </div>
		<br class="clear">
		</div><br/></div></div>
	 </div>
<?php  */}

function reporting_data(){	/*
    global $wpdb;
	$sql="select count(*) as total from ".$wpdb->prefix."users WHERE user_registered >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND user_registered  < CURDATE() ";
	$result = $wpdb->get_results($sql);
	$html='<h2>Weekly Report for <a href="'.site_url().'"target="_blank">Dev.Plus1games</a> </h2>';
	$html .="Total users registered within this week:<strong> &nbsp;".$result[0]->total."</strong> <br/>";

	$sqlsearchevent="select count(*) as totalsearchevent from zipsearch_record WHERE search_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND search_date < CURDATE() AND type='event'";
	$resultsearchevent = $wpdb->get_results($sqlsearchevent);
	$html .="Number of time users searched for events with ZIP within this week:<strong> &nbsp;".$resultsearchevent [0]->totalsearchevent."</strong><br/>";

	$sqlsearchlocation="select count(*) as totalsearchlocation from zipsearch_record WHERE search_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND search_date < CURDATE() AND type='location'";
	$resultsearchlocation = $wpdb->get_results($sqlsearchlocation);
	$html .="Number of time users searched for locations with ZIP  within this week:<strong> &nbsp;".$resultsearchlocation[0]->totalsearchlocation."</strong><br/>";

	$sqlloc="select  a.location_state,a.location_name,a.location_slug from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE b.post_date_gmt >=DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND b.post_date_gmt < CURDATE()";
    $result1 = $wpdb->get_results($sqlloc);
    $totalloc=count($result1);
	$html .="Total locations added within this week:<strong> &nbsp;".$totalloc."</strong><br/>";
	if(!empty($result1))
	{
		foreach($result1 as $v){
         $locations[$v->location_state][$v->location_slug]=$v->location_name;
	 }	
	 foreach($locations as $key=>$val){
          $html .="<p>Locations added within this week for state: &nbsp;<strong>".$key."</strong><br/>";
          foreach($val as $k1=>$v1){
                $html .="<strong>".$v1.":</strong> &nbsp;<a href='".site_url()."/locations/".$k1."' target='_blank'>".site_url()."/locations/".$k1."</a><br/>";

	      }
		  $html .="<br/></p>";
	 }
  }
  $sqlempty="select location_id,location_state,location_name,location_slug from wp_em_locations WHERE location_status=1";
  $resultempty = $wpdb->get_results($sqlempty);
  $emptycount=0;
  foreach($resultempty as $v){
	 $sqlfield="select field_id  ,field_name ,field_desc  from wp_em_location_fields where location_id='".$v->location_id."' AND field_location_status=1";
	 $resultfield = $wpdb->get_results($sqlfield);
	 $fieldcount=count($resultfield );
	 if(!empty($fieldcount)){
		if($fieldcount >= 2){
			if($resultfield[0]->field_name =='' && $resultfield[1]->field_name =='' && $resultfield[0]->field_desc =='' && $resultfield[1]->field_desc ==''){
				$emptylocations[$v->location_state][$v->location_slug]=$v->location_name;
				$emptycount++;
			}
		}else{
			if($resultfield[0]->field_name =='' && $resultfield[0]->field_desc =='' ){
				$emptylocations[$v->location_state][$v->location_slug]=$v->location_name;
				$emptycount++;
			}
		}

	 }else{
		  $emptylocations[$v->location_state][$v->location_slug]=$v->location_name;
		  $emptycount++;
	 }
	}
	if($emptycount >0){
		 $html .="Total number of incomplete locations are :<strong>".$emptycount."</strong>";
		foreach($emptylocations as $key=>$val){
		  $html .="<p>Incomplete locations for state: &nbsp;<strong>".$key."<br/></strong>";
		  foreach($val as $k1=>$v1){
				$html .="<strong>".$v1.":</strong> &nbsp;<a target='_blank' href='".site_url()."/locations/".$k1."'>".site_url()."/locations/".$k1."</a><br/>";

		  }
		  $html .="<br/></p>";
	 }
	}
	$sqlemptyphone="select location_id,location_state,location_name,location_slug from wp_em_locations WHERE location_status=1 AND location_website='' AND location_phone=''";
	$resultemptyphone = $wpdb->get_results($sqlemptyphone);
	$emptycountphone=count($resultemptyphone) ;
	if($emptycountphone > 0){
		foreach($resultemptyphone as $v){
			   $emptylocationsphone[$v->location_state][$v->location_slug]=$v->location_name;
		}
		$html .="Total number of locations having no website or phone are <strong>".$emptycountphone.'</strong>';
        foreach($emptylocationsphone as $key=>$val){
			$html .="<p>Locations having no website or phone for state: &nbsp;<strong>".$key."</strong><br/>";
			foreach($val as $k1=>$v1){
                $html .="<strong>".$v1.":</strong> &nbsp;".site_url()."/locations/".$k1."<br/>";

	        }
		    $html .="<br/></p>";
	    }
    }

	$sqllocationuser="SELECT DISTINCT post_author from wp_posts WHERE post_type	='location' AND (post_status='pending' OR post_status='publish') AND post_date_gmt >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND post_date_gmt  < CURDATE()";
	$resultlocationuser = $wpdb->get_results($sqllocationuser);
	if(!empty($resultlocationuser)){
			foreach($resultlocationuser as $v){
					$sqluserinfo="SELECT user_login from wp_users WHERE ID='{$v->post_author}'";
					$resultuserinfo = $wpdb->get_results($sqluserinfo);
					$resultuserinfo[0]->user_login;
					$sqluserlocno="SELECT ID  from wp_posts WHERE post_type ='location' AND (post_status='pending' OR post_status='publish') AND post_date_gmt >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND post_date_gmt  < CURDATE() AND post_author ='{$v->post_author}'";
					$resultuserlocno = $wpdb->get_results($sqluserlocno);
					$countuserlocno =count($resultuserlocno);
					$html .="<br/><p> Number of locations created within this week by user <strong>".$resultuserinfo[0]->user_login.": ".$countuserlocno." </strong>" ;
		   }
   }
	
	 return  $html;*/
 }
  function em_admin_importparks_page(){?>
	<h2> Upload CSV File For Import Parks</h2>
	<form action="" method="post" enctype="multipart/form-data">
		<input type="file" name="fileupload"/>
		<input type="submit" value="Upload" class="wpmu-button" name="Upload"/>
	</form>

 <?php
	 if(isset($_POST['Upload'])){	
		$abspath=explode('wp-admin',$_SERVER['SCRIPT_FILENAME'] );
		$path=$abspath[0].'wp-content/excel/Import/';
		$cpath=$abspath[0].'wp-content/excel/';
		$file=$abspath[0].'wp-content/excel/Import/modefirst.csv';
	
		if(file_exists($file)){
			unlink($file);
			}
			if($_FILES["fileupload"]["type"] == 'text/comma-separated-values'){
			 move_uploaded_file($_FILES["fileupload"]["tmp_name"],
						$path. $_FILES["fileupload"]["name"]);
			 echo "<br/><strong>Uploaded File:</strong>&nbsp".$_FILES["fileupload"]["name"];
			 copy($path. $_FILES["fileupload"]["name"],$cpath. $_FILES["fileupload"]["name"]);
			 rename($path. $_FILES["fileupload"]["name"], $path."modefirst.csv");
			 }
		 
			else{
				echo "Upload only CSV Files";
				}
	 }
	 ?>
		<br/><br/><br/>
		<h2>Import Parks</h2>
		<button  rel="import_location" class="wpmu-button"id="locationimport">Import</button><span class="indicator11" id="indicator-citymat11"> </span>
		<style>
			span.indicator11{
			   color:#999999;font-size:11px;font-style: italic;
			}
			span.indicator11{
			   color:green;
			}
			input[type="submit"] 
			{
				float: none;
			}
				
	    </style>


		<script type="text/javascript"> 
		jQuery(document).ready(function($){
			$("#locationimport").click(function(){
			$('#indicator-citymat11').text('running...');
			$.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/templates/scripts/import_locations.php',
			    data   :'action=savedata1',
				success:function(x){
				   if(x=='file'){
				   $('#indicator-citymat11').text('File doesnot exist.');
				   }
				   else if(x=='notstandard'){
				   $('#indicator-citymat11').text('File doesnot has Standard Format');
				   }
					else{
						$('#indicator-citymat11').text('Location Imported');
					}
			   }
			 });
		});
		});
		</script>

<?php	
}
?>