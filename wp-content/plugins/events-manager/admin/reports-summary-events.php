<?php 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $wpdb; 
$insertarr =array('server_addr'=>$_SERVER['SERVER_ADDR'], 'remote_addr'=>$_SERVER['REMOTE_ADDR'], 'user_agent'=>$_SERVER['HTTP_USER_AGENT'],
'server_time'=>$_SERVER['REQUEST_TIME'] );
//$wpdb->insert('wp_cron_chart',$insertarr);
			

        $sqlevents="SELECT a.location_state, a.location_name, c.event_name, c.event_id, c.location_id FROM wp_em_locations AS a JOIN wp_posts AS b ON a.post_id = b.ID JOIN wp_em_events AS c ON a.location_id = c.location_id WHERE c.event_status =1";
		$resultevent = $wpdb->get_results($sqlevents);
		$tevent1=count($resultevent);			
		if(!empty($resultevent))
			{
			foreach($resultevent as $v){
				 $events[$v->location_state][$v->event_id]=$v->event_name;
				}	
				 ksort($events);				 
				 foreach($events as $key=>$val){
					  $eventcount= count($val);
					  if(strlen($key) < 3){
						  $listevent_keys[]=$key;
						  $listevent_values[]=$eventcount;
						  $sqleventsweek="select  count(*) as total from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID JOIN wp_em_events AS c ON a.location_id = c.location_id WHERE (c.event_start_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND c.event_start_date < CURDATE() OR (c.event_end_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND c.event_end_date < CURDATE())) AND c.event_status =1 AND a.location_state='{$key}'";
			              $tevent2 = $wpdb->get_results($sqleventsweek);
						  $listevent2[$key]=$tevent2[0]->total;
					  }
				 }
		  }
		 $contentevent ='[easychart
		 type="horizbar" height="200" title="Events:Total vs. New" groupnames="Total, Current Week"    groupcolors="005599,229944" valuenames="'.implode(',',$listevent_keys).'" group1values="'.implode(',',$listevent_values).'" group2values="'.implode(',',$listevent2).'"]';
		 $contentevent = trim(preg_replace('/\s+/', ' ', $contentevent));         
		//echo do_shortcode( $contentevent ) ;

		$userevent="SELECT a.display_name,COUNT(b.event_owner)as counter FROM wp_users AS a LEFT JOIN wp_em_events AS b ON a.ID=b.event_owner LEFT JOIN wp_posts AS c ON b.post_id=c.ID WHERE a.ID!=1 AND (c.post_type='event' OR c.post_type='event-recurring') GROUP BY a.display_name  ORDER BY counter DESC LIMIT 0,5";

		$resultuserevent = $wpdb->get_results($userevent);
			foreach($resultuserevent as $userevent){
			$euname[]=$userevent->display_name;
		    $event_count[]=$userevent->counter;
			}
			$usereventchart='[easychart type="vertbar" height="300" width="350" title=" Top Users that are adding Events" groupnames="Total Events" valuenames="'.implode(',',$euname).'" group1values="'.implode(',',$event_count).'"]';
			//echo do_shortcode( $usereventchart ) ;

		 $postdate=date('Y-m-d H:i:s',time());
		 $title='Weekly Events Stats';
		 $post_arr=array(
				   'post_author'=>    1,			   
				   'post_title'=>	  $title,
				   'post_status'=>	  'publish',
				   'comment_status'=> 'closed',	
				   'post_type'=>	  'post',
				   'post_content'=>   'Here are weekly statistics for Events: <br/> '.$contentevent.'<br/><br/>'.$usereventchart
			  );
			 $chkduppost = "SELECT ID from wp_posts where post_date >=DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND post_type= 'post' AND post_title ='$title'";
			 $oldres = $wpdb->get_results($chkduppost);
			//if(empty($oldres)){
		      //  echo $post_id = wp_insert_post( $post_arr );
			// }
			
         
?>