msgid ""
msgstr ""
"Project-Id-Version: Multiple Rating v1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-06 17:53+0530\n"
"PO-Revision-Date: 2016-07-06 17:58+0530\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 1.8.8\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Textdomain-Support: yes\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SearchPath-0: .\n"

# @ multiple_rating
#: install.php:29
msgid "Multi Rating"
msgstr ""

#: multiple_rating.php:21
msgid "Option name (e.g. Atmosphere, Quality of service, etc)"
msgstr ""

#: multiple_rating.php:100
msgid "Your Membership has been expired. You can Renew it from "
msgstr ""

#: multiple_rating.php:100
msgid "Here"
msgstr ""

# @ comment-rating
#: multiple_rating.php:156
msgid "Comment Rating"
msgstr ""

# @ multiple_rating
#: multiple_rating.php:401 multiple_rating.php:1548
msgid "Rating"
msgstr ""

# @ multiple_rating
#: multiple_rating.php:436 multiple_rating.php:692 multiple_rating.php:1584
msgid "Show individual rating"
msgstr ""

# @ multiple_rating
#: multiple_rating.php:659 multiple_rating.php:844 multiple_rating.php:1306
msgid "reviews"
msgstr ""

# @ multiple_rating
#: multiple_rating.php:664 multiple_rating.php:840 multiple_rating.php:1311
msgid "review"
msgstr ""

#: multiple_rating.php:678 multiple_rating.php:1319
msgid "&nbsp;out of"
msgstr ""

#: templatic_login.php:4
msgid "Templatic Rating Update"
msgstr ""

#: templatic_login.php:36
msgid ""
"Invalid UserName or password. are you using templatic member username and "
"password?"
msgstr ""

#: templatic_login.php:60
msgid ""
"We don't find Templatic - MultiRating Plugin active in your templatic "
"account, you will not be able to update without a license"
msgstr ""

#: templatic_login.php:65
msgid ""
"Invalid UserName or password. Please enter templatic member's username and "
"password."
msgstr ""

#: templatic_login.php:85
msgid ""
"Templatic Login , enter your templatic credentials to take the updates of "
"Templatic-MultiRating Plugin."
msgstr ""

#: templatic_login.php:89
msgid "User Name"
msgstr ""

#: templatic_login.php:93
msgid "Password"
msgstr ""

#: templatic_login.php:98
msgid "Cancel"
msgstr ""

#: templatic_login.php:106
msgid "Templatic-MultipleRating Plugin"
msgstr ""

#: templatic_login.php:106
msgid "Update Now"
msgstr ""

# @ multiple_rating
#: templatic_rating_setting.php:120 templatic_rating_setting.php:133
#: templatic_rating_setting.php:146 templatic_rating_setting.php:217
#: templatic_rating_setting.php:229 templatic_rating_setting.php:241
#: templatic_rating_setting.php:333
msgid "Remove"
msgstr ""

# @ multiple_rating
#: templatic_rating_setting.php:156 templatic_rating_setting.php:251
#: templatic_rating_setting.php:343
msgid "+ Add New"
msgstr ""

# @ multiple_rating
#: templatic_rating_setting.php:271
msgid "Show average rating"
msgstr ""

# @ multiple_rating
#: templatic_rating_setting.php:273
msgid " On hover"
msgstr ""

# @ multiple_rating
#: templatic_rating_setting.php:274
msgid " On click"
msgstr ""

# @ default
#: templatic_rating_setting.php:274
msgid ""
"Selecting \"On hover\" will show the average ratings when a user hovers over "
"the rating stars. Selecting \"On click\" will show the average ratings when "
"the button is pressed. "
msgstr ""

# @ multiple_rating
#: templatic_rating_setting.php:320
msgid "Global rating option for"
msgstr ""

#: templatic_rating_setting.php:323
msgid ""
"Options added here will be automatically assigned to all categories within "
"this post type."
msgstr ""

#: templatic_rating_setting.php:363
msgid "Category-specific rating options"
msgstr ""

#: templatic_rating_setting.php:364
msgid ""
"Use this section to define unique rating options for specific categories."
msgstr ""

#: templatic_rating_system_main.php:11
msgid "Multi Rating Settings"
msgstr ""

#: wp-updates-plugin.php:41
msgid "There is a new version of Tevolution-rating plugin available "
msgstr ""

#: wp-updates-plugin.php:47
msgid "Templatic Multiple Rating Update"
msgstr ""

#: wp-updates-plugin.php:47
msgid "update now"
msgstr ""

#: wp-updates-plugin.php:89
msgid "An Unexpected HTTP Error occurred during the API request."
msgstr ""

#: wp-updates-plugin.php:89
msgid "Try again"
msgstr ""

#: wp-updates-plugin.php:94
msgid "An unknown error occurred"
msgstr ""
