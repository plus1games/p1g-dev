<?php
/*
 * if fieldmonetization plugin activate then remove detail page custom fields and add frontend editor related custom fields action
 */
add_action('wp_head','frontend_fieldsMonetization_custom_fields');

function frontend_fieldsMonetization_custom_fields(){
	
	if(is_single() && isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			global $templatic_settings;
			$templatic_settings=get_option('templatic_settings');			
			if($templatic_settings['templatic-category_custom_fields']=='Yes' || is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){
				
				remove_action('directory_inside_container_breadcrumb','directory_detail_custom_field');
				add_action('directory_inside_container_breadcrumb','frontend_editor_detail_custom_field');
				add_action('property_inside_container_breadcrumb','frontend_editor_detail_custom_field');
				
				add_action('directory_inside_container_breadcrumb','tmpl_frontend_editor_detail_custom_field_without_headingtype');
				add_action('property_inside_container_breadcrumb','tmpl_frontend_editor_detail_custom_field_without_headingtype');
			}
	}
}

/*
 * Fetch detail page custom fields as per heading type wise
 */
function frontend_editor_detail_custom_field(){
	$custom_post_type = tevolution_get_post_type();
	
	//if(is_single() && (in_array(get_post_type(),$custom_post_type)  && get_post_type()!='event')){
	if(is_single() && (in_array(get_post_type(),$custom_post_type))){	
		global $wpdb,$post,$htmlvar_name,$pos_title,$templatic_settings;
		
		$cus_post_type = get_post_type();
		if(function_exists('tmpl_fetch_heading_post_type')){
		
			$heading_type = tmpl_fetch_heading_post_type($cus_post_type);
		}
		
		if(count($heading_type) > 0)
		{
			foreach($heading_type as $key=>$heading)
			{	
				$htmlvar_name[$key] = get_frontend_editor_single_customfields(get_post_type(),$heading,$key);//custom fields for custom post type..
			}
		}
		return $htmlvar_name;
	}	
}


/*
 *  get detail page custom field as per detail page post type wise
 */

function get_frontend_editor_single_customfields($post_type,$heading='',$heading_key=''){
		
	global $wpdb,$post,$posttitle,$templatic_settings;	
	$package_select=get_post_meta($post->ID,'package_select',true);
	$field_monetiz_custom_fields=get_post_meta($package_select,'custom_fields',true);
	if($field_monetiz_custom_fields==''){
		$field_monetiz_custom_fields=array();	
	}
	$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type,'public'   => true, '_builtin' => true ));	
	$post_categories = get_the_terms( get_the_ID() ,$taxonomies[0]);
	/* Get the custom fields set as categorywise */
	$term_id='';
	if($templatic_settings['templatic-category_custom_fields']=='Yes'){
		foreach($post_categories as $post_category){
			$term_id.=$post_category->term_id.",";		
		}
	}
	
	$cur_lang_code=(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))? ICL_LANGUAGE_CODE :'';
	remove_all_actions('posts_where');		
	$post_query = null;
	remove_action('pre_get_posts','event_manager_pre_get_posts');
	remove_action('pre_get_posts','directory_pre_get_posts',12);
	add_filter('posts_join', 'custom_field_posts_where_filter');


	$args = array( 'post_type' => 'custom_fields',
				'posts_per_page' => -1	,
				'post_status' => array('publish'),
				'meta_query' => array('relation' => 'AND',
								array(
									'key'     => 'post_type_'.$post_type.'',
									'value'   => $post_type,
									'compare' => '=',
									'type'    => 'text'
								),		
								array(
									'key'     => 'is_active',
									'value'   =>  '1',
									'compare' => '='
								),
								array(
									'key'     => 'show_on_detail',
									'value'   =>  '1',
									'compare' => '='
								),
								array(
									'key'     => 'heading_type',
									'value'   =>  array('basic_inf',$heading),
									'compare' => 'IN'
								)
							),
				'meta_key' => 'sort_order',
				'orderby' => 'meta_value',
				'order' => 'ASC'
	);
	
	/* Get the custom fields category wise if category id not equal to blank */
	if($term_id!='' && !is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){
		$args['tax_query']	= array('relation' => 'OR',
									array('taxonomy' => $taxonomies[0],'field' => 'id','terms' => explode(',',substr($term_id,0,-1)),'operator'  => 'IN'),
									array('taxonomy' => 'category','field' => 'id','terms' => 1,'operator'  => 'IN')				
								 );
	}
	
	$post_query = new WP_Query($args);	
	remove_filter('posts_join', 'custom_field_posts_where_filter');
	
	$htmlvar_name='';
	if($post_query->have_posts())
	{
		while ($post_query->have_posts()) : $post_query->the_post();		
			
			if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php') && !in_array($post->ID, $field_monetiz_custom_fields) && !empty($field_monetiz_custom_fields)){
					continue;
			}
			$ctype = get_post_meta($post->ID,'ctype',true);
			$post_name=get_post_meta($post->ID,'htmlvar_name',true);
			$style_class=get_post_meta($post->ID,'style_class',true);
			$option_title=get_post_meta($post->ID,'option_title',true);
			$option_values=get_post_meta($post->ID,'option_values',true);
			$default_value=get_post_meta($post->ID,'default_value',true);
			$htmlvar_name[$post_name] = array( 'type'=>$ctype,
									    'label'=> $post->post_title,
										'style_class'=>$style_class,
										'option_title'=>$option_title,
										'option_values'=>$option_values,
										'default'=>$default_value,
										);			
		endwhile;
		wp_reset_query();
	}
	return $htmlvar_name;
	

}

/*
 * get detail page custom field as per detail page post type wise without heading type custom fields
 */

function tmpl_frontend_editor_detail_custom_field_without_headingtype(){
		
	global $wpdb,$post,$posttitle,$tmpl_flds_varname,$templatic_settings;	
	$post_type = get_post_type();
	
	$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type,'public'   => true, '_builtin' => true ));	
	$post_categories = get_the_terms( get_the_ID() ,$taxonomies[0]);
	/* Get the custom fields set as categorywise */
	$term_id='';
	if($templatic_settings['templatic-category_custom_fields']=='Yes'){
		foreach($post_categories as $post_category){
			$term_id.=$post_category->term_id.",";		
		}
	}
	
	$package_select=get_post_meta($post->ID,'package_select',true);
	$field_monetiz_custom_fields=get_post_meta($package_select,'custom_fields',true);
	if($field_monetiz_custom_fields==''){
		$field_monetiz_custom_fields=array();	
	}
	
	$cur_lang_code=(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))? ICL_LANGUAGE_CODE :'';
	remove_all_actions('posts_where');		
	$post_query = null;
	remove_action('pre_get_posts','event_manager_pre_get_posts');
	remove_action('pre_get_posts','directory_pre_get_posts',12);
	add_filter('posts_join', 'custom_field_posts_where_filter');


	$args = array( 'post_type' => 'custom_fields',
				'posts_per_page' => -1	,
				'post_status' => array('publish'),
				'meta_query' => array('relation' => 'AND',
								array(
									'key'     => 'post_type_'.$post_type.'',
									'value'   => $post_type,
									'compare' => '=',
									'type'    => 'text'
								),		
								array(
									'key'     => 'is_active',
									'value'   =>  '1',
									'compare' => '='
								),
								array(
									'key'     => 'show_on_detail',
									'value'   =>  '1',
									'compare' => '='
								),								
							),
				'meta_key' => 'sort_order',
				'orderby' => 'meta_value',
				'order' => 'ASC'
	);
	
	
	/* Get the custom fields category wise if category id not equal to blank */
	if($term_id!='' && !is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){
		$args['tax_query']	= array('relation' => 'OR',
									array('taxonomy' => $taxonomies[0],'field' => 'id','terms' => explode(',',substr($term_id,0,-1)),'operator'  => 'IN'),
									array('taxonomy' => 'category','field' => 'id','terms' => 1,'operator'  => 'IN')				
								 );
	}
	

	$post_query = new WP_Query($args);	
	remove_filter('posts_join', 'custom_field_posts_where_filter');
	
	$htmlvar_name='';
	$tmpl_flds_varname=array();
	if($post_query->have_posts())
	{
		while ($post_query->have_posts()) : $post_query->the_post();
		
			if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php') && !in_array($post->ID, $field_monetiz_custom_fields) && !empty($field_monetiz_custom_fields)){
				continue;
			}			
		
			$ctype = get_post_meta($post->ID,'ctype',true);
			$post_name=get_post_meta($post->ID,'htmlvar_name',true);
			$style_class=get_post_meta($post->ID,'style_class',true);
			$option_title=get_post_meta($post->ID,'option_title',true);
			$option_values=get_post_meta($post->ID,'option_values',true);
			$default_value=get_post_meta($post->ID,'default_value',true);
			$tmpl_flds_varname[$post_name] = array( 'type'=>$ctype,
									    'label'=> $post->post_title,
										'style_class'=>$style_class,
										'option_title'=>$option_title,
										'option_values'=>$option_values,
										'default'=>$default_value,
										);			
		endwhile;
		wp_reset_query();
	}
	
	return $tmpl_flds_varname;
}
?>