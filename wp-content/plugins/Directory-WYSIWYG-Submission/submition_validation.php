<script type="text/javascript">
var front_submission=1;
jQuery.noConflict();
jQuery(document).ready(function(){
//<![CDATA[
<?php
global $validation_info;
$js_code = '';
//$js_code .= '//global vars ';
$js_code .= 'var submit_form = jQuery("#submit_form");'; //form Id
$jsfunction = array();
for($i=0;$i<count($validation_info);$i++) {
	$title = $validation_info[$i]['title'];
	$name = $validation_info[$i]['name'];
	$validation_name = $validation_info[$i]['name'];
	$espan = $validation_info[$i]['espan'];
	$type = $validation_info[$i]['type'];
	$search_ctype = $validation_info[$i]['search_ctype'];
	$text = __($validation_info[$i]['text'],DOMAIN);
	$validation_type = $validation_info[$i]['validation_type'];
	$is_required = $validation_info[$i]['is_require'];
	$is_required_desc = $validation_info[$i]['field_require_desc'];
	if($is_required ==''){
		$is_required = 0;
	}
	$js_code .= '
	dml = document.forms[\'submit_form\'];
	var '.$name.' = jQuery("#'.$name.'"); ';
	$js_code .= '
	var '.$espan.' = jQuery("#'.$espan.'"); 
	';
	if($type=='selectbox' || $type=='checkbox')
	{
		$msg = sprintf(__("%s",DOMAIN),$text);
	}else
	{
		$msg = sprintf(__("%s",DOMAIN),$text);
	}
	$category_can_select_validation_msg = __("You cannot select more than ",DOMAIN); /* message used for while submitting a form with category selected greater than the number of category selection for particular price package. */ 
	$category_can_select_validation_message = __(" categories with this package.",DOMAIN); /* message used for while submitting a form with category selected greater than the number of category selection for particular price package.*/ 
	if($type == 'multicheckbox' || $type=='checkbox' || $type=='radio' || $type=='post_categories' || $type=='upload')
	{
		$js_code .= '
		function validate_'.$name.'()
		{
			if("'.$type.'" != "upload")
			  {
				var chklength = jQuery("#'.$name.'").length;
			  }
			if("'.$type.'" =="multicheckbox")
			  {
				chklength = document.getElementsByName("'.$name.'[]").length;
			  }
			if("'.$name.'" == "category"){
				chklength = document.getElementsByName("'.$name.'[]").length;
			}
			if("'.$type.'" =="radio")
			  {
				if(!jQuery("input[name='.$name.']:checked").length > 0) {
					flag = 1;
				}
				else
				{
					flag = 0;
				}
			  }
			
			if("'.$type.'" =="upload")
			  {
				  var id_value = jQuery('.$name.').val();
				  var valid_extensions = /(.txt|.pdf|.doc|.xls|.xlsx|.csv|.docx|.rar|.zip|.jpg|.jpeg|.gif|.png)$/i;
				  if(valid_extensions.test(id_value))
				  {
					  
				  }
				  else
				  { ';
				    if($text !='' && $type=='upload'){
					   $umsg = $text;
					}else{
					   $umsg = __("You are uploading invalid file type. Allowed file types are",DOMAIN)." : txt, pdf, doc, xls, csv, docx, xlsx, zip, rar";
					}
				   $js_code .= 'jQuery("#'.$name.'_error").html("'.$umsg.'");
				   return false;
				  }
			  }
 			var temp	  = "";
			var i = 0;
			if("'.$type.'" =="multicheckbox" || "'.$type.'"=="checkbox")
			  {
			chk_'.$name.' = document.getElementsByName("'.$name.'[]");
			if("'.$name.'" == "category"){
				chk_'.$name.' = document.getElementsByName("'.$name.'[]");
			}			
			if(chklength == 0){
				if ((chk_'.$name.'.checked == false)) {
					flag = 1;	
				} 
			} else {
				var flag      = 0;
				for(i=0;i<chklength;i++) {
					if ((chk_'.$name.'[i].checked == false)) { ';
						$js_code .= '
						flag = 1;
					} else {
						flag = 0;
						break;
					}
				}
			}
			  }
			if(flag == 1)
			{
				if("'.$name.'" == "category"){
					document.getElementById("'.$espan.'").innerHTML = "'.$msg.'";
				}else{
					jQuery("#'.$espan.'").text("'.$msg.'");
				}
				jQuery("#'.$espan.'").addClass("message_error2");
				 return false;
			}
			else{
				if("'.$name.'" == "category"){
					chklength = document.getElementsByName("'.$name.'[]").length;
					cat_count = 0;
					for(i=0;i<chklength;i++) {
						if ((chk_'.$name.'[i].checked == true)) { ';
							$js_code .= '
							cat_count =  cat_count + 1;
						} 
					}
					if(document.getElementById("category_can_select") && document.getElementById("category_can_select").value > 0)
					{
						if(cat_count > document.getElementById("category_can_select").value && chklength > 0)
						{
							
							document.getElementById("category_error").innerHTML = "'.$category_can_select_validation_msg.' "+document.getElementById("category_can_select").value+"'.$category_can_select_validation_message.' ";
							jQuery("#'.$espan.'").addClass("message_error2");
							return false;
						}
					}
				}	
				jQuery("#'.$espan.'").text("");
				jQuery("#'.$espan.'").removeClass("message_error2");
				return true;
			}
		}
	';
	}else {
		$js_code .= '
		function validate_'.$name.'()
		{';
			if($validation_type == 'email') {
				$js_code .= '
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;				
				if(jQuery("#'.$name.'").val() == "" && '.$is_required.') {';
				if($text){
					$emsg = $text;
				}else{
					$emsg = __("Please provide your email address",DOMAIN);
				}
			
				$js_code .= $name.'.addClass("error");
					jQuery("#'.$espan.'").text("'.$emsg.'");
					jQuery("#'.$espan.'").addClass("message_error2"); 
				return false;';
				$js_code .= ' } else if(!emailReg.test(jQuery("#'.$name.'").val().replace(/\s+$/,"")) && jQuery("#'.$name.'").val()) { ';
					if($text){
						$emsg = $text;
					}else{
						$emsg = __("Please provide your email address",DOMAIN);
					}
					$js_code .= $name.'.addClass("error");
					jQuery("#'.$espan.'").text("'.$emsg.'");
					jQuery("#'.$espan.'").addClass("message_error2");
					return false;';
				$js_code .= '
				} else {
					'.$name.'.removeClass("error");
					jQuery("#'.$espan.'").text("");
					jQuery("#'.$espan.'").removeClass("message_error2");
					return true;
				}';
			} if($validation_type == 'phone_no'){
				$js_code .= '
				var phonereg = /^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}$/;
				if(jQuery("#'.$name.'").val() == "" && '.$is_required.') { ';
					$msg = $text;
					$js_code .= $name.'.addClass("error");
					jQuery("#'.$espan.'").text("'.$msg.'");
					jQuery("#'.$espan.'").addClass("message_error2");
				return false;';
				$js_code .= ' } else if(!phonereg.test(jQuery("#'.$name.'").val()) && jQuery("#'.$name.'").val()) { ';
					$msg = __("Enter Valid Phone No.",DOMAIN);
					$js_code .= $name.'.addClass("error");
					jQuery("#'.$espan.'").text("'.$msg.'");
					jQuery("#'.$espan.'").addClass("message_error2");
					return false;';
				$js_code .= '
				} else {
					'.$name.'.removeClass("error");
					jQuery("#'.$espan.'").text("");
					jQuery("#'.$espan.'").removeClass("message_error2");
					return true;
				}';
			}if($validation_type == 'digit'){
				$js_code .= '
				var digitreg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
				if(jQuery("#'.$name.'").val() == "" && '.$is_required.') { ';
					$msg = trim($text);
				$js_code .= $name.'.addClass("error");
					jQuery("#'.$espan.'").text("'.$msg.'");
					jQuery("#'.$espan.'").addClass("message_error2");
				return false;';
				$js_code .= ' } else if(!digitreg.test(jQuery("#'.$name.'").val())) { ';
					$msg = __("Values must be all numbers.",DOMAIN);
					$js_code .= $name.'.addClass("error");
					jQuery("#'.$espan.'").text("'.$msg.'");
					jQuery("#'.$espan.'").addClass("message_error2");
					return false;';
				$js_code .= '
				} else {
					'.$name.'.removeClass("error");
					jQuery("#'.$espan.'").text("");
					jQuery("#'.$espan.'").removeClass("message_error2");
					return true;
				}';
			}
			if($type == 'texteditor'){
				$js_code .= 'if(jQuery("#'.$name.'").css("display") == "none")
				{
				if(tinyMCE.get("'.$name.'").getContent().replace(/<[^>]+>/g, "") == "") { ';
					$msg = $text;
				$js_code .= $name.'.addClass("error");
					jQuery("#'.$espan.'").text("'.$msg.'");
					jQuery("#'.$espan.'").addClass("message_error2");
				return false;';
				$js_code .= ' }  else {
					'.$name.'.removeClass("error");
					jQuery("#'.$espan.'").text("");
					jQuery("#'.$espan.'").removeClass("message_error2");
					return true;
				}
				}else
				{
					if(jQuery("#'.$name.'").val() == "")
					{
						jQuery("#'.$espan.'").text("'.$msg.'");
						jQuery("#'.$espan.'").addClass("message_error2");
						return false;
					}
					else
					{
						jQuery("#'.$espan.'").text("");
						jQuery("#'.$espan.'").removeClass("message_error2");
						return true;
					}
				}';
			}
			if($type == 'image_uploader'){
				$js_code .= 'if(jQuery("#imgarr").val() == "")
				{
					if("'.$msg.'" == "")
					{
						jQuery("#post_images_error").html("'.__("Please upload at least 1 image to the gallery !",DOMAIN).'");
						return false;
					}
					else
					{
						jQuery("#post_images_error").html("'.$msg.'");
						return false;
					}
				}';
			}
			$js_code .= 'if("'.$name.'" == "end_date")';
			{
				$js_code .= '
				{
					 if(jQuery("#'.$name.'").val() < jQuery("#st_date").val() || jQuery("#'.$name.'").val() == "")
					{
						';
						$js_code .= $name.'.addClass("error");
						jQuery("#'.$espan.'").text("'.$msg.'");
						jQuery("#'.$espan.'").addClass("message_error2");
						return false;
					}
					else
					{
						'.$name.'.removeClass("error");
						jQuery("#'.$espan.'").text("");
						jQuery("#'.$espan.'").removeClass("message_error2");
						return true;
					}
				}';
			}
		$js_code .= 'if((!jQuery("#select_category").val() || jQuery("#select_category").val()=="") && "'.$name.'"=="category")';
		$js_code .= '
			{
				
				jQuery("#'.$espan.'").text("'.$msg.'");
				jQuery("#'.$espan.'").addClass("message_error2");
				return false;
			}';
		$js_code .= 'if(jQuery("#'.$name.'").val() == "" && '.$is_required.')';
		$js_code .= '
			{
				jQuery("#'.$espan.'").text("'.$msg.'");
				jQuery("#'.$espan.'").addClass("message_error2");
				return false;
			}
			else{
				jQuery("#'.$espan.'").text("");
				jQuery("#'.$espan.'").removeClass("message_error2");
				return true;
			}
		}
		';
	}
	
	if($type == 'range_type' && $search_ctype=='slider_range' ){
		$js_code .= '
		function validate_'.$name.'_range_type()
		{
			
			var value=jQuery("#'.$name.'").val();
			var min_value=jQuery("#'.$name.'").attr("min");
			var max_value=jQuery("#'.$name.'").attr("max");
			if(parseInt(value) < parseInt(min_value)){	
				jQuery("#'.$espan.'_range_type").remove();
				jQuery("#'.$name.'").after("<span id=\"'.$espan.'_range_type\" class=\"message_error2\">'.__('Please select a value that higher than',DOMAIN).' "+min_value+"</span>");				
				return false;
			}else if(parseInt(value) > parseInt(max_value)){				
				jQuery("#'.$espan.'_range_type").remove();
				jQuery("#'.$name.'").after("<span id=\"'.$espan.'_range_type\" class=\"message_error2\">'.__('Please select a value that lower than',DOMAIN).' "+max_value+"</span>");				
				return false;
			}else if(isNaN(parseInt(value)) && value!=""){				
				jQuery("#'.$espan.'_range_type").remove();
				jQuery("#'.$name.'").after("<span id=\"'.$espan.'_range_type\" class=\"message_error2\">'.__('Please enter a number',DOMAIN).'</span>");				
				return false;
			}else{				
				jQuery("#'.$espan.'_range_type").remove();
				return true;
			}
			
		}';
		//$js_code .= $name.'.blur(validate_'.$name.'_range_type); ';
		//$js_code .= $name.'.keyup(validate_'.$name.'_range_type); ';
		
		$js_code .= $name.'.live("focus blur keyup change", function(event){validate_'.$name.'_range_type()});'."\r\n";
	}
	//$js_code .= '//On blur ';	
	//$js_code .= $name.'.blur(validate_'.$name.'); ';
	//$js_code .= '//On key press ';
	//$js_code .= $name.'.keyup(validate_'.$name.'); ';
	if($name=='category'){
		$js_code .='jQuery("input[name^=category],select[name^=category]").live("blur keyup change click", function(event){validate_'.$name.'()});'."\r\n";	
	}
	$js_code .='jQuery("#'.$name.'").live("blur keyup", function(event){validate_'.$name.'()});'."\r\n";
	 
	
	$jsfunction[] = 'validate_'.$name.'()';
}

if ( !is_user_logged_in() ) {
	$jsfunction_not_loggedin[] = 'validate_frontend_user_email()';
	$jsfunction_not_loggedin[] = 'validate_frontend_user_fname()';
	$js_code_not_loggedin='';
	$js_code_not_loggedin .= 'function validate_frontend_user_email(){
		var user_email = jQuery("#user_email"); 	
		var user_email_error = jQuery("#user_email_error"); 
		if(jQuery("#submit_form #user_email").val() == "" && jQuery("#submit_form #user_email").length > 0 )
			
			{
				user_email.addClass("error");
				user_email_error.text("'.__('Please Enter E-mail',FE_DOMAIN).'");
				user_email_error.removeClass("available_tick");
				user_email_error.addClass("message_error2");
				return false;
			}
			else{
			
				if(jQuery.trim(jQuery("#user_email").val()) != "")
				{
					var a = jQuery("#user_email").val();
					var emailReg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/;
					if(jQuery("#user_email").val() == "") { user_email.addClass("error");
						user_email_error.text("'.__("Please provide your email address",FE_DOMAIN).'");
						user_email_error.addClass("message_error2");
					return false; } else if(!emailReg.test(jQuery("#user_email").val().replace(/\s+$/,""))) { user_email.addClass("error");
						user_email_error.text("Please provide valid email address");
						user_email_error.addClass("message_error2");
						return false;
					} else {
						return true;
					}
				}
				{
					user_email.removeClass("error");
					user_email_error.text("");
					user_email_error.removeClass("message_error2");
					return true;
				}
			}
	}';
	$js_code_not_loggedin .= 'function validate_frontend_user_fname(){
		var user_fname = jQuery("#user_fname"); 	
		var user_fname_error = jQuery("#user_fname_error"); 
		if(jQuery("#submit_form #user_fname").val() == "" && jQuery("#submit_form #user_fname").length > 0 )			
		{
			user_fname.addClass("error");
			user_fname_error.text("'.__('Please Enter User name.',FE_DOMAIN).'");
			user_fname_error.removeClass("available_tick");
			user_fname_error.addClass("message_error2");
			return false;
		}
		else{
		
			if(jQuery.trim(jQuery("#user_fname").val()) != "")
			{
				var a = jQuery("#user_fname").val();
				var userLength = jQuery("#user_fname").val().length;
				if(jQuery("#user_fname").val() == "") { user_fname.addClass("error");
						user_fname_error.text("'.__('Please Enter User name',FE_DOMAIN).'");
						user_fname_error.addClass("message_error2");
						
				}else if(jQuery("#user_fname").val().match(/\ /)){ user_fname.addClass("error");
					user_fname_error.text("'.__('Usernames should not contain space.',FE_DOMAIN).'");
					user_fname_error.addClass("message_error2");
					return false;
				}else if(userLength < 4 ){ user_fname.addClass("error");
					user_fname_error.text("'.__('User name must be minimum 4 character long',FE_DOMAIN).'");
					user_fname_error.addClass("message_error2");
					return false;
				}else
				{
					return true;
					
				}
			}{
				user_fname.removeClass("error");
				user_fname_error.text("");
				user_fname_error.removeClass("message_error2");
				return true;
			}
		}
	}

	';
	
	if($jsfunction_not_loggedin)
	{
		$jsfunction_str_not_loggedin = implode(' & ', $jsfunction_not_loggedin);	
	}else{
		$jsfunction_str_not_loggedin='';
	}
}
if($jsfunction)
{
	$jsfunction_str = implode(' & ', $jsfunction);	
}else{
	$jsfunction_str='';
}
//$js_code .= '//On Submitting ';
$js_code .= '	
jQuery("#continue_submit_from,.frontend_submit_button").click(function()
{
	var package_select = jQuery("input[name=package_select]");	
	var package_type=package_select.attr("type");
	if (document.getElementsByName("package_select").length >0){
		if(package_type =="radio")
		{
			if (!jQuery("input:radio[name=package_select]:checked").val())
			 {
				jQuery("#all_packages_error").html("'.__('Please Select Price Package',FE_DOMAIN).'");
				return false; /* add comment return false nothing add and directoly submit then only price package error will be shown */
			 }
			else
			{
				jQuery("#all_packages_error").html("");
			}
		}
}';
	$js_code=apply_filters('submit_form_validation',$js_code);
	if($jsfunction_str !=''){
	$js_code.='/* Check terms and condition validation */
	if(jQuery("#term_and_condition").length){		
		if(!jQuery("#term_and_condition").attr("checked"))		
		{
			jQuery("#terms_error,.common_error_not_login").html("'.__('Please accept Terms and Conditions.',FE_DOMAIN).'");			
			return false; // add comment return false nothing add and directoly submit then only term condition error will be shown
		}else{
			jQuery("#terms_error").html("");	
		}
	}
	if('.$jsfunction_str.')
	{';
		
		if(is_user_logged_in()){
			$js_code.='jQuery("#common_error").html("");
			jQuery("#frontend_edit_process").show();
			jQuery.ajax({
				url:ajaxUrl,
				type:"POST",
				async: true,
				data: "action=frontend_edit_submit_data&"+jQuery(".frontend_edit_submit_form").serialize(),
				success:function(results){				
					//alert(results.toSource()+"=="+results);				
					jQuery("#frontend_submit_error").html("");
					if(results.recaptcha_error!="" && results.recaptcha_error!="undefined"){
						//alert(results.recaptcha_error);
						jQuery("#frontend_submit_error").html("<span>"+results.recaptcha_error+"</span>");
	
					}
					if(results.redirect_url!="" && results.redirect_url!="defined"){
						window.location=results.redirect_url;
					}
					jQuery("#frontend_edit_process").hide();
	
					return false;
				}
			})
			return false;
			';
			
		}else{
			$js_code.='return true;';	
		}
		
	$js_code.='}
	else
	{
		jQuery("#common_error,.common_error_not_login").html("'.__('Oops, looks like you forgot to enter a value in at least one compulsory field.',FE_DOMAIN).'");
		return false;
	}';
	}
	$js_code.='
});
';
$js_code .= '
});';
echo $js_code;

if(!is_user_logged_in()){
	$js_code_not_loggedin.='jQuery(".frontend_submit_button").click(function(){';
			if($jsfunction_str !=''){	
				$js_code_not_loggedin.='if('.$jsfunction_str_not_loggedin.'){
						jQuery("#common_error").html("");
						jQuery("#frontend_edit_process").show();
						jQuery.ajax({
							url:ajaxUrl,
							type:"POST",
							async: true,
							data: "action=frontend_edit_submit_data&"+jQuery(".frontend_edit_submit_form").serialize(),
							success:function(results){				
								//alert(results.toSource()+"=="+results);				
								jQuery("#frontend_submit_error").html("");
								if(results.recaptcha_error!="" && results.recaptcha_error!="undefined"){
									//alert(results.recaptcha_error);
									jQuery("#frontend_submit_error").html("<span>"+results.recaptcha_error+"</span>");
				
								}
								if(results.redirect_url!="" && results.redirect_url!="defined"){
									window.location=results.redirect_url;
								}
								jQuery("#frontend_edit_process").hide();
				
								return false;
							}
						});
						return false
				}else{
					jQuery("#common_error").html("'.__('Oops, looks like you forgot to enter a value in at least one compulsory field.',FE_DOMAIN).'");
					return false;
				}';
			}
	$js_code_not_loggedin .= '});';	
	echo $js_code_not_loggedin;	
}
?>
function hide_error(){
	if(jQuery("#term_and_condition").attr("checked"))
	{
		jQuery("#terms_error").html("");
	}
}
//]]>
</script>