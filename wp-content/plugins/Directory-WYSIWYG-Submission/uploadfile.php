<?php
require("../../../wp-load.php");
$structure = TEMPLATEPATH."/images/";

if(!is_dir($structure."tmp"))
{
	if (!mkdir($structure."tmp", 0777, true)) 
	 {
		die(__('Failed to create folders...',FE_DOMAIN));
	 }
}

$uploaddir = TEMPLATEPATH."/images/tmp/";
$nam = ($_FILES['frontend_uploadfile']['name']) ? $_FILES['frontend_uploadfile']['name'] : $_FILES['uploadcoupon']['name'];
$upload = '';


if(isset($_FILES['file']) && $_FILES['file']['error']==0){

	 $path_info = pathinfo($_FILES['file']['name']);
	 $dirinfo = wp_upload_dir();	 
	 $file_extension = $path_info["extension"];	 
	 $finalName = basename($_FILES['file']['name'],".$file_extension").time().".".$file_extension;
	 $finalName=str_replace(' ','',$finalName);
	 $file = $dirinfo['path'] .'/'.$finalName ;	 
	 $file_ext= substr($file, -4, 4);	 
	 if(in_array($file_ext,$extension_file))
	 {		 	
		 if (move_uploaded_file($_FILES['file']['tmp_name'], $file))
		 {
			 echo $dirinfo['url'] .'/'.$finalName ;
		 }
		 else
		 {
			echo "error";
		 }
	 }else{
	 	echo 'error';
	 }
	exit;
}


if($_FILES['frontend_uploadfile']['size'])
{
	$file_size= $_FILES['frontend_uploadfile']['size'];
}
if($_FILES['uploadcoupon']['size'])
{
	$file_size= $_FILES['uploadcoupon']['size'];
}
$tmpdata = get_option('templatic_settings');
$limit_size =  $tmpdata['templatic_image_size'];
if(!$limit_size)
{
	$limit_size = 50;
	update_option('templatic_image_size',$limit_size);
}
 
if($file_size[0])
{
	if(($file_size[0]/1024) >= $limit_size)
	{
		echo 'LIMIT';
		exit;
	}
}
if(count($nam) > 10)
{
	echo count($nam);
	die;
}
if(isset($_REQUEST['images_count']) && (count($nam) > $_REQUEST['images_count']))
{
	echo count($nam);
	die;
}
if(isset($_REQUEST['images_count']) && ($_REQUEST['total_count']+count($nam) > $_REQUEST['images_count']))
{
	echo $_REQUEST['total_count']+count($nam);
	die;
}

  
global $extension_file;

if(isset($_FILES['frontend_uploadfile'])){
	foreach($nam as $key=>$_nam)
	{
		 $path_info = pathinfo($_nam);
		 $file_extension = $path_info["extension"];
		 $finalName = basename($_nam,".$file_extension").time().".".$file_extension;
		 $finalName=str_replace(' ','',$finalName);
		 $file = $uploaddir .$finalName ;
		 $file_ext= substr($file, -4, 4);

		 if(in_array($file_ext,$extension_file))
		 {	 	 		 	
			 if (move_uploaded_file($_FILES['frontend_uploadfile']['tmp_name'][$key], $file))
			 {
				 echo $upload = $finalName.",";
			 }else
			 {
				echo "error";
			 }
		 }else
		 	echo 'error';
	}
}else{

	foreach($nam as $key=>$_nam)
	{
		 $path_info = pathinfo($_nam);
		 $file_extension = $path_info["extension"];
		 $finalName = basename($_nam,".$file_extension").time().".".$file_extension;
		 $finalName=str_replace(' ','',$finalName);
		 $file = $uploaddir .$finalName ;
		 $file_ext= substr($file, -4, 4);
		 $extension_file[]='.pdf';
		 $extension_file[]='.PDF';
		 if(in_array($file_ext,$extension_file))
		 {	 	 		 	
			if(move_uploaded_file($_FILES['uploadcoupon']['tmp_name'][$key], $file)){
			 	echo $upload = $finalName.",";	
			}
			else
			{
				echo "error";
			}
		 }else
		 	echo 'error';
	}
}
?>