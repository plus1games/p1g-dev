<?php 
/*
 * This file use for frontend coupon custom field uploader related script and function added
 */
 
/*
 * Coupon uploader script
 */
add_action('wp_footer','frontend_edit_coupons_upload_script',100);
function frontend_edit_coupons_upload_script(){

	if(is_single() && isset($_REQUEST['action'])){
	global $post;		
	$package_select=get_post_meta($post->ID,'package_select',true);
	
	$tmpdata = get_option('templatic_settings');
	/* gets the number from backend for how many coupons are allowed to upload */
	if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php'))
	{
		$num_of_allow_coupons = get_post_meta(get_post_meta($post->ID,'package_select',true),'coupons_allowed',true);
		$post_images_limit = ($num_of_allow_coupons == '' || $num_of_allow_coupons == 0) ? 10 : $num_of_allow_coupons;	
	}
	else
	{
		$post_images_limit = ($tmpdata['numberof_coupons'] == '')? 10 : $tmpdata['numberof_coupons'];
	}	
	
	?>

	<script type="text/javascript" src="<?php echo TEVOLUTION_COUPON_URL; ?>js/ajaxupload.3.5.js" ></script> <!-- script for multiple image upload -->
	<?php

	$tmpdata = get_option('templatic_settings');
	$templatic_image_size =  @$tmpdata['templatic_image_size'];
	if(!$templatic_image_size){ $templatic_image_size = '50'; }
	?>
	<script type="text/javascript">
	var temp = 1;
	var numof_coupons_can_upload = parseInt(<?php echo $num_of_coupons;?>);
	var html_var = '<?php echo $val['htmlvar_name']; ?>';

	var $uc = jQuery.noConflict();

	/* multiple file upload, validation and preview images */
	$uc(function()
	{
		var btnCpnUpload=$uc('#uploadvouchers');
		var status=$uc('#frontend_coupon_status');
		var $coupons_ids = jQuery('#frontend_post_coupons');
		var $coupons_gallery = jQuery('ul#frontend_coupons_gallery');
		var counter=0;
		new AjaxUpload_coupons(btnCpnUpload, {
		appendId: 'uploadvouchers', // id where to append image uploader	
		name: 'uploadcoupon[]', // name of image uploader
		action: frontend_plg_url+'uploadfile.php',
		data:{images_count:<?php echo $post_images_limit?>,total_count:counter},
		onSubmit: function(file, ext)
		{
			$uc('#post_coupons_error').html('');
			var file_extension = file.search('.php');
			var file_extension_js = file.search('.js');
			var file_extension_pdf = file.search('.pdf');
		
			if (! (ext && /^(jpg|png|jpeg|gif|pdf)$/.test(ext)) || file_extension != -1 || file_extension_js != -1){
			 // extension is not allowed 
			status.text("<?php _e('Only JPG, PNG, GIF or PDF files are allowed',FE_DOMAIN);?>");
			return false;
			}status.text("<?php _e('Uploading...',FE_DOMAIN); ?>");
			},	
		onComplete: function(file, response)
		{  		
			/*Image size validation*/
			if(response == 'LIMIT'){
				status.text("<?php _e('Your image size must be less then',FE_DOMAIN); echo " ".$templatic_image_size." "; _e('kilobytes',FE_DOMAIN); ?>");
				return false;
			}
		
				if(response > <?php echo $post_images_limit;?> )
				{			
					status.text("<?php _e('You can upload maximum',FE_DOMAIN);echo " ".$post_images_limit." "; _e('coupons',FE_DOMAIN); ?>");
					return false;
				}
				/* Directory theme wysiwyg plugin issue with field monetization plugin */
				var count=0;
				jQuery('#listing_coupons ul#frontend_coupons_gallery li').each(function(){
					count = count + 1;
				});		
				
				var limit = response.split(",").length-1;
				if(<?php echo $post_images_limit;?> >= 2)
				{
					cnt = count+limit;
				}
				else
				{
					cnt = count;
				}
				
				if(parseFloat(cnt) > <?php echo $post_images_limit;?> )
				{
					status.text("<?php _e('You can upload maximum',FE_DOMAIN);echo " ".$post_images_limit." "; _e('coupons',FE_DOMAIN); ?>") ;
					return false;
				}
			
					 
			var spl = response.split(",");
			//On completion clear the status
			status.text('');		
			attachment_name=$coupons_ids.val();

			for(var i =0;i<spl.length;i++)
			{
				if(spl[i]!=""){			  	
					var img_name = '<?php echo bloginfo("template_url")."/images/tmp/"; ?>'+spl[i];
					file_extension_pdf = img_name.search('.pdf');					
					if(file_extension_pdf != -1){						
						img_name ='<?php echo TEVOLUTION_COUPON_URL.'/images/pdfthumb.png' ?>';

					}
					var id_name=spl[i].split(".");
					$coupons_gallery.append('\
							<li class="image " data-attachment_id="' + spl[i] + '" data-attachment_src="'+img_name+'">\
								<img src="' + img_name + '" width="268" height="188"/>\
								<span>\
									<a href="#" class="delete" title="'+delete_image+'"><i class="fa fa-times-circle redcross"></i></a>\
								</span>\
								<div class="cpn_optopn">\
									<a href="#" class="button pr"><i class="fa fa-print"></i>&nbsp;<?php _e('Print Voucher',FE_DOMAIN);?></a>\
									<a href="#" class="button dw"><i class="fa fa-download"></i>&nbsp;<?php _e('Download Voucher',FE_DOMAIN);?></a>\
								</div>\
							</li>');
					attachment_name = attachment_name ? attachment_name + "," + spl[i] : spl[i];				
					$coupons_ids.val( attachment_name );
				}
				 
			}
		}		
		
		});
		

		$coupons_gallery.sortable({
			items: 'li.image',
			//items: ($this.children('li').length>1)? 'li.image' :'',
			cursor: 'move',
			scrollSensitivity:40,
			forcePlaceholderSize: true,
			forceHelperSize: false,
			helper: 'clone',
			opacity: 0.65,
			placeholder: 'wc-metabox-sortable-placeholder',
			start:function(event,ui){
				ui.item.css('background-color','#f6f6f6');
			},
			stop:function(event,ui){
				ui.item.removeAttr('style');
			},
			update: function(event, ui) {
				var attachment_ids = '';
				var j=0;
				jQuery('#frontend_coupons_gallery li.image').css('cursor','default').each(function() {
					var attachment_id = jQuery(this).attr( 'data-attachment_id' );
					var attachment_src = jQuery(this).attr( 'data-attachment_src' );
					attachment_ids = attachment_ids + attachment_id + ',';				
					if(attachment_src!="" && attachment_src!='undefined' ){
						var img_name = attachment_src;
					}else{
						var img_name = '<?php echo bloginfo("template_url")."/images/tmp/"; ?>'+attachment_id;
					}				
					j++;
				});
				$coupons_ids.val( attachment_ids );
			}
		});

		jQuery('ul#frontend_coupons_gallery').on( 'click', 'a.delete', function() {
			var attachment_ids = '';
			var j=0;
			jQuery(this).closest('li.image').remove();
			
			jQuery('#frontend_coupons_gallery li.image').css('cursor','default').each(function() {
				var attachment_id = jQuery(this).attr( 'data-attachment_id' );
				var attachment_src = jQuery(this).attr( 'data-attachment_src' );
				attachment_ids = attachment_ids + attachment_id + ',';
				if(attachment_src!="" && attachment_src!='undefined' ){
					var img_name = attachment_src;
				}else{
					var img_name = '<?php echo bloginfo("template_url")."/images/tmp/"; ?>'+attachment_id;
				}

				j++;
			});		
			$coupons_ids.val( attachment_ids );		
			var delete_id=jQuery(this).attr('id');
			if(delete_id!='' && delete_id!='undefined'){			
				jQuery.ajax({
					url:"<?php echo esc_js( get_bloginfo( 'wpurl' ) . '/wp-admin/admin-ajax.php' ); ?>",
					type:'POST',
					data:'action=delete_coupons_vouchers&coupon_id=' + delete_id+'&post_id=<?php echo $post->ID;?>',
					success:function(results) {
					}
				});
			}
			return false;
		} );
	});

	jQuery.noConflict();
	</script>
	<?php
	}
}//

if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
	/* for showing a coupons in details page */
	remove_action('listing_coupons','coupons_listing');

	/* for showing a coupons in details page */
	remove_action('listing_extra_details','coupons_listing');

	remove_action('show_listing_event','show_coupons_tab');
	remove_action('dir_end_tabs','show_coupons_tab');
}

/* for adding a tab for coupons in listing details page */
add_action('dir_end_tabs','frontend_edit_show_coupons_tab');
add_action('show_listing_event','frontend_edit_show_coupons_tab');
/* for showing a coupons in details page */
add_action('listing_coupons','frontend_coupons_listing');

/* for showing a coupons in details page */
add_action('listing_extra_details','frontend_coupons_listing');

/*
 * display coupan tab on is front end edit detail page 
 */
function frontend_edit_show_coupons_tab(){

	global $tmpl_flds_varname,$htmlvar_name;
   /* Show on front end editor when add listing */
   if(isset($_REQUEST['action']) && $_REQUEST['action'] =='edit' && ( $tmpl_flds_varname['post_coupons'] || $htmlvar_name['field_label']['post_coupons']  )){ ?>

   	 <li class="tab-title" role="presentational"><a href="#listing_coupons"><?php echo _e('Vouchers',FE_DOMAIN);?></a></li>

  <?php }
}

/*
 * display coupan code upload button on is front end edit detail page
 */
function frontend_coupons_listing(){
	
	/* Upload Voucher button for front Editor */
	global $post,$tmpl_flds_varname,$htmlvar_name,$custom_fields_as_tabs;		
	$coupons = get_post_meta(get_the_ID(),'coupon_ids',true);
	if($coupons){
		$coupons_arr = explode(',',$coupons);
	}
	if(isset($_REQUEST['action']) && $_REQUEST['action'] =='edit' && ( $tmpl_flds_varname['post_coupons'] || $htmlvar_name['field_label']['post_coupons'] || $custom_fields_as_tabs['post_coupons']))
	{ 
		if($custom_fields_as_tabs['post_coupons']['show_as_tabs'] == 1)
			$coupon_div_id = 'ft_post_coupons';
		else	
			$coupon_div_id = 'listing_coupons';
	?>	
		<!--editing post coupons -->
		
		<section id="<?php echo $coupon_div_id; ?>" class="content" role="tabpanel" aria-hidden="false">     
			<?php echo apply_filters('tmpl_voucher_title',''); ?>
	        <div class="entry-header-image clearfix">
				<ul id="frontend_coupons_gallery" class=" ui-sortable">
				<?php if(!empty($coupons_arr)): 
					foreach($coupons_arr as $key=>$cpns)
					{					
						$coupons = get_post($cpns);					
						$url = wp_get_attachment_url( $cpns );
						$cpn_name = basename($url);
						$cpn_name_gallery .=basename($url).',';
						$path_parts = pathinfo($cpn_name);
	    				$ext = strtolower($path_parts["extension"]);
	    				if($ext == 'pdf')
						{
							$url = TEVOLUTION_COUPON_URL.'/images/pdfthumb.png';
						}
				?>
					<li class="image " data-attachment_src="<?php echo $url?>" data-attachment_id="<?php echo $cpn_name;?>">
						<img src="<?php echo $url;?>"  width="268" height="188"/>
						<span><a id="<?php echo $coupons->ID;?>" class="delete" title="<?php _e('Delete image',FE_DOMAIN);?>" href="#"><i class="fa fa-times-circle redcross"></i></a></span>
						<?php if(!isset($_REQUEST['action']) && $_REQUEST['action'] != 'edit' && isset($_REQUEST['front_edit']) && $_REQUEST['front_edit'] != 1): ?>
							<div class="cpn_optopn">
								<a href='#' class="button pr"><i class="fa fa-print"></i>&nbsp;<?php _e('Print Voucher',FE_DOMAIN);?></a>
								<a href="#" class="button dw"><i class="fa fa-download"></i>&nbsp;<?php _e('Download Voucher',FE_DOMAIN);?></a>
							</div>
						<?php endif; ?>
					</li>

				<?php }
				endif;?>

				</ul>
				<input type="hidden" id="frontend_post_coupons" name="frontend_edit_post_coupons" value="<?php echo esc_attr( substr(@$cpn_name_gallery,0,-1) ); ?>" />
			</div>
			<div class="listing-coupon flexslider frontend_edit_coupons flex-viewport">
				<div id="uploadvouchers" class="upload button secondary_btn clearfix">
					<span><?php _e('Upload Vouchers',FE_DOMAIN);?></span>					
				</div>			
	        </div>
	        <span id="frontend_coupon_status" class="message_error2 clearfix"></span>
		</section>
	<?php } ?>		
	<?php
}




/*
 * This function save upload coupon data from frontend data save on detail page 
 */
add_action('frontend_edit_save_data','frontend_edit_coupons_save_data',10,2);
function frontend_edit_coupons_save_data($request,$post_id){
	$dirinfo = wp_upload_dir();	
	if(isset($_REQUEST['post_coupons']) && $_REQUEST['post_coupons']!=""){
		global $wpdb;
		$uploaddir = TEMPLATEPATH."/images/tmp/";
		$dirinfo = wp_upload_dir();
		$path = $dirinfo['basedir'].'/coupons';
		if(!is_dir($path)){
			mkdir($path);
		}
		$url = $dirinfo['baseurl'].'/coupons';
		$subdir = '/coupons';		
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		$post_images=explode(',',$_REQUEST['post_coupons']);
		$menu_order=0;
		foreach ($post_images as $image) {
			if($image!=""){
				$upload_img_path=$uploaddir._wp_relative_upload_path( $image );
				$wp_filetype = wp_check_filetype(basename($image), null );
				$dest=$path.'/'._wp_relative_upload_path( $image);
				if(file_exists($upload_img_path)){					
					copy($upload_img_path, $dest);						
					unlink($upload_img_path);
					$attachment = array('guid' => $url.'/'._wp_relative_upload_path( $image ),
									'post_mime_type' => $wp_filetype['type'],
									'post_title' => preg_replace('/\.[^.]+$/', '', basename($image)),
									'post_content' => '',
									'post_status' => 'inherit',
									'menu_order' => $menu_order++,
								);						
					$img_attachment=substr($subdir.'/'.$image,1);
					
					$cpns_ids[] = $attach_id = wp_insert_attachment( $attachment, $img_attachment );
					$upload_img_path=$path.'/'._wp_relative_upload_path( $image );
					$attach_data = wp_generate_attachment_metadata( $attach_id, $upload_img_path );
					wp_update_attachment_metadata( $attach_id, $attach_data );
				}
			}//finish foreach loop
			
		}//finish the foreach loop


		if($post_id)
		{
			$j = 1;
			$coupons_images=explode(',',$_REQUEST['post_coupons']);
			foreach($coupons_images as $arrVal)
			{
				$expName = array_slice(explode(".",$arrVal),0,1);				
				$wpdb->query("update $wpdb->posts set  menu_order = '".$j++."' where post_type='attachment' AND post_name = '".$expName[0]."'");
				$postid[] = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_type='attachment' and  post_name='".$expName[0]."'" );
			}
			if(!empty($postid)){
				$cpns_ids=array_filter($postid);
			}
			
		}

		$cpns = implode(',', $cpns_ids);		
		update_post_meta($post_id,'coupon_ids',$cpns);
		delete_post_meta($post_id, 'post_coupons');
	}
}

/*
 * insert sample coupan vouchers on frontend edit on detail page
 */ 
add_action('frontend_editor_image_upload','frontend_editor_coupan_upload_id',10,2);
function frontend_editor_coupan_upload_id($attach_id,$post_id){
	global $wpdb;
	if($_REQUEST['action'] != 'frontend_edit_submit_data'){
	/* Sample listing image */
	$uploaddir = TEMPLATIC_FRONTEND_DIR."/img/";
	$image='sample_image.jpg';
	$dirinfo = wp_upload_dir();
	$path = $dirinfo['path'];
	$url = $dirinfo['url'];
	$subdir = $dirinfo['subdir'];
	$basedir = $dirinfo['basedir'];
	$baseurl = $dirinfo['baseurl'];
	$upload_img_path=$uploaddir._wp_relative_upload_path( $image );
	$wp_filetype = wp_check_filetype(basename($image), null );

	$path_info = pathinfo($image);
	$file_extension = $path_info["extension"];		
	if(file_exists($upload_img_path)){
		for($i=0;$i<1;$i++){
			$image = basename($image,".$file_extension").$i."_coupons_".$post_id.".".$file_extension;
			$dest=$path.'/'._wp_relative_upload_path( $image);
			copy($upload_img_path, $dest);
			$attachment = array('guid' => $url.'/'._wp_relative_upload_path( $image ),
							'post_mime_type' => $wp_filetype['type'],
							'post_title' => preg_replace('/\.[^.]+$/', '', basename($image)),
							'post_content' => '',
							'post_status' => 'inherit'
						);						
			$img_attachment=substr($subdir.'/'.$image,1);
			
			$image_attach_id[]=$attach_id = wp_insert_attachment( $attachment, $img_attachment );
			$upload_img_path=$path.'/'._wp_relative_upload_path( $image );
			$attach_data = wp_generate_attachment_metadata( $attach_id, $upload_img_path );
			wp_update_attachment_metadata( $attach_id, $attach_data );
		}
	}
	$cpns = implode(',', $image_attach_id);		
	update_post_meta($post_id,'coupon_ids',$cpns);
	delete_post_meta($post_id, 'post_coupons');
	}
}

/*
 * Delete post_coupons custom fields from post meta
 */
add_action('frontend_edit_ajax_save_data','delete_coupan_vouchers_data',10,3);
function delete_coupan_vouchers_data($key,$val,$post_id){
	delete_post_meta($post_id, 'post_coupons');
}
/*
 * delete coupan vouchers
 */
add_action('wp_ajax_delete_coupons_vouchers','delete_coupons_vouchers');
function delete_coupons_vouchers(){
	
	$coupon_id=get_post_meta($_REQUEST['post_id'],'coupon_ids',true);
	$coupon_id=explode(',', $coupon_id);
	if(($key = array_search($_REQUEST['coupon_id'], $coupon_id)) !== false) {
	    unset($coupon_id[$key]);
	}	
	wp_delete_post($_REQUEST['coupon_id'],true);
	update_post_meta($_REQUEST['post_id'],'coupon_ids',implode(',',$coupon_id));

	delete_post_meta($_REQUEST['post_id'], 'post_coupons');

	echo '1';
	exit;
}
?>