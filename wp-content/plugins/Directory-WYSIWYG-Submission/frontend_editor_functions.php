<?php
/*
 * This file use for frontend editor plugin related function added
 * return tevolution login and register link on frontend submit form page
 */
add_filter('tevolution_login_redirect','frontend_edit_submit_page_redirect');
add_filter('tevolution_register_redirect','frontend_edit_submit_page_redirect');
function frontend_edit_submit_page_redirect($redirect_url){

	if(isset($_REQUEST['redirect_to']) && $_REQUEST['redirect_to']!=""){
		$redirect_url=$_REQUEST['redirect_to'];	
	}
	if(isset($_REQUEST['reg_redirect_link']) && $_REQUEST['reg_redirect_link']!=""){
		$redirect_url=$_REQUEST['reg_redirect_link'];	
	}
	return $redirect_url;
}
/*
 * save the Default custom fields value for user can edit his/her data on frontend detail page
 */
add_action('wp_ajax_nopriv_frontend_edit_submit_data','templatic_frontend_edit_submit_data');
add_action('wp_ajax_frontend_edit_submit_data','templatic_frontend_edit_submit_data');
function templatic_frontend_edit_submit_data(){
	global $wpdb,$monetization,$current_user;
	// unset the session custom fields
	unset($_SESSION['custom_fields']);
	
	$_REQUEST['package_select']=$_REQUEST['pkg_id'];
	$tmpdata = get_option('templatic_settings');
	$display =(isset($tmpdata['user_verification_page']) && $tmpdata['user_verification_page'] != "")? $tmpdata['user_verification_page']:"";
	$flg=1;
	/* submit page Wp-reCaptcha  */
	if(!empty($display) && in_array('submit',$display) ){
		
		$privatekey = $tmpdata['secret'];
		if($_REQUEST["g-recaptcha-response"]!="")
		{
			/*get the response from captcha that the entered captcha is valid or not*/
			$response = wp_remote_get("https://www.google.com/recaptcha/api/siteverify?secret=".$privatekey."&response=".$_REQUEST["g-recaptcha-response"]."&remoteip=".getenv("REMOTE_ADDR"));											
			/*decode the captcha response*/
			$responde_encode = json_decode($response['body']);
			if (!$responde_encode->success) {
				$flg=0;
				$send_data['recaptcha_error']=__('Please fill the captcha form.',FE_DOMAIN);
				$send_data['redirect_url']='';
			}
		}else{
			$flg=0;
			$send_data['recaptcha_error']=__('Please fill the captcha form.',FE_DOMAIN);
			$send_data['redirect_url']='';
		}
	}
	
	/* register user when first time user come for submit */
	if(isset($_REQUEST['user_email']) && $_REQUEST['user_email']!='' && isset($_REQUEST['user_fname']) && $_REQUEST['user_fname']!="" && $flg==1){
		$current_user_id = templ_insertuser_with_listing();	
		$current_user=get_userdata( $current_user_id );		
	}
	/* Finish captch validation */

	/* Front end action renew */
	if(isset($_REQUEST['frontend_action']) && $_REQUEST['frontend_action']=='renew'){
		$flg=0;
		$post_id=$_REQUEST['pid'];

		update_post_meta($post_id,'country_id',$_REQUEST['country_id']);
		update_post_meta($post_id,'zones_id',$_REQUEST['zones_id']);
		update_post_meta($post_id,'post_city_id',$_REQUEST['post_city_id']);
		update_post_meta($post_id,'is_frontend_edit_form',1);

		$package_alive_days=$monetization->templ_get_price_info($_REQUEST['package_select'],'');
		$payable_amount=$_REQUEST['total_price'];
		if(is_valid_coupon_plugin($_REQUEST['add_coupon']))
		{ 
			$coupon_amount = get_payable_amount_with_coupon_plugin($payable_amount,$_REQUEST['add_coupon']);
			$post_table = $wpdb->prefix."posts";
			$add_coupon	= "select ID from $post_table where post_title ='".$_REQUEST['add_coupon']."' and post_type ='coupon_code' and post_status='publish'";
			$coupon_id	= $wpdb->get_var($add_coupon);
			$coupondisc = get_post_meta($coupon_id,'coupondisc',true);
			$couponamt 	= get_post_meta($coupon_id,'couponamt',true);
			
			if($coupondisc == 'per' && $coupon_amount == 0 && $couponamt==100  ){
				$payable_amount = $coupon_amount;
			}elseif($coupon_amount > 0 )
			{
				$payable_amount = $coupon_amount;
			}
			update_post_meta($post_id,'add_coupon',$_REQUEST['add_coupon']);
		}
		update_post_meta($post_id,'package_select',$_REQUEST['package_select']);
		if(isset($_REQUEST['featured_h']))
			update_post_meta($post_id,'featured_h','h');
		if(isset($_REQUEST['featured_c']))
			update_post_meta($post_id,'featured_c','c');
		update_post_meta($post_id,'featured_type',$_REQUEST['featured_type']);
		update_post_meta($post_id,'alive_days',$package_alive_days[0]['alive_days']);
        update_post_meta($post_id,'total_price',$_REQUEST['total_price']);
        update_post_meta($post_id,'payable_amount',$payable_amount);


		/*         
         * insert post category
         */
		if(isset($_REQUEST['category']) && $_REQUEST['category']!=""){
			$_REQUEST['all_cat']='';
			foreach ($_REQUEST['category'] as $value) {
				$value_id=explode(',',$value);
				$_REQUEST['all_cat'].=$value_id[0].'|';
			}

			$_REQUEST['all_cat']=substr($_REQUEST['all_cat'],0,-1);
			
			$category_ids=explode('|',$_REQUEST['all_cat']);
			wp_delete_object_term_relationships( $post_id, $_REQUEST['cur_post_taxonomy']);
			foreach($category_ids as $key=> $_tax)
			{
				$term = get_term_by('id',$_tax,$_REQUEST['cur_post_taxonomy']);
				if(!empty($term)){
					wp_set_post_terms($post_id,$_tax,$_REQUEST['cur_post_taxonomy'],true);
				}
			}
		}
		$permalink=get_permalink($post_id);
		if(false===strpos($permalink,'?')){
		    $url_glue = '?';
		}else{
			$url_glue = '&';	
		}
	 	$send_data['redirect_url']=$permalink.$url_glue.'action=edit&renew=1';
	 	$send_data['recaptcha_error']='';
	}

	/* Frontend action is edit */
	if(isset($_REQUEST['frontend_action']) && $_REQUEST['frontend_action']=='edit'){
		$flg=0;
		$post_id=$_REQUEST['pid'];
		update_post_meta($post_id,'country_id',$_REQUEST['country_id']);
		update_post_meta($post_id,'zones_id',$_REQUEST['zones_id']);
		update_post_meta($post_id,'post_city_id',$_REQUEST['post_city_id']);
		update_post_meta($post_id,'is_frontend_edit_form',1);
		$permalink=get_permalink($post_id);
		if(false===strpos($permalink,'?')){
		    $url_glue = '?';
		}else{
			$url_glue = '&';	
		}
	 	$send_data['redirect_url']=$permalink.$url_glue.'action=edit&front_edit=1';
	 	$send_data['recaptcha_error']='';

	}

    /* go backend and edit update data */
	if(isset($_REQUEST['backandedit']) && $_REQUEST['backandedit']==1){
		$flg=0;
		$post_id=$_REQUEST['pid'];
		update_post_meta($post_id,'country_id',$_REQUEST['country_id']);
		update_post_meta($post_id,'zones_id',$_REQUEST['zones_id']);
		update_post_meta($post_id,'post_city_id',$_REQUEST['post_city_id']);
		update_post_meta($post_id,'is_frontend_edit_form',1);

		if(isset($_REQUEST['category']) && $_REQUEST['category']!=""){
			$_REQUEST['all_cat']='';
			foreach ($_REQUEST['category'] as $value) {
				$value_id=explode(',',$value);
				$_REQUEST['all_cat'].=$value_id[0].'|';
			}

			$_REQUEST['all_cat']=substr($_REQUEST['all_cat'],0,-1);
			

			$category_ids=explode('|',$_REQUEST['all_cat']);
			wp_delete_object_term_relationships( $post_id, $_REQUEST['cur_post_taxonomy']);
			foreach($category_ids as $key=> $_tax)
			{
				$term = get_term_by('id',$_tax,$_REQUEST['cur_post_taxonomy']);
				if(!empty($term)){
					wp_set_post_terms($post_id,$_tax,$_REQUEST['cur_post_taxonomy'],true);
				}
			}
		}

		$package_alive_days=$monetization->templ_get_price_info($_REQUEST['package_select'],'');
		$payable_amount=$_REQUEST['total_price'];
		if(is_valid_coupon_plugin($_REQUEST['add_coupon']))
		{ 
			$coupon_amount = get_payable_amount_with_coupon_plugin($payable_amount,$_REQUEST['add_coupon']);
			$post_table = $wpdb->prefix."posts";
			$add_coupon	= "select ID from $post_table where post_title ='".$_REQUEST['add_coupon']."' and post_type ='coupon_code' and post_status='publish'";
			$coupon_id	= $wpdb->get_var($add_coupon);
			$coupondisc = get_post_meta($coupon_id,'coupondisc',true);
			$couponamt 	= get_post_meta($coupon_id,'couponamt',true);
			
			if($coupondisc == 'per' && $coupon_amount == 0 && $couponamt==100  ){
				$payable_amount = $coupon_amount;
			}elseif($coupon_amount > 0 )
			{
				$payable_amount = $coupon_amount;
			}
			update_post_meta($post_id,'add_coupon',$_REQUEST['add_coupon']);
		}
		update_post_meta($post_id,'package_select',$_REQUEST['package_select']);
		if(isset($_REQUEST['featured_h']))
			update_post_meta($post_id,'featured_h','h');
		else
			update_post_meta($post_id,'featured_h','');
		if(isset($_REQUEST['featured_c']))
			update_post_meta($post_id,'featured_c','c');
		else
			update_post_meta($post_id,'featured_c','');
		update_post_meta($post_id,'featured_type',$_REQUEST['featured_type']);
		update_post_meta($post_id,'alive_days',$package_alive_days[0]['alive_days']);
        update_post_meta($post_id,'total_price',$_REQUEST['total_price']);
        update_post_meta($post_id,'payable_amount',$payable_amount);

        /* Insert User meta fields */
        $current_user_id=$my_post['post_author'];
        $submit_post_type = $my_post['post_type'];
		$package_post=get_post_meta($_REQUEST['package_select'],'limit_no_post',true);		
		$user_limit_post=get_user_meta($current_user_id,'total_list_of_post',true);
		
		
		if($package_post>$user_limit_post)
		{			
			$limit_post=get_user_meta($current_user_id,'total_list_of_post',true);
			update_user_meta($current_user_id,$submit_post_type.'_list_of_post',$limit_post+1);
			update_user_meta($current_user_id,'total_list_of_post',$limit_post+1);
			update_user_meta($current_user_id,$submit_post_type.'_package_select',$_REQUEST['package_select']);
			update_user_meta($current_user_id,'package_selected',$_REQUEST['package_select']);
		}else
		{
			update_user_meta($current_user_id,'package_selected',$_REQUEST['package_select']);
			update_user_meta($current_user_id,$submit_post_type.'_package_select',$_REQUEST['package_select']);
			update_user_meta($current_user_id,'total_list_of_post',1);
			update_user_meta($current_user_id,$submit_post_type.'_list_of_post',1);
		}
		$permalink=get_permalink($post_id);
		if(false===strpos($permalink,'?')){
		    $url_glue = '?';
		}else{
			$url_glue = '&';	
		}
	 	$send_data['redirect_url']=$permalink.$url_glue.'action=edit&preview=true';
	 	$send_data['recaptcha_error']='';
	}

	
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='frontend_edit_submit_data' && $flg==1){		

		$default_custom_metaboxes = get_frontend_single_post_fields($_REQUEST['cur_post_type']);//custom fields for all category.
		$my_post['post_type']=$_REQUEST['cur_post_type'];
		$my_post['post_title']=($default_custom_metaboxes['post_title']['default']!='')? $default_custom_metaboxes['post_title']['default'] : 'Click to edit your title' ;
		$my_post['post_content']=$default_custom_metaboxes['post_content']['default'];
		$my_post['post_status']= 'draft';
		$my_post['post_author']=$current_user->ID;
		$edit_post['post_name']=sanitize_title($_REQUEST['post_title']);		
		
		/* Insert default post */
		$post_id=wp_insert_post( $my_post);
		/* Finish the place geo_latitude and geo_longitude in postcodes table*/
		if(is_plugin_active('sitepress-multilingual-cms/sitepress.php')){
			if(function_exists('wpml_insert_templ_post'))
				wpml_insert_templ_post($post_id,$my_post['post_type']); /* insert post in language */
		}	
		$package_alive_days=$monetization->templ_get_price_info($_REQUEST['package_select'],'');		
		update_post_meta($post_id,'country_id',$_REQUEST['country_id']);
		update_post_meta($post_id,'zones_id',$_REQUEST['zones_id']);
		update_post_meta($post_id,'post_city_id',$_REQUEST['post_city_id']);
		update_post_meta($post_id,'is_frontend_edit_form',1);

		/*remove undefine string on total price poat data */
		$payable_amount=str_replace('undefine','',$_REQUEST['total_price']);
		$decimals=get_option('tmpl_price_num_decimals');
		$decimals=($decimals!='')?$decimals:2;
		$payable_amount = ($payable_amount);
		if(is_valid_coupon_plugin($_REQUEST['add_coupon']))
		{ 
			$coupon_amount = get_payable_amount_with_coupon_plugin($payable_amount,$_REQUEST['add_coupon']);
			$post_table = $wpdb->prefix."posts";
			$add_coupon	= "select ID from $post_table where post_title ='".$_REQUEST['add_coupon']."' and post_type ='coupon_code' and post_status='publish'";
			$coupon_id	= $wpdb->get_var($add_coupon);
			$coupondisc = get_post_meta($coupon_id,'coupondisc',true);
			$couponamt 	= get_post_meta($coupon_id,'couponamt',true);
			
			if($coupondisc == 'per' && $coupon_amount == 0 && $couponamt==100  ){
				$payable_amount = $coupon_amount;
			}elseif($coupon_amount > 0 )
			{
				$payable_amount = $coupon_amount;
			}
			update_post_meta($post_id,'add_coupon',$_REQUEST['add_coupon']);
		}


		update_post_meta($post_id,'package_select',$_REQUEST['package_select']);
		if(isset($_REQUEST['featured_h']))
			update_post_meta($post_id,'featured_h','h');
		if(isset($_REQUEST['featured_c']))
			update_post_meta($post_id,'featured_c','c');
		update_post_meta($post_id,'featured_type',$_REQUEST['featured_type']);
		update_post_meta($post_id,'alive_days',$package_alive_days[0]['alive_days']);
        update_post_meta($post_id,'total_price',$_REQUEST['total_price']);
        update_post_meta($post_id,'payable_amount',$payable_amount);

        /* Insert User meta fields */
        $current_user_id=$my_post['post_author'];
        $submit_post_type = $my_post['post_type'];
		$package_post=get_post_meta($_REQUEST['package_select'],'limit_no_post',true);		
		$user_limit_post=get_user_meta($current_user_id,$submit_post_type.'_list_of_post',true);
		$package_selected=get_user_meta($current_user_id,'package_selected',true);//fetch last package selected price packageid

		global $monetization,$is_package;
		$listing_price_info = $monetization->templ_get_price_info(get_post_meta($post_id,'package_select',true));
		$subscription_as_pay_post=$listing_price_info[0]['subscription_as_pay_post'];
		/* Get the selected package price */
		$package_price=$listing_price_info[0]['price'];
		$_POST['pkg_id']=get_post_meta($post_id,'package_select',true);
		$package_selected=get_user_meta($current_user_id,'package_selected',true);
		
		/* Get the submited list of post and selected package number of limit post*/
		$user_limit_post=get_user_meta($current_user_id,$_REQUEST['post_type'].'_list_of_post',true); 
		$user_limit_post=($user_limit_post!="")?$user_limit_post:'0';
		$package_limit=get_post_meta($_POST['pkg_id'],'limit_no_post',true);
		
		if(($payable_amount > 0 && $listing_price_info[0]['package_type']==2 && $subscription_as_pay_post!=1 && ($package_selected!=$_POST['pkg_id'] || $package_limit <= $user_limit_post )&& $_REQUEST['pid']=='' && (@$_REQUEST['package_free_submission'] == '' || @$_REQUEST['package_free_submission'] <=0 )) || ($payable_amount > 0 && $listing_price_info[0]['package_type']==2 && $subscription_as_pay_post!=1 && @$_REQUEST['upgrade'] == 'upgrade'))
		{ 
			$is_package=1;
		}
		else
		{
			$is_package=0;
		}
		
		update_post_meta($post_id,'is_package_type',$is_package);
		
		if($package_post>$user_limit_post && $package_selected==$_REQUEST['package_select'])
		{			
			$limit_post=get_user_meta($current_user_id,'total_list_of_post',true);
			update_user_meta($current_user_id,$submit_post_type.'_list_of_post',$limit_post+1);
			update_user_meta($current_user_id,'total_list_of_post',$limit_post+1);
			update_user_meta($current_user_id,$submit_post_type.'_package_select',$_REQUEST['package_select']);
			update_user_meta($current_user_id,'package_selected',$_REQUEST['package_select']);
		}else
		{
			update_user_meta($current_user_id,'package_selected',$_REQUEST['package_select']);
			update_user_meta($current_user_id,$submit_post_type.'_package_select',$_REQUEST['package_select']);
			update_user_meta($current_user_id,'total_list_of_post',1);
		}			
		/* End Insert User meta fields */

        /*         
         * insert post category
         */
		if(isset($_REQUEST['category']) && $_REQUEST['category']!=""){
			$_REQUEST['all_cat']='';
			foreach ($_REQUEST['category'] as $value) {				
				$value_id=explode(',',$value);
				$_REQUEST['all_cat'].=$value_id[0].'|';
			}

			$_REQUEST['all_cat']=substr($_REQUEST['all_cat'],0,-1);
			
			$category_ids=explode('|',$_REQUEST['all_cat']);			
			wp_delete_object_term_relationships( $post_id, $_REQUEST['cur_post_taxonomy']);
			foreach($category_ids as $key=> $_tax)
			{
				$term = get_term_by('id',$_tax,$_REQUEST['cur_post_taxonomy']);
				if(!empty($term)){
					wp_set_post_terms($post_id,$_tax,$_REQUEST['cur_post_taxonomy'],true);
				}
			}

		}

		/* Sample listing image */
		$uploaddir = TEMPLATIC_FRONTEND_DIR."/img/";
		$image='sample_image.jpg';
		$dirinfo = wp_upload_dir();
		$path = $dirinfo['path'];
		$url = $dirinfo['url'];
		$subdir = $dirinfo['subdir'];
		$basedir = $dirinfo['basedir'];
		$baseurl = $dirinfo['baseurl'];
		$upload_img_path=$uploaddir._wp_relative_upload_path( $image );
		$wp_filetype = wp_check_filetype(basename($image), null );

		$path_info = pathinfo($image);
		$file_extension = $path_info["extension"];		
		if(file_exists($upload_img_path)){
			for($i=0;$i<1;$i++){
				$image = basename($image,".$file_extension").".".$file_extension;
				$dest=$path.'/'._wp_relative_upload_path( $image);
				copy($upload_img_path, $dest);
				$attachment = array('guid' => $url.'/'._wp_relative_upload_path( $image ),
								'post_mime_type' => $wp_filetype['type'],
								'post_title' => preg_replace('/\.[^.]+$/', '', basename($image)),
								'post_content' => '',
								'post_status' => 'inherit'
							);						
				$img_attachment=substr($subdir.'/'.$image,1);
				
				$image_attach_id[]=$attach_id = wp_insert_attachment( $attachment, $img_attachment, $post_id );
				$upload_img_path=$path.'/'._wp_relative_upload_path( $image );
				$attach_data = wp_generate_attachment_metadata( $attach_id, $upload_img_path );
				wp_update_attachment_metadata( $attach_id, $attach_data );
			}

			do_action('frontend_editor_image_upload',$image_attach_id,$post_id);
		}
		/* End Sample listing image */
		/*
		 * save custom fields
		 */
		
		foreach($default_custom_metaboxes as $key=>$value){
			if($key!='category' && $key!='post_title' && $key!='post_content' && $key!='post_excerpt' && $key!='post_images' && $key!='post_city_id'){
				if($value['type']=='heading_type')
					continue;
				
				if(($value['default']=="" || $value['default']!="" )  && $value['type']!='multicheckbox'){				
					update_post_meta($post_id,$key,$value['default']);
				}elseif($value['type']=='multicheckbox'){
					update_post_meta($post_id,$key,explode(',',$value['default']));
				}

				/* if default value blank then take first option value*/
				if($value['default']=='' && $value['type']=='multicheckbox'){
					update_post_meta($post_id,$key,array($value['options'][0]));
				}elseif($value['default']=='' && ($value['type']=='radio' || $value['type']=='select')){
					update_post_meta($post_id,$key,$value['options'][0]);
				}

				if($value['type']=='geo_map'){
					update_post_meta($post_id,$key,$value['default']);
					$v=$value['default'];
					
					$http=(is_ssl())?"https://":"http://";
					$v = str_replace(' ','+',convert_chars(addslashes(iconv('', 'utf-8',$v))));

					$geocode = file_get_contents($http.'maps.google.com/maps/api/geocode/json?address='.$v.'&sensor=false');
					$output= json_decode($geocode);					
					$lat = $output->results[0]->geometry->location->lat;
					$long = $output->results[0]->geometry->location->lng;
					
					if(!$lat)
						$lat='40.721663613669676';
					if(!$long)
						$long='-74.0004371';
					
					update_post_meta($post_id, 'geo_latitude', $lat);
					update_post_meta($post_id, 'geo_longitude', $long);		
					update_post_meta($post_id, 'map_view', 'Road Map');
					update_post_meta($post_id, 'zooming_factor', '13');

				}
			}
		}

		update_post_meta($post_id, 'frontend_submit', '1');
		
		$permalink=get_permalink($post_id);
		if(false===strpos($permalink,'?')){
		    $url_glue = '?';
		}else{
			$url_glue = '&';	
		}
	 	$send_data['redirect_url']=$permalink.$url_glue.'action=edit&preview=true';
	 	$send_data['recaptcha_error']='';
		
		
	}

	wp_send_json($send_data);
	exit;
}




/*
 * display front end edit payment information box in detail page for save edit post and also pay for publishing post
 */
add_action('directory_before_container_breadcrumb','frontend_edit_paymentinfo_box',20);
add_action('event_before_container_breadcrumb','frontend_edit_paymentinfo_box',20);
add_action('tmpl_before_container_breadcrumb','frontend_edit_paymentinfo_box',20);
add_action('tmpl_before_frontend_edit_container','frontend_edit_paymentinfo_box',20);
function frontend_edit_paymentinfo_box(){
	global $wpdb,$post,$payable_amount,$alive_days,$monetization;
	$is_edit='';
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
		$is_edit=1;
	}
	if(is_single() && $is_edit==1){
		if(!isset($_REQUEST['front_edit'])):
		$custom_post_type=get_option('templatic_custom_post');		
		$post_type_label=strtolower($custom_post_type[get_post_type()]['labels']['singular_name']);
		?>
		<div class="submit-progress-steps columns clearfix">
			<ul>
				<li><span><?php _e('1. Enter mandatory fields',FE_DOMAIN);?></span></li>
				<li><span class="active"><?php echo sprintf(__('2. Add your %s',FE_DOMAIN),$post_type_label);?></span></li>
				<li><span><?php echo _e('3. Pay & Publish',FE_DOMAIN);?></span></li>
			</ul>
		</div>
	<?php
		endif;
		$package_select=get_post_meta($post->ID,'package_select',true);
		$total_price=get_post_meta($post->ID,'total_price',true);
		
		$listing_price_pkg = $monetization->templ_get_price_info($package_select,$total_price);	

		/* Get payable amount and alive days */
		$payable_amount = $total_price;
		$alive_days = $listing_price_pkg[0]['alive_days'];
		if(isset($listing_price_pkg[0]['alive_days'])){
			$alive_days = $listing_price_pkg[0]['alive_days'];
		}else{
			$alive_days = 30;
		}
		/* fetch coupon code */
		$post_id=$post->ID;
		$add_coupon=get_post_meta($post->ID,'add_coupon',true);
		if($add_coupon!=""){
			$payable_amount=get_post_meta($post->ID,'payable_amount',true);
		}
		$currency = do_action('before_currency').get_option('currency_symbol').do_action('after_currency');
		$position = get_option('currency_pos');
		if($position == '1'){
			$amt_display = $currency.$payable_amount;
		} else if($position == '2'){
			$amt_display = $currency.' '.$payable_amount;
		} else if($position == '3'){
			$amt_display = $payable_amount.$currency;
		} else {
			$amt_display = $payable_amount.' '.$currency;
		}
		if($payable_amount > 0 || $total_price > 0 )
		{				
			$message = __('You are going to submit a post that will cost you ',FE_DOMAIN).($amt_display)." ".__('for',FE_DOMAIN)." ".$alive_days." ".__('days',FE_DOMAIN); 
			
		}else if($payable_amount==0){
			$message = __('Once you are done editing the listing data publish your listing for ',FE_DOMAIN).$alive_days." ".__('days.',FE_DOMAIN);
		}

		if(isset($_REQUEST['front_edit']) && $_REQUEST['front_edit']==1){
			$message = sprintf(__('Click on the Update button below once you are done editing the %s details.',FE_DOMAIN),get_post_type());
		}
	global $current_user;
	$form_action_url=$_SERVER['REQUEST_URI'];
	?>
	<div class="frontend_published_box yellow-panel">
		<span class="frontend_edit_arrow"></span>
		<form method="post" action="<?php echo $form_action_url; ?>" id="frontendedit_payment_form" name="paynow_frm"  >
			<?php if(!isset($_REQUEST['front_edit'])):?>
			<h3><?php echo sprintf(__('Awesome! Your sample listing below is ready for you to edit.',FE_DOMAIN),get_post_type());?></h3>
			<?php endif;?>			
			<p class="frontend_edit_message">
				<?php 
				if(isset($_REQUEST['front_edit']) && $_REQUEST['front_edit']==1){
					echo $message;
				}else{
					echo sprintf(__('Simply click on the content, tabs or fields below to replace the samples with your own information.',FE_DOMAIN),get_post_type());
				}
				?>
			</p>			
	        <input type="hidden" name="frontend_edit_paynow" value="1">
			<input type="hidden" name="prev_page" id="prev_page" value="<?php echo $_SERVER["HTTP_REFERER"]; ?>">
		    <input type="hidden" name="paynow" value="1" />		    
			<span id="frontend_edit_post_process" style="display:none;"><i class="fa fa-circle-o-notch fa-spin"></i></span>
			<?php if(isset($_REQUEST['front_edit']) && $_REQUEST['front_edit']==1):?>
		    	<input type="hidden" id="front_edit" name="front_edit" value="<?php echo $post->ID;?>" />
		    	<span id="frontend-edit-save" class="frontend-edit-submit button button-primary" data-working="Updating…" data-default="Update" ><?php echo __('Update',FE_DOMAIN)?></span>
		    <?php else:?>
		    	<span id="frontend-edit-save" class="frontend-edit-submit button button-primary" data-working="Updating…" data-default="Update" ><?php echo __('Save Draft',FE_DOMAIN)?></span>
		    <?php endif;?>			
			<!-- Save frontend editing data -->
			<?php if(!isset($_REQUEST['front_edit'])):?>
			<!--a href="#frontend_templatic_payment" id='frontendedit_paynow' class='publist-listing button main_btn'><?php //_e('Publish',FE_DOMAIN); ?></a-->	
            <a href="javascript:void(0);" data-reveal-id="frontend_templatic_payment" id='frontendedit_paynow' class='publist-listing button main_btn'><?php _e('Publish',FE_DOMAIN); ?></a>	
			<!--input type='submit' name='paynow' id='frontendedit_paynow'  value='<?php _e($btn_value,FE_DOMAIN); ?>' class='publist-listing button' /-->
			<?php endif;?>
			<?php if(isset($_REQUEST['front_edit'])):?>
				<span id="frontend_edit_post_save" style="display:none;"><?php echo sprintf(__('Your %s updated',FE_DOMAIN),get_post_type());?></span>
			<?php else:?>
				<span id="frontend_edit_post_save" style="display:none;"><?php echo sprintf(__('Your %s draft updated',FE_DOMAIN),get_post_type());?></span>
			<?php endif;?>
			<a href="<?php echo $_SERVER['REQUEST_URI'];?>"  class="pub-link reset-link"><?php _e('Reset',FE_DOMAIN);?></a>
			<a data-url="<?php echo get_permalink($post->ID);?>" href="javascript:void(0)" class="pub-link priview-listing-link" id="priview-listing-link"><?php _e('Preview Listing',FE_DOMAIN);?></a>
			<!--a href="<?php echo get_author_posts_url($current_user->ID);?>"  class="frontend_edit_button pub-link priview-listing-link"><?php //_e('My Dashboard',FE_DOMAIN); ?></a-->
			<?php
			if(!isset($_REQUEST['front_edit'])):

				$args=array(	
					'post_type' => 'page',
					'post_per_page'=>1,
					'post_status' => 'publish',							
					'meta_query' => array(
										array(
											'key' => 'is_frontend_submit_form',
											'value' => '1',
											'compare' => '='
											),				
										array(
											'key' => 'submit_post_type',
											'value' =>  get_post_type( $post->ID ),
											'compare' => '='
											)
										)
					);
				remove_all_actions('posts_where');
				$the_query  = new WP_Query( $args );				
				if( $the_query->have_posts()):
					foreach($the_query as $post):
						if(@$post->ID != ""):
							$page_id=$post->ID;
							if(is_plugin_active('sitepress-multilingual-cms/sitepress.php') && function_exists('icl_object_id')){
								$page_id = icl_object_id( $post->ID, 'page', false, ICL_LANGUAGE_CODE );
								$page_upgrade_link = get_permalink(icl_object_id( $upgradeid, 'page', false, ICL_LANGUAGE_CODE ));
							}
						endif;	
					endforeach;
					wp_reset_query();
					wp_reset_postdata();
				endif;
				$page_link=get_permalink($page_id);
				$pkg_id = get_post_meta($post_id,'package_select',true);
				if(strpos($page_link, "?"))
				{
					$backendedit = $page_link."&amp;pid=".$post_id."&amp;backandedit=1&amp;pkg_id=".$pkg_id;
				}
				else
				{
					$backendedit = $page_link."?pid=".$post_id."&amp;backandedit=1&amp;pkg_id=".$pkg_id;
				}		
			?>
			<a href="<?php echo $backendedit;?>"  class="pub-link admin-link" id="frontend-go-back-link"><?php _e('Go Back & Edit',FE_DOMAIN);?></a>
			<?php endif;?>
		</form>
        
       <?php
			$no_price=1;
			$btn_value = ($payable_amount > 0)? __('Pay &amp; Publish',FE_DOMAIN) : $btn_value = __('Publish',FE_DOMAIN); 
			if($payable_amount>0 && !isset($_REQUEST['front_edit'])){					
				echo "<div id='frontend_templatic_payment' class='reveal-modal templ_popup_forms tmpl_login_frm_data clearfix' data-reveal>";
					echo '<a class="close-reveal-modal" href="#"></a>';	
				echo '<form method="post" action="'.$form_action_url.'" id="frontendedit_payment_form_redirect" name="paynow_frm"  >';
				if(isset($message) && $message != ""): ?>
				<h5 class="post_message"> <?php echo $message; ?> </h5>
				<?php endif;
				templatic_payment_option_preview_page(); // To display the payment gateways on preview page			
				echo "<input type='submit' name='paynow' id='frontendedit_paymentnow'  value='".__($btn_value,FE_DOMAIN)."' class='publist-listing button main_btn' />";
				echo '<input type="hidden" name="frontend_edit_paynow" value="1">';
				echo '<input type="hidden" name="prev_page" id="prev_page" value="'.$_SERVER["HTTP_REFERER"].'">';
		    	echo '<input type="hidden" name="paynow" value="1" />';
				echo "</form>";
				echo "</div>";
				$no_price=0;
			}
		?>
	</div>
	<?php
	}

}

/*
 * insert transaction entry and redirect selected payment method after call frontend edit payment button
 */
add_action('wp_head','tevolution_frontend_edit_paynow',0);
function tevolution_frontend_edit_paynow(){
	global $post,$trans_id,$payable_amount,$last_postid;
	
	if(is_single() && isset($_POST['frontend_edit_paynow']) && $_POST['frontend_edit_paynow']==1){
		global $current_user,$is_package;
		
		$is_package = get_post_meta($post->ID,'is_package_type',true);
		
		$tmpdata = get_option('templatic_settings');
		$current_user = wp_get_current_user();
		$current_user_id = $current_user->ID;
		$payable_amount=get_post_meta($post->ID,'payable_amount',true);

		/* set lat_post id as global */		
		$last_postid=$post->ID;
		/* create transaction entry */
		$trans_id = insert_transaction_detail($_POST['paymentmethod'],$last_postid,0,$is_package);
		
		$paymentmethod = update_post_meta($last_postid,'paymentmethod',$_POST['paymentmethod']);
		$paidamount = update_post_meta($last_postid,'paid_amount',$payable_amount);
		if($payable_amount<=0){			
			$update_post['ID']=$last_postid;
			$update_post['post_status']=$tmpdata['post_default_status'];
			wp_update_post($update_post);			
		}
		
		/* Start send admin email */
		$fromEmail = get_site_emailId_plugin();
		$fromEmailName = get_site_emailName_plugin();
		$store_name = '<a href="'.site_url().'">'.get_option('blogname').'</a>';
		$admin_email_id = get_option('admin_email');
		$tmpdata = get_option('templatic_settings');
		$email_content =  @stripslashes($tmpdata['post_submited_success_email_content']);
		$email_subject =  @stripslashes($tmpdata['post_submited_success_email_subject']);
		
		$email_content_user =  @stripslashes($tmpdata['payment_success_email_content_to_client']);
		$email_subject_user =  @stripslashes($tmpdata['payment_success_email_subject_to_client']);			
			
		$mail_post_type_object = '';
		$mail_post_title ='';
		if($last_postid){
			$mail_post_type_object = get_post_type_object(get_post_type($last_postid));
			$mail_post_title = $mail_post_type_object->labels->menu_name;
		}
			
		if(function_exists('icl_t')){
			icl_register_string(FE_DOMAIN,$mail_post_title,$mail_post_title);
			$mail_post_title = icl_t(FE_DOMAIN,$mail_post_title,$mail_post_title);
		}else{
			$mail_post_title = @$mail_post_title;
		}
		
		if(!$email_subject){
			$email_subject = __('A new post has been submitted on your site',FE_DOMAIN);
		}
		if($_REQUEST['pid']){
			$email_subject = __(sprintf('%s updated of ID:#%s',$mail_post_title,$last_postid));
		}
		if(isset($_SESSION['custom_fields']['renew'])){
			$email_subject = __(sprintf('%s renew of ID:#%s',$mail_post_title,$last_postid));
		}
		if(!$email_content){
			$email_content = __('<p>Howdy [#to_name#],</p><p>A new post has been submitted on your site. Here are some details about it</p><p>[#information_details#]</p><p>Thank You,<br/>[#site_name#]</p>',FE_DOMAIN);
		}
		if($_REQUEST['pid'] ){
			$email_content = __(sprintf('<p>Dear [#to_name#],</p>
			<p>%s has been updated on your site. Here is the information about the %s:</p>
			[#information_details#]
			<br>
			<p>[#site_name#]</p>',$mail_post_title,$mail_post_title),FE_DOMAIN);
		}
		if(isset($_SESSION['custom_fields']['renew'])){
			$email_content = __(sprintf('<p>Dear [#to_name#],</p>
			<p>%s has been renew on your site. Here is the information about the %s:</p>
			[#information_details#]
			<br>
			<p>[#site_name#]</p>',$mail_post_title,$mail_post_title),FE_DOMAIN);
			
		}				
			
		if(!$email_subject_user){
			$email_subject_user = __(sprintf('New %s listing of ID:#%s',$mail_post_title,$last_postid),FE_DOMAIN);	
		}
		if($_REQUEST['pid']){
			$email_subject_user = __(sprintf('%s updated of ID:#%s',$mail_post_title,$last_postid),FE_DOMAIN);
		}
		if(isset($_SESSION['custom_fields']['renew']))
		{
			$email_subject_user = __(sprintf('%s renew of ID:#%s',$mail_post_title,$last_postid),FE_DOMAIN);
			
		}	
		if(!$email_content_user)
		{
			$email_content_user = __("<p>Hello [#to_name#]</p><p>Here's some info about your payment...</p><p>[#transaction_details#]</p><p>If you'll have any questions about this payment please send an email to [#admin_email#]</p><p>Thanks!,<br/>[#site_name#]</p>",FE_DOMAIN);
		}
		if($_REQUEST['pid'])
		{
			$email_content_user = __(sprintf('<p>Dear [#to_name#],</p><p>Your %s has been updated by you . Here is the information about the %s:</p>[#information_details#]<br><p>[#site_name#]</p>',$mail_post_title,$mail_post_title),FE_DOMAIN);
		}
		if(isset($_SESSION['custom_fields']['renew']))
		{
			$email_content_user = __(sprintf('<p>Dear [#to_name#],</p><p>Your %s has been renew by you. Here is the information about the %s:</p>[#information_details#]<br><p>[#site_name#]</p>',$mail_post_title,$mail_post_title),FE_DOMAIN);
			
		}	
		$information_details = "<p>".__('ID',FE_DOMAIN)." : ".$last_postid."</p>";
		$information_details .= '<p>'.__('View more detail of',FE_DOMAIN).' <a href="'.get_permalink($last_postid).'">'.stripslashes($my_post['post_title']).'</a></p>';
		global $payable_amount;
		if($payable_amount > 0){
			$information_details .= '<p>'.__('Payment Status:',FE_DOMAIN).' <b>'.__('Pending',FE_DOMAIN).'</b></p>';
			$information_details .= '<p>'.__('Payment Method:',FE_DOMAIN).' <b>'.ucfirst(@$_POST['paymentmethod']).'</b></p>';
		}else{
			$information_details .= '<p>'.__('Payment Status:',FE_DOMAIN).' <b>'.__('Success',FE_DOMAIN).'</b></p>';
		}	
		$post_type=$post->post_type;
		$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type,'public'   => true, '_builtin' => true ));		
		$post_category = wp_get_post_terms($last_postid, $taxonomies[0], array("fields" => "ids"));		
		$show_on_email=get_frontend_single_post_fields($post_type);
		$suc_post = $post;
		$my_post = get_post($last_postid);
		$information_details='<style type="text/css">
				.cust_feild_details {
					max-width: 800px;
					}
					
				.cust_feild_details li  {
					border-bottom: 1px solid #ccc;
					padding: 8px;
					list-style: none;
					}
					
				.cust_feild_details li label {
					display: inline-block;
					vertical-align: top;
					width: 180px;
					}
		</style>';
			if($show_on_email)
			{
				$information_details.='<ul class="cust_feild_details">';
				foreach($show_on_email as $key=>$val)
				{		
					if($key == 'category')
					{
						$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $suc_post->post_type,'public'   => true, '_builtin' => true ));	
						
						$category_name = wp_get_post_terms($last_postid, $taxonomies[0]);
						if($category_name)
						{
							$_value = '';
							
							foreach($category_name as $value)
							 {
								$_value .= $value->name.",";
							 }
							 $information_details.= "<li><label>".__(sprintf('%s Category',$mail_post_title)).": </label> ".substr($_value,0,-1)."</li>";
						}
					}
					if($key=='post_title' && $val['show_in_email'])
					{
						$information_details.= '<li><label>'.$val['label'].' :</label>'.$my_post->post_title.'</li>';
					}
					if($key=='post_content' && $val['show_in_email'] && $my_post->post_content!='')
					{
						$information_details.= '<li><label>'.$val['label'].' :</label>'.$my_post->post_content.'</li>';
					}
					if($key=='post_excerpt' && $val['show_in_email'] && $my_post->post_excerpt!='')
					{
						$information_details.= '<li><label>'.$val['label'].' :</label>'.$my_post->post_excerpt.'</li>';
					}
					
					if($val['type'] == 'multicheckbox' && get_post_meta($last_postid,$val['htmlvar_name'],true) !='' && $val['show_in_email']=='1')
					{
						$information_details.='<li><label>'.$val['label'].' :</label> '. apply_filters('tevolution_submited_email', implode(",",get_post_meta($last_postid,$val['htmlvar_name'],true)),$val['htmlvar_name']).'</li>';
					}elseif($val['type']=='upload' && get_post_meta($last_postid,$val['htmlvar_name'],true) !='' && $val['show_in_email']=='1'){
						
						$value=apply_filters('tevolution_submited_email',get_post_meta($last_postid,$val['htmlvar_name'],true),$val['htmlvar_name']);
						$information_details.= '<li><label>'.$val['label'].' :</label> <img src="'.$value.'" width="200"></li>';
					}else{					
						if($val['show_in_email']=='1' && get_post_meta($last_postid,$val['htmlvar_name'],true)!="")
						{
							$information_details.= '<li><label>'.$val['label'].' :</label> '.apply_filters('tevolution_submited_email',get_post_meta($last_postid,$val['htmlvar_name'],true),$val['htmlvar_name']).'</li>';
						}
					}
					
				}
				if(get_post_meta($last_postid,'package_select',true))
				{
						$package_name = get_post(get_post_meta($last_postid,'package_select',true));
						 $information_details.= "<li><h4>".__('Price Package Information',FE_DOMAIN)."</h4></li>";
						 $information_details.= "<li><label>".__('Package Type',FE_DOMAIN).": </label>".$package_name->post_title."</li>";
					 
				}
				if(get_post_meta($last_postid,'alive_days',true))
				{
					 $information_details.= "<li><label>".__('Validity',FE_DOMAIN).": </label> ".get_post_meta($last_postid,'alive_days',true).' '.__('Days',FE_DOMAIN)."</li>";
				}
				if(get_user_meta($suc_post->post_author,'list_of_post',true))
				{
					 $information_details.= "<li><label>".__('Submited number of posts',FE_DOMAIN).": </label> ".get_user_meta($suc_post->post_author,'list_of_post',true)."</li>";
				}
				if(get_post_meta(get_post_meta($last_postid,'package_select',true),'recurring',true))
				{
					$package_name = get_post(get_post_meta($last_postid,'package_select',true));
					 $information_details.= "<li><label>".__('Recurring Charges',FE_DOMAIN).": </label> ".fetch_currency_with_position(get_post_meta($last_postid,'paid_amount',true))."</li>";
				}
				$information_details.='</ul>';
			}
			
			$search_array = array('[#to_name#]','[#information_details#]','[#transaction_details#]','[#site_name#]','[#submited_information_link#]','[#admin_email#]');
			$uinfo = get_userdata($current_user_id);
			$user_fname = $uinfo->display_name;
			$user_email = $uinfo->user_email;
			$link = get_permalink($last_postid);
			$replace_array_admin = array($fromEmailName,$information_details,$information_details,$store_name,'',get_option('admin_email'));
			$replace_array_client =  array($user_fname,$information_details,$information_details,$store_name,$link,get_option('admin_email'));
			$email_content_admin = str_replace($search_array,$replace_array_admin,$email_content);
			$email_content_client = str_replace($search_array,$replace_array_client,$email_content_user);

			templ_send_email($fromEmail,$fromEmailName,$fromEmail,$fromEmailName,$email_subject,$email_content_admin,$extra='');///To admin email			
			templ_send_email($fromEmail,$fromEmailName,$user_email,$user_fname,$email_subject_user,$email_content_client,$extra='');//to client email			
		/* End send admin email */
		if( ($payable_amount != '' || $payable_amount >= 0) && @$_REQUEST['paymentmethod']){
			payment_menthod_response_url(@$_REQUEST['paymentmethod'],$last_postid,'0',$last_postid,$payable_amount);

		}else{
			$suburl = "&pid=".$last_postid;
			if(is_plugin_active('sitepress-multilingual-cms/sitepress.php')){
				global $sitepress;
				if(isset($_REQUEST['lang'])){
					$url = get_option('siteurl').'/?page=success&lang='.$_REQUEST['lang'].$suburl;
				}elseif($sitepress->get_current_language()){
					$url = get_option( 'siteurl' ).'/'.$sitepress->get_current_language().'/?page=success'.$suburl;
						if($sitepress->get_default_language() != $sitepress->get_current_language()){
							$url = get_option( 'siteurl' ).'/'.$sitepress->get_current_language().'/?page=success'.$suburl;
						}else{
							$url = get_option( 'siteurl' ).'/?page=success'.$suburl;
						}
				}else{
					$url = get_option('siteurl').'/?page=success'.$suburl;
				}
			}else{
				$url = get_option('siteurl').'/?page=success'.$suburl;
			}			
			wp_redirect($url);
			exit;
		}
	}

}


/*
 * set froent end edit post link on author dashboard post edit button link
 */
add_filter('tevolution_post_edit_link','tevolution_frontend_post_edit_link',10,3);
function tevolution_frontend_post_edit_link($edit_url,$post_id,$action){	
	
	$post_link=get_permalink($post_id);			
	if(strpos($post_link, "?"))
	{
		$edit_url = $post_link."&amp;action=edit&amp;front_edit=1";
	}
	else
	{
		$edit_url = $post_link."?action=edit&amp;front_edit=1";
	}
	
	return $edit_url;
}

/*
 * set froent end editor renew submit post link on author dashboard post renew button link
 */
add_filter('tevolution_post_renew_link','tevolution_frontend_post_renew_link',10,3);
function tevolution_frontend_post_renew_link($renew_url,$post_id,$action){
	
	$args=array(	
		'post_type' => 'page',
		'post_per_page'=>1,
		'post_status' => 'publish',							
		'meta_query' => array(
							array(
								'key' => 'is_frontend_submit_form',
								'value' => '1',
								'compare' => '='
								),				
							array(
								'key' => 'submit_post_type',
								'value' =>  get_post_type( $post_id ),
								'compare' => '='
								)
							)
		);
	remove_all_actions('posts_where');
	$the_query  = new WP_Query( $args );		
	if( $the_query->have_posts()):
		foreach($the_query as $post):
			if(@$post->ID != ""):
				$page_id=$post->ID;
				if(is_plugin_active('sitepress-multilingual-cms/sitepress.php') && function_exists('icl_object_id')){
					$page_id = icl_object_id( $post->ID, 'page', false, ICL_LANGUAGE_CODE );
					$page_upgrade_link = get_permalink(icl_object_id( $upgradeid, 'page', false, ICL_LANGUAGE_CODE ));
				}
			endif;	
		endforeach;
	endif;
	$page_link=get_permalink($page_id);		
	if(strpos($page_link, "?"))
	{
		$renew_url = $page_link."&amp;pid=".$post_id."&amp;renew=1";
	}
	else
	{
		$renew_url = $page_link."?pid=".$post_id."&amp;renew=1";
	}

	return $renew_url;
}


/*
 * Use inline script for frontend editor custom fields validation
 */
add_action('wp_footer','frontend_edit_textfields');
function frontend_edit_textfields(){
	if(is_single() && isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
		global $post;
		$custom_fields= get_frontend_single_post_fields(get_post_type());		
		?>
		<script type="text/javascript">
		jQuery('.frontend-entry-title').keypress(function(e){if (e.keyCode == 13) {e.preventDefault();}});
		<?php
		foreach ($custom_fields as $key => $value) {
			if($key=='post_content'){
				?>
				//jQuery('.frontend-entry-content div.templatic-element').live("blur keyup change",function(e){frontend_check_validation(e,'<?php echo $value["validation_type"]?>','templatic-element','<?php echo $value["field_require_desc"]?>')});
				<?php
			}
			if($key=='post_title' && $value['validation_type']!=''){
				?>
				jQuery('.frontend-entry-title').live("blur keyup change",function(e){frontend_check_validation(e,'<?php echo $value["validation_type"]?>','frontend-entry-title','<?php echo addslashes($value["field_require_desc"]);?>')});
				<?php
			}
		
			if(($value['type']=='text' ||  $value['type']=='geo_map') && $key!="category" && $key!="post_title" && $key!="post_content" && $key!="post_excerpt" && $key!="post_images"){
				if($value['validation_type']=='email'){?>
					jQuery('.frontend_<?php echo $key;?>').live("blur keyup change",function(e){frontend_check_validation(e,'<?php echo $value["validation_type"]?>','frontend_<?php echo $key?>','<?php echo addslashes($value["field_require_desc"]);?>')});
				<?php	
				}else{
				?>
					jQuery('.frontend_<?php echo $key;?>').live("keydown blur keyup change",function(e){frontend_check_validation(e,'<?php echo $value["validation_type"]?>','frontend_<?php echo $key?>','<?php echo addslashes($value["field_require_desc"]);?>')});
				<?php
				}

			}			
		}
		?>		
		</script>
		<?php
	}
}

/*
 * Display event type custom field on froentend editting mode on edit event detail page
 */
add_action('event_user_attend','frontend_editor_event_user_attend');
function frontend_editor_event_user_attend(){
	global $htmlvar_name,$heading_title,$post;
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
		echo "<div class='frontend_edit_event_type clearfix'>";
			echo "<label>".$htmlvar_name['event_info']['event_type']['label']."</label>";
			echo "<ul class='hr_input_radio'>";
			$eventtype==get_post_meta($post->ID,'event_type',true);
			$option_values_arr = explode(',',$htmlvar_name['event_info']['event_type']['option_values']);
			$option_titles_arr = explode(',',$htmlvar_name['event_info']['event_type']['option_title']);
			if($option_title ==''){  $option_titles_arr = $option_values_arr;  }
			$event_type = array("Regular event", "Recurring event");
			$key='event_type';
			$value=get_post_meta($post->ID,'event_type',true);
			
			for($i=0;$i<count($event_type);$i++)
			{
				$chkcounter++;
				$seled='';				
				if($eventtype==$event_type[$i] ){ $seled='checked="checked"';}
				if (isset($value) && trim($value) == trim($option_values_arr[$i])){ $seled='checked="checked"';}				
				if (trim(@$value) == trim($event_type[$i])){ $seled="checked=checked";}
				$optiontitle=($option_titles_arr[$i])? $option_titles_arr[$i]:$event_type[$i] ;
				echo '<li>
						<input name="frontend_'.$key.'"  id="'.$key.'_'.$chkcounter.'" type="radio" value="'.$event_type[$i].'" '.$seled.'  '.$extra_parameter.' /> <label for="'.$key.'_'.$chkcounter.'">'.$optiontitle.'</label>
					</li>';				
			}
			do_action('tmpl_custom_fields_event_type_after');
			echo "</ul>";
		echo "</div>";
	}
}

/*
 * add frontend editor plugin class in detail page body class
 */
add_filter('body_class','frontend_edit_body_class');
function frontend_edit_body_class($body_class){
	if(is_single() && isset($_REQUEST['action']) && $_REQUEST['action']=='edit' ){
		$body_class[]='frontend_editor';
	}
	return  $body_class;
}

/*
 * display frontend editor link on detail page edit link
 */
add_filter('get_edit_post_link','frontend_editor_edit_post_link',10,3);
function frontend_editor_edit_post_link($edit_url,$post_id,$context){
	if(is_admin()){
		return $edit_url;
	}
	$args=array(	
		'post_type' => 'page',
		'post_per_page'=>1,
		'post_status' => 'publish',							
		'meta_query' => array(
							array(
								'key' => 'is_frontend_submit_form',
								'value' => '1',
								'compare' => '='
								),				
							array(
								'key' => 'submit_post_type',
								'value' =>  get_post_type( $post_id ),
								'compare' => '='
								)
							)
		);
	remove_all_actions('posts_where');
	$the_query  = new WP_Query( $args );		
	if( $the_query->have_posts()):
		foreach($the_query as $post):
			if(@$post->ID != ""):
				$page_id=$post->ID;
				if(is_plugin_active('sitepress-multilingual-cms/sitepress.php') && function_exists('icl_object_id')){
					$page_id = icl_object_id( $post->ID, 'page', false, ICL_LANGUAGE_CODE );					
				}
			endif;	
		endforeach;
	endif;
	if($page_id!=""){
		$post_link=get_permalink($post_id);
		if(strpos($post_link, "?"))
		{
			$edit_url = wp_nonce_url($post_link."&amp;action=edit&amp;front_edit=1",'edit_link');
		}
		else
		{
			$edit_url = wp_nonce_url($post_link."?action=edit&amp;front_edit=1",'edit_link');
		}
	}

	return $edit_url;
}

/* Return browser information */
function getBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
   
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }
   
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
   
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
   
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
   
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

add_action('before_google_map_container','frontend_editor_before_google_map_container');
add_action('after_google_map_container','frontend_editor_after_google_map_container');
/*
 * display Google map customizer message on frontend editing detail page
 */
function frontend_editor_before_google_map_container(){
	global $post;
	$is_edit='';
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit' && !isset($_REQUEST['page'])){
	  $is_edit=1;
	}
	if($is_edit==1):?>
        <div class="frontedit_map_info yellow-panel clearfix">
        <?php _e("You can also change your map view by selecting the 'Satellite' option on the map and the zooming factor by dragging the slider on the left. You can also drag and drop the map icon to set the Address",FE_DOMAIN);?>
        </div>
        <input type="hidden" value="<?php echo get_post_meta($post->ID,'geo_latitude',true);?>" id="geo_latitude" name="geo_latitude" />
        <input type="hidden" value="<?php echo get_post_meta($post->ID,'geo_longitude',true);?>" id="geo_longitude" name="geo_longitude" />
        <input type="hidden" value="<?php echo get_post_meta($post->ID,'zooming_factor',true);?>" id="zooming_factor" name="zooming_factor">
        <input type="hidden" value="<?php echo get_post_meta($post->ID,'map_view',true);?>" id="map_view" name="map_view">
    <div class="map_customizer_wrap">
        <div id="panel" style="margin-left:-100px">        
        <input type="button" onclick="toggleStreetView();" value="<?php _e('Toggle Street View',FE_DOMAIN);?>">
        </div>
    <?php endif;
}
/*
 * Return: close map customizer wrap div
 */
function frontend_editor_after_google_map_container(){
	$is_edit='';
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit' && !isset($_REQUEST['page'])){
	  $is_edit=1;
	}
	if($is_edit==1):?>
		</div>
	<?php endif;
}

/* Display oembed video related description */
add_action('oembed_video_description','frontend_oembed_video_description');
function frontend_oembed_video_description(){
	?>
    <div class="frontedit_video_info yellow-panel">
		<?php _e('Embed your video using its direct link. See the list of supported video sites <a href="https://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">here</a>.',FE_DOMAIN);?>
    </div>
    <?php	
}

/* 
 * Display frontend edit status in backend if user didnt submit on detail page and leave without payment
 */
add_filter('tmpl_get_transaction_status','frontend_get_transaction_status',10,3);
function frontend_get_transaction_status($result,$tid,$pid){
	$frontend_submit=get_post_meta($pid,'frontend_submit',true);
	if($tid==0 && $frontend_submit==1){
		$result = '<a style="color:#E66F00; font-weight:normal;"  href="javascript:void(0);">'.__('Submission In Progress',FE_DOMAIN).'</a>';
		$result;
	}
	return $result;
}

/*
 * Delete post if user didnt payment
 */
add_action('admin_init','frontend_delete_submit_post_type',1);
function frontend_delete_submit_post_type(){
$time = gmdate( 'Y-m-d H:i:s', ( time() - 3600 + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS ) ) );
	global $pagenow,$wpdb;
	if($pagenow=='edit.php'){
		$post_type=(isset($_REQUEST['post_type']) && $_REQUEST['post_type']!="")?$_REQUEST['post_type']: 'post';
		$args=array('post_type' => $post_type,
					'posts_per_page' => -1	,
					'meta_key' => 'frontend_submit',
					'meta_value' => '1',
					'post_status' => 'draft',
					
					);		
		$post_query = new WP_Query($args);
		if($post_query->have_posts()){
			$i=0;
			while ($post_query->have_posts()) : $post_query->the_post();
				if(strtotime($time) > strtotime($post_query->posts[$i]->post_date) && get_post_meta(get_the_ID(),'frontend_submit',true) ){
					wp_delete_post(get_the_ID(),1);
				}
				$i++;
			endwhile;
		}
		
	}	
}

/* Delete frontend submit option after publish button */ 
add_action('wp_head','frontend_delete_submit_post_type_on_paynowform',0);
function frontend_delete_submit_post_type_on_paynowform(){
	global $post;
	if(is_single()){
		$post_id=$post->ID;		
		if(isset($_REQUEST['frontend_edit_paynow']) && $_REQUEST['frontend_edit_paynow']==1){
			delete_post_meta($post_id,'frontend_submit');	
		}
	}
	
}


/*
 * Return category  custom field from field on submit form page
 */
function front_end_fetch_submit_page_form_fields($taxonomy='')
{
	global $post,$wpdb;
	$tmpdata = get_option('templatic_settings');
	$form_fields = array();	
	$form_fields['category'] = array(
							   'name' 	      => $taxonomy,
							   'espan'	      => 'category_span',
							   'type'	           => $tmpdata['templatic-category_type'],
							   'text'	           => __('Please select Category',DOMAIN),
							   'validation_type' => 'require'
							   );
	
	
	return $form_fields;
}

?>