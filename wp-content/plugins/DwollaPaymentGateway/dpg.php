<?php
/*
Plugin Name: Dwolla Payment Gateway
Plugin URI: http://www.iotis.com	
Description: This plugin will generate dwolla button on the basis of shortcode [dwolla_button].
Author: Muhammad Junaid Tufail
Version: 1.0
Author URI: http://www.facebook.com/muhammadjunaidtufail
 */
function cb_init()
{
	register_setting('cb_options','cb_DestinationAccId');
	register_setting('cb_options','cb_ButtonText');
	register_setting('cb_options','cb_ProdName');
	register_setting('cb_options','cb_ProdDesc');
	register_setting('cb_options','cb_Amount');
	register_setting('cb_options','cb_ShippCost');
	register_setting('cb_options','cb_tax');
	register_setting('cb_options','cb_redirecturl');
	register_setting('cb_options','cb_clientid');
	register_setting('cb_options','cb_clientsecret');
	register_setting('cb_options','cb_redirectcomeurl');
	register_setting('cb_options','cb_destaccountid');
}
add_action('admin_init','cb_init');

function cb_option_page()
{
	?>
	<div class="wrap"><?php screen_icon(); ?>
	<h2>Dwolla Payment Gateway Option Page</h2>
	<p>Welcome to the Dwolla Payment Gateway Plugin. Here you can configure plugin settings. Use [dwolla_button] shortcode.</p>
	<form action="options.php" method="POST" id="cc-comments-email-options-form">
	<?php settings_fields('cb_options'); ?>
    <h1> Dwolla Payment Button Options </h1>
    <table border="0">
    <tr>
	<td><h3><label for="cb_DestinationAccId">Data Key: </label> </h3></td>><td><input
		type="text" id="cb_DestinationAccId" name="cb_DestinationAccId" size="70"
		value="<?php echo esc_attr( get_option('cb_DestinationAccId') ); ?>" /> </td>
        </tr>
         <tr>
	<td><h3><label for="cb_ButtonText">Button Text: </label> </h3></td>><td><input
		type="text" id="cb_ButtonText" name="cb_ButtonText" size="30"
		value="<?php echo esc_attr( get_option('cb_ButtonText') ); ?>" /> </td>
        </tr>
        <tr>
	<td><h3><label for="cb_ProdName">Product Name: </label> </h3></td>><td><input
		type="text" id="cb_ProdName" name="cb_ProdName" size="30"
		value="<?php echo esc_attr( get_option('cb_ProdName') ); ?>" /> </td>
        </tr>
       <tr>
	<td><h3><label for="cb_ProdDesc">Product Description: </label> </h3></td>><td><input
		type="text" id="cb_ProdDesc" name="cb_ProdDesc" size="30"
		value="<?php echo esc_attr( get_option('cb_ProdDesc') ); ?>" /> </td>
        </tr>
        <tr>
	<td><h3><label for="cb_Amount">Amount in $: </label> </h3></td>><td><input
		type="text" id="cb_Amount" name="cb_Amount"
		value="<?php echo esc_attr( get_option('cb_Amount') ); ?>" /> </td>
        </tr>
         <tr>
	<td><h3><label for="cb_ShippCost">Shipping Cost: </label> </h3></td>><td><input
		type="text" id="cb_ShippCost" name="cb_ShippCost"
		value="<?php echo esc_attr( get_option('cb_ShippCost') ); ?>" /> </td>
        </tr>
         <tr>
	<td><h3><label for="cb_tax">Tax: </label> </h3></td>><td><input
		type="text" id="cb_tax" name="cb_tax"
		value="<?php if(get_option('cb_tax') == '' ) { echo '0'; } else { echo esc_attr( get_option('cb_tax') ); }?>" /> </td>
        </tr>
        <tr>
	<td><h3><label for="cb_redirecturl">Redirect URL: </label> </h3></td>><td><input
		type="text" id="cb_redirecturl" name="cb_redirecturl" size="40"
		value="<?php echo esc_attr( get_option('cb_redirecturl') ); ?>" /> </td>
        </tr>
        </table>
        
        <h1>Dwolla Payment Options (Using API)</h1>
    <table border="0">
    <tr>
	<td><h3><label for="cb_clientid">Your App Id: </label> </h3></td>><td><input
		type="text" id="cb_clientid" name="cb_clientid" size="70"
		value="<?php echo esc_attr( get_option('cb_clientid') ); ?>" /> </td>
        </tr>
    <tr>
	<td><h3><label for="cb_clientsecret">Your App Secret Key: </label> </h3></td>><td><input
		type="text" id="cb_clientsecret" name="cb_clientsecret" size="70"
		value="<?php echo esc_attr( get_option('cb_clientsecret') ); ?>" /> </td>
        </tr>
    <tr>
	<td><h3><label for="cb_redirectcomeurl">Redirect URL: </label> </h3></td>><td><input
		type="text" id="cb_redirectcomeurl" name="cb_redirectcomeurl" size="70"
		value="<?php echo esc_attr( get_option('cb_redirectcomeurl') ); ?>" /> Will redirect to this URL after allowing app</td> 
        </tr>
        <tr>
	<td><h3><label for="cb_destaccountid">Your Account Id: </label> </h3></td>><td><input
		type="text" id="cb_destaccountid" name="cb_destaccountid" size="70"
		value="<?php echo esc_attr( get_option('cb_destaccountid') ); ?>" /></td> 
        </tr>
         
        </table>
        
	<p><input type="submit" name="submit" value="Save Changes" /></p>
	</form>
	</div>
	<?php
}
function cb_plugin_menu()
{
	add_menu_page('Dwolla Payment Gateway Settings','Dwolla Payment Gateway', 'manage_options', 'cb-links-plugin', 'cb_option_page');
}
add_action('admin_menu', 'cb_plugin_menu');

function cb_dwolla_link()
{

global $wpdb;
	$destaccid = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_DestinationAccId'"));
	$buttext = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_ButtonText'"));
	$prodname = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_ProdName'"));
	$proddesc= $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_ProdDesc'"));
	$Amountin = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_Amount'"));
	$shipcost = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_ShippCost'"));
	$tax = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_tax'"));
	$redurl = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_redirecturl'"));
	
	return '<a data-key="'.$destaccid.'" data-tax="'.$tax.'" data-shipping="'.$shipcost.'" data-amount="'.$Amountin.'" data-desc="'.$proddesc.'" data-name="'.$prodname.'" class="dwolla_button" href="'.$redurl.'">'.$buttext.'</a>';	
	
	}
add_shortcode('dwolla_button', 'cb_dwolla_link');

//$clientappid = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_clientid'"));
	//$slientappsecret = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_clientsecret'"));
	//$redirectbackurl = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_redirectcomeurl'"));

function cb_dwolla_link1()
{

return '<form action="'.plugins_url().'/DwollaPaymentGateway/example.php" method="post" name="form" id="form" class="form" >
    <input name="btnSave" type="Submit" value = "" id = "btnSave" class="formButton" width="100" style="background-image: url('.plugins_url().'/DwollaPaymentGateway/images/badge-accept-dwolla-sm.png); background-repeat:no-repeat; width:150px; height:36px;border:none; margin-top:10px; cursor:pointer" /></form>';    
	
	}
add_shortcode('dwolla_checkout', 'cb_dwolla_link1');
function cb_dwolla_link2()
{
include 'dwolla.php';
$dwolla = new dwolla();
$dwolla->get_oauth_token($_GET['code']);

$balance = $dwolla->balance();  
echo "Your balance is $" . $balance['Response'];

if(isset($_POST["btnSave"]))
{
	global $wpdb;
	$pin = $_POST["pin"];
	$destinationId = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'cb_destaccountid'"));
	$destinationType = 'Dwolla';
	$amount = $_POST["dAmount"];
	$facilitatorAmount = 0;
	$assumeCosts = false;
	$notes = $_POST["dnotes"];
	$data = array(
            'pin'               => $pin,
            'destinationId'     => $destinationId,
            'destinationType'   => $destinationType,
            'amount'            => $amount,
            'facilitatorAmount' => $facilitatorAmount,
            'assumeCosts'       => $assumeCosts,
            'notes'             => $notes
        );
	$reciept = $dwolla->transactions("send", $data);
print_r($reciept);
}

echo '<form action="" method="post" name="form1" id="form1" class="form1" >
<table width="95%" style="margin-left:32px;margin-top:30px;">
  
  <tr>
    <td><label for="pin">Enter Your PIN</label></td>
    <td><input type="text" size="50" id="pin" name="pin" value=""> <label class="opt">(your 4 digit pin)</label></td>
  </tr>
 <tr>
    <td><label for="dAmount">Amount to Transfer</label></td>
    <td><input type="text" size="50" id="dAmount" name="dAmount" value=""> <label class="opt">($)</label></td>
  </tr>
  <tr>
    <td style="vertical-align:top;"><label for="dnotes">Notes</label></td>
    <td><textarea id="dnotes" name="dnotes" cols="40" rows="12" ></textarea> <label class="opt">(Not more then 250 characters)</label></td>
  </tr>
  <tr>
  <td></td>
  <td>
<input name="btnSave" type="Submit" value = "" id = "btnSave" class="formButton" width="100" style="background-image: url('.plugins_url().'/DwollaPaymentGateway/images/btn-pay-with-dwolla.png); background-repeat:no-repeat; width:139px; height:36px;margin-left:50px; border:none; margin-top:10px; cursor:pointer" /></td>
</table></form>';
}
add_shortcode('dwolla_paynow', 'cb_dwolla_link2');
if (function_exists ('shortcode_unautop')) {
	add_filter ('widget_text', 'shortcode_unautop');
}
add_filter ('widget_text', 'do_shortcode');
?>