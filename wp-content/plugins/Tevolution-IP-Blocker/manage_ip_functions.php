<?php
/*FUNCTION TO ADD META BOX ON EDIT POST PAGE */
function admin_init_func()
{
	$posttaxonomy = get_option("templatic_custom_post");	
	if($posttaxonomy){
	foreach($posttaxonomy as $key=>$_posttaxonomy):
		add_meta_box("block_ip", "Block IP", "fetch_ip_post_author",$key, "side", "high");
	endforeach;
	}
}
/* EOF - ADD META BOX */
/* FUNCTION TO FETCH IP ADDRESS OF THE AUTHORS */
function fetch_ip_post_author()
{ ?>
	<script type="text/javascript">
		/* <![CDATA[ */
		function block_ip(postid)
		{
			window.location = "<?php echo site_url(); ?>/wp-admin/post.php?post="+postid+"&action=edit&blockip="+postid;	
		}
		function unblock_ip(postid)
		{
			window.location = "<?php echo site_url(); ?>/wp-admin/post.php?post="+postid+"&action=edit&unblockip="+postid;
		}
		/* ]]> */
	</script>
	<?php global $post;
	$remote_ip =  get_post_meta($post->ID,'remote_ip',true);
	if($remote_ip == '')
	{
		echo "<p>" . IP_IS . ": ".getenv("REMOTE_ADDR"). "</p>";
		echo "<input type='hidden' name='remote_ip' id='remote_ip' value='".getenv("REMOTE_ADDR")."'/>";
		echo "<input type='hidden' name='ip_status' id='ip_status' value='0'/>";
	}else{
		$rip = get_post_meta($post->ID,'remote_ip',true);
		$ipstatus = get_post_meta($post->ID,'ip_status',true);
		if($rip != ""){
			echo SUBMITTED_IP . ": <strong>".$rip."</strong><br/>";
			global $wpdb,$post;
			$ipstatus = get_post_meta($post->ID,'ip_status',true);
			if($ipstatus == 0 || $ipstatus == "")
			{
				$parray = $wpdb->get_results("select * from $wpdb->postmeta where post_id != '".$post->ID."' and meta_value='".$rip."'");
				foreach($parray as $pa)
				{
					$blocked = get_post_meta($pa->post_id,'ip_status',true);
					if($blocked == 1)
					{
						break;
						return $blocked;
					}
				}
				if(isset($blocked) && $blocked != 0)
				{
					echo "<a href='javascript:void(0)' onclick='unblock_ip(".$post->ID.")'>" . UNBLOCK_IP . "</a>";
					update_post_meta($post->ID,'ip_status',1);
				}
				else
				{
					echo "<a href='javascript:void(0)' onclick='block_ip(".$post->ID.")'>" . BLOCK_IP . "</a>";
				}
			}
			else
			{
				echo "<a href='javascript:void(0)' onclick='unblock_ip(".$post->ID.")'>" . UNBLOCK_IP . "</a>";
			}
		}else{
			echo IP_NOT_DETECTED;
		}
	}
}

/* EOF - FETCH IP OF AUTHOR */
/* FUNCTION TO SAVE THE POST DATA */
if(!function_exists('save_post_data'))
{
	function save_post_data($post_id)
	{
		global $globals;
		$pID = @$_POST['post_ID'];
		/* SAVE REMOTE IP */
		if( get_post_meta( $pID, 'remote_ip' ) == "" )
		{
			add_post_meta($pID, 'remote_ip', @$_POST['remote_ip'], true );
		}
		elseif(isset($_POST['remote_ip']) && $_POST['remote_ip'] != get_post_meta($pID, 'remote_ip', true))
		{
			update_post_meta($pID, 'remote_ip', $_POST['remote_ip']);
		}
		elseif(isset($_POST['remote_ip']) && $_POST['remote_ip'] == "")
		{
			delete_post_meta($pID, 'remote_ip', get_post_meta($pID, 'remote_ip', true));
		}
		/* SAVE IP STATUS */
		if( get_post_meta( $pID, 'ip_status' ) == "" )
		{
			add_post_meta($pID, 'ip_status', @$_POST['ip_status'], true );
		}
		elseif(isset($_POST['ip_status']) && $_POST['ip_status'] != get_post_meta($pID, 'ip_status', true))
		{
			update_post_meta($pID, 'ip_status', $_POST['ip_status']);
		}
		elseif(isset($_POST['ip_status']) && $_POST['ip_status'] == "")
		{
			delete_post_meta($pID, 'ip_status', get_post_meta($pID, 'ip_status', true));
		}
	}
}
/* EOF - SAVE POST DATA */
function templ_fetch_ip(){
	global $wpdb,$ip_db_table_name;
	
	$ip_db_table_name = $wpdb->prefix."ip_settings";
	$ip = $wpdb->get_row("select * from $ip_db_table_name where ipaddress = '".getenv("REMOTE_ADDR")."' and ipstatus=1");
	
	return $ip;
}
add_action('save_post', 'save_post_data'); /* CALL A FUNCTION TO SAVE THE POST */
/*
*check whether url is ssl enable or not.
*/
function is_on_ssl_url()
{
	$tmpdata = get_option('templatic_settings');
	if(isset($tmpdata['templatic-is_allow_ssl']) && $tmpdata['templatic-is_allow_ssl'] == 'Yes')
	{
		return true;
	}
	else
	{
		return false;
	}
}
/*
*replace http with https if ssl is enable.
*/
function tmpl_get_ssl_normal_url($url)
{
	if(is_on_ssl_url())
	{
		$url = str_replace('http://','https://',$url);
	}
	return $url;
}