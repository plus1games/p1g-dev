<?php
global $wp_query,$wpdb,$wp_rewrite,$ip_db_table_name,$pagenow;
$ip_db_table_name= $wpdb->prefix ."ip_settings";

if( (isset($_REQUEST['page']) && $_REQUEST['page']=='templatic_settings') && (isset($_REQUEST['tab']) && $_REQUEST['tab']=='security-settings'))
{	
	/* CREATE A TABLE TO STORE THE BLOCK IP ADDRESS DATA */
	if($wpdb->get_var("SHOW TABLES LIKE \"$ip_db_table_name\"") != $ip_db_table_name && $ip_db_table_name !=''){
		$ip_table = 'CREATE TABLE IF NOT EXISTS `'.$ip_db_table_name.'` (
		  `ipid` int(11) NOT NULL AUTO_INCREMENT,
		  `ipaddress` varchar(255) NOT NULL,
		  `ipstatus` varchar(25) NOT NULL,
		  PRIMARY KEY (`ipid`)
		)DEFAULT CHARSET=utf8';
		$wpdb->query($ip_table);
	}
	/* EOF - CREATE A TABLE */
}



/*Generate security tab in general setting */
add_filter('templatic_general_settings_tab', 'security_setting',12); 
/*display security related option in security tab setting */
add_action('templatic_general_data_security-settings','security_setting_general');	

/*
 * Add Filter for create the security settings tab on general setting menu
 */
function security_setting($tabs ) {
	$tabs['security-settings']=__('Security Settings',TMIP_DOMAIN);
	return $tabs;
}
/*
 * Satrt the security main general tab
 */
function security_setting_general($tab)
{		


	$tmpdata = get_option('templatic_settings');
	switch($tab)
	{
		case 'security-settings':
		?>	
       <table id="submit_page_settings" class="tmpl-general-settings form-table active-tab" style="display: table;">
        <tr>
            <th colspan="2">
            	<div class="tevo_sub_title" style="margin-top: 0px;"><?php echo __("IP Blocking and SSL Settings",TMIP_DOMAIN);?></div>
            </th>
        </tr>
        <tr>
            <th><label for="ilc_intro"><?php echo __('Blocked IP addresses',TMIP_DOMAIN); ?></label></th>
            <td>
				<?php global $ip_db_table_name,$wpdb;
				$ip_db_table_name= $wpdb->prefix ."ip_settings";
                $parray = $wpdb->get_results("select ipaddress from $ip_db_table_name where ipstatus='1'");
                $mvalue = ""; 
				foreach($parray as $pay)
                {
					$ip = $pay->ipaddress;
					$val = $pay->ipaddress;
					if($val != ""){
						$mvalue .= $val.",";
					}
                }
				?>
                <textarea name="block_ip" id= "block_ip" class="tb_textarea" ><?php echo trim($mvalue); ?></textarea>
                <input type="hidden" name="ipaddress2" id="ipaddress2" value="<?php echo trim($mvalue); ?>"/>
                <p class="description"><?php echo __("Enter IP addresses that you want to block. Separate multiple IPs with a comma.",TMIP_DOMAIN); ?>.</p>
            </td>
        </tr>
        <tr>
            <th><label><?php echo __('Use SSL on submission and registration pages',TMIP_DOMAIN);	$templatic_is_allow_ssl =  @$tmpdata['templatic-is_allow_ssl']; ?></label></th>
            <td>
                <label for="templatic-is_allow_ssl"> <input type="checkbox" id="templatic-is_allow_ssl" name="templatic-is_allow_ssl" value="Yes" <?php if($templatic_is_allow_ssl == 'Yes' ){?>checked="checked"<?php }?> /> <?php echo __('Enable',TMIP_DOMAIN);?>
                <label for="ilc_tag_class"></label><p class="description"><?php echo __('Enable this option only if you have a SSL certificate for your domain. Enabling it without the certificate will break the registration / submission process.',TMIP_DOMAIN);?>.</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p class="submit" style="clear: both;">
              <input type="submit" name="Submit" class="button button-primary button-hero" value="Save All Settings">
              <input type="hidden" name="settings-submit" value="Y">
            </p>
            </td>
        </tr>
        </table>
		<?php
		break;		
	}
}
/*Finish the main security general tab */


function insert_ip_address_data($block_ip_data){

	global $post,$wpdb;
	$countip = explode(",",$block_ip_data);
	$countoldips = explode(",",$_POST['ipaddress2']);
	$new_array= array_diff($countoldips,$countip);
	/* SHOW ARRAY DIFFERENCE AND UPDATE FIELDS */
	if($new_array != "")
	{
		for($i=0 ; $i <=count($new_array) ; $i++)
		{
			foreach($new_array as $nip)
			{
				$nip = trim($nip);
				if($nip != "")
				{
					$ipres1 = $wpdb->get_results("select * from $wpdb->postmeta where meta_key='remote_ip' and meta_value = '".$nip."'");
					if(mysql_affected_rows() > 0)
					{
						foreach($ipres1 as $ipobj1)
						{
							update_post_meta($ipobj1->post_id,'ip_status',0);
						}
					}
					global $ip_db_table_name;
					$wpdb->update($ip_db_table_name,array('ipstatus' => '0'),array('ipaddress' => $nip));
				}
			}
		}
	}
	$new_insert_array= array_diff($countip,$countoldips);
	/* SHOW ARRAY DIFFERENCE AND INSERT FIELDS */
	if($new_insert_array != "")
	{
		global $ip_db_table_name;
		for($i=0 ; $i <=count($new_insert_array) ; $i++)
		{
			foreach($new_insert_array as $nip)
			{
				if($nip!= "")
				{
					$ipres1 = $wpdb->get_results("select * from $wpdb->postmeta where meta_key='remote_ip' and meta_value = '".$nip."'");
					$ipstatus = $wpdb->get_row("select * from $ip_db_table_name where ipaddress='".$nip."'");
					if($ipstatus != "")
					{
						$wpdb->update($ip_db_table_name,array('ipstatus' => '1'),array('ipaddress' => $nip));
					}
					else
					{
						$ipinsert = $wpdb->query("INSERT INTO $ip_db_table_name(`ipid`, `ipaddress`, `ipstatus`) VALUES (NULL,'".$nip."', '1')");
					}
					if(mysql_affected_rows() > 0)
					{
						foreach($ipres1 as $ipobj1)
						{
							update_post_meta($ipobj1->post_id,'ip_status',0);
						}
					}
					global $ip_db_table_name;
					$wpdb->update($ip_db_table_name,array('ipstatus' => '1'),array('ipaddress' => $nip));
				}
			}
		}
	}
	global $ip_db_table_name;
	for($i =0; $i <= count($countip); $i++)
	{
		$countip[$i];
		if($countip[$i] != "")
		{
			$ipres = $wpdb->get_results("select * from $wpdb->postmeta where meta_value = '".$countip[$i]."'");
			if(mysql_affected_rows() > 0)
			{
				foreach($ipres as $ipobj)
				{
					update_post_meta($ipobj->post_id,'ip_status',1);
				}
			}
		}
	}
	
}
?>