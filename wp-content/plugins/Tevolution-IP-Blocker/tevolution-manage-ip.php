<?php
/*
Plugin Name: Tevolution IP Blocker
Plugin URI: http://templatic.com/docs/tevolution-guide/
Description: A Directory add-on which allows you to block visitors based on their IP. Blocked visitors will not be able to submit listings on the site.
Version: 1.0
Author: Templatic
Author URI: http://templatic.com/
*/


define('MANAGE_IP_FOLDER_NAME','Tevolution-IP-Blocker');
define('MANAGE_IP_VERSION','1.0');
define('MANAGE_IP_PLUGIN_NAME','Tevolution IP Blocker Plugin');
define('MANAGE_IP_SLUG','Tevolution-Manage-IP/tevolution-manage-ip.php');

// Plugin Folder URL
define( 'MANAGE_IP_URL', plugin_dir_url( __FILE__ ) );
// Plugin Folder Path
define( 'MANAGE_IP_DIR', plugin_dir_path( __FILE__ ) );

define( 'TMIP_DOMAIN', 'templatic');  //tevolution* deprecated

/* define security related Constants */	
define('IS',__('Is',TMIP_DOMAIN));
define('IP_NOT_DETECTED',__('IP not detected',TMIP_DOMAIN));
define('IP_IS',__('IP for this post is',TMIP_DOMAIN));
define('SUBMITTED_IP',__('Post is submitted from IP',TMIP_DOMAIN));
define('BLOCK_IP',__('Block IP',TMIP_DOMAIN));
define('UNBLOCK_IP',__('Unblock IP',TMIP_DOMAIN));

$locale = get_locale();
load_textdomain( TMIP_DOMAIN, plugin_dir_path( __FILE__ ).'languages/'.$locale.'.mo' );



add_action('admin_init','templ_tevolution_manage_id_files');
function templ_tevolution_manage_id_files(){
	require_once(MANAGE_IP_DIR.'install.php');
}
add_action("admin_init", "admin_init_func"); /* CALL A FUNCTION TO CREATE A META BOX IN BACK END */

require_once(MANAGE_IP_DIR.'manage_ip_functions.php');

/*tmpl_submit_form_before_content function will be return if any ip address blocked */
add_filter('submit_form_return','tmpl_submit_form_before_content');
function tmpl_submit_form_before_content(){
	$ip = (function_exists('templ_fetch_ip'))?templ_fetch_ip():'';
	if($ip != ""){ 
		echo '<div class="error_msg">';
		_e(BLOCK_IP,FE_DOMAIN);
		echo '</div>';
		return true;
	}	
	return false;
}


/*Manage ip plugin setting link will be display in plgin section  */
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ),'tmpl_manageip_action_links'  );
function tmpl_manageip_action_links($links){
	if(!is_plugin_active('Tevolution/templatic.php')){
		return $links;
	}
	
	$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=templatic_settings&tab=security-settings' ) . '">' . __( 'Settings', TMIP_DOMAIN ) . '</a>',
	);
	return array_merge( $plugin_links, $links );
}


add_action('admin_init','tmpl_manage_ip_auto_update');
function tmpl_manage_ip_auto_update(){
	global $pagenow;
	remove_action( 'after_plugin_row_Tevolution-Manage-IP/tevolution-manage-ip.php', 'wp_plugin_update_row' ,10, 2 );
	/* for auto updates */
	if($pagenow=='plugins.php'){		
		require_once('manage_ip-updates-plugin.php');
		new WPUpdatesManageIPUpdater( 'http://templatic.com/updates/api/index.php', plugin_basename(__FILE__) );
	}
}

/*
 * Function Name: tevolution_update_login
 * Return: update tevolution plugin version after templatic member login
 */
add_action('wp_ajax_tevolution_manage_ip','tevolution_manage_ip_update_login');
function tevolution_manage_ip_update_login()
{
	check_ajax_referer( 'tevolution_manage_ip', '_ajax_nonce' );
	$plugin_dir = rtrim( plugin_dir_path(__FILE__), '/' );	
	require_once( $plugin_dir .  '/templatic_login.php' );	
	exit;
}
?>