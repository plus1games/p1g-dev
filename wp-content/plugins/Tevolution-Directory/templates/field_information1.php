<?php 
$pid = "";
$pid= get_the_ID();
$fielddata = $wpdb->get_results("select * from wp_em_location_fields where post_id ='".$pid."'");
if(!empty($fielddata)){?>
<div class="form_row clearfix custom_fileds field_information" style="width:100%; overflow:scroll;margin-top:20px;">
	<table  id="locationfields" border="1" width="100%">
		<tr height="26" style="font-weight:bold;background:#FFFBCC">
				<th>Field Category</th>
				<th>Field Name</th>
				<th>Indoor/Outdoor</th>
				<th>Public/Private</th>
				<th>Description</th>
				<th>Size</th>
				<th>Type</th>
		</tr>
		<?php
			foreach( $fielddata as $fd ){?>
				<tr>
						<?php	
							$args5=array(
							'orderby'    => 'name',
							'taxonomy'   => 'listingcategory',
							'order'      => 'ASC',
							'parent'     => '345',
							'show_count' => 0,
							'hide_empty' => 0,
							'pad_counts' => true,					
							); 
						$categories=get_categories($args5);
						?>
					<input type="hidden" name="field_id[]" value="<?php echo $fd->field_id; ?>" >
					<td>
						<!-- <input type="text" name="field_category[]" value=""> -->		
						<?php foreach($categories as $category){	
								if($fd->field_category == trim($category->term_id)){
									echo $category->name;
								}
							}  ?>
					</td>
					<td><?php echo $fd->field_name; ?></td>
					<td><!-- <input type="text" name="field_indoor[]" value=""> -->
						<?php echo $fd->field_indoor; ?>
					</td>
					<td>
						<?php echo $fd->field_public; ?>
					</td>
					<td><?php echo $fd->field_desc; ?></td>	
					<td><?php echo $fd->field_size; ?></td>	
					<td><?php echo $fd->field_type; ?></td>	
				</tr>
	<?php	}
		 ?>
	</table>
</div>
<?php }else{
			echo "No fields found for this place.";
		}?>