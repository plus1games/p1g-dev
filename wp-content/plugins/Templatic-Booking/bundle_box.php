<?php
/*
 * Add action for display the booking system bundle box in templatic system dashboard.
 */
if(isset($_REQUEST['page']) && $_REQUEST['page'] =='templatic_system_menu'){
	add_action('templconnector_bundle_box','add_templatic_booking_bundle_box');
}
if(!function_exists('add_templatic_booking_bundle_box')){
function add_templatic_booking_bundle_box()
{	
?>
    <div id="templatic_booking_system" class="postbox widget_div">
        <div title="Click to toggle" class="handlediv"></div>
            <h3 class="hndle"><span><?php _e('Templatic Booking',BKG_DOMAIN); ?></span></h3>
        <div class="inside">
			<img class="dashboard_img" src="'.TEMPL_PLUGIN_URL.'/tmplconnector/monetize/images/booking-cal.png" />
            <?php
            _e('The classic feature for booking system from templatic. This feature gives you complete access for book your hotels and rooms.',BKG_DOMAIN);
            ?>
            <div class="clearfixb"></div>
            
            <?php if(!is_active_addons('templatic_booking_system')) :?>
            <div id="publishing-action">
                <a href="<?php echo home_url()."/wp-admin/admin.php?page=templatic_system_menu&activated=templatic_booking_system&true=1";?>" class="templatic-tooltip button-primary"><?php _e('Activate &rarr;',BKG_DOMAIN); ?></a>
            </div>
            <?php  endif;?>
    <?php  if (is_active_addons('templatic_booking_system')) : ?>
            <div class="settings_style">
            <a href="<?php echo home_url()."/wp-admin/admin.php?page=templatic_system_menu&deactivate=templatic_booking_system&true=0";?>" class="deactive_lnk"><?php _e('Deactivate ',BKG_DOMAIN); ?></a>|
            <a class="templatic-tooltip set_lnk" href="<?php echo home_url()."/wp-admin/admin.php?page=templatic_booking_system";?>"><?php _e('Settings',BKG_DOMAIN); ?></a>
            <?php 
			global $wpdb;
			$user_meta_table = $wpdb->prefix."usermeta";
			$chk_tour = explode( ',', (string) get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true ) );
			$flag=0;
			if ( in_array( 'templatic_booking_system_plugin_install', $chk_tour )){ 
				$flag=1;
			}else{
				$flag=0;
			}
			if($flag==0){
		?>
				| <a class="templatic-tooltip set_lnk"  href="<?php echo home_url().'/wp-admin/admin.php?page=templatic_booking_system';?>"><?php _e('Start tour',BKG_DOMAIN); ?></a>
                 <?php }?>
            </div>
    <?php endif; ?>
        </div>
    </div>
<?php
}
}
?>