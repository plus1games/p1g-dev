<?php   
/*
Plugin Name: Templatic Booking
Plugin URI: http://templatic.com/docs/booking-system-add-on/
Description: Templatic Booking gives the booking capabilities to user.
Version: 1.1.1
Author: Templatic
Author URI: http://templatic.com/
*/
// Define constants for booking system
define('BOOKING_PLUGINS_VERSION','1.1.1');
define('BKG_DOMAIN','templatic_booking');
define('BOOKING_PLUGINS_SLUG','Templatic-Booking/templatic_booking.php');
define("TEMPLATIC_BOOKING_URL",plugin_dir_url( __FILE__ ));
define('TEMPLATIC_BOOKING_PATH',dirname(__FILE__));
define('OCCUPY_TEXT',__('Occupancy',BKG_DOMAIN));
if(!defined('POST_DETAIL')){ define('POST_DETAIL',__('Your submitted Information',BKG_DOMAIN)); };
define('TAX_TEXT',__('TAX',BKG_DOMAIN));
define('DISCOUNT_AMT',__('Discount Amount',BKG_DOMAIN));
define('TOTAL_CHARGE_TEXT',__('Total Charges',BKG_DOMAIN));
ini_set('set_time_limit', 0);ini_set('max_execution_time', 0);

//Include plugin.php file to use wordpress default plugin related functions
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
ob_start();

if(strstr($_SERVER['REQUEST_URI'],'plugins.php') && !isset($_REQUEST['action'])){
	require_once('wp-updates-plugin.php');
	new WPUpdatesBookingSystemPluginUpdater( 'http://templatic.com/updates/api/index.php', plugin_basename(__FILE__) );
}

//Check whether Tevolution plugin is active or not to use booking system plugin.

if(is_plugin_active('Tevolution/templatic.php'))
{
	//Include the tevolution plugins main file to use the core functionalities of plugin.
	if(file_exists(ABSPATH . 'wp-content/plugins/Tevolution/templatic.php')){
		include_once( ABSPATH . 'wp-content/plugins/Tevolution/templatic.php');
	}
	//alter the seasonal table for seasonal_minimum_stay field.
	global $wpdb,$booking_seasonal_price_table;
	$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";
	$booking_seasonal_price_table_min_stay = $wpdb->get_var("SHOW COLUMNS FROM $booking_seasonal_price_table LIKE 'seasonal_minimum_stay'");
		if('seasonal_minimum_stay' != $booking_seasonal_price_table_min_stay)	{
			$booking_seasonal_price_table_min_stay = $wpdb->query("ALTER TABLE $booking_seasonal_price_table  ADD `seasonal_minimum_stay` varchar(255) NULL AFTER `set_price_for`");
		}
	remove_action('pre_get_posts', 'advance_search_template_function',11);
	remove_action('tevolution_display_rating','tevolution_display_rating');
	remove_action('templ_post_title','tevolution_listing_after_title',12);
	add_action('pre_get_posts', 'advance_search_template_function',10);
	// For only temparary bases to update plugin active script
	//update_option('templatic_booking_system','Active');
	
	/* get locale */
	$locale = get_locale();
	
	//Include booking system's main files.
	load_textdomain( BKG_DOMAIN, plugin_dir_path( __FILE__ ).'language/'.$locale.'.mo' );
	load_plugin_textdomain( BKG_DOMAIN,false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	
	include(dirname(__FILE__)."/language/booking_language.php");
	include(dirname(__FILE__)."/bundle_box.php");	
	include(dirname(__FILE__)."/install.php");
	global $wp_query;
	
	function Get_Current_Single_Page_Name($single_template){
		global $post,$current_user;
		$Current_User_Id = $current_user->ID;
		if($post->post_type == 'booking'){
			if(strstr($single_template,'single-booking.php')){
				if(current_user_can('Administrator') ){}
				else{
					wp_die( __('You do not have sufficient permissions to access this page.') );
				}
			}
		 }
		 return $single_template;
	}
	add_filter( "single_template", "Get_Current_Single_Page_Name" ) ;
	function Get_Current_Taxonomy_Page_Name(){
		global $posts,$current_user;
		get_currentuserinfo();
		$current_post_type = get_post_type();
		$Current_User_Id = $current_user->ID;
		$Current_User_Role = $current_user->roles[0];
		if($current_post_type == "booking"){
			$admin_roles = wp_get_current_user();
			if(!empty($admin_roles->roles)){
				if(!in_array('administrator',$admin_roles->roles)){
					wp_die( __('You do not have sufficient permissions to access this page.') );
				}
			}
		}
	}
	//FILTER TO REMOVE POST TYPE DISPLAYING FROM 404 PAGE SETTINGS IN CUSTOMIZER START
	add_filter('get_post_types_for_404_page','remove_post_type_for_404_page',10);
	function remove_post_type_for_404_page($post_types){
		unset($post_types[CUSTOM_POST_TYPE_TEMPLATIC_BOOKING]);
		return $post_types;
	}
	//FILTER TO REMOVE POST TYPE DISPLAYING FROM 404 PAGE SETTINGS IN CUSTOMIZER FINISH
	
	//FILTER TO REMOVE POST TYPE DISPLAYING FROM SITEMAP PAGE TEMPLATE START
	add_filter('get_post_types_for_sitemap_page_template','remove_post_type_for_sitemap_page_template',10);
	function remove_post_type_for_sitemap_page_template($post_types){
		unset($post_types[CUSTOM_POST_TYPE_TEMPLATIC_BOOKING]);
		return $post_types;
	}
	//FILTER TO REMOVE POST TYPE DISPLAYING FROM SITEMAP PAGE TEMPLATE FINISH
	//Check if current user is admin to see booking detail page
	add_action('wp_head','check_isadmin_for_booking_detail');
	
	add_filter( "taxonomy_template", "Get_Current_Taxonomy_Page_Name" ) ;
	add_filter('success_page_sidebar_post_type',"get_booking_success_page_post_type",10,2);
	add_filter('SelectBoxOptions','PluginSelectBoxOptions',10,2);
	function PluginSelectBoxOptions($values,$name){

		if($_REQUEST['id']!=""){	
			$id_post = $_REQUEST['id'];
		}elseif($_REQUEST['houses_rooms']!=""){
			$id_post = $_REQUEST['houses_rooms'];
		}
		elseif(isset($_REQUEST['post_id']) && $_REQUEST['post_id'] != '')
			$id_post = $_REQUEST['post_id'];
		elseif(isset($_REQUEST['post']) && $_REQUEST['post'] != '')
		{
			$id_post = get_post_meta($_REQUEST['post'],'property_room_id',true);	
		}
		$post_type_1 = '';
		if($id_post){
			$post_type_1 = get_post_type($id_post);
			$post_type_search = in_array($post_type_1,array_keys(get_option('templatic_custom_post')));
			if(!$post_type_search && $post_type_1 !='post'){
				$post_type_1 = '';
			}
		}
		
		if($post_type_1!="" && "room" == $post_type_1 && "no_of_rooms" == $name){
			if($id_post){
				$array = array();
				$number_of_rooms = get_post_meta($id_post,'no_of_rooms',true);
				if($number_of_rooms>0){
					for($i=1;$i<=$number_of_rooms;$i++){
						$array[] = $i;
					}
					return $array;
				}
			}
		}elseif($post_type_1!="" && ("room" == $post_type_1 || CUSTOM_POST_TYPE_TEMPLATIC_HOUSE == $post_type_1) && "home_capacity" == $name){
			if($id_post){
				$array = array();
				$TotalGuest = get_post_meta($id_post,'home_capacity',true);
				if($TotalGuest>0){
					for($i1=1;$i1<=$TotalGuest;$i1++){
						$array[] = $i1;
					}
					return $array;
				}
			}
		}else{
			return $values;
		}
		
	}
	add_filter('SelectBoxSelectedOptions','PluginSelectBoxSelectedOptions',10,2);
	function PluginSelectBoxSelectedOptions($value,$name){
		$no_of_rooms = '';
		$home_capacity = '';
		if(@$_REQUEST['id']!=""){	
			$id_post = @$_REQUEST['id'];
		}elseif($_REQUEST['houses_rooms']!=""){
			$id_post = @$_REQUEST['houses_rooms'];
		}
		elseif(isset($_REQUEST['post']) && @$_REQUEST['post'] != '')
		{
			$id_post = get_post_meta($_REQUEST['post'],'property_room_id',true);
			$no_of_rooms = get_post_meta($_REQUEST['post'],'no_of_rooms',true);
			$home_capacity = get_post_meta($_REQUEST['post'],'home_capacity',true);
		}
		$post_type_1 = '';
		if(@$id_post){
			$post_type_1 = get_post_type(@$id_post);
			$post_type_search = in_array($post_type_1,array_keys(get_option('templatic_custom_post')));
			if(!$post_type_search && $post_type_1 !='post'){
				$post_type_1 = '';
			}
		}
		$val = '';
		if($post_type_1!="" && CUSTOM_POST_TYPE_TEMPLATIC_ROOM == $post_type_1 && "no_of_rooms" == $name){
			$val = $_REQUEST[$name];
		}elseif($post_type_1!="" && (CUSTOM_POST_TYPE_TEMPLATIC_ROOM == $post_type_1 || CUSTOM_POST_TYPE_TEMPLATIC_HOUSE == $post_type_1) && "home_capacity" == $name){
			$val = $_REQUEST[$name];
		}
		if($post_type_1!="" && CUSTOM_POST_TYPE_TEMPLATIC_ROOM == $post_type_1 && "no_of_rooms" == $name && $no_of_rooms != ''){
			$val = $no_of_rooms;
		}elseif($post_type_1!="" && (CUSTOM_POST_TYPE_TEMPLATIC_ROOM == $post_type_1 || CUSTOM_POST_TYPE_TEMPLATIC_HOUSE == $post_type_1) && "home_capacity" == $name && $home_capacity != ''){
			$val = $home_capacity;
		}
		return $val;
	}
	
	function tmpl_booking_full_copy( $source, $target ){
		global $upload_folder_path;
		$imagepatharr = explode('/',$upload_folder_path."dummy");
		$year_path = ABSPATH;
		for($i=0;$i<count($imagepatharr);$i++)
		{
		  if($imagepatharr[$i])
		  {
			  $year_path .= $imagepatharr[$i]."/";
			  if (!file_exists($year_path)){
				  mkdir($year_path, 0777);
			  }     
			}
		}
		@mkdir( $target );
			$d = dir( $source );
			
		if ( is_dir( $source ) ) {
			@mkdir( $target );
			$d = dir( $source );
			while ( FALSE !== ( $entry = $d->read() ) ) {
				if ( $entry == '.' || $entry == '..' ) {
					continue;
				}
				$Entry = $source . '/' . $entry; 
				if ( is_dir( $Entry ) ) {
					tmpl_booking_full_copy( $Entry, $target . '/' . $entry );
					continue;
				}
				copy( $Entry, $target . '/' . $entry );
			}
		
			$d->close();
		}else {
			copy( $source, $target );
		}
	}
	
	
	global $upload_folder_path;
	global $blog_id;
	if(get_option('upload_path') && !strstr(get_option('upload_path'),'wp-content/uploads'))
	{
		$upload_folder_path = "wp-content/blogs.dir/$blog_id/files/";
	}else
	{
		$upload_folder_path = "wp-content/uploads/";
	}
	global $blog_id;
	if($blog_id){ $thumb_url = "&amp;bid=$blog_id";}
	$dirinfo = wp_upload_dir();
	$target = $dirinfo['basedir']."/dummy"; 
	tmpl_booking_full_copy( plugin_dir_path( __FILE__ )."images/dummy/", $target );
	
	
	if(isset($_REQUEST['insert_data']) && $_REQUEST['insert_data'] == 1)
	{
		update_option('insert_dummy_data','YES');
		add_action('init','add_category',11);
	}
	function add_category()
	{
		
		global $wpdb;
		$taxonomies = get_object_taxonomies( (object) array( 'post_type' => 'room','public'   => true, '_builtin' => true ));	
		$booking_term_table = $wpdb->prefix."terms";
		//ADD CATEGORY FOR ROOM POST TYPE START
		$cats = array('Delux room','Exclusive room');
		
		for($c=0; $c < count($cats) ; $c++){
			$catname = $cats [$c];
			$catid = $wpdb->get_var("select term_id from $booking_term_table where name='".$catname."'");
			if(!$catid){
				wp_insert_term( $catname, $taxonomies[0]);
			} 
		}
		$tags = array('room');

		for($c=0; $c < count($tags) ; $c++){
			$tagname = $tags [$c];
			$tagid = $wpdb->get_var("select term_id from $booking_term_table where name='".$tagname."'");
			if(!$tagid){
				wp_insert_term( $tagname, $taxonomies[1]);
			} 
		}
		//ADD CATEGORY FOR ROOM POST TYPE FINISH
		
		//ADD CATEGORY FOR HOUSE POST TYPE START
		$cats1 = array('Houses');
		$taxonomies = get_object_taxonomies( (object) array( 'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,'public'   => true, '_builtin' => true ));
		for($c1=0; $c1 < count($cats1) ; $c1++){
			$catname1 = $cats1[$c1];
			$catid1 = $wpdb->get_var("select term_id from $booking_term_table where name='".$catname1."'");
			if(!$catid1){
				wp_insert_term( $catname1, $taxonomies[0]);
			} 
		}
		
		$tags = array('house');

		for($c=0; $c < count($tags) ; $c++){
			$tagname = $tags [$c];
			$tagid = $wpdb->get_var("select term_id from $booking_term_table where name='".$tagname."'");
			if(!$tagid){
				wp_insert_term( $tagname, $taxonomies[1]);
			} 
		}
		//ADD CATEGORY FOR HOUSE POST TYPE FINISH
		
		
		//ADD CATEGORY FOR BOOKING POST TYPE START
		$cats2 = array('Laundry','Airport pickup');
		$taxonomies = get_object_taxonomies( (object) array( 'post_type' => 'booking','public'   => true, '_builtin' => true ));
		for($c2=0; $c2 < count($cats2) ; $c2++){
			global $wpdb;
			$catname2 = $cats2[$c2];
			$catid2 = $wpdb->get_var("select term_id from $booking_term_table where name='".$catname2."'");
			if(!$catid2){
				wp_insert_term( $catname2, $taxonomies[0]);
				if($wpdb->query("SHOW COLUMNS FROM $booking_term_table where Field='used_for'")){}
				else{
					$sql="ALTER table $booking_term_table add used_for varchar(255)";
					$wpdb->query($sql);
				}
				$SqlUpdatePrice = $wpdb->query("update $booking_term_table set term_price=10, used_for='house,room'  where name like '$catname2'");
			} 
		}
		//ADD CATEGORY FOR BOOKING POST TYPE FINISH
	//BOOKING SYSTEM ROOMS AND HOUSES ENTRY START.
			include_once(dirname(__FILE__)."/library/booking_custom_post_type/rooms_data.php");
		//BOOKING SYSTEM ROOMS AND HOUSES ENTRY FINISH.
		
			//wp_safe_redirect(admin_url().'/themes.php?dummy_insert=1');
	}
	global $wpdb;
	$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";
	$field_check1 = $wpdb->get_var("SHOW COLUMNS FROM $booking_seasonal_price_table LIKE 'seasonal_title'");
		if('seasonal_title' != $field_check1)	{
			$dbuser_table_alter = $wpdb->query("ALTER TABLE $booking_seasonal_price_table ADD seasonal_title text NULL");
		}
	
	register_activation_hook(__FILE__,'add_booking_custom_post_types');
	if(!function_exists('add_booking_custom_post_types')){
	function add_booking_custom_post_types(){
		update_option('templatic_booking_system','Active');
		update_option('monetization','Active');
		/*
		@Deactivate registration module and delete its related pages
		*/
		delete_option('templatic-login');
		global $wpdb;
		$photo_page_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts where post_title like 'Login' and post_type='page'");
		wp_delete_post($photo_page_id,true);
		$photo_page_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts where post_title like 'Register' and post_type='page'");
		wp_delete_post($photo_page_id,true);
		$photo_page_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts where post_title like 'Profile' and post_type='page'");
		wp_delete_post($photo_page_id,true);
		
		
		update_option('templatic_booking_redirect','Booking_Active');
		global $wpdb;
		$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";
		$booking_availability_table = $wpdb->prefix."booking_availability_check";
		$booking_closing_table = $wpdb->prefix."booking_closing_check";
		$booking_term_table = $wpdb->prefix."terms";
		
		//CUSTOM FIELD SETTINGS START  is_active
		
		$post_category = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'category' and $wpdb->posts.post_type = 'custom_fields'");
		$post_category;
		if($post_category > 0){
			update_post_meta($post_category,'is_active',0);
		 }
		$post_title = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'post_title' and $wpdb->posts.post_type = 'custom_fields'");
		if($post_title > 0){
			update_post_meta($post_title,'is_active',0);
		 }
		$post_content1 = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'post_content' and $wpdb->posts.post_type = 'custom_fields'");
		 if($post_content1 > 0){
			update_post_meta($post_content1,'is_active',0);
		 }
		$post_excerpt = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'post_excerpt' and $wpdb->posts.post_type = 'custom_fields'");
		 if($post_excerpt > 0){
			update_post_meta($post_excerpt,'is_active',0);
		 }
		$post_images = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'post_images' and $wpdb->posts.post_type = 'custom_fields'");
		if($post_images > 0){
			update_post_meta($post_images,'is_active',0);
		}
		$basic_inf = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'basic_inf' and $wpdb->posts.post_type = 'custom_fields'");
		if($basic_inf > 0){
			update_post_meta($basic_inf,'is_active',1);
		}
		//add_action('init','templatic_booking_system_custom_post_type');
		
		if($wpdb->query("SHOW COLUMNS FROM $booking_term_table where Field='used_for'")){}
		else{
			$sql="ALTER table $booking_term_table add used_for varchar(255)";
			$wpdb->query($sql);
		}
		
		$templatic_settings = get_option('templatic_settings');
		$templatic_settings['post_pre_bank_trasfer_msg_content'] = '<p>'.__('Thank you, your request has been received successfully.',BKG_DOMAIN).'</p><p>'.__('To approve your booking please transfer the amount of',BKG_DOMAIN).' <b>[#payable_amt#] </b> '.__('at our bank with the following information',BKG_DOMAIN).' :</p></br><p>'.__('Bank Name',BKG_DOMAIN).' : <b>[#bank_name#]</b></p><p>'.__('Account Number',BKG_DOMAIN).' : <b>[#account_number#]</b></p><br><p>'.__('Please include the ID as reference',BKG_DOMAIN).' :#[#submition_Id#]</p></br><p>'.__('Thank you for visit at',BKG_DOMAIN).' [#site_name#].</p>';
		update_option('templatic_settings',$templatic_settings);
		

		if($wpdb->get_var("SHOW TABLES LIKE \"$booking_seasonal_price_table\"") != $booking_seasonal_price_table){
			$sql_create_seasonal_price_table = "CREATE TABLE IF NOT EXISTS $booking_seasonal_price_table (
			  seasonal_prices_id int(8) NOT NULL AUTO_INCREMENT,
			  seasonal_title text NULL,
			  from_date date NOT NULL,
			  to_date date NOT NULL,
			  prices_per_persons text NOT NULL,
			  set_price_for INT(32) NOT NULL,
			  status varchar(30) NOT NULL,
			  persons varchar(255) NOT NULL,
			  PRIMARY KEY (seasonal_prices_id))";
			$wpdb->query($sql_create_seasonal_price_table);
		}
		
		if($wpdb->get_var("SHOW TABLES LIKE \"$booking_availability_table\"") != $booking_availability_table){
			$sql_create_availability_table = "CREATE TABLE IF NOT EXISTS $booking_availability_table (
			  availability_id int(8) NOT NULL AUTO_INCREMENT,
			  property_id int(8) NOT NULL,
			  booking_post_id int(8) NOT NULL,
			  from_date date NOT NULL,
			  to_date date NOT NULL,
			  status varchar(30) NOT NULL,
			  PRIMARY KEY (availability_id))";
			$wpdb->query($sql_create_availability_table);
		}
		
		if($wpdb->get_var("SHOW TABLES LIKE \"$booking_closing_table\"") != $booking_closing_table){
			$sql_create_closing_table = "CREATE TABLE IF NOT EXISTS $booking_closing_table (
			  closing_id int(8) NOT NULL AUTO_INCREMENT,
			  property_id int(8) NOT NULL,
			  closing_start_date date NOT NULL,
			  closing_end_date date NOT NULL,
			  PRIMARY KEY (closing_id))";
			$wpdb->query($sql_create_closing_table);
		}
	}
	}
	
	add_action('admin_init', 'booking_plugin_redirect');
	/*
	Name : booking_plugin_redirect
	Description : Redirect on plugin dashboard after activating plugin
	*/
	function booking_plugin_redirect(){ 
		$current_post_type = '';
		if( isset( $_REQUEST['post_type'] ) && @$_REQUEST['post_type'] != "" ){
			$current_post_type = $_REQUEST['post_type'];
		}elseif( isset( $_REQUEST['post'] ) && @$_REQUEST['post'] != "" ){
			$current_post_type = get_post_type( $_REQUEST['post'] );
		}
		if( $current_post_type == CUSTOM_POST_TYPE_TEMPLATIC_ROOM || $current_post_type == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE || $current_post_type == CUSTOM_POST_TYPE_TEMPLATIC_BOOKING ){
			remove_meta_box('ptthemes-settings-price-package',$current_post_type,'normal');
		}
		if (get_option('templatic_booking_redirect') == 'Booking_Active'){
			global $wpdb;
			$paymentsql = $wpdb->get_results("select * from $wpdb->options where option_name like 'payment_method_%'");
			for($i=0;$i<count($paymentsql);$i++)
			{
				if($paymentsql[$i]->option_name!= 'payment_method_prebanktransfer' && $paymentsql[$i]->option_name!= 'payment_method_paypal')
				{
					$old_paymentsql = $wpdb->query("delete from $wpdb->options where option_name ='".$paymentsql[$i]->option_name."'");
				}
			}
			templatic_booking_system_custom_post_type();
			update_option('templatic_booking_redirect', 'Not_Booking_Active');
			wp_redirect(admin_url().'admin.php?page=templatic_booking_system&data_installed=1');
		}
		
			booking_column();
		
	}
	
	add_action('init','remove_sidebar');
	function remove_sidebar(){
		unregister_sidebar('add_booking_submit_sidebar');
		unregister_sidebar('add_house_submit_sidebar');
		unregister_sidebar('add_room_submit_sidebar');
		unregister_sidebar('booking_tag_tag_listing_sidebar');
	}
	
	// attach required stylesheets
	add_action('admin_head','attach_backend_stylesheets_scripts');
	add_action('wp_head','attach_backend_stylesheets_scripts');
	add_action('admin_footer','attach_backend_calendar_script');
	add_action('admin_head','change_update_status');
	add_action('wp_footer','attach_backend_calendar_script');
	// Add the metabox for post type selection. i.e. which post type page should use for.
	add_action('admin_init','templatic_booking_meta_box');
	$templatic_settings = get_option( "templatic_settings" );
	// remove featured listinf radio buttons
	add_action('init','templatic_booking_remove',11);

	if(!function_exists('templatic_booking_remove')){
		function templatic_booking_remove(){ 
				global $post;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] =='edit'){
					global $post_type;
				}else{
					$post_type = @$_REQUEST['post_type'];
				}
				if($post_type =='booking' || $post_type == CUSTOM_POST_TYPE_TEMPLATIC_ROOM || $post_type == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
				remove_action('tevolution_featured_list','tevolution_featured_list_fn');
				}
		}
	}
	
	if(!function_exists('templatic_booking_meta_box')){
		function templatic_booking_meta_box(){
			add_meta_box('templatic_booking_checkinout_box','Booking Settings','templatic_booking_checkinout_callback',CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,'normal','high');
			add_meta_box("templatic_booking_status", "Booking Status", "update_booking_status",CUSTOM_POST_TYPE_TEMPLATIC_BOOKING, "side", "high");
			add_meta_box('templatic_booking_closing_days','Closing Days','closing_hosue_days',CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,'normal','high');
			add_meta_box('templatic_booking_closing_days','Closing Days','closing_hosue_days',CUSTOM_POST_TYPE_TEMPLATIC_ROOM,'normal','high');
			add_meta_box('templatic_booking_checkinout_box','Price Settings','templatic_room_checkinout_callback',CUSTOM_POST_TYPE_TEMPLATIC_ROOM,'normal','high');
			add_meta_box('templatic_booking_checkinout_box','Price Settings','templatic_room_checkinout_callback',CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,'normal','high');
			
		}
	}

		add_action('save_post', 'templatic_booking_checkinout_save_data');

	// Display any errors
	function wpse_5102_admin_notice_handler($post) {
		global $post;

		$errors = get_post_meta( @$post->ID,'booking_error',true);
	
		if($errors && (!isset($_REQUEST['post_type']) && $_REQUEST['post_type'] == '')) {
	
			echo '<div class="error"><p>' . $errors . '</p></div>';
	
		}   
	
	}
	add_action( 'admin_notices', 'wpse_5102_admin_notice_handler' );
	//templ_captcha_integrate('submit'); // Display recaptcha in submit form //
	
	if(!function_exists('templatic_room_checkinout_callback')){
		function templatic_room_checkinout_callback(){
		global $wpdb;
		$templatic_settings = get_option( "templatic_settings" );
		$property_id = @$_REQUEST['post'];
		$sql_custom_max_field = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_capacity' and $wpdb->posts.post_type = 'custom_fields'");
		$GetPostMeta = get_post_meta($sql_custom_max_field,'option_values',true);
		$max_adults_allowed = explode(",",$GetPostMeta);
			$html='
		<table style="width:35%" class="widefat post" id="set_price_tbl">
			<thead>
				<tr>
					<th><label for="no_of_persons" class="form-textfield-label">No. of persons</label></th>
					<th><label for="price_day" class="form-textfield-label">Price/Day</label></th>
					<th><label for="price_week" class="form-textfield-label">Price/Week</label></th>
					<th><label for="price_month" class="form-textfield-label">Price/Month</label></th>
				</tr>
			</thead>
			<tbody>';
			
				if(count($max_adults_allowed)>0){
						for($i=1;$i<=count($max_adults_allowed);$i++){//echo "<pre>";print_r($templatic_settings);echo "</pre>";
							$price_per_day = @$templatic_settings["templatic_booking_system_settings"]["templatic_booking_manage_prices_settings"]["price_for_$property_id"]["price_guests_$i"]["price_per_day"];
							$price_per_week = @$templatic_settings["templatic_booking_system_settings"]["templatic_booking_manage_prices_settings"]["price_for_$property_id"]["price_guests_$i"]["price_per_week"];
							$price_per_month = @$templatic_settings["templatic_booking_system_settings"]["templatic_booking_manage_prices_settings"]["price_for_$property_id"]["price_guests_$i"]["price_per_month"];
							
							$html .= '<tr>
								<td><label class="form-textfield-label">'.$i.'</label></td>
								<td><input type="text" style="width:80px;" name="price_day_'.$i.'" id="price_day_'.$i.'" value="'.$price_per_day.'"/></td>
								<td><input type="text" style="width:80px;" name="price_week_'.$i.'" id="price_week_'.$i.'" value="'.$price_per_week.'"/></td>
								<td><input type="text" style="width:80px;" name="price_month_'.$i.'" id="price_month_'.$i.'" value="'.$price_per_month.'"/></td>
							</tr>';
						}
				}else{	
					$html .= '<tr><td colspan="3"><b>Please set the Maximum Adults first from General Settings.</b></label></td></tr>';
				}
			
$html .='   <input type="hidden" name="manage_price_submit" value="y"> </tbody>
		</table>';
_e($html);
		}
	}
	if(!function_exists('closing_hosue_days')){
		function closing_hosue_days(){
		global $post;
		?>
		<script type="text/javascript">
		function addrow()
	{
		 var tbl = document.getElementById('tblAddress');
		 var lastRow = tbl.rows.length;
		  // if there's no header row in the table, then iteration = lastRow + 1
		 var iteration = lastRow;
		 //var iteration = lastRow + 1;
		 var row = tbl.insertRow(lastRow);
		 
		  //  cell 0
		 var cell0 = row.insertCell(0);
		 var el = document.createElement('input');
		  el.type = 'text';
		  el.name = 'closing_start_date_name[]';
		  el.id = 'closing_start_date_id'+iteration;
		  cell0.appendChild(el);
		  jQuery(function(){
						var pickerOpts = {
								showOn: "both",
								dateFormat: 'yy-mm-dd',
								buttonImage: "<?php echo TEMPLATIC_BOOKING_URL; ?>images/cal.png",
								buttonText: "Show Datepicker"
							};	
							jQuery("#closing_start_date_id"+iteration).datepicker(pickerOpts);
						});
		  //cell 1
		 var cell1 = row.insertCell(1);
		 var el = document.createElement('input');
		 el.type = 'text';
		 el.name = 'closing_end_date_name[]';
		 el.id = 'closing_end_date_id'+iteration;
		 cell1.appendChild(el);
		 jQuery(function(){
						var pickerOpts = {
								showOn: "both",
								dateFormat: 'yy-mm-dd',
								buttonImage: "<?php echo TEMPLATIC_BOOKING_URL; ?>images/cal.png",
								buttonText: "Show Datepicker"
							};	
							jQuery("#closing_end_date_id"+iteration).datepicker(pickerOpts);
						});
		 
		
		  
		  var cell3 = row.insertCell(2);
		  el = document.createElement('div');
		  cell3.innerHTML = '<a href="javascript:void(0)" class="l_remove" onclick="removeRow(this)">Remove</a>';
		  cell3.appendChild(el);
	   
		}
		function removeRow(theLink)
		{ 
			//var _row = rows.parentElement.parentElement;
			//document.getElementById('tblAddress').deleteRow(_row.rowIndex);
			//var tbl = document.getElementById('tblAddress');
			//var lastRow = tbl.theLink.length;
			//var lastRowi = tbl.theLink.rowIndex;
			//if (lastRow > 2) tbl.deleteRow(lastRow - 1);
			var theRow = theLink.parentNode.parentNode;
			var theBod = theRow.parentNode;
			theBod.removeChild(theRow);
		}
		function ResetClosingDates(){
			jQuery.noConflict();
			jQuery("#closing_start_date_id1").val('0000-00-00');
			jQuery("#closing_end_date_id1").val('0000-00-00');
		}
		</script>
			
			 <table id="tblAddress">
                    <tr>
                      <td class="txtBase"><?php _e('Start Date',@DOMIAN); ?> </td>
                      <td class="txtBase"><?php _e('End Date',@DOMIAN); ?></td>
                    </tr>
                <?php 
				global $wpdb;
				$booking_closing_table = $wpdb->prefix."booking_closing_check";
				if(@$_REQUEST['post'])
				{
					$closing_days = $wpdb->get_results("select * from $booking_closing_table where property_id=".$_REQUEST['post']);
				}
				if(!empty($closing_days[0]->closing_start_date)):
					for($i=0;$i<sizeof($closing_days);$i++):
				?>
				<script type="text/javascript">	
						jQuery(function(){
						var pickerOpts = {
								showOn: "both",
								dateFormat: 'yy-mm-dd',
								buttonImage: "<?php echo TEMPLATIC_BOOKING_URL; ?>images/cal.png",
								buttonText: "Show Datepicker"
							};	
							jQuery("#closing_start_date_id"+<?php echo ($i+1);?>).datepicker(pickerOpts);
						});
						jQuery(function(){
						var pickerOpts = {
								showOn: "both",
								dateFormat: 'yy-mm-dd',
								buttonImage: "<?php echo TEMPLATIC_BOOKING_URL; ?>images/cal.png",
								buttonText: "Show Datepicker"
							};	
							jQuery("#closing_end_date_id"+<?php echo ($i+1);?>).datepicker(pickerOpts);
						});
					</script>
                    <tr>
                    	<td><input type="text" class="textfield" name="closing_start_date_name[]" id="closing_start_date_id<?php echo ($i+1);?>" value="<?php echo $closing_days[$i]->closing_start_date?>" Placeholder="<?php _e(PAPER_NOTE,BKG_DOMAIN)?>"/></td>     
						<td><input type="text" class="textfield" name="closing_end_date_name[]" id="closing_end_date_id<?php echo ($i+1);?>" value = "<?php echo $closing_days[$i]->closing_end_date;?>"/></td>
						<?php if($i==0){?>
								<td><a class="l_remove" onclick="ResetClosingDates();" href="javascript:void(0)"><?php _e('Reset',BKG_DOMAIN);?></a></td>
						<?php }else{  ?>
                       <td><a class="l_remove" onclick="removeRow(this)" href="javascript:void(0)"><?php _e('Remove',BKG_DOMAIN);?></a></td>
					   <?php } ?>
                    </tr>                    
                    <?php endfor;
				else:				
				?><script type="text/javascript">	
						jQuery(function(){
						var pickerOpts = {
								showOn: "both",
								dateFormat: 'yy-mm-dd',
								buttonImage: "<?php echo TEMPLATIC_BOOKING_URL; ?>images/cal.png",
								buttonText: "Show Datepicker"
							};	
							jQuery("#closing_start_date_id1").datepicker(pickerOpts);
						});
						jQuery(function(){
						var pickerOpts = {
								showOn: "both",
								dateFormat: 'yy-mm-dd',
								buttonImage: "<?php echo TEMPLATIC_BOOKING_URL; ?>images/cal.png",
								buttonText: "Show Datepicker"
							};	
							jQuery("#closing_end_date_id1").datepicker(pickerOpts);
						});
					</script>
                    <tr>     
			     	<td><input type="text" class="textfield" name="closing_start_date_name[]" id="closing_start_date_id1" value="" /></td>     
    				<td colspan="2"><input type="text" class="textfield" name="closing_end_date_name[]" id="closing_end_date_id1" value = ""/></td>
                        
                    </tr>       
                    <?php endif;?>           
    			</table>			
			<a style="display:inline-block;padding-top:10px;clear:both;" href="javascript:void(0)" class="link" onClick="addrow();"><?php _e('+ Add New',BKG_DOMAIN);?></a>
			<p class="description"><?php _e('Your client will not be able to book this '.$post->post_type.' on closing dates',BKG_DOMAIN); ?></p>
		<?php
		}
	}
	
	if(!function_exists('update_booking_status')){
	function update_booking_status(){
		global $post;
		$CurPostID = $post->ID;
		$GetStatus = get_post_meta($CurPostID,'status',true);
		if($GetStatus=="Pending"){
			$SelectedStatusPending = "selected=selected";
		}elseif($GetStatus=="Approved"){
			$SelectedStatusApproved = "selected=selected";
		}elseif($GetStatus=="Cancelled"){
			$SelectedStatusCancelled = "selected=selected";
		}
		$Html = '<div id="misc-publishing-actions"><div class="misc-pub-section"><label for="post_status">'.__("Status",BKG_DOMAIN).': </label>';
		$Html .= '<input type="hidden" name="current_post_id" id="current_post_id" value="'.$CurPostID.'"/>
					<select name="status" id="status" class="newtag form-input-tip">
						<option value="Pending"'. @$SelectedStatusPending.'>Pending</option>
						<option value="Approved"'. @$SelectedStatusApproved.'>Approved</option>
						<option value="Cancelled"'. @$SelectedStatusCancelled.'>Cancelled</option>
				    </select>
					<input class="button" id="update_status" type="button" value="Update">
					<span class="auto_spinner" id="auto_spinner" style="display:none;"><i class="fa fa-circle-o-notch fa-spin"></i></span>
					<div class="msg_update" id="msg_update" style="font-weight:bold;clear:both"></div>
					<div><small>'.__("(Approve or reject booking request and notify users by mail)",BKG_DOMAIN).'</small></div>
				  </div></div>';
		_e($Html,BKG_DOMAIN);
	}
	}
	
	if(!function_exists('attach_backend_stylesheets_scripts')){
		function attach_backend_stylesheets_scripts(){
			echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/templatic_booking_styles.css".'" rel="stylesheet">';
			//echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/theme.css".'" rel="stylesheet">';
			echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/fullcalendar.css".'" rel="stylesheet">';
			echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/base.css".'" rel="stylesheet">';
			echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/clean.css".'" rel="stylesheet">';
			if(is_admin())
				echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/date_range.css".'" rel="stylesheet">';
		}
	}
	
	if(!function_exists('attach_backend_calendar_script')){
	function attach_backend_calendar_script($post){
		global $wp_query;
		//include_once(TEMPLATIC_BOOKING_PATH."/library/includes/full_calendar.php");
		?>
		<script  type="text/javascript" >
			var today_name = '<?php _e("today",BKG_DOMAIN);?>';
			var month_name = '<?php _e("month",BKG_DOMAIN);?>';
			var week_name = '<?php _e("week",BKG_DOMAIN);?>';
			var day_name = '<?php _e("day",BKG_DOMAIN);?>';
			var lc_week_list = ["<?php _e("sun",BKG_DOMAIN);?>","<?php _e("mon",BKG_DOMAIN);?>","<?php _e("tue",BKG_DOMAIN);?>","<?php _e("wed",BKG_DOMAIN);?>","<?php _e("thu",BKG_DOMAIN);?>","<?php _e("fri",BKG_DOMAIN);?>","<?php _e("sat",BKG_DOMAIN);?>"];
			var month_list = ["<?php _e('January',BKG_DOMAIN); ?>", "<?php _e('February',BKG_DOMAIN); ?>", "<?php _e('March',BKG_DOMAIN); ?>", "<?php _e('April',BKG_DOMAIN); ?>", "<?php _e('May',BKG_DOMAIN); ?>", "<?php _e('June',BKG_DOMAIN); ?>", "<?php _e('July',BKG_DOMAIN); ?>", "<?php _e('August',BKG_DOMAIN); ?>", "<?php _e('September',BKG_DOMAIN); ?>", "<?php _e('October',BKG_DOMAIN); ?>", "<?php _e('November',BKG_DOMAIN); ?>", "<?php _e('December',BKG_DOMAIN); ?>"];
			var daysMin_list = ["<?php _e('Su',BKG_DOMAIN); ?>", "<?php _e('Mo',BKG_DOMAIN); ?>", "<?php _e('Tu',BKG_DOMAIN); ?>", "<?php _e('We',BKG_DOMAIN); ?>", "<?php _e('Th',BKG_DOMAIN); ?>", "<?php _e('Fr',BKG_DOMAIN); ?>", "<?php _e('Sa',BKG_DOMAIN); ?>"];
			var monthsShort_list = ["<?php _e('Jan',BKG_DOMAIN); ?>", "<?php _e('Feb',BKG_DOMAIN); ?>", "<?php _e('Mar',BKG_DOMAIN); ?>", "<?php _e('Apr',BKG_DOMAIN); ?>", "<?php _e('May',BKG_DOMAIN); ?>", "<?php _e('Jun',BKG_DOMAIN); ?>", "<?php _e('Jul',BKG_DOMAIN); ?>", "<?php _e('Aug',BKG_DOMAIN); ?>", "<?php _e('Sep',BKG_DOMAIN); ?>", "<?php _e('Oct',BKG_DOMAIN); ?>", "<?php _e('Nov',BKG_DOMAIN); ?>", "<?php _e('Dec',BKG_DOMAIN); ?>"];
			var daysfull_list = ["<?php _e("Sunday",BKG_DOMAIN);?>","<?php _e("Monday",BKG_DOMAIN);?>","<?php _e("Tuesday",BKG_DOMAIN);?>","<?php _e("Wednesday",BKG_DOMAIN);?>","<?php _e("Thursday",BKG_DOMAIN);?>","<?php _e("Friday",BKG_DOMAIN);?>","<?php _e("Saturday",BKG_DOMAIN);?>"];
			var dayShortTermList = ["<?php _e("Sun",BKG_DOMAIN);?>","<?php _e("Mon",BKG_DOMAIN);?>","<?php _e("Tue",BKG_DOMAIN);?>","<?php _e("Wed",BKG_DOMAIN);?>","<?php _e("Thu",BKG_DOMAIN);?>","<?php _e("Fri",BKG_DOMAIN);?>","<?php _e("Sat",BKG_DOMAIN);?>"];
		</script>
		<?php
		echo '<script type="text/javascript" src="'.TEMPLATIC_BOOKING_URL."js/datepicker.js".'"></script>';
		echo '<script type="text/javascript" src="'.TEMPLATIC_BOOKING_URL."js/fullcalendar.min.js".'"></script>';
		//include_once(TEMPLATIC_BOOKING_PATH."/js/datepicker.php");
		$currency = get_option("currency_symbol");
		$position = get_option("currency_pos");
		$tax_amount = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_amount'];
		$tax_type = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_type'];
		$services_price = '';
		
		if(is_admin())
		{
		?>
	
		<script type="text/javascript">
			var $date_picker = jQuery.noConflict();
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();
			
			
			
			
			
			/*	Calendar for backend  */
			
			$date_picker(document).ready(function() {
				 <?php 
					global $post;
					$checkin_date1 = get_post_meta($_REQUEST['post'],'checkin_date');
					$checkout_date1 = get_post_meta($_REQUEST['post'],'checkout_date');
					if($checkin_date[0]!="" || $checkout_date[0]!=""){
						$checkin_date = date_i18n(get_option('date_format'), strtotime($checkin_date[0]));
						$checkout_date = date_i18n(get_option('date_format'), strtotime($checkout_date[0]));
					}	
					$time = strtotime($checkout_date1[0]) - strtotime($checkin_date1[0]);
				 ?>
				var from = new Date(<?php if($checkin_date1[0]!=""){echo strtotime($checkin_date1[0]) * 1000;}?>);
				var to = new Date(<?php if($checkout_date1[0]!=""){echo strtotime($checkout_date1[0]) * 1000;}?>);
				var my_current = new Date();
				$date_picker('#datepicker-calendar').DatePicker({
					inline: true,
					date: [from, to],
					calendars: 2,
					mode: 'range',
					current: new Date(to.getFullYear(), to.getMonth() + 1, 1),
					onChange: function(dates,el) {
					  var numItems = $date_picker('.datepickerSelected').length;
					  if(numItems>1){
						$date_picker('#datepicker-calendar').hide();
					  }
					  jQuery("#booking_check_in_date_error").html('');
					   var chk_in_month = dates[0].getMonth()+1;
					  var chk_out_month = dates[1].getMonth()+1;
						
					 var first_date = dates[0].getFullYear()+'-'+chk_in_month+'-'+dates[0].getDate();
					 var second_date = dates[1].getFullYear()+'-'+chk_out_month+'-'+dates[1].getDate();
					  var d = new Date();
					  var from_date_validation = d.setDate(d.getDate()-1);
					 if(first_date == second_date)
					 {
						$date_picker('#date-range-field span').text('Invalid date selection');
						$date_picker('#checkindate').val('');
						$date_picker('#checkoutdate').val('');
					 }else if(dates[0] < from_date_validation || dates[1] < from_date_validation){
						$date_picker('#date-range-field span').text('Invalid date selection');
						$date_picker('#checkindate').val('');
						$date_picker('#checkoutdate').val('');
					  }else{
						var chk_in_month = dates[0].getMonth()+1;
						var chk_out_month = dates[1].getMonth()+1;
						
						$date_picker('#date-range-field span').text(dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+dates[0].getFullYear()+' - '+
														dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear());
						$date_picker('#checkindate').val(dates[0].getFullYear()+'-'+chk_in_month+'-'+dates[0].getDate());
						$date_picker('#checkoutdate').val(dates[1].getFullYear()+'-'+chk_out_month+'-'+dates[1].getDate());
						chechk_room_available();
						check_min_house_stay();
					  }								
					 }
				   });
				  
				   <?php 
						global $post;
						if(isset($_REQUEST['post']) && $_REQUEST['post']!="" && is_admin()){
							$checkin_date = get_post_meta($_REQUEST['post'],'checkin_date');
							$checkout_date = get_post_meta($_REQUEST['post'],'checkout_date');
							if($checkin_date[0]!="" || $checkout_date[0]!=""){
								$checkin_date = date_i18n(get_option('date_format'), strtotime($checkin_date[0]));
								$checkout_date = date_i18n(get_option('date_format'), strtotime($checkout_date[0]));
				   ?>
								$date_picker('#date-range-field span').text('<?php echo $checkin_date.' - '.$checkout_date; ?>');
					<?php 	}else{?>
								$date_picker('#date-range-field span').text("<?php _e('Select Check-in/Check-Out date',BKG_DOMAIN);?>");
					<?php	}
						}else{?>
							$date_picker('#date-range-field span').text("<?php _e('Select Check-in/Check-Out date',BKG_DOMAIN);?>");
					<?php }?>
				   $date_picker('#date-range-field').bind('click', function(){
					 $date_picker('#datepicker-calendar').toggle();
					 if($date_picker('#date-range-field a').text().charCodeAt(0) == 9660) {
					   $date_picker('#date-range-field a').html('&#9650;');
					   $date_picker('#date-range-field').css({borderBottomLeftRadius:0, borderBottomRightRadius:0});
					   $date_picker('#date-range-field a').css({borderBottomRightRadius:0});
					 } else {
					   $date_picker('#date-range-field a').html('&#9660;');
					   $date_picker('#date-range-field');
					   $date_picker('#date-range-field a').css({borderBottomRightRadius:5});
					 }
					 return false;
				   });
				   $date_picker('html').click(function() {
					 if($date_picker('#datepicker-calendar').is(":visible")) {
					   $date_picker('#datepicker-calendar').hide();
					   $date_picker('#date-range-field a').html('&#9660;');
					   $date_picker('#date-range-field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
					   $date_picker('#date-range-field a').css({borderBottomRightRadius:5});
					 }
				   });
				   $date_picker('#datepicker-calendar').click(function(event){
					 event.stopPropagation();
				   });
			 });
			 function chechk_room_available(){
			 var dataString = $date_picker('#post').serialize();
			 $date_picker.ajax({
								type: "POST",
								url: "<?php echo TEMPLATIC_BOOKING_URL.'library/includes/ajax_manage_available_room.php';?>",
								data: dataString,
								success: function(no_room){
											var booking_type = $date_picker("#booking_post_type").val();
											var no_of_rooms  = parseInt($date_picker("#no_of_rooms").val());
											if(booking_type == 'room')
											{
												$date_picker("#no_of_rooms").html("");
												 var options = $date_picker("#no_of_rooms");
												 $date_picker('#no_of_rooms').removeClass("no_booking");
												 if(no_room >0)
												 {
													 for(var i =1; i<= no_room; i++) {
														options.append($date_picker("<option />").val(i).text(i));
													}
												}
												else
												{
													$date_picker('#no_of_rooms').addClass("no_booking");
													$date_picker('#no_of_rooms').append('<option>No Booking Available</option>');
												}
											}
											else if(booking_type == 'house')
											{
												$date_picker("#home_capacity").html("");
												 var options = $date_picker("#home_capacity");
												 $date_picker('#home_capacity').removeClass("no_booking");
												 if(no_room >0 && no_room != 'booked')
												 {
													 for(var i =1; i<= no_room; i++) {
														options.append($date_picker("<option />").val(i).text(i));
													}
												}
												if(no_room == 'booked')
												{
													$date_picker('#home_capacity').addClass("no_booking");
													$date_picker("#home_capacity").append("<option>No Booking Available</option>");
												}
											}
									}
							
						});
				}
			 function load_property_posts(post_id){
				if (post_type!=""){
				 // document.getElementById("process").style.display ="block";
				}
				if(post_type=="room"){
					//document.getElementById("show_hide_tr").style.display ="table-row";
				}else{
					//document.getElementById("show_hide_tr").style.display ="none";
				}
				if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				}else{// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				var ids = '';

				if(document.getElementById('home_capacity_id'))
					ids = document.getElementById('home_capacity_id').value+',';
				if(document.getElementById('no_of_rooms_id'))
					ids += document.getElementById('no_of_rooms_id').value+',';
				if(document.getElementById('tax_id'))
					ids += document.getElementById('tax_id').value+',';
				if(document.getElementById('banner_id'))
					ids += document.getElementById('banner_id').value;
				xmlhttp.onreadystatechange=function(){
					if (xmlhttp.readyState==4 && xmlhttp.status==200){
						chechk_room_available();
						check_min_house_stay();
					 //document.getElementById("process").style.display ="none";
					document.getElementById("custom_fields").innerHTML=xmlhttp.responseText;
					}
				} 
				url = "<?php echo TEMPLATIC_BOOKING_URL.'/library/includes/load_room_house.php?post_id=';?>"+post_id+"&ids="+ids;
				xmlhttp.open("GET",url,true);
				xmlhttp.send();
			 }
			 
			 function show_hide_seasonal_prices_box(){
				$date_picker('#seasonal_prices_div').slideToggle('slow');
			}
			
			function delete_seasonal_price(id){
				if(id>0 && id!=""){
					if(confirm("<?php _e("Are you sure you want to delete this seasonal price?",BKG_DOMAIN);?>")){
						location.href="<?php echo admin_url();?>"+"admin.php?page=templatic_booking_system&tab=manage_prices&del_id="+id;
					}
				}
			}
			function view_seasonal_details(id){
				if(id!=""){
					$date_picker('#view_seasonal_in_detail_'+id).slideToggle('slow');
					$date_picker('#view_seasonal_tr_detail_'+id).css('disply','none');
				}
			}
			function load_room_house(post_type){
				 <?php if(is_plugin_active('wpml-translation-management/plugin.php')){
							global $sitepress;
							$current_lang_code= ICL_LANGUAGE_CODE;
							$language="&language=".$current_lang_code;
						}?>
				if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp1=new XMLHttpRequest();
				}else{// code for IE6, IE5
				  xmlhttp1=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp1.onreadystatechange=function(){
					if (xmlhttp1.readyState==4 && xmlhttp1.status==200){
						if(post_type == 'house' || post_type == ''){
							if($date_picker('#tr_no_of_room').length > 0){
								document.getElementById("tr_no_of_room").style.display = 'none';
							}
						}else{
							if($date_picker('#tr_no_of_room').length > 0){
								document.getElementById("tr_no_of_room").style.display = '';
							}
							
						} 
						document.getElementById("rooms_houses").innerHTML=xmlhttp1.responseText;
					}
				} 
				url = "<?php echo TEMPLATIC_BOOKING_URL.'/library/includes/fetch_room_house.php?post_type=';?>"+post_type+"<?php echo @$language;?>";
				xmlhttp1.open("GET",url,true);
				xmlhttp1.send();
			}
			 function check_min_house_stay()
				   {
				   		 var dataString = $date_picker('#post').serialize();
				   		if(document.getElementById('booking_post_type'))
						{
							var booking_type = document.getElementById('booking_post_type').value;
							if(booking_type == 'house')
							{
									var msg = '<?php _e("Still you can submit because as you have logged in as an admin.",BKG_DOMAIN);?>';
									$date_picker.ajax({
												type: "POST",
												url: "<?php echo TEMPLATIC_BOOKING_URL.'library/includes/house_minimum_stay.php';?>",
												data: dataString,
												success: function(minimum_stay){
															if(minimum_stay !='' )
															{
																$date_picker('#min_stay_tr').addClass("no_booking");
																$date_picker('#min_stay_tr').css('display','');
																$date_picker('#min_stay_td').html(minimum_stay+msg);
															}
															else
															{
																$date_picker('#min_stay_tr').css('display','none');
															}
													}
								});
							}
						}
				   }
		</script>
	<?php
		}
	}
	}
	
	if(!function_exists('change_update_status')){
	function change_update_status(){ ?>
		<script type="text/javascript">
			var $change = jQuery.noConflict();
			$change(document).ready(function() {
				$change('#update_status').click(function(event){
				  var post_status = $change('#status :selected').val();
				  var post_id = $change('#current_post_id').val();
				  var datastring = '&post_id=' + post_id + '&status=' + post_status;
				  $change('#auto_spinner').css('display','inline');
				  $change('#msg_update').html("");
				  $change.ajax({
						type: "POST",
						url: "<?php echo TEMPLATIC_BOOKING_URL.'/library/includes/update_booking_status.php';?>",
						data: datastring,
						success: function(html){
							$change('#auto_spinner').css('display','none');
							if(html==1){
								$change('#msg_update').html("<?php _e("Please add booking first!",BKG_DOMAIN);?>");
								$change('#msg_update').css('color','red');
								$change('#msg_update').delay(3000).fadeOut();
								$change('#msg_update').css('display','block');
							}else{
								$change('#msg_update').html("<?php _e("Status changed successfully",BKG_DOMAIN);?>");
								$change('#msg_update').css('color','black');
								$change('#msg_update').delay(3000).fadeOut();
								$change('#msg_update').css('display','block');
							}	
						},
						error:function(html){
							$change('#auto_spinner').css('display','none');
							$change('#msg_update').html("<?php _e("Failed to change status",BKG_DOMAIN);?>");
							$change('#msg_update').css('color','red');
							$change('#msg_update').delay(3000).fadeOut();
							$change('#msg_update').css('display','block');
						}
				  });
			   });
			});   
		</script>
	<?php }
	}
	
	if(!function_exists('templatic_booking_checkinout_callback')){
	function templatic_booking_checkinout_callback($post){
		global $wp_query;
		echo '<input type="hidden" name="booking_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
		$checkin_date = get_post_meta($post->ID,'checkin_date',true);
		$checkout_date = get_post_meta($post->ID,'checkout_date',true);
		$booking_post_type = get_post_meta($post->ID,'booking_post_type');
		$property_id = get_post_meta($post->ID,'property_room_id',true);
		$booking = $post;
		$no_of_rooms_id = '';
		$home_capacity_id = '';
		if( @$checkin_date[0]!="" || @$checkout_date[0]!=""){
			//$checkin_date = date('d F, Y', strtotime($checkin_date[0]));
			//$checkout_date = date('d F, Y', strtotime($checkout_date[0]));
		}	
		echo '<input type="hidden" name="checkindate" id="checkindate" value="'.$checkin_date.'" />';
		echo '<input type="hidden" name="checkoutdate" id="checkoutdate" value="'.$checkout_date.'" />';
		
	?>
		<table id="tvolution_fields" class="form-table" style="width:100%">
		<tr id="min_stay_tr" style="display:none">
			<td id="min_stay_td" colspan="2">
			</td>
			<td>
			</td>
		</tr>
			<tr>
				<th><label for="booking_post_type"><?php _e("Booking For",BKG_DOMAIN);?></label></th>
				<td>
					<select name="booking_post_type" id="booking_post_type" onchange="load_room_house(this.value);">
						<option value=""><?php _e("Please Select",BKG_DOMAIN);?></option>
						<option value="<?php echo CUSTOM_POST_TYPE_TEMPLATIC_ROOM;?>" <?php if($booking_post_type[0] == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){echo "selected=selected";}?>><?php echo CUSTOM_MENU_NAME_ROOM;?></option>
						<option value="<?php echo CUSTOM_POST_TYPE_TEMPLATIC_HOUSE;?>" <?php if($booking_post_type[0] == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){echo "selected=selected";}?>><?php echo CUSTOM_MENU_NAME_HOUSE;?></option>
					</select>
				</td>
			</tr>
			<tr id="rooms_houses">
				<th><label for="property_id"><?php _e("Select Room/House",BKG_DOMAIN);?></label></th>
				<td>
					<select name="property_id" id="property_id" onchange="chechk_room_available();"> <?php //if($booking_post_type == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){echo "selected=selected";}?>
						<option value=""><?php _e("Please Select",BKG_DOMAIN);?></option>
						<?php 
							if($booking_post_type[0]!=""){
								$args=array(
										'post_type'  => $booking_post_type[0],	
										'posts_per_page' => -1,
										'post_status' => array('publish'),	
										'order' => 'ASC'	
									);
								$posts_properties = new WP_Query($args);
								if($posts_properties){
									while ($posts_properties->have_posts()): $posts_properties->the_post();$the_id = get_the_id();?>
										<option value="<?php echo $the_id;?>" <?php if($property_id == $the_id){echo "selected=selected";}?>><?php echo the_title();?></option>
							<?php   endwhile; wp_reset_query();
								}else{
									echo '<option value="">No room/house available</option>';
								}
							}	
						?>
					</select>
					<span id='process' style='display:none;'><i class="fa fa-circle-o-notch fa-spin"></i></span>
				</td>
			</tr>
			<tr>
				<th><label for="date-range"><?php _e("Check-In/Check-Out date",BKG_DOMAIN);?></label></th>
				<td>
					<div id="date-range">
					  <div id="date-range-field">
						<span></span>
						<a href="#">&#9660;</a>
					  </div>
					  <div id="datepicker-calendar"></div>
					  <span id="booking_check_in_date_error" class="message_error2"></span>
					</div>	
					<p class="description"><?php _e("Select Arrival and Departure Date.",BKG_DOMAIN);?></p>
				</td>
			</tr>
			
			
			<?php
				if(get_post_meta( @$_REQUEST['post'],'booking_post_type',true) != 'house')
				{
			?>
			<tr id="tr_no_of_room">
				<th><label for="no_of_rooms"><?php _e("No of Rooms",BKG_DOMAIN);?></label></th>
				<td>
					<select name="no_of_rooms" id="no_of_rooms" >
						<option value=""><?php _e("Please Select",BKG_DOMAIN);?></option>
						<?php
						global $wpdb,$post,$sitepress;
						$args=
						array( 
							'post_type' => 'custom_fields',
							'posts_per_page' => 1,
							'post_status' => array('publish'),
							'name' => 'no_of_rooms'
						);
						$post_query = null;
						$post_query = new WP_Query($args);		
						if($post_query){
							while($post_query->have_posts()){
								$post_query->the_post();
								global $post;
								$no_of_rooms_id = $post->ID;
								$option_values = get_post_meta($post->ID,'option_values',true);
								$value = get_post_meta($_REQUEST['post'],'no_of_rooms',true);
								?>
								<?php if($option_values){
									$option_values_arr =get_post_meta($property_id,'no_of_rooms',true);
									for($i=1;$i<=$value;$i++)
									{
									?>
									<option value="<?php echo $i; ?>" <?php if($value==$i){ echo 'selected="selected"';} else if($default_value==$i){ echo 'selected="selected"';}?>><?php echo $i; ?></option>
									<?php	
									}
									?>
								<?php }?>
							<?php }wp_reset_query();
						}
						?>
					</select>
					<input type="hidden" name="no_of_rooms_id" id="no_of_rooms_id" value="<?php echo $no_of_rooms_id; ?>" />
				</td>
			</tr>
			<?php } ?>
			<tr id="custom_fields">
			<?php 
					global $wpdb,$post,$sitepress;
					$tax_id ='';
					$banner_id = '';
						$args=
						array( 
							'post_type' => 'custom_fields',
							'posts_per_page' => 1	,
							'post_status' => array('publish'),
							'name' => 'include_tax'
						);
						$post_query = null;
						$post_query = new WP_Query($args);		
						if($post_query){
							while($post_query->have_posts()){
								$post_query->the_post();
								global $post;
								$tax_id = $post->ID;
							}wp_reset_query();
						}
						$args=
						array( 
							'post_type' => 'custom_fields',
							'posts_per_page' => 1	,
							'post_status' => array('publish'),
							'name' => 'room_house_image'
						);
						$post_query = null;
						$post_query = new WP_Query($args);		
						if($post_query){
							while($post_query->have_posts()){
								$post_query->the_post();
								global $post;
								$banner_id = $post->ID;
							}wp_reset_query();
						}
						
					$my_post_type = get_post_type($property_id);
					$templatic_settings = get_option( "templatic_settings" );
					$custom_metaboxes = array();
					
					$exclude_post_id_array = $no_of_rooms_id.",".$home_capacity_id.",".$banner_id.",".$tax_id;
					 $custom_metaboxes[] =  get_post_custom_fields_templ_plugin($my_post_type,'','','',$exclude_post_id_array);//custom fields for custom post type..
					
					if($templatic_settings['templatic-category_custom_fields'] == 'No'){
					?>
				<td colspan="2">
				<?php
					$post = $booking;
					display_custom_post_field_plugin($custom_metaboxes,$property_id,$my_post_type);//displaty custom fields html.
				?>
				</td>
				<?php
				}
				?>
			</tr>
			<tr>
				<th><label for="total_amount"><?php _e("Total Charges",BKG_DOMAIN);?></label></th>
				<td><?php $_default_amt = get_post_meta($post->ID,'total_amount',true);?>
					<input type="text" class="regular-text pt_input_text" name="total_amount" id="total_amount" value="<?php echo $_default_amt;?>"/>
					<p class="description"><?php _e("Enter total charges included with tax.",BKG_DOMAIN);?></p>
				</td>
			</tr>
			<input type="hidden" name="tax_id" id="tax_id" value="<?php echo $tax_id; ?>" />
			<input type="hidden" name="banner_id" id="banner_id" value="<?php echo $banner_id; ?>" />
			<input type="hidden" name="booking_error" id="booking_error" value="<?php echo   get_post_meta($post->ID,'booking_error',true); ?>" />
		</table>
	<?php
	}
	}
	
	if(!function_exists('templatic_booking_checkinout_save_data')){
	function templatic_booking_checkinout_save_data($post_id){
		  // verify if this is an auto save routine. 
		  // If it is our form has not been submitted, so we dont want to do anything
		  global $wpdb;
		  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			  return;
		  // verify this came from the our screen and with proper authorization,
		  $other_post_type = @$_POST['post_type'];
		  $settings = get_option( "templatic_settings" );
		  
		  if(isset($_REQUEST['manage_price_submit']) && $_REQUEST['manage_price_submit'] != '')
			{
				
				$sql_custom_max_field = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_capacity' and $wpdb->posts.post_type = 'custom_fields'");
				$GetPostMeta = get_post_meta($sql_custom_max_field,'option_values',true);
				$max_guests = explode(",",$GetPostMeta);
				$manage_prices_settings_array = array();
				$price_for_property_id = $post_id;
				for($j=1;$j<=count($max_guests);$j++){
					$price_per_day = ($_REQUEST["price_day_$j"]) ? $_REQUEST["price_day_$j"] : 0;
					$price_per_week = ($_REQUEST["price_week_$j"]) ? $_REQUEST["price_week_$j"] : 0;
					$price_per_month = ($_REQUEST["price_month_$j"]) ? $_REQUEST["price_month_$j"] : 0;
					
					$manage_prices_settings_array["price_guests_$j"] = array("price_per_day" => $price_per_day,
														  "price_per_week" => $price_per_week,
														  "price_per_month" => $price_per_month,
															);
				}
				$settings["templatic_booking_system_settings"]['templatic_booking_manage_prices_settings']["price_for_$price_for_property_id"] = $manage_prices_settings_array;
				update_option('templatic_settings',$settings);
			}
		  if($other_post_type == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE || $other_post_type == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
			$other_post_id = $_POST['post_ID'];
			$closing_end_date = $_POST['closing_end_date'];
			$closing_start_date = $_POST['closing_start_date'];
			$booking_closing_table = $wpdb->prefix."booking_closing_check";

			if($_REQUEST['closing_start_date_name'][0] != '' && $_REQUEST['closing_end_date_name'][0] != '')
			{
				$sql_closing_check = $wpdb->get_results("select * from $booking_closing_table where property_id=$other_post_id");
				
				if(!empty($sql_closing_check[0]->closing_start_date))
				{
					
					$sep = ",";
					$closing_start_date = "";
					$closing_end_date = "";
					$sql_closing_delete = $wpdb->query("delete from $booking_closing_table where  property_id=$other_post_id");
					if($_REQUEST['closing_start_date_name'][0] != '' )
					{
						for($c=0;$c<count($_REQUEST['closing_start_date_name']);$c++)
						{
							if($c == (count($_REQUEST['closing_start_date_name'])-1))
								$sep = "";
							$closing_start_date .= $_REQUEST['closing_start_date_name'][$c].$sep;
							$closing_end_date .= $_REQUEST['closing_end_date_name'][$c].$sep;
							$sql_closing_insert = $wpdb->query("INSERT INTO $booking_closing_table SET property_id=$other_post_id, closing_start_date='".$_REQUEST['closing_start_date_name'][$c]."', closing_end_date='".$_REQUEST['closing_end_date_name'][$c]."'");
						}
						$post_ID = $_POST['post_ID'];
						update_post_meta($post_ID,'closing_start_date',$closing_start_date);
						update_post_meta($post_ID,'closing_end_date',$closing_end_date);
					}
				
				}	
				else{
					$sep = ",";
					$closing_start_date = "";
					$closing_end_date = "";
			
					if($_REQUEST['closing_start_date_name'][0] != '' )
					{
						for($c=0;$c<count($_REQUEST['closing_start_date_name']);$c++)
						{
							if($c == (count($_REQUEST['closing_start_date_name'])-1))
								$sep = "";
							$closing_start_date .= $_REQUEST['closing_start_date_name'][$c].$sep;
							$closing_end_date .= $_REQUEST['closing_end_date_name'][$c].$sep;
							$sql_closing_insert = $wpdb->query("INSERT INTO $booking_closing_table SET property_id=$other_post_id, closing_start_date='".$_REQUEST['closing_start_date_name'][$c]."', closing_end_date='".$_REQUEST['closing_end_date_name'][$c]."'");
						}
						$post_ID = $_POST['post_ID'];
						update_post_meta($post_ID,'closing_start_date',$closing_start_date);
						update_post_meta($post_ID,'closing_end_date',$closing_end_date);
					}
				}
		  	}
		  }
		  // because save_post can be triggered at other times
		  if ( !wp_verify_nonce( @$_POST['booking_nonce'], basename(__FILE__) ) )
			  return $post_id;
		  
		  // Check permissions
		  if ( 'page' == $_POST['post_type'] ) {
			if ( !current_user_can( 'edit_page', $post_id ) )
				return;
		  }else{
			if ( !current_user_can( 'edit_post', $post_id ) )
				return;
		  }

		  //find and save the data
		  $post_ID = $_POST['post_ID'];
		  $booking_post_type = $_POST['booking_post_type'];
		  $property_id = $_POST['property_id'];
		  $booking_date = date('Y-m-d');
		  $checkin_date = $_POST['checkindate'];
		  $checkout_date = $_POST['checkoutdate'];
		  //$total_guest = $_POST['total_guest'];
		  //$no_of_rooms = $_POST['no_of_rooms'];
		  $total_amount = $_POST['total_amount'];
		  $status = $_POST['status'];
		  
		  $paymentmethod = 'prebanktransfer';
		  
		  $currency = get_option("currency_symbol");
		  $position = get_option("currency_pos");
		  $total_amount = str_replace(get_option('currency_symbol'),'',$total_amount);
		  if($position == 1){
				$total_amounts = $currency.$total_amount;
		  }else if($position == 2){
				$total_amounts = $currency." ".$total_amount;
		  }else if($position == 3){
				$total_amounts = $total_amount.$currency;
		  }else if($position == 4){
				$total_amounts = $total_amount." ".$currency;
		  }
		  
		  //FUNCTION TO GET ALL THE CUSTOM POST META BY POST TYPE START
			$pt_metaboxes = get_post_admin_custom_fields_templ_plugin($booking_post_type);
		  //FUNCTION TO GET ALL THE CUSTOM POST META BY POST TYPE FINISH
		  
		  //ADD,UPDATE,DELETE POST METAS START
		    if($booking_post_type!=""){
				update_post_meta($post_ID,'booking_post_type',$booking_post_type);
			}
			if($property_id!=""){
				update_post_meta($post_ID,'property_room_id',$property_id);
			}
			if($booking_date!=""){
				update_post_meta($post_ID,'booking_date',$booking_date);
			}
			if($checkin_date!=""){
				update_post_meta($post_ID,'checkin_date',$checkin_date);
			}
			if($checkout_date!=""){
				update_post_meta($post_ID,'checkout_date',$checkout_date);
			}
			if($total_amounts!=""){
				update_post_meta($post_ID,'total_amount',$total_amounts);
			}
			if($paymentmethod!=""){
				update_post_meta($post_ID,'paymentmethod',$paymentmethod);
			}
			if($_REQUEST['booking_error']!="" || get_post_meta($property_id,'no_of_rooms',true) < $_REQUEST['no_of_rooms'] ){
				global $wpdb;
				$where = array( 'ID' => $post_ID );
				$wpdb->update( $wpdb->posts, array( 'post_status' => 'draft' ), $where );

				update_post_meta($post_ID,'booking_error',$_REQUEST['booking_error']);
			}
			
			$options = get_option('templatic_settings');
			$tax_type = $options['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_type'];
			if($tax_type!=""){
				update_post_meta($post_ID,'tax_type',$tax_type);
			}

			foreach ($pt_metaboxes as $pt_metabox) { // On Save.. this gets looped in the header response and saves the values submitted
				if($pt_metabox['type'] == 'text' OR $pt_metabox['type'] == 'select' OR $pt_metabox['type'] == 'checkbox' OR $pt_metabox['type'] == 'textarea' OR $pt_metabox['type'] == 'radio'  OR $pt_metabox['type'] == 'upload' OR $pt_metabox['type'] == 'date' OR $pt_metabox['type'] == 'multicheckbox' OR $pt_metabox['type'] == 'geo_map' OR $pt_metabox['type'] == 'texteditor'){ // Normal Type Things...
					$var = $pt_metabox["name"];
					if( get_post_meta( $post_ID, $pt_metabox["name"] ) == "" ){
						add_post_meta($post_ID, $pt_metabox["name"], $_POST[$var], true );
					}elseif($_POST[$var] != get_post_meta($post_ID, $pt_metabox["name"], true)){
						update_post_meta($post_ID, $pt_metabox["name"], $_POST[$var]);
					}elseif($_POST[$var] == ""){
						delete_post_meta($post_ID, $pt_metabox["name"], get_post_meta($post_ID, $pt_metabox["name"], true));
					}
				}
			}
			//ADD,UPDATE,DELETE POST METAS FINISH
			
			
			//update_post_meta($post_ID,'total_guest',$total_guest);
			
			$booking_availability_table = $wpdb->prefix."booking_availability_check";
			$checkout_previous_date = date('Y-m-d', strtotime($checkout_date .' -1 day'));
			$sql_availability_check = $wpdb->get_var("select booking_post_id from $booking_availability_table where booking_post_id=$post_ID");
			if($sql_availability_check){
				$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET property_id=$property_id, from_date='$checkin_date', to_date='$checkout_previous_date', status='$status' where booking_post_id=$post_ID");
			}else{
				$sql_availability_insert = $wpdb->query("INSERT INTO $booking_availability_table SET property_id=$property_id, booking_post_id=$post_ID, from_date='$checkin_date', to_date='$checkout_previous_date', status='Pending'");
			}
			$transection_db_table_name = $wpdb->prefix . "transactions";
			
			if($wpdb->query("SHOW COLUMNS FROM $transection_db_table_name where Field='prop_id'")){}
				else{
					$sql="ALTER table $transection_db_table_name add prop_id bigint(20)";
					$wpdb->query($sql);
				}
			$billing_Address = $_POST['booking_street'].' '.$_POST['booking_city_state'].', '.$_POST['booking_country'];
			//echo $_POST['total_amount'];exit;
			
			/* $transaction_insert = 'INSERT INTO '.$transection_db_table_name.' set 
				post_id='.$post_ID.',
				user_id = '.$_POST['post_author'].',
				post_title ="'.$_POST['post_title'].'",
				payment_method="'.$paymentmethod.'",
				payable_amt='.str_replace('$','',$_POST['total_amount']).',
				payment_date="'.date("Y-m-d H:i:s").'",
				paypal_transection_id="",
				status="0",
				user_name="'.$_POST['post_title'].'",
				pay_email="'.$_POST['booking_email'].'",
				billing_name="'.$_POST['post_title'].'",
				billing_add="'.$billing_Address.'",prop_id="'.$_POST['property_id'].'"';
				global $trans_id;
				$wpdb->query($transaction_insert);
				$trans_id = mysql_insert_id(); */
			
			
			
     	  // Do something with $mydata 
		  // probably using add_post_meta(), update_post_meta(), or 
		  // a custom table (see Further Reading section below)
		}
	}
	add_action( 'init', 'submit_booking_info' ,10);
	function submit_booking_info()
	{
		if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'paynow'){ 
			if(file_exists(TEMPLATIC_BOOKING_PATH."/library/includes/booking_paynow.php")){
				include_once( TEMPLATIC_BOOKING_PATH."/library/includes/booking_paynow.php");
			}
		}
	}
	
	//Delete data of booking system from tevolution on plugin deactivation
	register_deactivation_hook(__FILE__,'unregister_booking_taxonomy');
	if(!function_exists('unregister_booking_taxonomy')){
	function unregister_booking_taxonomy(){
		 delete_option('templatic_booking_system');	
		 delete_option('insert_dummy_data');	
		 $post_type = get_option("templatic_custom_post");
		 $taxonomy = get_option("templatic_custom_taxonomy");
		 $tag = get_option("templatic_custom_tags");
		 $taxonomy_slug = $post_type['room']['slugs'][0];
		 $taxonomy_slug1 = $post_type['house']['slugs'][0];
		 $taxonomy_slug2 = $post_type['booking']['slugs'][0];
		 $tag_slug = $post_type['room']['slugs'][1];
		 $tag_slug1 = $post_type['house']['slugs'][1];
		 $tag_slug2 = $post_type['booking']['slugs'][1];
		 
		 unset($post_type['room']);
		 unset($post_type['house']);
		 unset($post_type['booking']);
		 //unset($taxonomy[$taxonomy_slug]);
		 //unset($taxonomy[$taxonomy_slug1]);
		 //unset($taxonomy[$taxonomy_slug2]);
		 unset($tag[$tag_slug]);
		 unset($tag[$tag_slug1]);
		 unset($tag[$tag_slug2]);
		 
		 update_option("templatic_custom_post",$post_type);
		 update_option("templatic_custom_taxonomy",$taxonomy);
		 update_option("templatic_custom_tags",$tag);
		 unlink(get_template_directory()."/taxonomy-".$taxonomy_slug.".php");
		 unlink(get_template_directory()."/taxonomy-".$taxonomy_slug1.".php");
		 unlink(get_template_directory()."/taxonomy-".$taxonomy_slug2.".php");
		 unlink(get_template_directory()."/taxonomy-".$tag_slug.".php");
		 unlink(get_template_directory()."/taxonomy-".$tag_slug1.".php");
		 unlink(get_template_directory()."/taxonomy-".$tag_slug2.".php");
		 unlink(get_template_directory()."/single-".'room'.".php");
		 unlink(get_template_directory()."/single-".'house'.".php");
		 unlink(get_template_directory()."/single-".'booking'.".php");
		 
		 
		 global $wpdb;
		 //delete_option('sidebars_widgets'); //delete widgets
		 $productArray = array();
		 $pids_sql = "select p.ID from $wpdb->posts p join $wpdb->postmeta pm on pm.post_id=p.ID where meta_key='booking_dummy_content' and meta_value=1";
		 $pids_info = $wpdb->get_results($pids_sql);
		 foreach($pids_info as $pids_info_obj){
		 	wp_delete_post($pids_info_obj->ID,true);
		 }
		 
	}
	}
	
	/*remove price field on category page*/
	add_action('init','remove_price_tag',11);
	function remove_price_tag()
	{
		$post_type = get_option("templatic_custom_post");
		$taxonomy_slug = $post_type['room']['slugs'][0];
		$taxonomy_slug1 = $post_type['house']['slugs'][0];
		$taxonomy_slug2 = $post_type['booking']['slugs'][0];
		remove_action($taxonomy_slug.'_add_form_fields','category_custom_fields_AddField');
		remove_action($taxonomy_slug1.'_add_form_fields','category_custom_fields_AddField');
		remove_action($taxonomy_slug2.'_add_form_fields','category_custom_fields_AddField');
		add_action($taxonomy_slug2.'_add_form_fields','booking_category_custom_fields_AddField');
		remove_filter('manage_edit-'.$taxonomy_slug.'_columns', 'edit_price_cat_column');	
	}
	function booking_category_custom_fields_AddField()
	{
		booking_add_category_price_field($tax,'add');
	}
	/* EOF - ADD CATEGORY PRICE */
	
	/* NAME : FUNCTION TO ADD/EDIT CATEGORY PRICE FIELD
	ARGUMENTS : TAXONOMY NAME, OPERATION
	DESCRIPTION : THIS FUNCTION ADDS/EDITS THE CATEGORY PRICE FIELD IN BACK END */
	function booking_add_category_price_field($tax,$screen)
	{
		if((isset($tax->taxonomy) && $tax->taxonomy != '') || (isset($tax->term_price) && $tax->term_price != ''))
		{
			$taxonomy = $tax->taxonomy;
			$term_price = $tax->term_price;
		}
			$currency_symbol = get_option('currency_symbol');			
			?>
				<tr class="form-field">
					<th scope="row" valign="top"><label for="cat_price"><?php _e("Category Price", BKG_DOMAIN); echo ' ('.$currency_symbol.')'?></label></th>
					<td><input type="text"  name="cat_price" id="category_price" value="<?php if(isset($term_price) && $term_price != '') { echo $term_price; } ?>"  size="20"/>
					<p class="description"><?php _e('Here you can set the category price for a particular category for a single day per room.',BKG_DOMAIN);?>.</p>
					</td>
				</tr>
	
		<?php
	}
	if(isset($_REQUEST['update_transaction_status']) && $_REQUEST['update_transaction_status']!= '')
	{
		$my_post = array();
		if(count($_REQUEST['cf'])>0)
		{
			for($i=0;$i<count($_REQUEST['cf']);$i++)
			{
				$cf = explode(",",$_REQUEST['cf'][$i]);
				$my_post['ID'] = $cf[1];
				
				if(isset($_REQUEST['action']) && $_REQUEST['action']== 'confirm')
				{
					$status = 'publish';
					$UpdatedStatus = 'Approved';
				}
				elseif(isset($_REQUEST['action']) && $_REQUEST['action']== 'pending')
				{
					$status = 'draft';
					$UpdatedStatus = 'Pending';
				}
				
				$my_post['post_status'] = $status;
				wp_update_post( $my_post );
				update_post_meta($cf[1],'status',$UpdatedStatus);
				$booking_availability_table = $wpdb->prefix."booking_availability_check";
				$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET status='$UpdatedStatus' where booking_post_id=".$cf[1]);
			}
		}
		else
		{
			$my_post['ID'] = $_REQUEST['update_transaction_status'];
			if(isset($_REQUEST['ostatus']) && $_REQUEST['ostatus']== 1)
			{
				$status = 'publish';
				$UpdatedStatus = 'Approved';
			}
			elseif(isset($_REQUEST['ostatus']) && $_REQUEST['ostatus']== 0)
			{
				$status = 'draft';
				$UpdatedStatus = 'Pending';
			}
			elseif(isset($_REQUEST['ostatus']) && $_REQUEST['ostatus']== 2)
			{
				$status = 'draft';
				$UpdatedStatus = 'Cancelled';
			}
			$my_post['post_status'] = $status;
			wp_update_post( $my_post );
			update_post_meta($_REQUEST['update_transaction_status'],'status',$UpdatedStatus);
			$booking_availability_table = $wpdb->prefix."booking_availability_check";
			$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET status='$UpdatedStatus' where booking_post_id=".$_REQUEST['update_transaction_status']);
		}
	}
}

function my_custom_bulk_actions($actions){
		 ?>
    <script type="text/javascript">
      jQuery(document).ready(function() {
        jQuery('<option>').val('Pending').text('<?php _e('Pending')?>').appendTo("select[name='action']");
		jQuery('<option>').val('Approved').text('<?php _e('Approved')?>').appendTo("select[name='action']");
		jQuery('<option>').val('Cancelled').text('<?php _e('Cancelled')?>').appendTo("select[name='action']");
      });
    </script>
    <?php
    return $actions;
}
add_filter('bulk_actions-edit-booking','my_custom_bulk_actions');
add_action('load-edit.php', 'custom_bulk_action');
function custom_bulk_action()
{
	global $wpdb;
	if(isset($_REQUEST['post_type']) && $_REQUEST['post_type'] == 'booking')
	{     
		$my_post = array();
		if(count(@$_REQUEST['post'])>0)
		{
			for($i=0;$i<count(@$_REQUEST['post']);$i++)
			{
				$cf = $_REQUEST['post'];
				$my_post['ID'] = $cf[$i];
				$default_status = get_post_meta($cf[$i],'status',true);
				if(isset($_REQUEST['action']) && $_REQUEST['action']== 'Approved')
				{
					$status = 'publish';
					$UpdatedStatus = 'Approved';
					$tr_status = 1;
				}
				elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'Cancelled')
				{
					$status = 'draft';
					$UpdatedStatus = 'Cancelled';
					$tr_status = 0;
				}
				elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'Pending')
				{
					$status = 'draft';
					$UpdatedStatus = 'Pending';
					$tr_status = 0;
				}else{
					$UpdatedStatus = $default_status;
				}
				
				$my_post['post_status'] = $status;
				wp_update_post( $my_post );
				$transection_db_table_name = $wpdb->prefix . "transactions";
				$transaction_insert = 'UPDATE '.$transection_db_table_name.' set status='.$tr_status.' where post_id='.$cf[$i];
				global $trans_id;
				$wpdb->query($transaction_insert);
				$trans_id = mysql_insert_id();
				
				update_post_meta($cf[$i],'status',$UpdatedStatus);
				$booking_availability_table = $wpdb->prefix."booking_availability_check";
				$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET status='$UpdatedStatus' where booking_post_id=".$cf[$i]);
				
				$PostId = $cf[$i];
				$fromEmail = get_site_emailId_plugin();
				$fromEmailName = get_site_emailName_plugin();
				$toEmail = get_post_meta($PostId,'booking_email',true);
				$toEmailName = get_the_title($PostId);
				$store_name = get_option('blogname');
				if(strtolower($UpdatedStatus) == strtolower("Approved")){
				///////EMAIL START//////
				
					$templatic_settings = get_option('templatic_settings');
					$email_content = $templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_msg'];
					$email_subject = $templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_sub'];
					if(!$email_subject)
					{
						$email_subject = __('Booking-Confirmed');	
					}
					if(!$email_content)
					{
						$email_content = '<p>'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('Your booking is confirmed. We hope you enjoy the stay at our Hotel.',BKG_DOMAIN).'</p>
										  <p>'.__('In case you have any queries, please email us at',BKG_DOMAIN).' [CONTACT_MAIL]. '.__('We will be glad to assist you.',BKG_DOMAIN).'</p>
										  <p>'.__('If you want to cancel your booking request then click on link given below.',BKG_DOMAIN).'</p>
		                                  <p>[CANCEL_URL]<br />'.__('Thank you for staying at our Hotel.',BKG_DOMAIN).'</p>
		                                  <p><br />[ADMIN_NAME]</p>';
					}
					$cancel="<a href='".get_option('home')."/?page_id=$page_name_id&booking_id=".$PostId."'>".get_option('home')."/?page_id=$page_name_id&booking_id=".$PostId."</a>";
					$property_email = get_post_meta($PostId,'property_room_id',true);
					$BookingPostType = get_post_meta($PostId,'booking_post_type',true);
					$email_info = get_post_meta($property_email,'home_contact_mail',true);//Fetch email id from house's email meta information   
					$hotel_email_info = get_option('admin_email');
				
					if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
						$hotel_name = get_the_title($property_email);
						$addresses = get_post_meta($property_email,'address',true);
						$address_exclude = explode(',',$addresses);
						$home_street = $address_exclude[0];
						$home_state = $address_exclude[1];
						$home_country = $address_exclude[2];
					}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
						$AllTemplaticSettings = get_option('templatic_settings');
						$hotel_name = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
						$home_street = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
						$home_state = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
						$home_country = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
					}
					
					$regard_content = '<strong>'.$hotel_name.'</strong><br />'.$home_street.'<br />'.$home_state;
					if($home_country!=""){
						$regard_content .= ', '.$home_country;
					}
					
					$serach_array = array('[USER_NAME]', '[CONTACT_MAIL]','[CANCEL_URL]','[ADMIN_NAME]');
					$replace_array = array($toEmailName, '<i>'.$hotel_email_info.'</i>',$cancel,$regard_content);
					$mail_content = str_replace($serach_array,$replace_array,$email_content);
					
					templ_send_email($fromEmail,$fromEmailName,$toEmail,$toEmailName,$email_subject,stripslashes($mail_content),$extra='');//to client email 
				//////EMAIL END////////
			}elseif(strtolower($UpdatedStatus) == strtolower("Cancelled") || strtolower($UpdatedStatus) == strtolower("Pending")){
				///////EMAIL START//////
					$post_data = get_post($PostId);
					$templatic_settings = get_option('templatic_settings');
					$email_content = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_msg'];
					$email_subject = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_sub'];
					if(!$email_subject)
					{
						$email_subject = __('Booking- Could not confirm');	
					}
					if(!$email_content)
					{
						$email_content = '<p>'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('Your booking request is rejected. Please try again later.',BKG_DOMAIN).'<br /><br />'.__('Thanks for your interest.',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>';
					}
					$property_id = get_post_meta($PostId,'property_room_id',true);
					$BookingPostType = get_post_meta($PostId,'booking_post_type',true);
					
					
					if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
						$hotel_name = get_the_title($property_id);
						$addresses = get_post_meta($property_id,'address',true);
						$address_exclude = explode(',',$addresses);
						$home_street = $address_exclude[0];
						$home_state = $address_exclude[1];
						$home_country = $address_exclude[2];
					}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
						$hotel_name = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
						$home_street = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
						$home_state = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
						$home_country = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
					}
					$regard_content = '<strong>'.$hotel_name.'</strong><br />'.$home_street.'<br />'.$home_state;
					if($home_country!=""){
						$regard_content .= ', '.$home_country;
					}
					$serach_array = array('[USER_NAME]', '[ADMIN_NAME]');
					$replace_array = array($toEmailName, $regard_content);
					$mail_content = str_replace($serach_array,$replace_array,$email_content);
					templ_send_email($fromEmail,$fromEmailName,$toEmail,$toEmailName,$email_subject,stripslashes($mail_content),$extra='');//to client email 
				//////EMAIL END////////
			}
			}
		}
		
		
	}
}
/*
Name : update_admin_booking_status
Description : to update booking table if we trash the booking.
*/
if(strstr($_SERVER['REQUEST_URI'],'wp-admin') )
{
	add_action('trash_booking', 'update_admin_booking_status',1,1); // to delete the post of old recurrencies
}
function update_admin_booking_status($post_id)
{
	global $wpdb;
	$booking_availability_table = $wpdb->prefix."booking_availability_check";
	if(!is_array($_REQUEST['post']))
	{
		$_REQUEST['post'] = array($_REQUEST['post']);
	}
	 for($i=0;$i<count($_REQUEST['post']);$i++)
	 {
	 	$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET status='Pending' where booking_post_id=".$_REQUEST['post'][$i]);
	}	
}

add_action('wp_ajax_booking_system','booking_system_update_login', 10);
/*
 * Function Name: Booking System Login
 * Return: update Booking System plugin version after templatic member login
 */

function booking_system_update_login(){	
	check_ajax_referer( 'booking_system', '_ajax_nonce' );
	$plugin_dir = rtrim( plugin_dir_path(__FILE__), '/' );	
	require_once( $plugin_dir .  '/templatic_login.php' );	
	exit;
}
add_action('admin_init','booking_admin_init_callback',9);
function booking_admin_init_callback(){
	add_filter('reset_exclude_post_types','reset_exclude_post_types_callback',9);
}
if(!function_exists('reset_exclude_post_types_callback')){
	function reset_exclude_post_types_callback($exclude_post_types){
		$exclude_post_types[] = "booking";
		$exclude_post_types[] = CUSTOM_POST_TYPE_TEMPLATIC_HOUSE;
		$exclude_post_types[] = CUSTOM_POST_TYPE_TEMPLATIC_ROOM;
		return $exclude_post_types;
	}
}
/*
*@ Removed sorting option and grid view list view option from booking plugin.
*/
remove_action('templ_after_categories_description','templ_after_categories_sort_by');
/*
 * Function Name: booking_after_categories_view
 *
 */
add_action('templ_after_categories_description','booking_after_categories_view');
function booking_after_categories_view()
{
	global $wpdb,$wp_query;
	$current_term = $wp_query->get_queried_object();
	$templatic_settings=get_option('templatic_settings');
	
	/*custom post type ter, link */
	if(!is_tax() && is_archive() && !is_search())
	{			
		$post_type=(get_post_type()!='')? get_post_type() : get_query_var('post_type');
		$permalink = get_post_type_archive_link($post_type);
		$permalink=str_replace('&tevolution_sortby=alphabetical&sortby='.$_REQUEST['sortby'],'',$permalink);
	}elseif(is_search()){
		$search_query_str=str_replace('&tevolution_sortby=alphabetical&sortby='.$_REQUEST['sortby'],'',$_SERVER['QUERY_STRING']);
		$permalink=get_bloginfo('siteurl')."?".$search_query_str;
	}else{
		$permalink =  get_term_link( $current_term->slug, $current_term->taxonomy );
		$permalink=str_replace('&tevolution_sortby=alphabetical&sortby='.$_REQUEST['sortby'],'',$permalink);
	}
	$post_type=get_post_type_object( get_post_type());
		
	if(false===strpos($permalink,'?')){
	    $url_glue = '?';
	}else{
		$url_glue = '&amp;';	
	}
	//wp_enqueue_script( 'toggle-script', TEVOLUTION_PAGE_TEMPLATES_URL.'js/script.js' );
	?>
     <script src="<?php echo TEVOLUTION_PAGE_TEMPLATES_URL.'js/script.js';?>"></script>
     <?php
	if(have_posts()!=''):
	?>
     
     <div class='tevolution_manager_tab clearfix'>
     	<div class="sort_options">
		<ul class='view_mode viewsbox'>
			<li><a class='switcher first gridview' id='gridview' href='#'><?php _e(GRID_VIEW_TEXT);?></a></li>
			<li><a class='switcher last listview active' id='listview' href='#'><?php _e(LIST_VIEW_TEXT); ?></a></li>
			<?php if($templatic_settings['category_googlemap_widget']=='yes'):?> <li><a class='map_icon' id='event_map' href='#'><?php _e('MAP',BKG_DOMAIN);?></a></li><?php endif;?>
		</ul>
		</div>
	</div>
		<?php
	endif;
}
/* 
 * Function Name: tevolution_booking_house_csvfile
 * Display the product sample csv file
 */
add_action('tevolution_house_sample_csvfile','tevolution_booking_house_csvfile');
function tevolution_booking_house_csvfile(){
	?>
     <a href="<?php echo TEMPLATIC_BOOKING_URL.'library/includes/house_sample.csv';?>"><?php _e('(Sample csv file)',BKG_DOMAIN);?></a>
     <?php	
}

/* 
 * Function Name: tevolution_booking_room_csvfile
 * Display the product sample csv file
 */
add_action('tevolution_room_sample_csvfile','tevolution_booking_room_csvfile');
function tevolution_booking_room_csvfile(){
	?>
     <a href="<?php echo TEMPLATIC_BOOKING_URL.'library/includes/room_sample.csv';?>"><?php _e('(Sample csv file)',BKG_DOMAIN);?></a>
     <?php	
}

/*
 * Function Name: tempaltic_booking_post_type
 * unset the booking plugin post type as per directory post type template
 */
add_filter('directory_post_type_template','tempaltic_booking_post_type');
function tempaltic_booking_post_type($post_type){
	foreach ($post_type as $key => $value) {
		if($value=='house' || $value=='room' || $value=='booking'){
			unset($post_type[$key]);
		}
	}	
	return $post_type;
}
/*
 * name : min_stay_room_type
 * description : ajax to fetch the room type while adding seasonal price to check post type
 * */
add_action('wp_ajax_min_stay_room_type','min_stay_room_type');
function min_stay_room_type()
{
	if(isset($_REQUEST['pid']) && $_REQUEST['pid']!='')
	{
		echo get_post_type($_REQUEST['pid']);exit;
	}
}
/*
 * name : hide_below_header_category_page_sidebar
 * description : hide below header widget area.
 * */
add_filter('admin_init','hide_below_header_category_page_sidebar');
function hide_below_header_category_page_sidebar()
{
	global $pagenow;	
	if($pagenow != 'customize.php'):
		$args = get_option('templatic_custom_taxonomy');
		$args1 = get_option('templatic_custom_post');
		if($args):
			foreach($args as $key=> $_args)
			{
				unregister_sidebar('after_'.$_args["labels"]["singular_name"].'_header');
			}
		endif;
	endif;			
}
?>