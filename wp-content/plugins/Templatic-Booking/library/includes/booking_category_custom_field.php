<?php
/*
 * Add The custom Category's custom field
 */
function booking_category_custom_fields_AddFieldsAction($tag){
	booking_category_custom_fields_AddFields($tag,'add');
}

function booking_category_custom_fields_AddFields($tag,$screen){
	$tax = @$tag->taxonomy;
	$term_price=isset($tag->term_price)?$tag->term_price:0;
	$term_service = @$tag->used_for;
	$currency_symbol=get_option('currency_symbol');
	if($currency_symbol==""){
		$currency_symbol="$";
	}	
	$custom_post_types_args = array();
	$custom_post_types = get_post_types($custom_post_types_args,'objects');   
	$templatic_post_types = get_option('templatic_custom_post');
	//Get the post label from tevolution custom taxonomy labels
	$house_post_type_name = $templatic_post_types['house']['labels']['name'];
	$room_post_type_name = $templatic_post_types['room']['labels']['name'];	
?>
		<tr class="form-field" style="padding: 8px;">
			<th valign="top" scope="row"><label for="services_post_type"><?php _e("Select post type", BKG_DOMAIN);?></label><br/></th>
			<td style="padding:5px 0 10px 15px;">
				<?php
					foreach ($custom_post_types as $content_type){
						if($content_type->name == strtolower(trim(CUSTOM_POST_TYPE_TEMPLATIC_HOUSE)) || $content_type->name == strtolower(trim(CUSTOM_POST_TYPE_TEMPLATIC_ROOM))){
				?>
							<label><input type="checkbox" name="services_post_type[]" id="services_post_type" value="<?php echo $content_type->name;?>" <?php if($content_type->name =='house' && $term_service == "house"){echo 'checked=checked';}elseif($content_type->name =='room' && $term_service == "room"){echo 'checked=checked';}elseif($term_service == "house,room"){echo 'checked=checked';}?> style="width: auto ! important;"/> <?php if($content_type->name == "house"){echo $house_post_type_name;}elseif($content_type->name == "room"){echo $room_post_type_name;}?></label><br/>
				<?php   }
					}
				?>			
				<p class="description"><?php _e('Select Post type in which you want to use this service.',BKG_DOMAIN);?></p>
			</td>
			
		</tr>
	<?php

}
/*
 * Edit the custom post category's custom filed
 */
function booking_category_custom_fields_EditFields($tag){
	booking_category_custom_fields_AddFields($tag,'edit');	
}

/*
 * Add / Update Custom category's filed
 */
function booking_category_custom_fields_AlterFields($termId){
	if(isset($_REQUEST['services_post_type']) && $_REQUEST['services_post_type'] != "" && !empty($_REQUEST['services_post_type'])){
		global $wpdb;
		$term_table=$wpdb->prefix."terms";	
		$services = $_POST['services_post_type'];
		$services = implode(',',$_POST['services_post_type']);
		//update the service price value in terms table field
		$sql="update $term_table set used_for='".$services."' where term_id=".$termId;
		$wpdb->query($sql);
		
		
	}
}
	
/*
 * Function Name: booking_service_categories_columns
 * Argument: columns
 * Return: service columns header
 */  
function booking_service_categories_columns($columns){
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'name' => __('Service Name'),
		'price' =>  __('Price'),
		'description' => __('Description'),
		'slug' => __('Slug'),
		'posts' => __('Bookings')
		);
	return $columns;
}	
	
	  

/*
 * Function Name: booking_manage_service_columns
 * Argument: output, columns name, service id
 * Return output
 */
function booking_manage_service_columns($out, $column_name, $service_id) {
	$service = get_term($service_id, 'services');		
	//echo "<pre>";print_r($service);echo "</pre>";
	switch ($column_name) {
		case '':	
			$currency_symbol=get_option('currency_symbol');			
			if($currency_symbol=="")
				$currency_symbol="$";
			$price=display_amount_with_currency_plugin($service->term_price);
			if(!$price){
				$price='10';
			}else{
				$price=$price;
			}	
			$out .=$price; 
			break;
 
		default:
			break;
	}
	return $out;	
}  
    
?>