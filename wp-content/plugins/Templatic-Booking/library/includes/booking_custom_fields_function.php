<?php
/* 
Name :get_search_post_fields_templ_plugin
description : Returns all default custom fields
*/
function get_booking_post_fields_templ_plugin($post_types,$category_id='',$taxonomy='',$heading_type='') {
	global $wpdb,$post,$wp_query;
	remove_all_actions('posts_where');
	if($heading_type){
		$args=
		array(
		'post_type' => 'custom_fields',
		'posts_per_page' => -1	,
		'post_status' => array('publish'),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'post_type_'.$post_types,
				'value' => $post_types,
				'compare' => '=',
				'type'=> 'text'
			),
			array(
				'key' => 'show_on_page',
				'value' =>  array('user_side','both_side'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			array(
				'key' => 'is_active',
				'value' =>  '1',
				'compare' => '='
			),
			array(
				'key' => 'heading_type',
				'value' =>  array('basic_inf',$heading_type),
				'compare' => 'IN'
			),

		),
		 
		 'meta_key' => 'sort_order',
		 'orderby' => 'meta_value_num',
		 'meta_value_num'=>'sort_order',
		);
	}else{
		$args=
		array( 
		'post_type' => 'custom_fields',
		'posts_per_page' => -1	,
		'post_status' => array('publish'),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'post_type_'.$post_types,
				'value' => $post_types,
				'compare' => '=',
				'type'=> 'text'
			),
			array(
				'key' => 'show_on_page',
				'value' =>  array('user_side','both_side'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			array(
				'key' => 'is_active',
				'value' =>  '1',
				'compare' => '='
			)

		),
		 
		 'meta_key' => 'sort_order',
		 'orderby' => 'meta_value_num',
		 'meta_value_num'=>'sort_order',
		);
	}
	
	$post_query = null;
	add_filter('posts_join', 'custom_field_posts_where_filter');
	$post_query = new WP_Query($args);
	remove_filter('posts_join', 'custom_field_posts_where_filter');
	///echo "<pre>";print_r($post_query);echo "</pre>";
	$post_meta_info = $post_query;
	$return_arr = array();
	if($post_meta_info){
		while ($post_meta_info->have_posts()) : $post_meta_info->the_post();
			if(get_post_meta($post->ID,"ctype",true)){
				$options = explode(',',get_post_meta($post->ID,"option_values",true));
			}
			$custom_fields = array(
					"name"		=> get_post_meta($post->ID,"htmlvar_name",true),
					"label" 	=> $post->post_title,
					"htmlvar_name" 	=> get_post_meta($post->ID,"htmlvar_name",true),
					"default" 	=> get_post_meta($post->ID,"default_value",true),
					"type" 		=> get_post_meta($post->ID,"ctype",true),
					"desc"      => $post->post_content,
					"option_values" => get_post_meta($post->ID,"option_values",true),
					"is_require"  => get_post_meta($post->ID,"is_require",true),
					"is_active"  => get_post_meta($post->ID,"is_active",true),
					"show_on_listing"  => get_post_meta($post->ID,"show_on_listing",true),
					"show_on_detail"  => get_post_meta($post->ID,"show_on_detail",true),
					"validation_type"  => get_post_meta($post->ID,"validation_type",true),
					"style_class"  => get_post_meta($post->ID,"style_class",true),
					"extra_parameter"  => get_post_meta($post->ID,"extra_parameter",true),
					);
			if($options)
			{
				$custom_fields["options"]=$options;
			}
			$return_arr[get_post_meta($post->ID,"htmlvar_name",true)] = $custom_fields;
			/*validate custom  fields  code starts here */
				global $validation_info,$form_fields;
	
			$title = get_post_meta($post->ID,'site_title',true);
			$name = get_post_meta($post->ID,'htmlvar_name',true);
			$type = get_post_meta($post->ID,'ctype',true);
			$require_msg = get_post_meta($post->ID,'field_require_desc',true);
			$validation_type = get_post_meta($post->ID,'validation_type',true);
			$is_require = get_post_meta($post->ID,'is_require',true);
			
			if($name != 'category')
			{
				$form_fields[$name] = array(
						   'title'	=> $title,
						   'name'	=> $name,
						   'espan'	=> $name.'_error',
						   'type'	=> $type,
						   'text'	=> $require_msg,
						   'is_require' => $is_require,
						   'validation_type' => $validation_type);
			}
		endwhile;
	}
	
 	foreach($form_fields as $key=>$val)
			{
				$str = ''; $fval = '';
				$field_val = $key.'_val';
				if(!isset($val['title']))
				   {
					 $val['title'] = '';
				   }
				$validation_info[] = array(
											   'title'	=> $val['title'],
											   'name'	=> $key,
											   'espan'	=> $key.'_error',
											   'type'	=> $val['type'],
											   'text'	=> $val['text'],
											   'is_require'=>$val['is_require'],
											   'validation_type'	=> $val['validation_type']);
			}
			
	return $return_arr;
}
/* sent booking mails */

function templatic_booking_mail_success_text_display($booking_id){
	global $wpdb;
	$post_data = get_post($booking_id);
	$post_meta_data = get_post_meta($booking_id);
	$check_in_date = strtotime($post_meta_data['checkin_date'][0]);
	$check_out_date = strtotime($post_meta_data['checkout_date'][0]);
	$total_guest = $post_meta_data['home_capacity'][0];
	$total_amount = $post_meta_data['total_amount'][0];
	$booking_street = $post_meta_data['booking_street'][0];
	$booking_city_state = $post_meta_data['booking_city_state'][0];
	$booking_country = $post_meta_data['booking_country'][0];
	$booking_phone = $post_meta_data['booking_phone'][0];
	
	
	$days_between = ceil(abs($check_out_date - $check_in_date) / 86400);
	
	
	$user_details = unserialize($post_meta_data['booking_user_details'][0]);
	$paymentmethod = $post_meta_data['paymentmethod'][0];
	$paymentupdsql = "select option_value from $wpdb->options where option_name ='payment_method_".$paymentmethod."'";
	$paymentupdinfo = $wpdb->get_results($paymentupdsql);
	if($paymentupdinfo){
		foreach($paymentupdinfo as $paymentupdinfoObj)	{
			$option_value = unserialize($paymentupdinfoObj->option_value);
			$name = $option_value['name'];
			$option_value_str = serialize($option_value);
		}
	}
	$property_id = get_post_meta($booking_id,'property_room_id',true);
	
	if(function_exists('get_post_custom_fields_templ_booking_plugin'))
	{
		if(get_post_type( $property_id ) == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE)
			$post_type = CUSTOM_POST_TYPE_TEMPLATIC_HOUSE;
		elseif(get_post_type( $property_id ) == CUSTOM_POST_TYPE_TEMPLATIC_ROOM)
			$post_type = CUSTOM_POST_TYPE_TEMPLATIC_ROOM;
		
		$default_custom_metaboxes_mail = get_post_custom_fields_templ_booking_plugin_mail($post_type,'','','');
	}
	$s_filecontent = '
					<span><label style="width:150px;float:left;font-weight:bold;">'.__('Arrival/Check-in',BKG_DOMAIN).'</label> '.date_i18n(get_option('date_format'),strtotime($post_meta_data['checkin_date'][0])).'</span><br />
					<span><label style="width:150px;float:left;font-weight:bold;">'.__('Departure/Check-out',BKG_DOMAIN).'</label> '.date_i18n(get_option('date_format'),strtotime($post_meta_data['checkout_date'][0])).'</span><br />
					<span><label style="width:150px;float:left;font-weight:bold;">'.__('Day(s)',BKG_DOMAIN).'</label>  '.$days_between.'</span><br />';
					if(get_post_meta($booking_id,'discount_amt',true) > 0)
					{
						$s_filecontent .= '<span><label style="width:150px;float:left;font-weight:bold;">'.__('Discount Amount',BKG_DOMAIN).'</label>  '.fetch_currency_with_position(get_post_meta($booking_id,'discount_amt',true)).'</span><br />';
					}
			if($default_custom_metaboxes_mail){
				foreach($default_custom_metaboxes_mail as $key=>$val){//echo "<pre>";print_r($val);echo "</pre>";
					if($val['type'] == 'multicheckbox' && get_post_meta($booking_id,$val['htmlvar_name'],true) !='' && $val['show_in_email']=='1')
					{
						$s_filecontent.='<span><label style="width:150px;float:left;font-weight:bold;">'.$val['label'].' </label>  '.  implode(",",get_post_meta($booking_id,$val['htmlvar_name'],true)).'</span><br />';
					}else{
						if($val['show_in_email']=='1' && get_post_meta($booking_id,$val['htmlvar_name'],true)!="")
						{
							$s_filecontent.= '<span><label style="width:150px;float:left;font-weight:bold;">'.$val['label'].' </label>  '.get_post_meta($booking_id,$val['htmlvar_name'],true).'</span><br />';
						}
					}
				}
			}
	$deposite_percentage = get_option('deposite_percentage');
	if($deposite_percentage!= '')
	{
		$s_filecontent .= '<span><label style="width:150px;float:left;font-weight:bold;">'.__('Deposit Percentage','templatic').'</label>  '. $deposite_percentage.'% </span><br />';
	}
	if(tex_include($property_id)){
			$tax_type = get_post_meta($booking_id,'tax_type',true);
			$templatic_settings = get_option( "templatic_settings" );
			$tax_amount = $templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_amount'];
			if($tax_type == 'exact'){
				$tax_type = display_amount_with_currency_plugin($tax_amount);
			}elseif($tax_type == 'percent'){
				$ttl_room_house = get_post_meta($booking_id,'room_house_total',true);
				$ttl_room_services = get_post_meta($booking_id,'booking_service_prices',true);
				$ttl_price = $ttl_room_house + $ttl_room_services;
				$tx_price = ($ttl_price * $tax_amount) / 100;
				$tax_type = display_amount_with_currency_plugin($tx_price);
			}else{
				$tax_type = display_amount_with_currency_plugin($tax_amount);
			}
			$s_filecontent .= '<span><label style="width:150px;float:left;font-weight:bold;">'.TAX_TEXT.'</label>'.$tax_type.'</span><br />';
		}
	if(get_post_meta($booking_id,'cost_subtotal',true)!= '')
	{
		$s_filecontent .= '<span><label style="width:150px;float:left;font-weight:bold;">'.__('Rest Amount','templatic').'</label>  '. fetch_currency_with_position(($tax_type+get_post_meta($booking_id,'cost_subtotal',true))-$total_amount).' </span><br />';
	}
	$s_filecontent .= '<span><label style="width:150px;float:left;font-weight:bold;">'.__('Total Charges',BKG_DOMAIN).'</label>  '.$total_amount.' </span><br />';
	$s_filecontent .= 	'<br/><span><b>'.__('Customer Address',BKG_DOMAIN).' </b> </span><br/>
		'.$post_data->post_title.'<br />
		'.$booking_street.', '.$booking_city_state.',<br />'.$booking_country.'<br /> '.$booking_phone.'<br/><br/><span><b>'.__('Payment Mode',BKG_DOMAIN).'</b>  </span><br/>'.$name;
	return $s_filecontent;
}
// Get the total number of days in current month START.
function getMonthDays($Month, $Year){
	if( is_callable("cal_days_in_month")){
		return cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
	}else{
		return date("d",mktime(0,0,0,$Month+1,0,$Year));
	}
}
// Get the total number of days in current month FINISH.

//Add action for booking success notification message for success page START.

if((isset($_REQUEST['pid']) && $_REQUEST['pid']!="") || (isset($_REQUEST['cf']) && $_REQUEST['cf']!="") || (isset($_REQUEST['update_transaction_status']) && $_REQUEST['update_transaction_status']!="") ){
	$PostType = get_post_type($_REQUEST['pid']);
	
	if(isset($_REQUEST['cf']) && $_REQUEST['cf'] !='')
	{
		$cf = explode(",",$_REQUEST['cf'][0]);
		$PostType = get_post_type($cf[1]);
		$PostType = get_post_type(get_post_meta($cf[1],'property_room_id',true));
	}
	if(isset($_REQUEST['update_transaction_status']) && $_REQUEST['update_transaction_status'] !='')
	{
		$PostType = get_post_type($_REQUEST['update_transaction_status']);
	}

	if($PostType!="" && ($PostType== 'room' || $PostType== 'house' || $PostType =='booking')){
		add_action('init','remove_tevolution_booking_system_message');
	}	
}
function tex_include($room_type_id){
	global $wpdb;
	/*$room_type_master = $wpdb->prefix . "room_type_master";
	$has_tax_value = mysql_query("select has_tax from $room_type_master where room_type_id = '".$room_type_id."'");
	$has_tax_res = mysql_fetch_array($has_tax_value);*/
	$has_tax_res = get_post_meta($room_type_id,'include_tax',true);
	if($has_tax_res == 'Yes'){
		return true;	
	} else {
		return false;	
	}
}
function success_text_display($booking_id,$border='0'){
	global $wpdb;
	$booking_master = $wpdb->prefix . "booking_master";
	$booking_personal_info = $wpdb->prefix . "booking_personal_info";
	$booking_transaction = $wpdb->prefix . "booking_transaction";
	$booking_check_avilability = $wpdb->prefix . "booking_check_avilability";
	$booking_log = $wpdb->prefix . "booking_master_log";
	

	$room_id  = get_post_meta($booking_id,'property_room_id',true);
	$room_house_post_type = get_post_type($room_id);
	$house_room_title = '';
	$house_room_occupancy_title = '';
	$house_room_occupancy_value = '';
	$colspan = 0;
	$rooms_booked = '';
	if($room_house_post_type == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
		$house_room_title = __("House",BKG_DOMAIN);
		$rooms_booked = 1;
	}elseif($room_house_post_type == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
		$house_room_title = __("ROOM",BKG_DOMAIN);
		$rooms_booked = get_post_meta($booking_id,'no_of_rooms',true).__(' x ','templatic');
	}
	$days_between = ceil(abs(strtotime(get_post_meta($booking_id,'checkout_date',true)) - strtotime(get_post_meta($booking_id,'checkin_date',true))) / 86400);
	$booking_service_prices = get_post_meta($booking_id,'booking_service_prices',true); 
	
	$s_filecontent = '<table style="width:100%;" border="'.$border.'" cellpadding="0" cellspacing="0" class="table">
			<tr>
				<th align="center" class="title">'.$house_room_title.'</th>
				<th align="center" class="title">'.__("Check-In/Check-Out Date",BKG_DOMAIN).'</th>
				<th align="center" class="title">'.__("Total Days",BKG_DOMAIN).'</th>
				<th align="center" class="title">'.__(OCCUPY_TEXT,BKG_DOMAIN).'</th>
				<th align="right" class="title">'.__('Total Price',BKG_DOMAIN).'</th>
			</tr>
			<tr>
				<td class="row1">'.$rooms_booked.get_the_title($room_id).'</td>
				<td class="row1">'.date_i18n(get_option('date_format'),strtotime(get_post_meta($booking_id,'checkin_date',true))).' to '.date_i18n(get_option('date_format'),strtotime(get_post_meta($booking_id,'checkout_date',true))).'</td>
				<td class="row1">'.$days_between.'</td>
				<td class="row1">'.get_post_meta($booking_id,'home_capacity',true).'</td>
				<td align="right" class="row1">'.display_amount_with_currency_plugin(get_post_meta($booking_id,'room_house_total',true)).'</td>
			</tr>';
			
			if(get_post_meta($booking_id,'discount_amt',true) > 0){
				$s_filecontent .= '<tr>
				<td colspan="4" style="text-align:right" class="row1">'.__(DISCOUNT_AMT,BKG_DOMAIN).'</td><td align="right" class="row1">'.fetch_currency_with_position(get_post_meta($booking_id,'discount_amt',true)).'</td></tr>';
			}
			$terms = get_the_terms( $booking_id, 'services' );
			if($terms){
				foreach($terms as $skey => $svalue){
					global $wpdb;
					if(get_option('service_type') == 'once' )
					{
						$service_price = display_amount_with_currency_plugin(($svalue->term_price));
					}
					else
					{
						$service_price = display_amount_with_currency_plugin(($rooms_booked*$days_between * $svalue->term_price));
					}
					$s_filecontent .= '<tr>
						<td colspan="4" style="text-align:right" class="row1">'.$svalue->name.' </td><td align="right" class="row1">'. $service_price;
					$s_filecontent .= '</td></tr>';
				}
			}
			
			if(tex_include($room_id)){
				$tax_type = get_post_meta($booking_id,'tax_type',true);
				$templatic_settings = get_option( "templatic_settings" );
				$tax_amount = $templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_amount'];
				if($tax_type == 'exact'){
					$tax_type = display_amount_with_currency_plugin($tax_amount);
				}elseif($tax_type == 'percent'){
					$ttl_room_house = get_post_meta($booking_id,'room_house_total',true);
					$ttl_room_services = get_post_meta($booking_id,'booking_service_prices',true);
					$ttl_price = $ttl_room_house + $ttl_room_services;
					$tx_price = ($ttl_price * $tax_amount) / 100;
					$tax_type = display_amount_with_currency_plugin($tx_price);
				}else{
					$tax_type = display_amount_with_currency_plugin($tax_amount);
				}
				$s_filecontent .= '<tr>
				<td colspan="4" style="text-align:right" class="row1">'.__(TAX_TEXT,BKG_DOMAIN).'</td><td align="right" class="row1">'.$tax_type.'</td></tr>';
			}
			$deposite_percentage = get_option('deposite_percentage');
			if($deposite_percentage!= '')
			{
				$s_filecontent .= '<tr>
						<td colspan="4" style="text-align:right" class="row1">'.__('Deposit Percentage',BKG_DOMAIN).'</td><td align="right" class="row1">'. $deposite_percentage.'%';
					$s_filecontent .= '</td></tr>';
			}
			if( get_post_meta($booking_id,'cost_subtotal',true)!= '')
			{
				$s_filecontent .= '<tr>
						<td colspan="4" style="text-align:right" class="row1">'.__('Rest Amount',BKG_DOMAIN).'</td><td align="right" class="row1">'.display_amount_with_currency_plugin(($tax_type+(get_post_meta($booking_id,'cost_subtotal',true))-get_post_meta($booking_id,'total_amount',true)));
					$s_filecontent .= '</td></tr>';
			}
			$s_filecontent .= '
				<tr>
				<td colspan="4" style="text-align:right" class="total_amount_title">'.__(TOTAL_CHARGE_TEXT,BKG_DOMAIN).'</td><td align="right" class="total_amount_title">'. get_post_meta($booking_id,'total_amount',true).'</td></tr>
		</table>';
		
		return $s_filecontent;
}

function remove_tevolution_booking_system_message(){
	remove_action('tevolution_submition_success_msg','tevolution_submition_success_msg_fn');
	remove_action('tevolution_submition_success_post_content','tevolution_submition_success_post_submited_content');
	remove_action('tevolution_transaction_msg','tevolution_transaction_msg_fn');
	remove_action('tevolution_transaction_mail','tevolution_transaction_mail_fn');
	add_action('tevolution_submition_success_msg','booking_tevolution_submition_success_msg_fn');
	add_action('tevolution_submition_success_post_content','booking_submition_success_post_submited_content');
	add_action('tevolution_transaction_msg','booking_transaction_msg_fn');
	add_action('tevolution_transaction_mail','booking_transaction_mail_fn');
}
add_filter('delete_tevolution_transaction_filter_value','delete_tevolution_transaction_filter_value');
add_filter('delete_tevolution_transaction_filter_text','delete_tevolution_transaction_filter_value');
function delete_tevolution_transaction_filter_value()
{
	return __('Cancelled',BKG_DOMAIN);
}
function booking_transaction_msg_fn()
{
	if(count($_REQUEST['cf'])>0)
	{
		for($i=0;$i<count($_REQUEST['cf']);$i++)
		{
		$cf = explode(",",$_REQUEST['cf'][$i]);
		$orderId = $cf[0];
		if(isset($_REQUEST['action']) && $_REQUEST['action'] !='')
		{
			global $wpdb,$transection_db_table_name;
			$transection_db_table_name = $wpdb->prefix . "transactions";
			
			$ordersql = "select * from $transection_db_table_name where trans_id=\"$orderId\"";
			$orderinfo = $wpdb->get_row($ordersql);
		
			$pid = $orderinfo->post_id;
			$payment_type = $orderinfo->payment_method;
			$payment_date =  date_i18n(get_option('date_format'),strtotime($orderinfo->payment_date));
			$trans_status = $wpdb->query("update $transection_db_table_name SET status = '".$_REQUEST['ostatus']."' where trans_id = '".$orderId."'");
			$user_detail = get_userdata($orderinfo->user_id); // get user details 
			$user_email = $user_detail->user_email;
			$user_login = $user_detail->display_name;
			$my_post['ID'] = $pid;
			$to = get_site_emailId_plugin();
			if(isset($_REQUEST['action']) && $_REQUEST['action']== 'confirm')
			{
				$payment_status = APPROVED_TEXT;
				$status = 'publish';
				$productinfosql = "select ID,post_title,guid,post_author from $wpdb->posts where ID = $pid";
				$productinfo = get_post($pid);
				$post_name = $productinfo->post_title;
				$transaction_details .="<p>".__('Payment Details for ',BKG_DOMAIN).$post_name."</p>";
				$transaction_details .= templatic_booking_mail_success_text_display($pid);
				$transaction_details = $transaction_details;
				$subject = get_option('post_payment_success_admin_email_subject');
				if(!$subject)
				{
					$subject = __("Payment Success Confirmation Email",BKG_DOMAIN);
				}
				$content = get_option('payment_success_email_content_to_admin');
				if(!$content)
				{
					$content = "<p>".__('Dear',BKG_DOMAIN).' [#to_name#],</p><p>[#transaction_details#]</p></br><p>'.__('We hope you enjoy . Thanks!',BKG_DOMAIN).'</p><p>[#site_name#]</p>';
				}
				$store_name = get_option('blogname');
				$fromEmail = get_option('admin_email');
				$fromEmailName = stripslashes(get_option('blogname'));	
				$search_array = array('[#to_name#]','[#transaction_details#]','[#site_name#]');
				$replace_array = array($fromEmail,$transaction_details,$store_name);
				$filecontent = str_replace($search_array,$replace_array,$content);
				@templ_send_email($fromEmail,$fromEmailName,$to,$user_login,$subject,stripslashes($filecontent),''); // email to admin
				$transaction_details = "";
				// post details
					$post_link = site_url().'/?ptype=preview&alook=1&pid='.$pid;
					$post_title = '<a href="'.$post_link.'">'.stripslashes($productinfo->post_title).'</a>'; 
					$aid = $productinfo->post_author;
					$userInfo = get_userdata($aid);
					$to_name = __('Customer',BKG_DOMAIN);
					$to_email = get_post_meta($pid,'booking_email',true);
					$user_email = get_post_meta($pid,'booking_email',true);
				$transaction_details .="<p>".__('Payment Details for ',BKG_DOMAIN).$post_name."</p>";
				$transaction_details .= templatic_booking_mail_success_text_display($pid);
				$transaction_details = __($transaction_details,BKG_DOMAIN);
				
				$subject = get_option('payment_success_email_subject_to_client');
				if(!$subject)
				{
					$subject = __("Payment Success Confirmation Email",BKG_DOMAIN);
				}
				$content = get_option('payment_success_email_content_to_client');
				if(!$content)
				{
					$content = "<p>Dear [#to_name#],</p><p>[#transaction_details#]</p><br><p>We hope you enjoy. Thanks!</p><p>[#site_name#]</p>";
					$content = "<p>".__('Dear',BKG_DOMAIN).' [#to_name#],</p><p>[#transaction_details#]</p></br><p>'.__('We hope you enjoy . Thanks!',BKG_DOMAIN).'</p><p>[#site_name#]</p>';
				}
				$store_name = get_option('blogname');
				$search_array = array('[#to_name#]','[#transaction_details#]','[#site_name#]');
				$replace_array = array($to_name,$transaction_details,$store_name);
				$content = str_replace($search_array,$replace_array,$content);
				//@mail($user_email,$subject,$content,$headers);// email to client
				templ_send_email($fromEmail,$fromEmailName,$user_email,$user_login,$subject,stripslashes($content),$extra='');
			}
			elseif(isset($_REQUEST['action']) && $_REQUEST['action']== 'pending')
			{
				$payment_status = PENDING_MONI;
				$status = 'draft';
				$productinfosql = "select ID,post_title,guid,post_author from $wpdb->posts where ID = $pid";
				$productinfo = get_post($pid);
				$post_name = $productinfo->post_title;
				$templatic_settings = get_option( "templatic_settings" );
				$subject = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_sub'];
				$content = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_msg'];
				$subject = get_option('post_payment_success_admin_email_subject');
				if(!$subject)
				{
					$subject = __("Booking- Could not confirm","templatic");
				}
				
				if(!$content)
				{
					$content = '<p>'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('Your booking request is rejected. Please try again later.',BKG_DOMAIN).'<br /><br />'.__('Thanks for your interest.',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>';
				}
				$store_name = get_option('blogname');
				$fromEmail = get_option('admin_email');
				$fromEmailName = stripslashes(get_option('blogname'));	
				$property_id = get_post_meta($pid,'property_room_id',true);
				$BookingPostType = get_post_meta($pid,'booking_post_type',true);
				$post_link = site_url().'/?ptype=preview&alook=1&pid='.$pid;
				$post_title = '<a href="'.$post_link.'">'.stripslashes($productinfo->post_title).'</a>'; 
				$aid = $productinfo->post_author;
				$userInfo = get_userdata($aid);
				$to_name = __('Customer',BKG_DOMAIN);
				$to_email = get_post_meta($pid,'booking_email',true);
				$user_email = get_post_meta($pid,'booking_email',true);
			
				if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
					$hotel_name = get_the_title($property_id);
					$addresses = get_post_meta($property_id,'address',true);
					$address_exclude = explode(',',$addresses);
					$home_street = $address_exclude[0];
					$home_state = $address_exclude[1];
					$home_country = $address_exclude[2];
				}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
					$hotel_name = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
					$home_street = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
					$home_state = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
					$home_country = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
				}
				$regard_content = '<strong>'.$hotel_name.'</strong><br />'.$home_street.'<br />'.$home_state;
				if($home_country!=""){
					$regard_content .= ', '.$home_country;
				}
				$search_array = array('[USER_NAME]','[ADMIN_NAME]');
				$replace_array = array($fromEmail,$regard_content);
				$filecontent = str_replace($search_array,$replace_array,$content);
				//echo $fromEmail."=<br/>".$fromEmailName."=<br/>".$user_email."=<br/>".$user_login."=<br/>".$subject."=<br/>".stripslashes($filecontent);exit;
				templ_send_email($fromEmail,$fromEmailName,$user_email,$user_login,$subject,stripslashes($filecontent),$extra='');
			}
			elseif(isset($_REQUEST['action']) && $_REQUEST['action']== 'Cancelled')
			{
				$payment_status = PENDING_MONI;
				$status = 'draft';
				$productinfosql = "select ID,post_title,guid,post_author from $wpdb->posts where ID = $pid";
				$productinfo = get_post($pid);
				$post_name = $productinfo->post_title;
				$templatic_settings = get_option( "templatic_settings" );
				$subject = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_sub'];
				$content = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_msg'];
				$subject = get_option('post_payment_success_admin_email_subject');
				if(!$subject)
				{
					$subject = __("Your Booking is Cancelled",BKG_DOMAIN);
				}
				
				if(!$content)
				{
					$content = "<p>".__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('Your booking has been cancelled. To book another date feel free to visit our site at any time.',BKG_DOMAIN).'<br /><br />'.__('Kind regards',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>';
				}
				$store_name = get_option('blogname');
				$fromEmail = get_option('admin_email');
				$fromEmailName = stripslashes(get_option('blogname'));	
				$property_id = get_post_meta($pid,'property_room_id',true);
				$BookingPostType = get_post_meta($pid,'booking_post_type',true);
				$post_link = site_url().'/?ptype=preview&alook=1&pid='.$pid;
				$post_title = '<a href="'.$post_link.'">'.stripslashes($productinfo->post_title).'</a>'; 
				$aid = $productinfo->post_author;
				$userInfo = get_userdata($aid);
				$to_name = __('Customer',BKG_DOMAIN);
				$to_email = get_post_meta($pid,'booking_email',true);
				$user_email = get_post_meta($pid,'booking_email',true);
			
				if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
					$hotel_name = get_the_title($property_id);
					$addresses = get_post_meta($property_id,'address',true);
					$address_exclude = explode(',',$addresses);
					$home_street = $address_exclude[0];
					$home_state = $address_exclude[1];
					$home_country = $address_exclude[2];
				}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
					$hotel_name = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
					$home_street = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
					$home_state = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
					$home_country = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
				}
				$regard_content = '<strong>'.$hotel_name.'</strong><br />'.$home_street.'<br />'.$home_state;
				if($home_country!=""){
					$regard_content .= ', '.$home_country;
				}
				$search_array = array('[USER_NAME]','[ADMIN_NAME]');
				$replace_array = array($fromEmail,$regard_content);
				$filecontent = str_replace($search_array,$replace_array,$content);
				//echo $fromEmail."=<br/>".$fromEmailName."=<br/>".$user_email."=<br/>".$user_login."=<br/>".$subject."=<br/>".stripslashes($filecontent);exit;
				templ_send_email($fromEmail,$fromEmailName,$user_email,$user_login,$subject,stripslashes($filecontent),$extra='');
			}
			
			$my_post['post_status'] = $status;
			wp_update_post( $my_post );
			update_post_meta($pid,'status',$payment_status);
			$booking_availability_table = $wpdb->prefix."booking_availability_check";
			$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET status='$payment_status' where booking_post_id=".$pid);
			}
		}
	}
	
}
function booking_transaction_mail_fn()
{
	if(isset($_REQUEST['submit']) && $_REQUEST['submit'] !='')
	{
		$orderId = $_REQUEST['trans_id'];
		global $wpdb,$transection_db_table_name;
		$transection_db_table_name = $wpdb->prefix . "transactions";
		
		$ordersql = "select * from $transection_db_table_name where trans_id=\"$orderId\"";
		$orderinfo = $wpdb->get_row($ordersql);
	
		$pid = $orderinfo->post_id;
		$payment_type = $orderinfo->payment_method;
		$payment_date =  date_i18n(get_option('date_format'),strtotime($orderinfo->payment_date));
		$trans_status = $wpdb->query("update $transection_db_table_name SET status = '".$_REQUEST['ostatus']."' where trans_id = '".$orderId."'");
		$user_detail = get_userdata($orderinfo->user_id); // get user details 
		$user_email = $user_detail->user_email;
		$user_login = $user_detail->display_name;
		$my_post['ID'] = $pid;
		if(isset($_REQUEST['ostatus']) && $_REQUEST['ostatus']== 1)
			$status = 'publish';
		else
			$status = 'draft';
		$my_post['post_status'] = $status;
		wp_update_post( $my_post );
		$templatic_settings = get_option( "templatic_settings" );
		if(isset($_REQUEST['ostatus']) && $_REQUEST['ostatus']== 1)
		{
			$payment_status = APPROVED_TEXT;
			$subject = __("Booking-Confirmed","templatic");
		}
		elseif(isset($_REQUEST['ostatus']) && $_REQUEST['ostatus']== 2)
		{
			$payment_status = ORDER_CANCEL_TEXT;
			$subject = __("Booking Cancelled","templatic");
		}
		elseif(isset($_REQUEST['ostatus']) && $_REQUEST['ostatus']== 0)
		{
			$payment_status = PENDING_MONI;
			$subject = __("Payment Pending Email","templatic");
		}
		update_post_meta($pid,'status',$payment_status);
		$booking_availability_table = $wpdb->prefix."booking_availability_check";
		$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET status='$payment_status' where booking_post_id=".$pid);
		$to = get_site_emailId_plugin();
		$productinfosql = "select ID,post_title,guid,post_author from $wpdb->posts where ID = $pid";
		$productinfo = get_post($pid);
	   $post_name = $productinfo->post_title;
		$transaction_details .="<p>". __('Payment Details for ',BKG_DOMAIN).$post_name."</p>";
			$transaction_details .= templatic_booking_mail_success_text_display($pid);
				$transaction_details = $transaction_details;
			$content = get_option('payment_success_email_content_to_admin');
			if(!$content)
			{
				$content = "<p>".__('Dear',BKG_DOMAIN).' [#to_name#],</p><p>[#transaction_details#]</p><br/><p>'.__('We hope you enjoy . Thanks!',BKG_DOMAIN).'</p><p>[#site_name#]</p>';
			}
			$store_name = get_option('blogname');
			$fromEmail = get_option('admin_email');
			$fromEmailName = stripslashes(get_option('blogname'));	
			$search_array = array('[#to_name#]','[#transaction_details#]','[#site_name#]');
			$replace_array = array($fromEmail,$transaction_details,$store_name);
			$filecontent = str_replace($search_array,$replace_array,$content);
			@templ_send_email($fromEmail,$fromEmailName,$to,$user_login,$subject,stripslashes($filecontent),''); // email to admin
			$transaction_details = "";
			// post details
				$post_link = site_url().'/?ptype=preview&alook=1&pid='.$pid;
				$post_title = '<a href="'.$post_link.'">'.stripslashes($productinfo->post_title).'</a>'; 
				$aid = $productinfo->post_author;
				$userInfo = get_userdata($aid);
				$to_name = __('Customer',BKG_DOMAIN);
				$to_email = get_post_meta($pid,'booking_email',true);
				$user_email = get_post_meta($pid,'booking_email',true);
			
			$transaction_details .="<p>". __('Payment Details for ',BKG_DOMAIN).$post_name."</p>";
				$transaction_details .= templatic_booking_mail_success_text_display($pid);
				$transaction_details = __($transaction_details,BKG_DOMAIN);

			$content = get_option('payment_success_email_content_to_client');
			if(!$content)
			{
				$content = "<p>".__('Dear',BKG_DOMAIN).' [#to_name#],</p><p>[#transaction_details#]</p><br/><p>'.__('We hope you enjoy . Thanks!',BKG_DOMAIN).'</p><p>[#site_name#]</p>';
			}
			$store_name = get_option('blogname');
			$search_array = array('[#to_name#]','[#transaction_details#]','[#site_name#]');
			$replace_array = array($to_name,$transaction_details,$store_name);
			$content = str_replace($search_array,$replace_array,$content);
			//@mail($user_email,$subject,$content,$headers);// email to client
			templ_send_email($fromEmail,$fromEmailName,$user_email,$user_login,$subject,stripslashes($content),$extra='');
	}
}
remove_action('tmpl_related_post','related_post_by_categories');
function booking_tevolution_submition_success_msg_fn(){
	global $wpdb;
	if(isset($_REQUEST['pid']) && $_REQUEST['pid']!=""){
		$PostType = get_post_type($_REQUEST['pid']);
		if($PostType!="" && ($PostType=='room' || $PostType=='house' || $PostType=='booking')){
			// Initialize contents and variables required for notification description START.
				$paymentmethod = get_post_meta($_REQUEST['pid'],'paymentmethod',true);
				$tmpdata = get_option('templatic_settings');
				$paid_amount = display_amount_with_currency_plugin(str_replace(get_option('currency_symbol'),"",get_post_meta($_REQUEST['pid'],'total_amount',true)));
				$post_default_status = $tmpdata['post_default_status'];
				$post_link="";
				if($post_default_status == 'publish'){
					$post_link = '';
				}else{
					$post_link ='';
				}
				$store_name = get_option('blogname');

				if($paymentmethod == 'prebanktransfer')
				{
					$paymentupdsql = "select option_value from $wpdb->options where option_name='payment_method_".$paymentmethod."'";
					$paymentupdinfo = $wpdb->get_results($paymentupdsql);
					$paymentInfo = unserialize($paymentupdinfo[0]->option_value);
					$payOpts = $paymentInfo['payOpts'];
					$bankInfo = $payOpts[0]['value'];
					$accountinfo = $payOpts[1]['value'];
				}
				$BookingId = $_REQUEST['pid'];
				$siteName = "<a href='".home_url()."'>".$store_name."</a>";
				$BookingPropertyId = get_post_meta($BookingId,'property_room_id',true);
				$ContactMail = get_post_meta($BookingPropertyId,'home_contact_mail',true);
				
				$search_array = array('[PAYABLE_AMOUNT]','[BANK_NAME]','[ACCOUNT_NUMBER]','[SUBMISSION_ID]','[store_name]','[SUBMITTED_INFORMATION_LINK]','[SITE_NAME]','[CONTACT_MAIL]');
				$replace_array = array($paid_amount,@$bankInfo,@$accountinfo,$BookingId,$store_name,$post_link,$siteName,$ContactMail);
			// Initialize contents and variables required for notification description FINISH.

			
			if($paymentmethod == 'prebanktransfer'){
				$filecontent = stripslashes($tmpdata['templatic_booking_system_settings']['templatic_booking_notification_settings']['prebank_message']);
				if(function_exists('icl_register_string')){
					$context = get_option('blogname');
					icl_register_string($context,"pre bank transfer Successfull",$filecontent);
					$filecontent = icl_t($context,"pre bank transfer Successfull",$filecontent);
				}
				
				if(!$filecontent){
					$filecontent = __(PRE_BANK_PAYMENT_MSG,BKG_DOMAIN);
				}
			}else{
				$filecontent = stripslashes($tmpdata['templatic_booking_system_settings']['templatic_booking_notification_settings']['templatic_booking_success_message']);
				if(!$filecontent){
					$filecontent = __(BOOKING_SUCCESS_MSG,BKG_DOMAIN);
				}
				global $wpdb,$transection_db_table_name;
				$transection_db_table_name = $wpdb->prefix . "transactions";
			
				$ordersql = "select trans_id from $transection_db_table_name where post_id=".$_REQUEST['pid'];
				$orderinfo = $wpdb->get_row($ordersql);
				$filecontent .= __('Transaction Number:',BKG_DOMAIN).' '. $orderinfo->trans_id;
			}
			
			$booking_title = __('Your Booking Detail','templatic');
			$filecontent = str_replace($search_array,$replace_array,$filecontent); 
			$filecontent .= '<div class="title-container"><h3>'.$booking_title.'</h3></div><div class="booking">'.success_text_display($_REQUEST['pid'],'0').'</div>'; 
			_e($filecontent,BKG_DOMAIN);
		}
	}	
}
function booking_submition_success_post_submited_content()
{
	?>
     <!-- Short Detail of post -->
	<div class="title-container">
		<h3><?php  _e(POST_DETAIL,BKG_DOMAIN);?></h3>
	</div>
    <div class="submited_info">
	<?php
	global $wpdb,$post;
	remove_all_actions('posts_where');
	$cus_post_type = get_post_type($_REQUEST['pid']);
	$args = 
	array( 'post_type' => 'custom_fields',
	'posts_per_page' => -1	,
	'post_status' => array('publish'),
	'meta_query' => array(
	   'relation' => 'AND',
		array(
			'key' => 'post_type_'.$cus_post_type.'',
			'value' => $cus_post_type,
			'compare' => '=',
			'type'=> 'text'
		),
		array(
			'key' => 'show_on_page',
			'value' =>  array('user_side','both_side'),
			'compare' => 'IN'
		),
		array(
			'key' => 'is_active',
			'value' =>  '1',
			'compare' => '='
		),
		array(
			'key' => 'show_on_success',
			'value' =>  '1',
			'compare' => '='
		)
	),
		'meta_key' => 'sort_order',
		'orderby' => 'meta_value',
		'order' => 'ASC'
	);
	$post_query = null;
	add_filter('posts_join', 'custom_field_posts_where_filter');
	$post_meta_info = new WP_Query($args);	
	
	remove_filter('posts_join', 'custom_field_posts_where_filter');
	$suc_post = get_post($_REQUEST['pid']);
		if($post_meta_info)
		  {
			echo "<div class='grid02 rc_rightcol clearfix'>";
			echo "<ul class='list'>";
			while ($post_meta_info->have_posts()) : $post_meta_info->the_post();
				$post->post_name=get_post_meta(get_the_ID(),'htmlvar_name',true);
				if(get_post_meta($_REQUEST['pid'],$post->post_name,true))
					  {
						if(get_post_meta($post->ID,"ctype",true) == 'multicheckbox')
						  {
							$_value = '';
							foreach(get_post_meta($_REQUEST['pid'],$post->post_name,true) as $value)
							 {
								$_value .= $value.",";
							 }
							 echo "<li><p>".stripslashes($post->post_title)."</p> <p> ".substr($_value,0,-1)."</p></li>";
						  }
						else
						 {
							 $custom_field=get_post_meta($_REQUEST['pid'],$post->post_name,true);
							 if(substr($custom_field, -4 ) == '.jpg' || substr($custom_field, -4 ) == '.png' || substr($custom_field, -4 ) == '.gif' || substr($custom_field, -4 ) == '.JPG' 
											|| substr($custom_field, -4 ) == '.PNG' || substr($custom_field, -4 ) == '.GIF'){
								  echo "<li><p>".stripslashes($post->post_title)."</p> <p> <img src='".$custom_field."' /></p></li>";
							 }							 
							 else
							 {
							   if(get_post_meta($post->ID,'ctype',true) == 'upload')
							    {
							   	  echo "<li><p>".stripslashes($post->post_title)."</p><p>".__('Click here to download File ',BKG_DOMAIN)."<a href=".get_post_meta($_REQUEST['pid'],$post->post_name,true).">".__('Download',BKG_DOMAIN)."</a></p></li>";
							    }
							   else
							    {
  								  echo "<li><p>".stripslashes($post->post_title)."</p> <p> ".get_post_meta($_REQUEST['pid'],$post->post_name,true)."</p></li>";
								}
							 }
						 }
					  }
					if($post->post_name == 'post_content' && $suc_post->post_content!='')
					 {
						$suc_post_con = $suc_post->post_content;
					 }
					if($post->post_name == 'post_excerpt' && $suc_post->post_excerpt!='')
					 {
						$suc_post_excerpt = $suc_post->post_excerpt;
					 }

					if(get_post_meta($post->ID,"ctype",true) == 'geo_map')
					 {
						$add_str = get_post_meta($_REQUEST['pid'],'address',true);
						$geo_latitude = get_post_meta($_REQUEST['pid'],'geo_latitude',true);
						$geo_longitude = get_post_meta($_REQUEST['pid'],'geo_longitude',true);
						$map_view = get_post_meta($_REQUEST['pid'],'map_view',true);
					 }
  
			endwhile;
			if(is_active_addons('monetization') && $paidamount > 0){
				fetch_payment_description($_REQUEST['pid']);
			}
			echo "</ul>";
			echo "</div>";
		  }		 
		do_action('after_tevolution_success_msg');
		$transection_db_table_name = $wpdb->prefix.'transactions';
		$transsql_select = "select * from $transection_db_table_name where post_id = ". $_REQUEST['pid'];
		$transsql_result = $wpdb->get_row($transsql_select);
		if($transsql_result->status){
			update_post_meta($_REQUEST['pid'],'status','Approved');
		}
	?>
	</div>
	<?php if(isset($suc_post_con)): ?>
	    <div class="row">
		  <div class="twelve columns">
			  <div class="title_space">
				 <div class="title-container">
					<h1><?php _e('Post Description', BKG_DOMAIN);?></h1>
				 </div>
				 <p><?php echo nl2br($suc_post_con); ?></p>
			  </div>
		   </div>
	    </div>
	<?php endif; ?>
	
	<?php if(isset($suc_post_excerpt)): ?>
		 <div class="row">
			<div class="twelve columns">
				<div class="title_space">
					<div class="title-container">
						<h1><?php _e('Post Excerpt',BKG_DOMAIN);?></h1>
					</div>
					<p><?php echo nl2br($suc_post_excerpt); ?></p>
				</div>
			</div>
		</div>
	<?php endif; ?>
	
	<?php
	if(@$add_str)
	{
	?>
		<div class="row">
			<div class="title_space">
				<div class="title-container">
					<h1><?php _e('Map',BKG_DOMAIN); ?></h1>
				</div>
				<p><strong><?php _e('Location',BKG_DOMAIN); echo " : "; echo $add_str;?></strong></p>
			</div>
			<div id="gmap" class="graybox img-pad">
				<?php if($geo_longitude &&  $geo_latitude):
						$pimgarr = bdw_get_images_plugin($_REQUEST['pid'],'thumb',1);
						$pimg = $pimgarr[0];
						if(!$pimg):
							$pimg = plugin_dir_url( __FILE__ )."images/img_not_available.png";
						endif;	
						$title = stripslashes($suc_post->post_title);
						$address = $add_str;
						require_once (TEMPL_MONETIZE_FOLDER_PATH . 'templatic-custom_fields/preview_map.php');
						$retstr ="";
						$retstr .= "<div class=\"forrent\"><img src=\"$pimg\" width=\"192\" height=\"134\" alt=\"\" />";
						$retstr .= "<h6><a href=\"\" class=\"ptitle\" style=\"color:#444444;font-size:14px;\"><span>$title</span></a></h6>";
						if($address){$retstr .= "<span style=\"font-size:10px;\">$address</span>";}
						$retstr .= "</div>";
						preview_address_google_map_plugin($geo_latitude,$geo_longitude,$retstr,$map_view);
					  else:
				?>
						<iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo $add_str;?>&amp;ie=UTF8&amp;z=14&amp;iwloc=A&amp;output=embed" height="358" width="100%" scrolling="no" frameborder="0" ></iframe>
				<?php endif; ?>
			</div>
		</div>
	<?php } ?>
	
	
	<!-- End Short Detail of post -->

     <?php
}
//Add action for booking success notification message for success page FINISH.

//Action for displaying custom fields for booking START.
//add_action('after_tevolution_success_msg','booking_tevolution_after_tevolution_custom_fields');
function booking_tevolution_after_tevolution_custom_fields(){
	$PostType = get_post_type($_REQUEST['pid']);
	if($PostType!="" && ($PostType=='room' || $PostType=='house' || $PostType=='booking')){
		$BookingId = $_REQUEST['pid'];
		$CustomPostMeta = get_post_custom($BookingId);
		$PropertyId = $CustomPostMeta['property_room_id'][0];
		$TotalGuest = $CustomPostMeta['home_capacity'][0];
		$BookingDate = $CustomPostMeta['booking_date'][0];
		$CheckInDate = date_i18n(get_option('date_format'), strtotime($CustomPostMeta['checkin_date'][0]));
		$CheckOutDate = date_i18n(get_option('date_format'), strtotime($CustomPostMeta['checkout_date'][0]));
		$BookingPostType = $CustomPostMeta['booking_post_type'][0];
		$HotelName = get_the_title($PropertyId);
		$HotelStreet = get_post_meta($PropertyId,'home_street',true);
		$HotelState = get_post_meta($PropertyId,'home_state',true);
		$HotelCountry = get_post_meta($PropertyId,'home_country',true);
		$NumberOfRooms = $CustomPostMeta['no_of_rooms'][0];
		
		$AllTemplaticSettings = get_option('templatic_settings');
		$HotelStreetAddress = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
		$HotelStateAddress = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
		$HotelCountryAddress = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
		
		$PropertyTitle = '';
		if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
			$PropertyTitle = __('Room Name',BKG_DOMAIN);
			$MsgHotel = '<span>'.__('Number Of Rooms',BKG_DOMAIN).': '.$NumberOfRooms.'</span><br>';
			$MsgHotelAddress = '<span>'.__('Hotel Address',BKG_DOMAIN).': '.$HotelStreetAddress.' '.$HotelStateAddress.' - '.$HotelCountryAddress.'</span><br/>';
		}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
			$PropertyTitle = __('House Name',BKG_DOMAIN);
			$MsgHouse = '<span>'.__('House Address',BKG_DOMAIN).': '.$HotelStreet.' '.$HotelState.' - '.$HotelCountry.'</span><br/>';
		}
		
		$Message = '<br/>';
		$Message .= '<b>'.__('You have submitted booking for',BKG_DOMAIN).':</b><br/><hr style="background:#EAEAEA">';
		$Message .= '<span>'.$PropertyTitle.': '.$HotelName.'</span><br/>';
		$Message .= '<span>'.__('Booking Date',BKG_DOMAIN).': '.$CheckInDate.' - '.$CheckOutDate.' </span><br/>';
		$Message .= '<span>'.__('Total Adults',BKG_DOMAIN).': '.$TotalGuest.'</span><br/>';
		if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
			$Message .= $MsgHotel;
			$Message .= $MsgHotelAddress;
		}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
			$Message .= $MsgHouse;
		}
		_e($Message);
	}
}
//Action for displaying custom fields for booking FINISH.
add_filter('CustomFieldsHeadingTitle','HeadingForCustomFields');
function HeadingForCustomFields($HeadingTitle){
	global $post,$detail_post_type;
	$post_object = get_post_type_object($detail_post_type);
	$post_type_label = $post_object->labels->name;
	return __("{$post_type_label} Information",BKG_DOMAIN);
}
add_action('templ_before_post_title','remove_shareing_buttons_option',20);
function remove_shareing_buttons_option()
{
	if(is_single())
	{
		remove_action('tmpl_detail_page_custom_fields_collection','detail_fields_colletion');
		add_action('templ_before_post_content','detail_fields_colletion');
		add_action('templ_before_post_content','booking_detail_after_content'); 
	}
	if(is_archive() || is_tax() || is_tag()){
		remove_action('tmpl_detail_page_custom_fields_collection','detail_fields_colletion');			
		add_action('templ_after_post_content','booking_detail_after_content'); 
	}
	add_image_size( 'home-page-slider', 980, 528, true );
	remove_action('templ_post_info','send_friend_inquiry_email');
	add_action('templ_after_post_content','send_friend_inquiry_email');
}
//add_action('templ_listing_custom_field','booking_detail_after_content'); 
//Action for Booking Detail page content START.
function booking_detail_after_content(){
	global $post,$wp_query;
	$PostType = get_post_type();
	
	if($PostType!="" && ($PostType== CUSTOM_POST_TYPE_TEMPLATIC_ROOM|| $PostType==CUSTOM_POST_TYPE_TEMPLATIC_HOUSE || $PostType=='booking')){
		$pages = get_pages(array(
			'meta_key' => '_wp_page_template',
			'meta_value' => 'page-template_booking_form.php'
		));
		$args = array(
					'post_type' => 'page',
					'meta_key' => '_wp_page_template',
					'meta_value' => 'page-template_booking_form.php'
					);
		$Pages = new WP_Query($args);
		
		
		$page_id = $pages[0]->ID;
		$BookingId = $post->ID;
		$BookingEmail = get_post_meta($BookingId,'booking_email',true);
		$BookingPhone = get_post_meta($BookingId,'booking_phone',true);
		$BookingAddress = get_post_meta($BookingId,'booking_street',true).', '.get_post_meta($BookingId,'booking_city_state',true).' - '.get_post_meta($BookingId,'booking_country',true);
		$BookingPostType = get_post_meta($BookingId,'booking_post_type',true);
		$PropertyId = get_post_meta($BookingId,'property_room_id',true);
		$_PropertyTitle = get_the_title($PropertyId);
		$CheckInDate = date_i18n(get_option('date_format'), strtotime(get_post_meta($BookingId,'checkin_date',true)));
		$CheckOutDate = date_i18n(get_option('date_format'), strtotime(get_post_meta($BookingId,'checkout_date',true)));
		$TotalGuest = get_post_meta($BookingId,'home_capacity',true);
		$NumberOfRooms = get_post_meta($BookingId,'no_of_rooms',true);
		$BookingStatus = get_post_meta($BookingId,'status',true);
		$PaymentMethod = get_post_meta($BookingId,'paymentmethod',true);
		
		if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
			$PropertyTitle = __('Room',BKG_DOMAIN);
			$PropertyName = __('Room Name',BKG_DOMAIN);
			$MsgHotel = '<span>'.__('Number Of Rooms',BKG_DOMAIN).': '.$NumberOfRooms.'</span><br>';
		}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
			$PropertyTitle = __('House',BKG_DOMAIN);
			$PropertyName = __('House Name',BKG_DOMAIN);
		}
		$DetailContent = '';
		$DetailContent = '<div class="suc_user_detail"><h3>'.__('User Details',BKG_DOMAIN).'</h3>';
		$DetailContent .= '<span>'.__('Email',BKG_DOMAIN).':</span> '.$BookingEmail.'<br/>';
		$DetailContent .= '<span>'.__('Contact No',BKG_DOMAIN).'.:</span> '.$BookingPhone.'<br/>';
		$DetailContent .= '<span>'.__('Address',BKG_DOMAIN).':</span> '.$BookingAddress.'<br/></div>';
		
		$DetailContent .= '<div class="suc_user_detail"><h3>'.__('Booking Details',BKG_DOMAIN).'</h3>';
		$DetailContent .= '<span>'.__('Booking requested for',BKG_DOMAIN).':</span> '.$PropertyTitle.'<br/>';
		$DetailContent .= '<span>'.$PropertyName.':</span>'.$_PropertyTitle.'<br/>';
		$DetailContent .= '<span>'.__('Booking Date',BKG_DOMAIN).':</span> '.$CheckInDate.' - '.$CheckOutDate.'<br/>';
		$DetailContent .= '<span>'.__('Total Adults',BKG_DOMAIN).':</span> '.$TotalGuest.'<br/>';
		$DetailContent .= $MsgHotel;
		$DetailContent .= '<span>'.__('Booking Status',BKG_DOMAIN).':</span> '.$BookingStatus.'<br/>';
		$DetailContent .= '<span>'.__('Payment Method',BKG_DOMAIN).':</span> '.$PaymentMethod.'<br/>';
		$DetailContent .= '</div>';
		$CurrentPostType = $post->post_type;
		$page_id=get_option('booking_system_form');
		if(is_tax() && $CurrentPostType !="" && ($CurrentPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM || $CurrentPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE )){
			
			$BookNowLink = '<span class="booknow_span"><a href="'.home_url().'/?page_id='.$page_id.'&id='.$BookingId.'" class="booknow_btn button">'.__("Book Now",BKG_DOMAIN).'</a></span>';
			echo $BookNowLink;
		}elseif(!is_tax() && is_single() && $CurrentPostType !="" && ($CurrentPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM || $CurrentPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE )){
			$BookNowLink = '<span class="booknow_span"><a href="'.home_url().'/?page_id='.$page_id.'&id='.$BookingId.'" class="booknow_btn button">'.__("Book Now",BKG_DOMAIN).'</a></span>';
			echo $BookNowLink;
		}elseif(!is_tax() && is_single() && $CurrentPostType !="" && $CurrentPostType == CUSTOM_POST_TYPE_TEMPLATIC_BOOKING){
			echo $DetailContent;
		}
	}
}
//Action for Booking Detail page content FINISH.

//CUSTOM META FIELDS FOR BACKEND START.
function booking_display_custom_post_field_plugin($custom_metaboxes,$post_id,$post_type){
	$tmpdata = get_option('templatic_settings');
	foreach($custom_metaboxes as $heading=>$_custom_metaboxes)
	  {		 
		$activ = fetch_active_heading($heading);
		if($activ):
			if($heading == '[#taxonomy_name#]' && $_custom_metaboxes)
			{
				$PostTypeObject = get_post_type_object($post_type);
				$PostTypeName = $PostTypeObject->labels->name;
				
			?>	
            	<div class="sec_title"><h3><?php echo ucfirst($PostTypeName); ?> <?php _e('Information',BKG_DOMAIN); ?></h3></div>
			<?php
            }
			else
			{
				if($_custom_metaboxes){
				echo "<div class='sec_title'><h3>".$heading."</h3>";
				if($_custom_metaboxes['basic_inf']['desc']!=""){echo '<p>'.$_custom_metaboxes['basic_inf']['desc'].'</p>';}
				echo "</div>";
				}
			}
		endif;	
		foreach($_custom_metaboxes as $key=>$val) {
			$name = $val['name'];
			$site_title = $val['label'];
			$type = $val['type'];
			$htmlvar_name = $val['htmlvar_name'];			
			
			//set the post category , post title, post content, post image and post expert replace as per post type
			if($htmlvar_name=="category")
			{
				$site_title=str_replace('Post Category',ucfirst($PostTypeName).' Category',$site_title);
			}
			if($htmlvar_name=="post_title")
			{
				$site_title=str_replace('Post Title',ucfirst($PostTypeName).' Title',$site_title);
			}
			if($htmlvar_name=="post_content")
			{
				$site_title=str_replace('Post Content',ucfirst($PostTypeName).' Content',$site_title);
			}
			if($htmlvar_name=="post_excerpt")
			{
				$site_title=str_replace('Post Excerpt',ucfirst($PostTypeName).' Excerpt',$site_title);
			}
			if($htmlvar_name=="post_images")
			{
				$site_title=str_replace('Post Images',ucfirst($PostTypeName).' Images',$site_title);
			}
			//finish post type wise replace post category, post title, post content, post expert, post images
			$admin_desc = $val['desc'];
			$option_values = $val['option_values'];
			$default_value = $val['default'];
			$style_class = $val['style_class'];
			$extra_parameter = $val['extra_parameter'];
			if(!$extra_parameter){ $extra_parameter ='';}
			/* Is required CHECK BOF */
			$is_required = '';
			$input_type = '';
			if($val['validation_type'] != ''){
				if($val['is_require'] == '1'){
				$is_required = '<span class="required">*</span>';
				}
				
				$is_required_msg = '<span id="'.$name.'_error" class="message_error2"></span>';
			} else {
				$is_required = '';
				$is_required_msg = '';
			}
			/* Is required CHECK EOF */
			if(@$post_id)
			{
				$post_info = get_post($post_id);
				if($name == 'post_title') {
					$value = $post_info->post_title;
				}
				elseif($name == 'post_content') {
					$value = $post_info->post_content;
				}
				elseif($name == 'post_excerpt'){
					$value = $post_info->post_excerpt;
				}
				else {
					$value = get_post_meta($post_id, $name,true);
				}
			
			}
			if(@$_SESSION[$session_variable] && @$_REQUEST['backandedit'])
			{
				$value = @$_SESSION[$session_variable][$name];
			}
		?>
		<div class="form_row clearfix <?php echo $style_class;?>">
		   <?php if($type=='text'){?>
		   <label><?php echo $site_title.$is_required; ?></label>
		   <?php if($name == 'geo_latitude' || $name == 'geo_longitude') {
				$extra_script = 'onblur="changeMap();"';
				
			} else {
				$extra_script = '';
				
			}?>
             <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
			 <input name="<?php echo $name;?>" id="<?php echo $name;?>" value="<?php if(isset($value)){ echo stripslashes($value); } else { echo @$val['default']; } ?>" type="text" class="textfield <?php echo $style_class;?>" <?php echo $extra_parameter; ?> <?php echo $extra_script;?> />
              <?php echo $is_required_msg;?>
			 	<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
             <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			<?php
			}elseif($type=='date'){
				//jquery data picker			
			?>     
				<script type="text/javascript">
					jQuery(function(){
						var pickerOpts = {						
							showOn: "both",
							dateFormat: 'yy-mm-dd',
							buttonImage: "<?php echo TEMPL_PLUGIN_URL;?>css/datepicker/images/cal.png",
							buttonText: "<?php _e('Show Datepicker',BKG_DOMAIN); ?>",
							buttonImageOnly: true,
							onChangeMonthYear: function(year, month, inst) {
							  	jQuery("#<?php echo $name;?>").blur();
						     },
						     onSelect: function(dateText, inst) {
							     jQuery("#<?php echo $name;?>").blur();
						     }
						};	
						jQuery("#<?php echo $name;?>").datepicker(pickerOpts);
					});
				</script>
				<label><?php echo $site_title.$is_required; ?></label>
                <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
				<input type="text" name="<?php echo $name;?>" id="<?php echo $name;?>" class="textfield <?php echo $style_class;?>" value="<?php echo esc_attr(stripslashes($value)); ?>" size="25" <?php echo 	$extra_parameter;?> />
				 <?php echo $is_required_msg;?>
				<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
                <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>	          
			<?php
			}
			elseif($type=='multicheckbox')
			{ ?>
			 <label><?php echo $site_title.$is_required; ?></label>
			<?php
				$options = $val['option_values'];
				if(!isset($post_id) && !$_REQUEST['backandedit'])
				{
					$default_value = explode(",",$val['default']);
				}
	
				if($options)
				{  
					$chkcounter = 0;
					echo '<div class="form_cat_left">';
					do_action('tmpl_custom_fields_'.$name.'_before');
					$option_values_arr = explode(',',$options);
					for($i=0;$i<count($option_values_arr);$i++)
					{
						$chkcounter++;
						$seled='';
						if(isset($post_id) || $_REQUEST['backandedit'])
						  {
							$default_value = $value;
						  }
						if($default_value !=''){
						if(in_array($option_values_arr[$i],$default_value)){ 
						$seled='checked="checked"';} }	
											
						echo '
						<div class="form_cat">
							<label>
								<input name="'.$key.'[]"  id="'.$key.'_'.$chkcounter.'" type="checkbox" value="'.$option_values_arr[$i].'" '.$seled.'  '.$extra_parameter.' /> '.$option_values_arr[$i].'
							</label>
						</div>';
					}
					echo '</div>';
					?>
                     <?php echo $is_required_msg;?>
					<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
					<?php
					do_action('tmpl_custom_fields_'.$name.'_after');
				}
			}		
			elseif($type=='texteditor'){	?>
				<label><?php echo $site_title.$is_required; ?></label>
                <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
				<?php
					// default settings
					$settings =   array(
						'wpautop' => true, // use wpautop?
						'media_buttons' => false, // show insert/upload button(s)
						'textarea_name' => $name, // set the textarea name to something different, square brackets [] can be used here
						'textarea_rows' => '10', // rows="..."
						'tabindex' => '',
						'editor_css' => '<style>.wp-editor-wrap{width:640px;margin-left:0px;}</style>', // intended for extra styles for both visual and HTML editors buttons, needs to include the <style> tags, can use "scoped".
						'editor_class' => '', // add extra class(es) to the editor textarea
						'teeny' => false, // output the minimal editor config used in Press This
						'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
						'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
						'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
					);				
					if(isset($value) && $value != '') 
					{  $content=$value; }
					else{$content= $val['default']; } 				
					wp_editor( stripslashes($content), $name, $settings);
				?>
                 <?php echo $is_required_msg;?>
				<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
                <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			<?php
			}elseif($type=='textarea'){ 
			?>
                <label><?php echo $site_title.$is_required; ?></label>
                <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
                <textarea name="<?php echo $name;?>" id="<?php echo $name;?>" class="<?php if($style_class != '') { echo $style_class;}?> textarea" <?php echo $extra_parameter;?>><?php if(isset($value))echo stripslashes($value);?></textarea>
               	 <?php echo $is_required_msg;?>
                <?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
                <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			<?php
			}elseif($type=='radio'){
			?>
			<?php if($name != 'position_filled' || @$post_id): ?>
			 <label class="r_lbl"><?php echo $site_title.$is_required; ?></label>
            <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
			<?php
				$options = $val['option_values'];
				if($options)
				{  $chkcounter = 0;
					echo '<div class="form_cat_left">';
					$option_values_arr = explode(',',$options);
					for($i=0;$i<count($option_values_arr);$i++)
					{
						$chkcounter++;
						$seled='';
						if($default_value == $option_values_arr[$i]){ $seled='checked="checked"';}
						if (isset($value) && trim($value) == trim($option_values_arr[$i])){ $seled='checked="checked"';}
						echo '<div class="form_cat">
									<label class="r_lbl">
										<input name="'.$key.'"  id="'.$key.'_'.$chkcounter.'" type="radio" value="'.$option_values_arr[$i].'" '.$seled.'  '.$extra_parameter.' /> '.$option_values_arr[$i].'
									</label>
								</div>';
					}
					echo '</div>';
				}
				?>
                 <?php echo $is_required_msg;?>
				<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
				<?php
			 do_action('tmpl_custom_fields_'.$name.'_after');
			 endif;	
			}elseif($type=='select'){
			?>
			 <label><?php echo $site_title.$is_required; ?></label>
				<?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
                <select name="<?php echo $name;?>" id="<?php echo $name;?>" class="textfield textfield_x" <?php echo $extra_parameter;?>>
				<option value=""><?php _e("Please select",BKG_DOMAIN);?></option>
				<?php if($option_values){
				$option_values_arr = explode(',',$option_values);
				for($i=0;$i<count($option_values_arr);$i++)
				{
				?>
				<option value="<?php echo $option_values_arr[$i]; ?>" <?php if($value==$option_values_arr[$i]){ echo 'selected="selected"';} else if($default_value==$option_values_arr[$i]){ echo 'selected="selected"';}?>><?php echo $option_values_arr[$i]; ?></option>
				<?php	
				}
				?>
				<?php }?>
			   
				</select>
                 <?php echo $is_required_msg;?>
				<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
                <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			<?php
			}
			elseif(!isset($_REQUEST['action']) && $type=='post_categories' && $tmpdata['templatic-category_custom_fields'] == 'No')
				{
				/* fetch catgories on action */ ?>
				<div class="form_row clearfix">
				  
						<label><?php echo $site_title.$is_required; ?></label>
						 <div class="category_label"><?php require_once (TEMPL_MONETIZE_FOLDER_PATH.'templatic-custom_fields/category.php');?></div>
						 <?php echo $is_required_msg;?>
						 <span class="message_note msgcat"><?php _e(CATEGORY_MSG,BKG_DOMAIN);?></span>					
				 </div>    
				<?php }
			else if($type=='upload'){ ?>
			 <label><?php echo $site_title.$is_required; ?></label>
             <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
			 <input type="file" value="<?php echo $_SESSION['upload_file']; ?>" name="<?php echo $name; ?>" class="fileupload" id="<?php echo $name; ?>" />
             <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			 <?php if($post_id): ?>
				<p class="resumback"><a href="<?php echo get_post_meta($post_id,$name, $single = true); ?>"><?php echo basename(get_post_meta($post_id,$name, $single = true)); ?></a></p>
			 <?php elseif($_SESSION['upload_file'] && @$_REQUEST['backandedit']): ?>
				<p class="resumback"><a href="<?php echo $_SESSION['upload_file'][$name]; ?>"><?php echo basename($_SESSION['upload_file'][$name]); ?></a></p>
			 <?php endif; ?>
             <?php echo $is_required_msg;?>
			<?php } 
		
			if($type == 'image_uploader' ) {?>
			 	<label><?php echo $site_title ?></label>
				<?php include (TEMPL_MONETIZE_FOLDER_PATH."templatic-custom_fields/image_uploader.php"); ?>
				<span class="message_note"><?php echo $admin_desc;?></span>
                <span class="message_error2" id="post_images_error"></span>
			<?php } ?> 
		  <?php if($type=='geo_map') { ?>
			 <?php include_once(TEMPL_MONETIZE_FOLDER_PATH."templatic-custom_fields/location_add_map.php"); ?>
			<?php if($admin_desc):?>
				 <span class="message_note"><?php echo $admin_desc;?></span>
			<?php else:?>
					 <span class="message_note"><?php echo $GET_MAP_MSG;?></span>
			<?php endif; ?>
			<?php } ?>
			<div class="clearfix"></div>
		 </div>    
		<?php
		}
	}
}
//CUSTOM META FIELDS FOR BACKEND FINISH.

function get_post_custom_fields_templ_booking_plugin($post_types,$category_id='',$taxonomy='',$heading_type='') {
	global $wpdb,$post,$_wp_additional_image_sizes,$sitepress;
	$category_id = explode(",",$category_id);
 	$tmpdata = get_option('templatic_settings');
	remove_all_actions('posts_where');
	if($tmpdata['templatic-category_custom_fields'] == 'No')
	{
		if($heading_type)
		  {
			$args=
			array( 
			'post_type' => 'custom_fields',
			'posts_per_page' => -1	,
			'post_status' => array('publish'),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'post_type_'.$post_types.'',
					'value' => array('all',$post_types),
					'compare' => 'IN',
					'type'=> 'text'
				),
				array(
					'key' => 'post_type',
					'value' => $post_types,
					'compare' => 'LIKE',
					'type'=> 'text'
				),

				array(
					'key' => 'show_on_page',
					'value' =>  array('user_side','both_side'),
					'compare' => 'IN',
					'type'=> 'text'
				),
				
				array(
					'key' => 'is_active',
					'value' =>  '1',
					'compare' => '='
				),
				array(
					'key' => 'heading_type',
					'value' =>  array('basic_inf',$heading_type),
					'compare' => 'IN'
				)
	
			),		 
			'meta_key' => 'sort_order',
			'orderby' => 'meta_value_num',
			'meta_value_num'=>'sort_order',
			'order' => 'ASC'
			);
		  }
		 else
		  {
			$args=
			array( 
			'post_type' => 'custom_fields',
			'posts_per_page' => -1	,
			'post_status' => array('publish'),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'post_type_'.$post_types.'',
					'value' => array('all',$post_types),
					'compare' => 'In',
					'type'=> 'text'
				),
				array(
					'key' => 'show_on_page',
					'value' =>  array('user_side','both_side'),
					'compare' => 'IN',
					'type'=> 'text'
				),
				
				array(
					'key' => 'is_active',
					'value' =>  '1',
					'compare' => '='
				)),
			'meta_key' => 'sort_order',
			'orderby' => 'meta_value_num',
			'meta_value_num'=>'sort_order',
			'order' => 'ASC'
			);
		  }
	}

	$return_arr = array();
	$post_query = "";
	$post_query = new WP_Query($args);	
	if($post_query){
		for($i=0;$i<count($post_query->posts);$i++) {
			$post = $post_query->posts[$i];
			if(get_post_meta($post->ID,"ctype",true)){
				$options = explode(',',get_post_meta($post->ID,"option_values",true));
			}

			$custom_fields = array(
					"name"		=> get_post_meta($post->ID,"htmlvar_name",true),
					"label" 	=> $post->post_title,
					"htmlvar_name" 	=> get_post_meta($post->ID,"htmlvar_name",true),
					"default" 	=> get_post_meta($post->ID,"default_value",true),
					"type" 		=> get_post_meta($post->ID,"ctype",true),
					"desc"      => $post->post_content,
					"option_values" => get_post_meta($post->ID,"option_values",true),
					"is_require"  => get_post_meta($post->ID,"is_require",true),
					"is_active"  => get_post_meta($post->ID,"is_active",true),
					"show_on_listing"  => get_post_meta($post->ID,"show_on_listing",true),
					"show_on_detail"  => get_post_meta($post->ID,"show_on_detail",true),
					"validation_type"  => get_post_meta($post->ID,"validation_type",true),
					"style_class"  => get_post_meta($post->ID,"style_class",true),
					"extra_parameter"  => get_post_meta($post->ID,"extra_parameter",true),
					"show_in_email" =>get_post_meta($post->ID,"show_in_email",true),
					);
			if($options)
			{
				$custom_fields["options"]=$options;
			}
			$return_arr[get_post_meta($post->ID,"htmlvar_name",true)] = $custom_fields;
			
		}
	}
	
	return $return_arr;
}
function get_post_custom_fields_templ_booking_plugin_mail($post_types,$category_id='',$taxonomy='',$heading_type='') {
	global $wpdb,$post,$_wp_additional_image_sizes,$sitepress;
	$category_id = explode(",",$category_id);
 	$tmpdata = get_option('templatic_settings');
	remove_all_actions('posts_where');
	if($tmpdata['templatic-category_custom_fields'] == 'No')
	{
		if($heading_type)
		  {
			$args=
			array( 
			'post_type' => 'custom_fields',
			'posts_per_page' => -1	,
			'post_status' => array('publish'),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'post_type_'.$post_types.'',
					'value' => array('all',$post_types),
					'compare' => 'IN',
					'type'=> 'text'
				),
				array(
					'key' => 'post_type',
					'value' => $post_types,
					'compare' => 'LIKE',
					'type'=> 'text'
				),

				array(
					'key' => 'show_on_page',
					'value' =>  array('user_side','both_side'),
					'compare' => 'IN',
					'type'=> 'text'
				),
				array(
					'key' => 'show_in_email',
					'value' =>  '1',
					'compare' => '='
				),
				array(
					'key' => 'is_active',
					'value' =>  '1',
					'compare' => '='
				),
				array(
					'key' => 'heading_type',
					'value' =>  array('basic_inf',$heading_type),
					'compare' => 'IN'
				)
	
			),		 
			'meta_key' => 'sort_order',
			'orderby' => 'meta_value_num',
			'meta_value_num'=>'sort_order',
			'order' => 'ASC'
			);
		  }
		 else
		  {
			$args=
			array( 
			'post_type' => 'custom_fields',
			'posts_per_page' => -1	,
			'post_status' => array('publish'),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'post_type_'.$post_types.'',
					'value' => array('all',$post_types),
					'compare' => 'In',
					'type'=> 'text'
				),
				array(
					'key' => 'show_on_page',
					'value' =>  array('user_side','both_side'),
					'compare' => 'IN',
					'type'=> 'text'
				),
				array(
					'key' => 'show_in_email',
					'value' =>  '1',
					'compare' => '='
				),
				array(
					'key' => 'is_active',
					'value' =>  '1',
					'compare' => '='
				)),
			'meta_key' => 'sort_order',
			'orderby' => 'meta_value_num',
			'meta_value_num'=>'sort_order',
			'order' => 'ASC'
			);
		  }
	}

	$return_arr = array();
	$post_query = "";
	$post_query = new WP_Query($args);	
	if($post_query){
		for($i=0;$i<count($post_query->posts);$i++) {
			$post = $post_query->posts[$i];
			if(get_post_meta($post->ID,"ctype",true)){
				$options = explode(',',get_post_meta($post->ID,"option_values",true));
			}

			$custom_fields = array(
					"name"		=> get_post_meta($post->ID,"htmlvar_name",true),
					"label" 	=> $post->post_title,
					"htmlvar_name" 	=> get_post_meta($post->ID,"htmlvar_name",true),
					"default" 	=> get_post_meta($post->ID,"default_value",true),
					"type" 		=> get_post_meta($post->ID,"ctype",true),
					"desc"      => $post->post_content,
					"option_values" => get_post_meta($post->ID,"option_values",true),
					"is_require"  => get_post_meta($post->ID,"is_require",true),
					"is_active"  => get_post_meta($post->ID,"is_active",true),
					"show_on_listing"  => get_post_meta($post->ID,"show_on_listing",true),
					"show_on_detail"  => get_post_meta($post->ID,"show_on_detail",true),
					"validation_type"  => get_post_meta($post->ID,"validation_type",true),
					"style_class"  => get_post_meta($post->ID,"style_class",true),
					"extra_parameter"  => get_post_meta($post->ID,"extra_parameter",true),
					"show_in_email" =>get_post_meta($post->ID,"show_in_email",true),
					);
			if($options)
			{
				$custom_fields["options"]=$options;
			}
			$return_arr[get_post_meta($post->ID,"htmlvar_name",true)] = $custom_fields;
			
		}
	}
	
	return $return_arr;
}
function get_booking_success_page_post_type($posttype,$postid=''){
	$get_post_type = '';
	if(isset($posttype) && $posttype !="" && $posttype == CUSTOM_POST_TYPE_TEMPLATIC_BOOKING){
		if(isset($postid) && $postid !=""){
			$get_post_type = get_post_type(get_post_meta($postid,'property_room_id',true));
			return $get_post_type;
		}
	}
}
/* if(is_plugin_active('wpml-string-translation/plugin.php')){
	add_filter('posts_where','booking_widget_wpml_filter');
} */

add_action('advance_search_action','tmp_post_init_search');
function tmp_post_init_search(){
	if(isset($_REQUEST['search_template']) && $_REQUEST['search_template']==1){
		if(is_plugin_active('wpml-string-translation/plugin.php')){
			add_filter('posts_where','booking_widget_wpml_filter',13);
		}
	}
}
function booking_widget_wpml_filter($where){
	global $wpdb;
	if(is_plugin_active('wpml-string-translation/plugin.php')){
		$language = ICL_LANGUAGE_CODE;
		$where .= " AND t.language_code='".$language."'";
	}
	return $where;
}

add_filter('client_booking_success_email','return_client_mail',10,2);
if(!function_exists('return_client_mail')){
 function return_client_mail($user_mail,$post_id){
 	$users_email = get_post_meta($post_id,'booking_email',true);
 	return $users_email;
 }
}
add_filter('client_booking_success_name','return_client_mail_name',10,2);
if(!function_exists('return_client_mail_name')){
 function return_client_mail_name($user_mail,$post_id){
 	$booking_first_name = get_post_meta($post_id,'booking_first_name',true);
	$booking_last_name = get_post_meta($post_id,'booking_last_name',true);
	$booking_name = $booking_first_name.' '.$booking_last_name;
 	return $booking_name;
 }
}

function check_calendar_closing_days($available_booking='',$set_room_house = '')
{
	global $post,$wp_query,$booking_info;
	
	if($set_room_house == '')
		$set_room_house = (isset($_REQUEST['set_room_house']) && $_REQUEST['set_room_house'] !='') ? $_REQUEST['set_room_house'] : get_the_ID() ;	
	if(isset($set_room_house) && $set_room_house !='')
	{
		$calendar_post_type = get_post_type($set_room_house);
		$args_houses = array(
						'p'			  => $set_room_house,
						'post_type'      => $calendar_post_type,
						'posts_per_page' => -1,
						'post_status'    => array('publish'),
						'order'          => 'ASC'
					);
		$closing_start_date="" ;
		$closing_end_date="";
		$post_houses=new WP_Query($args_houses);
		if($post_houses)
		{
			while ($post_houses->have_posts())
			{
				$post_houses->the_post();
				$owner_name = get_the_title();
				$closing_start_dates = get_post_meta(get_the_ID(),'closing_start_date',true);
				$closing_end_dates = get_post_meta(get_the_ID(),'closing_end_date',true);
				
				$closing_start_date_array = explode(",",$closing_start_dates);
				$closing_end_date_array = explode(",",$closing_end_dates);
				for($l=0;$l<count($closing_start_date_array);$l++)
				{
					$closing_start_date = $closing_start_date_array[$l];
					$closing_end_date = $closing_end_date_array[$l];
					
					$closing_fromyear=substr($closing_start_date_array[$l],0,4);
					$closing_frommonth=substr($closing_start_date_array[$l],5,2);
					if(($closing_frommonth==01 || $closing_frommonth==1)){
						$closing_frommonth=12;
						$closing_fromyear=$closing_fromyear-1;
					}else{
						$closing_frommonth=$closing_frommonth-1;
						$closing_fromyear=$closing_fromyear;
					}
					$closing_fromday=substr($closing_start_dates[$l],8,8);
					
					$closing_endyear=substr($closing_end_dates[$l],0,4);
					$closing_endmonth=substr($closing_end_dates[$l],5,2);
					
					if(($closing_endmonth==01 || $closing_endmonth==1)){
						$closing_endmonth=12;
						$closing_endyear=$closing_endyear-1;
					}else{
						$closing_endmonth=$closing_endmonth-1;
						$closing_endyear=$closing_endyear;
					}
					$closing_endday=substr($closing_end_date_array[$l],8,8);
					if(strtotime($closing_start_date)<=strtotime($available_booking) && strtotime($closing_end_date)>=strtotime($available_booking))
					{
						return $closing_start_date;
					}
					else 
						$closing_start_date= "";
				}
				
			}
		}
	}
}
if(!function_exists('check_isadmin_for_booking_detail')){
	function check_isadmin_for_booking_detail(){
		global $post,$current_user;
		get_currentuserinfo();
		if(!is_user_logged_in() && is_single() && $post->post_type == 'booking'){
			wp_die( __( 'You do not have sufficient permissions to access this page.',CHILD_DOMAIN ) );
		}elseif(is_user_logged_in() && !user_can_access_admin_page() && is_single() && $post->post_type == 'booking'){
			wp_die( __( 'You do not have sufficient permissions to access this page.',CHILD_DOMAIN ) );
		}elseif(!is_user_logged_in() && is_tax() && $post->post_type == 'booking'){
			wp_die( __( 'You do not have sufficient permissions to access this page.',CHILD_DOMAIN ) );
		}elseif(is_user_logged_in() && $current_user->ID != 1  && is_tax() && $post->post_type == 'booking'){
			wp_die( __( 'You do not have sufficient permissions to access this page.',CHILD_DOMAIN ) );
		}else{}
	}
}
remove_action('templ_taxonomy_content','templ_taxonomy_category_content');
add_action('templ_taxonomy_content','booking_taxonomy_category_content');
function booking_taxonomy_category_content()
{
	$tmpdata = get_option('templatic_settings');
	if($tmpdata['listing_hide_excerpt']=='' || !in_array(get_post_type(),$tmpdata['listing_hide_excerpt'])){
		if(function_exists('supreme_prefix')){
			$theme_settings = get_option(supreme_prefix()."_theme_settings");
		}else{
			$theme_settings = get_option("supreme_theme_settings");
		}
		if($theme_settings['supreme_archive_display_excerpt']){
			echo '<div class="entry-summary">';
			the_excerpt();
			echo '</div>';
		}else{
			echo '<div class="entry-content">';
			the_content(); 
			echo '</div>';
		}
	}
}
if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'success')
{
	add_action('wp_head','show_background_color');
	function show_background_color()
	{
	/* Get the background image. */
		$image = get_background_image();
		/* If there's an image, just call the normal WordPress callback. We won't do anything here. */
		if ( !empty( $image ) ) {
			_custom_background_cb();
			return;
		}
		/* Get the background color. */
		$color = get_background_color();
		/* If no background color, return. */
		if ( empty( $color ) )
			return;
		/* Use 'background' instead of 'background-color'. */
		$style = "background: #{$color};";
	?>
	<style type="text/css">
	body.custom-background {
	<?php echo trim( $style );
	?>
	}
	</style>
<?php 
	}
}
/* Add action for display the image in taxonomy page */
remove_action('tmpl_category_page_image','tmpl_category_page_image');
remove_action('tmpl_archive_page_image','tmpl_category_page_image'); 
add_action('tmpl_category_page_image','booking_category_page_image');
add_action('tmpl_archive_page_image','booking_category_page_image');
/*
 * Function Name: tmpl_category_page_image
 */
function booking_category_page_image()
{
	global $post;		
	if ( has_post_thumbnail()):
		echo '<a href="'.get_permalink().'" class="event_img">';
		if($featured){echo '<span class="featured_tag">'.__('Featured',EDOMAIN).'</span>';}
		the_post_thumbnail('event-listing-image'); 
		echo '</a>';
	else:
	$post_img = bdw_get_images_plugin($post->ID,'thumbnail');
	$thumb_img = $post_img[0]['file'];
	$attachment_id = $post_img[0]['id'];
	$attach_data = get_post($attachment_id);
	$img_title = $attach_data->post_title;
	$img_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
	?>
    <?php if($thumb_img):?>
						<a href="<?php the_permalink();?>" class="post_img">
							<img src="<?php echo $thumb_img; ?>"  alt="<?php echo $img_alt; ?>" title="<?php echo $img_title; ?>" />
						</a>
    <?php else:?>
					<a href="<?php the_permalink();?>" class="post_img no_image_avail">
						<img src="<?php echo TEMPLATIC_BOOKING_URL; ?>/images/noimage-150x150.jpg" alt="" height="150" width="150"  />
					</a>	
    <?php endif;
	endif;?>
 <?php
}
/*
 * name: get_admin_event_calendar
 * description: shows the calendar with booked date.
 */
if(!function_exists('get_admin_event_calendar')){
	function get_admin_event_calendar($set_room_house='',$id=''){
		$monthNames = array(__('January',BKG_DOMAIN), __('February',BKG_DOMAIN), __('March',BKG_DOMAIN), __('April',BKG_DOMAIN), __('May',BKG_DOMAIN), __('June',BKG_DOMAIN), __('July',BKG_DOMAIN), __('August',BKG_DOMAIN), __('September',BKG_DOMAIN), __('October',BKG_DOMAIN	), __('November',BKG_DOMAIN), __('December',BKG_DOMAIN));
	
		if (!isset($_REQUEST["mnth"])) $_REQUEST["mnth"] = date("n");
		if (!isset($_REQUEST["yr"])) $_REQUEST["yr"] = date("Y");
		
		$cMonth = $_REQUEST["mnth"];
		$cYear = $_REQUEST["yr"];
		
		$prev_year = $cYear;
		$next_year = $cYear;
		$prev_month = $cMonth-1;
		$next_month = $cMonth+1;
		
		if ($prev_month == 0 ) {
			$prev_month = 12;
			$prev_year = $cYear - 1;
		}
		if ($next_month == 13 ) {
			$next_month = 1;
			$next_year = $cYear + 1;
		}
		global $post;
		if(is_admin())
		{
			$mainlink = admin_url('admin.php?page=templatic_booking_system&tab=booking_calendar');
		}
		else
		{
			$mainlink = get_permalink($post->ID);
		}
		$calendar_id = '';
		if(@$id != '')
		{
			$calendar_id = '#'.$id;
		}
		if(strstr($_SERVER['REQUEST_URI'],'?mnth') && strstr($_SERVER['REQUEST_URI'],'&yr'))
		{
			$replacestr = "?mnth=".$_REQUEST['mnth'].'&yr='.$_REQUEST['yr']."&set_room_house=". $set_room_house;
		}
		elseif(strstr($_SERVER['REQUEST_URI'],'&mnth') && strstr($_SERVER['REQUEST_URI'],'&yr'))
		{
			$replacestr = "&mnth=".$_REQUEST['mnth'].'&yr='.$_REQUEST['yr']."&set_room_house=". $set_room_house;
		}
		if(strstr($_SERVER['REQUEST_URI'],'?') && !strstr($_SERVER['REQUEST_URI'],'?mnth'))
		{
			$pre_link = $mainlink."&mnth=". $prev_month . "&yr=" . $prev_year ."&set_room_house=". $set_room_house.$calendar_id;
			$next_link = $mainlink."&mnth=". $next_month . "&yr=" . $next_year ."&set_room_house=". $set_room_house.$calendar_id;
		}
		else
		{
			$pre_link = $mainlink."?mnth=". $prev_month . "&yr=" . $prev_year ."&set_room_house=". $set_room_house.$calendar_id;	
			$next_link = $mainlink."?mnth=". $next_month . "&yr=" . $next_year ."&set_room_house=". $set_room_house.$calendar_id;
		}
		?> 
		
		 <div class="calendar_box">
			  <table width="100%">
				   <tr align="center">
						<td >
							 <table width="100%">
								  <tr align="center" class="title">
									   <td width="10%" class="title"> <a href="<?php echo $pre_link; ?>" > <img src="<?php echo TEMPLATIC_BOOKING_URL; ?>images/previous2.png" alt=""  /></a></td>
									   <td class="title"><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></td>
									   <td width="10%" class="title" style="border-right:1px solid #fff;"><a href="<?php echo $next_link; ?>" >  <img src="<?php echo TEMPLATIC_BOOKING_URL; ?>images/next2.png" alt=""  /></a> </td>
								  </tr>
							 </table>
						</td>
				</tr>
				   <tr>
						<td align="center">
						<?php 
						if(isset($set_room_house) && @$set_room_house !=""){ ?>
							 <table width="100%" border="0" cellpadding="2" cellspacing="2"  class="calendar_widget">
								  <tr>
									   <td align="center" class="days"><?php _e('Sun',BKG_DOMAIN); ?></td>
									   <td align="center" class="days"><?php _e('Mon',BKG_DOMAIN); ?></td>
									   <td align="center" class="days"><?php _e('Tue',BKG_DOMAIN); ?></td>
									   <td align="center" class="days"><?php _e('Wed',BKG_DOMAIN); ?></td>
									   <td align="center" class="days"><?php _e('Thu',BKG_DOMAIN); ?></td>
									   <td align="center" class="days"><?php _e('Fri',BKG_DOMAIN); ?></td>
									   <td align="center" class="days"><?php _e('Sat',BKG_DOMAIN); ?></td>
								  </tr> 
							 <?php
							 $timestamp = mktime(0,0,0,$cMonth,1,$cYear);
							 $maxday = date("t",$timestamp);
							 $thismonth = getdate ($timestamp);
							 $startday = $thismonth['wday'];
							 
							 if($_GET['m'])
							 {
								$m = $_GET['m'];	
								$py=substr($m,0,4);
								$pm=substr($m,4,2);
								$pd=substr($m,6,2);
								$monthstdate = "$cYear-$cMonth-01";
								$monthenddate = "$cYear-$cMonth-$maxday";
							 }
							 global $wpdb,$closing_days_table;
							 for ($i=0; $i<($maxday+$startday); $i++) {
									if(($i % 7) == 0 ) echo "<tr>\n";
							if($i < $startday){
								echo "<td></td>\n";
							}
							else 
							{
								$cal_date = $i - $startday + 1;
								$calday = $cal_date;							
								if(strlen($cal_date)==1)
								{
									$calday=$cal_date;
								}
								$cMonth1 = $cMonth;
								if(strlen($cMonth)==1)
								{
									$cMonth1=$cMonth;
								}
								$urlddate = "$cYear$cMonth1$calday";
								$thelink = site_url()."/?s=cal_event&m=$urlddate";
								
								$the_cal_date = $cal_date;
								if(strlen($the_cal_date)==1){$the_cal_date = '0'.$the_cal_date;}
								$todaydate = "$cYear-$cMonth1-$the_cal_date";
								global $booking_master,$additional_price_table;
								
								$event_of_month_sql = "select * from $booking_master where check_in_date <= \"$todaydate\" and check_out_date >\"$todaydate\" and booking_status = 'confirmed'";
								$args=array(
										'post_type'      => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,	
										'posts_per_page' => -1,									
										'post_status'    => array('publish'),
										'meta_query'     => array(
															'relation' => 'AND',
																array(
																	'key'     => 'status',
																	'value'   => 'Approved',
																	'compare' => '=',
																),
																array(
																	'key' 	=> 'property_room_id',
																	'value'	=> @$set_room_house,
																	'compare'	=> '='
																),
																array(
																	'key' 	=> 'checkin_date',
																	'value'	=> $todaydate,
																	'compare'	=> '<=',
																	'type' => 'DATE'
																),
																array(
																	'key' 	=> 'checkout_date',
																	'value'	=> $todaydate,
																	'compare'	=> '>',
																	'type' => 'DATE'
																),
																
															),	
										'order'          => 'ASC'	
									);
								$post_calendar=new WP_Query($args);															
								$event_post_info_perday = 0;
								$booked_no_of_rooms = 0;
								$rooms_left = 0;
								if($post_calendar && isset($set_room_house) && @$set_room_house != '')
								{
									while ($post_calendar->have_posts())
									{
										$post_calendar->the_post();			
										$property_room_id=get_post_meta(get_the_ID(),'property_room_id',true);
										$no_of_rooms=get_post_meta($property_room_id,'no_of_rooms',true);
										$booked_no_of_rooms+=get_post_meta(get_the_ID(),'no_of_rooms',true);
										if($booked_no_of_rooms >= $no_of_rooms)
											$event_post_info_perday = $booked_no_of_rooms;
										else
											$rooms_left = $no_of_rooms - $booked_no_of_rooms;
										
										if(get_post_type($property_room_id)=='house')
										{ 
											$event_post_info_perday=1;
										}									
									}
									wp_reset_query();
								}
								wp_reset_query();							
								
								$caldate1 = mktime(0,0,0,$cYear,$cMonth,$calday);
								echo "<td align='center' valign='middle'>";
								$available_booking = date($cYear."-".$cMonth."-".$calday);
								$curdate = date('Y-m-d');	
								$closeing_date = check_calendar_closing_days($available_booking,$set_room_house);	
								if($event_post_info_perday )
								{
									echo "<span class=\"booked\">". ($cal_date) . "</span>";
								}
								else if($closeing_date!=''){
									echo "<span class=\"closed\">". ($cal_date) . "</span>";
								}else if(strtotime($available_booking) <= strtotime($curdate)){
									echo "<span class=\"past\">". ($cal_date) . "</span>";
								}else if(strtotime($available_booking) > strtotime($curdate) && $rooms_left)
								{
									echo "<span class=\"no_event\" >". "<b class=\"room_left\" >".$rooms_left."</b>".($cal_date) . "</span>";
								}
								else if(strtotime($available_booking) > strtotime($curdate) )
								{
									echo "<span class=\"no_event\" >". "<b class=\"room_left\" >".get_post_meta(@$set_room_house,'no_of_rooms',true)."</b>".($cal_date) . "</span>";
								}
								echo "</td>\n";
							}
							
							if(($i % 7) == 6 ) echo "</tr>\n";
							 }
							 ?>
							 </table>
						<?php
						}else{
						echo '<table style="border:0;min-height:200px" width="100%" cellpadding="0" cellspacing="0">
						
						<tr><td  style="border:0;text-align:center"><b>';
						_e('Please select room/house first',BKG_DOMAIN);
						echo '</b></td></tr></table>';
						}
						?>
						</td>
				   </tr>
			  </table>
		 </div>
	<?php 
	}
}
/*
 * name: single_booking_calendar
 * description: fetch the calendar on detail page.
 * */
add_action('templ_after_post_content','single_booking_calendar',10); 
function single_booking_calendar()
{
	global $post;
	if(get_option('detail_show_cal') && is_single() && ($post->post_type == 'house' || $post->post_type == 'room')){
	?>
	<div id="calendar" class="clearfix">
		<div class="templatic_settings">
			<div><h2 class="custom_field_title" style="font-size:22px;"><?php _e('Availability',BKG_DOMAIN); ?></h2></div>
		<?php get_admin_event_calendar($post->ID,'calendar'); ?>
			
				<div style="float:left;">
					<div class="display_availability"><span class="allow_booking"></span><b> <?php _e("Available",BKG_DOMAIN);?></b></div>
					<div class="display_availability"><span class="book_not_available"></span><b> <?php _e("Not Available",BKG_DOMAIN);?></b></div>
					<div class="display_availability"><span class="book_closed"></span><b> <?php _e("Booking Closed",BKG_DOMAIN);?></b></div>
					<div class="display_availability"><span class="rooms_left"></span><b> <?php _e("Number of rooms/house left",BKG_DOMAIN);?></b></div>
				</div>
				<div style="float:right;">
					<?php 
					$page_id=get_option('booking_system_form');
					$BookingId = $post->ID;
					$BookNowLink = '<span ><a href="'.home_url().'/?page_id='.$page_id.'&id='.$BookingId.'" class="booknow_btn button">'.__("Book Now",BKG_DOMAIN).'</a></span>';
					echo $BookNowLink;
					?>
				</div>
			
		</div>
	</div>	
	<?php
	}
}
?>
