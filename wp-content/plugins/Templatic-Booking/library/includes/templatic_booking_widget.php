<?php
if(!class_exists('templatic_booking_widget')){
	class templatic_booking_widget extends WP_Widget {
		function templatic_booking_widget() {
		//Constructor
			$widget_ops = array('classname' => 'widget booking', 'description' => apply_filters('templ_templ_bookan_appointment_filter','Display hotel booking form on frontend') );		
			$this->WP_Widget('templatic_booking_widget', apply_filters('templ_bookan_appointment_title_filter','T &rarr; Booking Widget'), $widget_ops);
		}
		function widget($args, $instance) {
		// prints the widget
			extract($args, EXTR_SKIP);
			$title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
			$desc = empty($instance['desc']) ? '' : apply_filters('widget_desc', $instance['desc']);
			if(function_exists('icl_t')){
				$desc = icl_t(BKG_DOMAIN,$desc,$desc);
			}
			$use_booking_widget_for = empty($instance['use_booking_widget_for']) ? 'room' : $instance['use_booking_widget_for'];
			$book_calendar_text = empty($instance['book_calendar_text']) ? __('Check Availability',BKG_DOMAIN) : $instance['book_calendar_text'];
			
			global $post,$wp_query,$wpdb;
			$page_id1=get_option('booking_system_calendar');
			$booking_calendar_url = get_permalink($page_id1);
			
			$booking_calendar_link = empty($instance['book_calendar_link']) ? $booking_calendar_url : $instance['book_calendar_link'];
			?>						
		  
			<div id="booking">
					<div class="widget booking_small_widget clearfix">
                <?php if($title){?><h3><?php echo $title; ?></h3><?php }?>
                <?php if($desc){?><p><?php echo $desc; ?></p><?php }?>
				<?php 
					add_action('wp_footer','check_availability_function');
					if(!function_exists('check_availability_function')){
					function check_availability_function(){
				?>
				<script type="text/javascript">
					var $date_picker = jQuery.noConflict();
					function find_available_date(){
						var checkindate = $date_picker('#check_in_date').val();
						var checkoutdate = $date_picker('#check_out_date').val();
						var propertyid = $date_picker('#houses_rooms').val();
						var dataString2 = '&checkindate='+checkindate+'&checkoutdate='+checkoutdate+'&propertyid='+propertyid;
						
					/*	$date_picker.ajax({
							type: "GET",
							url: "<?php //echo TEMPLATIC_BOOKING_URL.'/library/includes/find_available_dates.php';?>",
							data: dataString2,
							success: function(html){
								if(html==0){$date_picker("#next_step").prop('disabled',false);}else{
									$date_picker("#next_step").prop('disabled',true);
									$date_picker('.reservation_frm #date-range-field span').text('<?php //_e('Booking not available',BKG_DOMAIN); ?>');
								}
							},
							error: function(){
								alert('<?php //_e("An error occured while processing Ajax",BKG_DOMAIN);?>');
							}
						});*/
					}
					$date_picker(document).ready(function() {
						var from = new Date();
						var to = new Date(from.getTime() + 200 * 60 * 60 * 24 * 14);
						$date_picker('.reservation_frm #datepicker-calendar').DatePicker({
							inline: true,
							calendars: 2,
							mode: 'range',
							current: new Date(to.getFullYear(), to.getMonth()+1, 1),
							onChange: function(dates,el) {
							 var chk_in_month = dates[0].getMonth()+1;
							  var chk_out_month = dates[1].getMonth()+1;
								
							 var first_date = dates[0].getFullYear()+'-'+chk_in_month+'-'+dates[0].getDate();
							 var second_date = dates[1].getFullYear()+'-'+chk_out_month+'-'+dates[1].getDate();
							 
							 var d = new Date();
					 		 var from_date_validation = d.setDate(d.getDate()-1);
							 
							 if(first_date == second_date)
							 {
								$date_picker("#next_step").prop('disabled',true);
								$date_picker('.reservation_frm #date-range-field span').text('<?php _e("Invalid date",BKG_DOMAIN);?>');
								$date_picker('#check_in_date').val('');
								$date_picker('#check_out_date').val('');
							 }else if(dates[0] < from_date_validation || dates[1] < from_date_validation){
								$date_picker('.reservation_frm #date-range-field span').text('<?php _e("Invalid date",BKG_DOMAIN);?>');
								$date_picker('#check_in_date').val('');
								$date_picker('#check_out_date').val('');
							  }else{
								var chk_in_month = dates[0].getMonth()+1;
								var chk_out_month = dates[1].getMonth()+1;
								$date_picker('.reservation_frm #date-range-field span').text(dates[0].getDate()+' '+dates[0].getMonthName(false)+' - '+
																dates[1].getDate()+' '+dates[1].getMonthName(false));
								$date_picker('#check_in_date').val(dates[0].getFullYear()+'-'+chk_in_month+'-'+dates[0].getDate());
								$date_picker('#check_out_date').val(dates[1].getFullYear()+'-'+chk_out_month+'-'+dates[1].getDate());
								$date_picker("#next_step").prop('disabled',false);
							  }								
							 }
						   });
						   $date_picker('.reservation_frm #date-range-field span').text('<?php _e("Select Date",BKG_DOMAIN);?>');
						   $date_picker('.reservation_frm #date-range-field').bind('click', function(){
							 $date_picker('.reservation_frm #datepicker-calendar').toggle();
							 if($date_picker('.reservation_frm #date-range-field a').text().charCodeAt(0) == 9660) {
							 } else {
							 }
							 return false;
						   });
						   $date_picker('html').click(function() {
							 if($date_picker('.reservation_frm #datepicker-calendar').is(":visible")) {
							   $date_picker('.reservation_frm #datepicker-calendar').hide();
							 }
						   });
						   $date_picker('.reservation_frm #datepicker-calendar').click(function(event){
							 event.stopPropagation();
						   });
						   
						   $date_picker("#close_cal").click(function(){
							   $date_picker('.booking_small_widget .reservation_frm #datepicker-calendar').hide();
						   });
					 });
					 
					 function get_post_type(id){
						  <?php if(is_plugin_active('wpml-translation-management/plugin.php')){
							global $sitepress;
							$current_lang_code= ICL_LANGUAGE_CODE;
							$language="&language=".$current_lang_code;
						}?>
						find_available_date();
						var dataString1 = '<?php echo @$language;?>&post_id='+id;
						$date_picker.ajax({
							type: "GET",
							url: "<?php echo TEMPLATIC_BOOKING_URL;?>library/includes/get_post_type_by_id.php",
							data: dataString1,
							success: function(html){
							//alert(html);
								$date_picker('#adults_rooms').html(html);
							}
						});
					 }
				</script>	
				<?php 
					}}
					global $post,$wp_query,$wpdb;
					$page_id=get_option('booking_system_form');
					if(function_exists('icl_object_id')){
						$page_id = icl_object_id($page_id, 'page', false,ICL_LANGUAGE_CODE);
					}	
					$booking_url = get_permalink($page_id);
				?>
				<form name="reservation_frm" class="reservation_frm" id="reservation_frm" action="<?php echo $booking_url;?>" method="post">
					<input type="hidden" name="check_in_date" id="check_in_date" value=""/>
					<input type="hidden" name="check_out_date" id="check_out_date" value=""/>
					<input type="hidden" name="pid" id="pid" value="1"/>
					<div class="form_row clearfix">
						<label><?php _e("Check-In/Check-Out date",BKG_DOMAIN);?><span class="required">*</span></label>
						<div class="clearfix"></div>
						<div id="date-range">
						  <div id="date-range-field">
							<span></span>
							<a href="#">&#9660;</a>
						  </div>
						  <div id="datepicker-calendar">
							<span class="booking_calendar_link">
								<a href="<?php echo $booking_calendar_link; ?>" target="_blank"><?php echo $book_calendar_text; ?></a>
								<img id="close_cal" class="close_cal" src="<?php echo TEMPLATIC_BOOKING_URL.'/images/close_btn.png'?>" alt="Close" title="Close"/>
							</span>
						  </div>
						  <span id="booking_check_in_date_error" class="message_error2"></span>
						</div>	
					</div>
					<div class="form_row clearfix">
						<?php 
							$prepare_label_house = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_HOUSE);
							$label_house = @$prepare_label_house->labels->name;
							$prepare_label_hotel = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_ROOM);
							$label_hotel = @$prepare_label_hotel->labels->name;
							
							if($use_booking_widget_for=="house"){
								$lbl_house = "Select ".$label_house;
								if(function_exists('icl_register_string')){
									icl_register_string(BKG_DOMAIN,$lbl_house,$lbl_house);
								}
								if(function_exists('icl_t')){
									$label_house1 = icl_t(BKG_DOMAIN,$lbl_house,$lbl_house);
								}else{
									$label_house1 = __($lbl_house,BKG_DOMAIN); 
								}
						?>
								<select name="houses_rooms" id="houses_rooms" onchange="get_post_type(this.value);">
									<option value=""><?php echo $label_house1;?></option>
										<?php 
										if(is_plugin_active('wpml-string-translation/plugin.php')){
											add_filter('posts_where','booking_widget_wpml_filter');
										}
											$args_house = array(
																'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
																'posts_per_page' => -1,
																'post_status' => 'publish',
																);
											remove_all_actions("posts_where");
											remove_all_actions("posts_orderby");					
											if(is_plugin_active('wpml-string-translation/plugin.php')){
												add_filter('posts_where','booking_widget_wpml_filter');
											}
											$posts_houses = new WP_Query($args_house);
											remove_filter('posts_where','booking_widget_wpml_filter');
											if($posts_houses){
												while($posts_houses->have_posts()){
													$posts_houses->the_post();
													global $post;
													if($post->post_title!="" && $post->post_title != "Auto Draft"){				
											?>
														<option value="<?php echo $post->ID;?>" <?php //if($_REQUEST['houses_rooms'] == $post->ID){echo 'selected="selected"';}?>><?php echo $post->post_title;?></option>
												<?php 
													}
												}wp_reset_query();
											}else{?>
													<option value=""><?php _e("No Houses Available",BKG_DOMAIN); ?></option>
										<?php	}
										?>	
								</select>
						<?php }elseif($use_booking_widget_for=="room"){
								$lbl_hotel = "Select ".$label_hotel;
								if(function_exists('icl_register_string')){
									icl_register_string(BKG_DOMAIN,$lbl_hotel,$lbl_hotel);
								}
								if(function_exists('icl_t')){
									$label_hotel1 = icl_t(BKG_DOMAIN,$lbl_hotel,$lbl_hotel);
								}else{
									$label_hotel1 = __($lbl_hotel,BKG_DOMAIN); 
								}
						?>
								<select name="houses_rooms" id="houses_rooms" onchange="get_post_type(this.value);">
									<option value=""><?php echo $label_hotel1;?></option>
										<?php 
										$args_hotel = array(
															'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
															'posts_per_page' => -1,
															'post_status' => 'publish',
															);
											remove_all_actions("posts_where");
											remove_all_actions("posts_orderby");
											if(is_plugin_active('wpml-string-translation/plugin.php')){
												add_filter('posts_where','booking_widget_wpml_filter');
											}
											$posts_hotel = new WP_Query($args_hotel);
											remove_filter('posts_where','booking_widget_wpml_filter');
											if($posts_hotel){
												while($posts_hotel->have_posts()){
													$posts_hotel->the_post();
													global $post;
													if($post->post_title!="" && $post->post_title != "Auto Draft"){		
											?>
														<option value="<?php echo $post->ID;?>" <?php //if($_REQUEST['houses_rooms'] == $post->ID){echo 'selected="selected"';}?>><?php echo $post->post_title;?></option>
												<?php 
													}
												}wp_reset_query();
											}else{?>
													<option value=""><?php _e("No Rooms Available",BKG_DOMAIN); ?></option>
										<?php	} ?>
										
								</select>

							<?php }	?>
					</div>
					<div id="adults_rooms">
						<div class="form_row clearfix">
							<select name="home_capacity" id="home_capacity">
								<option value=""><?php _e("Total Guests",BKG_DOMAIN);?></option>
							</select>
						</div>
						<?php if($use_booking_widget_for=="room"){ ?>
						<div class="form_row clearfix" id="rooms_widget">
							<select name="no_of_rooms" id="no_of_rooms">
								<option value=""><?php _e("Number of rooms",BKG_DOMAIN);?></option>
							</select>
						</div>
						<?php } ?>
					</div>
				  <input type="submit" name="next_step" id="next_step" value="<?php _e("Book Now",BKG_DOMAIN);?>"  class="b_submit" />	
				</form>
			</div>
			</div> <!-- booking #end -->
				 

		<?php
		}
		function update($new_instance, $old_instance) {
		//save the widget
			$instance = $old_instance;		
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['desc'] = strip_tags($new_instance['desc']);
			$instance['use_booking_widget_for'] = strip_tags($new_instance['use_booking_widget_for']);
			$instance['book_calendar_text'] = strip_tags($new_instance['book_calendar_text']);
			$instance['book_calendar_link'] = strip_tags($new_instance['book_calendar_link']);
			return $instance;
		}
		function form($instance){
		//widgetform in backend
			$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'desc' => '', 'use_booking_widget_for' => '', 'book_calendar_text' => '', 'book_calendar_link' => '') );		
			$title = strip_tags($instance['title']);
			$desc = strip_tags($instance['desc']);
			$use_booking_widget_for = strip_tags($instance['use_booking_widget_for']);
			$book_calendar_text = strip_tags($instance['book_calendar_text']);
			
			global $post,$wp_query,$wpdb;
			$page_id2 = get_option('booking_system_calendar');
			$book_calendar_url = get_permalink($page_id2);
			
			$book_calendar_link = ($instance['book_calendar_link']) ? $instance['book_calendar_link'] : $book_calendar_url;
		?>
			<p><label for="<?php  echo $this->get_field_id('title'); ?>"><?php _e('Title : ',BKG_DOMAIN);?> <input class="widefat" id="<?php  echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
			
			<p><label for="<?php echo $this->get_field_id('desc'); ?>"><?php _e('Short Description',BKG_DOMAIN);?> : <textarea class="widefat" rows="6" cols="20" id="<?php echo $this->get_field_id('desc'); ?>" name="<?php echo $this->get_field_name('desc'); ?>"><?php echo attribute_escape($desc); ?></textarea></label></p>
			
			<p><label for="<?php echo $this->get_field_id('use_booking_widget_for'); ?>"><?php _e('Post Type',BKG_DOMAIN);?> : 
					<select name="<?php echo $this->get_field_name('use_booking_widget_for'); ?>" id="<?php echo $this->get_field_id('use_booking_widget_for'); ?>">
						<option value=""><?php _e('Please Select',BKG_DOMAIN);?></option>
						 <?php
							$all_post_types = get_post_types();
							foreach($all_post_types as $post_types){
							if( $post_types != "page"  && $post_types != "attachment" && $post_types != "post" && $post_types != "booking" && $post_types != "revision" && $post_types != "nav_menu_item" ){
								?>
									<option value="<?php echo $post_types;?>" <?php if($post_types== $instance['use_booking_widget_for'])echo "selected";?>><?php echo esc_attr($post_types);?></option>
								<?php				
							}
						}
					?>	
					</select>
				</label>
			</p>
			<p>
				<label for="<?php  echo $this->get_field_id('book_calendar_text'); ?>"><?php _e('Booking Calendar Text:',BKG_DOMAIN);?> <input class="widefat" id="<?php  echo $this->get_field_id('book_calendar_text'); ?>" name="<?php echo $this->get_field_name('book_calendar_text'); ?>" type="text" value="<?php echo attribute_escape($book_calendar_text); ?>" /></label>
			</p>
			<p>
				<label for="<?php  echo $this->get_field_id('book_calendar_link'); ?>"><?php _e('Booking Calendar Link:',BKG_DOMAIN);?> <input class="widefat" id="<?php  echo $this->get_field_id('book_calendar_link'); ?>" name="<?php echo $this->get_field_name('book_calendar_link'); ?>" type="text" value="<?php echo attribute_escape($book_calendar_link); ?>" /></label>
			</p>
      
		<?php
		}
	}
	register_widget('templatic_booking_widget');
}
?>