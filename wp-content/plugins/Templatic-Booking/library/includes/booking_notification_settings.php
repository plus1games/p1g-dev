<?php
global $wpdb;
	$templatic_settings = get_option( "templatic_settings" );
	$templatic_booking_success_message = $templatic_settings['templatic_booking_system_settings']['templatic_booking_notification_settings']['templatic_booking_success_message'];
	$prebank_message = $templatic_settings['templatic_booking_system_settings']['templatic_booking_notification_settings']['prebank_message'];
?>
<div class="templatic_settings">
	<form class="form_style" action="" method="post">
		<p class="description"><?php _e("Edit notification messages from here. These messages will be shown in website.",BKG_DOMAIN);?></p>
		<table style="width:100%">
			<tbody>
				<tr>
					<td colspan="2"><h3><?php _e("Notification Email Settings",BKG_DOMAIN);?></h3></td>
				</tr>
				<tr>
					<td colspan="2">
						<table style="width:60%" class="widefat post">
							<thead>
								<tr>
									<th><label for="email_type" class="form-textfield-label"><?php _e("Notification Title",BKG_DOMAIN);?></label></th>
									<th><label for="email_desc" class="form-textfield-label"><?php _e("Description",BKG_DOMAIN);?></label></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><label class="form-textfield-label"><?php _e("Booking Success Message",BKG_DOMAIN);?></label></td>
									<td>
										<textarea name="templatic_booking_success_message" style="width:350px; height:130px;"><?php if($templatic_booking_success_message!=""){echo __(stripslashes($templatic_booking_success_message),BKG_DOMAIN);}else{echo __(stripslashes(BOOKING_SUCCESS_MSG),BKG_DOMAIN);}?></textarea>
									</td>
								</tr>
								<tr>
									<td><label class="form-textfield-label"><?php _e("Payment via bank transfer success notification",BKG_DOMAIN);?></label></td>
									<td>
										<textarea name="prebank_message" style="width:350px; height:130px;"><?php if($prebank_message!=""){echo __(stripslashes($prebank_message),BKG_DOMAIN);}else{echo __(stripslashes(PRE_BANK_PAYMENT_MSG),BKG_DOMAIN);}?></textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<?php 
							$legend_display = '<h3>Legends : </h3>';
							$legend_display .= '<p style="line-height:30px;width:100%;"><label style="float:left;width:200px;">[USER_NAME]</label> : '.__('Name of the recipient.',BKG_DOMAIN).'<br />
												<label style="float:left;width:200px;">[CONTACT_MAIL]</label> : '.__('Email of the sender',BKG_DOMAIN).'<br />
												<label style="float:left;width:200px;">[PAYABLE_AMOUNT]</label> : '.__('Total payable amount',BKG_DOMAIN).'<br />
												<label style="float:left;width:200px;">[BANK_NAME]</label> : '.__('Name of the bank',BKG_DOMAIN).'<br />
												<label style="float:left;width:200px;">[ACCOUNT_NUMBER]</label> : '.__('Account Number',BKG_DOMAIN).'<br />
												<label style="float:left;width:200px;">[SUBMISSION_ID]</label> : '.__('Submitted booking Id',BKG_DOMAIN).'<br />
												<label style="float:left;width:200px;">[SUBMITTED_INFORMATION_LINK]</label> : '.__('Submission information link',BKG_DOMAIN).'<br />
												<label style="float:left;width:200px;">[SITE_NAME]</label> : '.__('Name of site',BKG_DOMAIN).'<br />
												';
							echo $legend_display;
						?>
					</td>
				</tr>
			</tbody>	
		</table>
		<p class="submit" style="clear: both;">
		  <input type="submit" name="Submit" class="button-primary" value="<?php _e('Save All Settings',BKG_DOMAIN);?>">
		  <input type="hidden" name="notification_submit" value="y">
		</p>
	</form>
</div>