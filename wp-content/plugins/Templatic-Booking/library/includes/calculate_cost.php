<?php 
$file = dirname(__FILE__);
$file = substr($file,0,stripos($file, "wp-content"));
require($file . "/wp-load.php");
//print_r($_REQUEST);//exit;
global $wpdb;

$templatic_settings = get_option( "templatic_settings" );

$property_id = $_REQUEST['property_id'];
$check_in_date = $_REQUEST['checkindate'];
$check_out_date = $_REQUEST['checkoutdate'];
$time_check_in_date = strtotime($_REQUEST['checkindate']);
$time_check_out_date = strtotime($_REQUEST['checkoutdate']);
$final_price=0;
$final_amount=0;
$final_price_seasonal = 0;
$final_price_normal = 0;
$count_seasonal_price = 0;
$seasonal_cnt = 0;
$availability_flag = 0;
$seasonal_id = 0;
$final_price_seasonal_recurrence_month = 0;
$final_price_seasonal_recurrence_week = 0;
$final_price_seasonal_recurrence = 0;
$guests = $_REQUEST['home_capacity'];
$coupon_code_text = $_REQUEST['coupon_code_text'];
if(($time_check_out_date == $time_check_in_date)){
	$days_between = 1;
}else{
	$days_between = ceil(abs($time_check_out_date - $time_check_in_date) / 86400);
}
$post_tble = $wpdb->prefix.'posts';
$post_meta_tbl = $wpdb->prefix.'postmeta';


$booking_availability_table = $wpdb->prefix."booking_availability_check";
$availability_chkout_date = date_i18n('Y-m-d',strtotime(date_i18n('Y-m-d', strtotime($check_out_date)) . "-1 day"));
$result_availability = $wpdb->get_results("select availability_id,property_id,booking_post_id from $booking_availability_table where status='Approved' and property_id=$property_id and ((from_date between '".$check_in_date."' and '".$availability_chkout_date."') and (to_date between '".$check_in_date."' and '".$availability_chkout_date."') or (from_date <= '".$check_in_date."' and to_date >= '".$check_in_date."') or (from_date >= '".$availability_chkout_date."' and to_date <= '".$availability_chkout_date."') or (from_date >= '".$check_in_date."' and from_date <= '".$availability_chkout_date."') or (to_date > '".$check_in_date."' and to_date < '".$availability_chkout_date."'))");
$result_closing = 0;
$booking_closing_table = $wpdb->prefix."booking_closing_check";
$result_closing = $wpdb->get_var("select closing_id from $booking_closing_table where property_id=$property_id and ((closing_start_date between '".$check_in_date."' and '".$check_out_date."') or (closing_end_date between '".$check_in_date."' and '".$check_out_date."') or (closing_start_date <= '".$check_in_date."' and closing_end_date >= '".$check_in_date."') or (closing_end_date >= '".$check_out_date."' and closing_end_date <= '".$check_out_date."') or ( '".$check_in_date."'  between closing_start_date and closing_end_date  ) or ( '".$check_out_date."'  between closing_start_date and closing_end_date ))");

if(count($result_availability)>0 || ($result_closing<=0  && $result_closing != '')){
 	$no_of_rooms = get_post_meta($result_availability[0]->property_id,'no_of_rooms',true);
	$booked_no_of_rooms = 0;

	for($i=0;$i<count($result_availability);$i++)
		$booked_no_of_rooms += get_post_meta($result_availability[$i]->booking_post_id,'no_of_rooms',true);
	
	$booked_no_of_rooms +=  $_REQUEST['hidden_no_of_rooms'];

	if($no_of_rooms - $booked_no_of_rooms <0)
	{
		$availability_flag = 1;
	}
}else{
	if($result_closing>0){
		$availability_flag = 1;
	}else{
		$availability_flag = 0;
	}	
}
$seasonal = 0;
if($availability_flag != 1){
	
	$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";

	$minimun_stay = 0;
	$sql_booking_seasonal_calc = $wpdb->get_results(("select seasonal_prices_id,from_date,to_date,prices_per_persons,seasonal_minimum_stay from $booking_seasonal_price_table where status='enable' and set_price_for = ".$property_id." and (( from_date between '".$check_in_date."' and '".$check_out_date."' ) or ( to_date between '".$check_in_date."' and '".$check_out_date."'  ) or ( '".$check_in_date."'  between from_date and to_date  ) or ( '".$check_out_date."'  between from_date and to_date ))"));
	if(count($sql_booking_seasonal_calc)>0){
		
		$count_seasonal_price = 1;
		
		for($l=0;$l<$days_between;$l++)
		{
			$count = 0;
			for($k=0;$k<count($sql_booking_seasonal_calc);$k++)
			{
				if(strtotime($sql_booking_seasonal_calc[$k]->from_date) <= strtotime($check_in_date) && (strtotime($sql_booking_seasonal_calc[$k]->to_date) >= strtotime($check_in_date)))
				{
					// get the maximum minimum stay from seasonal period// 
					if($minimun_stay < $sql_booking_seasonal_calc[$k]->seasonal_minimum_stay)
					{
						$minimun_stay = $sql_booking_seasonal_calc[$k]->seasonal_minimum_stay;
					}
					// SEASONAL calculation including if multiple seasonal date prices found// 
					$count = 1;
					$sql_2="select * from $booking_seasonal_price_table where seasonal_prices_id = ".$sql_booking_seasonal_calc[$k]->seasonal_prices_id."  and (from_date <=$check_in_date and to_date >= $check_in_date OR  '".$check_in_date."'  between from_date and to_date) limit 1";
					$sql_2_data=$wpdb->get_row($sql_2);
					if($seasonal_id != $sql_booking_seasonal_calc[$k]->seasonal_prices_id)
					{
						$seasonal_id = $sql_booking_seasonal_calc[$k]->seasonal_prices_id;
						$seasonal_cnt = 0;
						$final_price_seasonal_recurrence += $final_price_seasonal;
						$final_price_seasonal_recurrence_month += $final_price_seasonal_recurrence; 
						$final_price_seasonal_recurrence_week += $final_price_seasonal;
						$final_price_seasonal = 0;
					}
					$seasonal_cnt = $seasonal_cnt+1; 
					
					$price_data=unserialize($sql_2_data->prices_per_persons);
					//	Calculation based on days or weeks or months
					if($seasonal_cnt >= '30') {
						if($seasonal_cnt >= '30' && ($price_data['price_per_month'][($guests-1)] != 0 ||$price_data['price_per_month'][($guests-1)] != '')) {
							$total_month = floor($seasonal_cnt/30);
							$final_price_seasonal = $final_price_seasonal_recurrence_month + $price_data['price_per_month'][($guests-1)] * $total_month;
							$days_left = $seasonal_cnt % 30;
							
							if($days_left != '0'){
								if($days_left >= '7'){
									$total_week = floor($days_left/7);
									$final_price_seasonal = $final_price_seasonal + $price_data['price_per_week'][($guests-1)] * $total_week;
									$month_remaining_days = $days_left % 7;
									if($month_remaining_days != '0'){
										$final_price_seasonal = $final_price_seasonal + ($price_data['price_per_day'][($guests-1)] * $month_remaining_days);
									}
								} else {
									$final_price_seasonal = $final_price_seasonal + ($price_data['price_per_day'][($guests-1)] * $days_left);
								}
							}
						} else {
							$final_price_seasonal = $price_data['price_per_day'][($guests-1)] * $seasonal_cnt;
						}
					}
					
					 if($seasonal_cnt >= '7' && $seasonal_cnt < '30'){
						if($seasonal_cnt < '30' ){
							if($seasonal_cnt >= '7'){
								$total_week = floor($seasonal_cnt/7);
								$final_price_seasonal = $final_price_seasonal_recurrence_week + $price_data['price_per_week'][($guests-1)] * $total_week;
								$remain_week = $seasonal_cnt % 7;
								if($remain_week != '0'){
									$final_price_seasonal = $final_price_seasonal + ($price_data['price_per_day'][($guests-1)] * $remain_week);
								}
							}
						} else {
							if( $price_data['price_per_week'][$guests] != 0 ||  $price_data['price_per_week'][($guests-1)] != '') {
								$final_price_seasonal +=  $price_data['price_per_month'][($guests-1)];
							} else {
								$final_price_seasonal +=  $price_data['price_per_day'][($guests-1)];
							}
						}
						
					}	
										
					elseif($seasonal_cnt < '7'){
							$final_price_seasonal += $final_price_seasonal_recurrence + $price_data['price_per_day'][($guests-1)];
							//$final_price_seasonal_recurrence = 0;
						}
				}
				//if($seasonal_cnt ==2)
					$final_price_seasonal_recurrence = 0;
			}
			//$seasonal_cnt = 0;/*clear seasonal count.*/
			for($k=0;$k<count($sql_booking_seasonal_calc);$k++)
			{
				if(strtotime($sql_booking_seasonal_calc[$k]->from_date) > strtotime($check_in_date) && (strtotime($sql_booking_seasonal_calc[$k]->to_date) < strtotime($check_in_date)) || $count == 0 )
				{
					if($a == $check_in_date)
					{
						continue;
					}
					else
					{
					// NORMAL  normal price calculation if seasonal and normal both date found then normal price calculation//
						$a = $check_in_date;
						$seasonal_cnt = 0;
						$normal_seasonal_cnt += 1;
						$final_price_day_normal = $templatic_settings['templatic_booking_system_settings']['templatic_booking_manage_prices_settings']["price_for_".$property_id]["price_guests_".$guests]["price_per_day"];
						$final_price_week_normal = $templatic_settings['templatic_booking_system_settings']['templatic_booking_manage_prices_settings']["price_for_".$property_id]["price_guests_$guests"]["price_per_week"];
						$final_price_month_normal = $templatic_settings['templatic_booking_system_settings']['templatic_booking_manage_prices_settings']["price_for_".$property_id]["price_guests_$guests"]["price_per_month"];
						
						//	Calculation based on days or weeks or months
						if($normal_seasonal_cnt >= '30') 
						{
							if($normal_seasonal_cnt >= '30' && ($final_price_month_normal != 0 || $final_price_month_normal != '')) 
							{
								$total_month = floor($normal_seasonal_cnt/30);
								$final_price_normal = $final_price_month_normal * $total_month;
								$days_left = $normal_seasonal_cnt % 30;
								
								if($days_left != '0')
								{
									if($days_left >= '7')
									{
										$total_week = floor($days_left/7);
										$final_price_normal = $final_price_normal + $final_price_week_normal * $total_week;
										$month_remaining_days = $days_left % 7;
										
										if($month_remaining_days != '0')
										{
											$final_price_normal = $final_price_normal + ($final_price_day_normal * $month_remaining_days);
										}
									} 
									else
									{
										$final_price_normal = $final_price_normal + ($final_price_day_normal * $days_left);
									}
								}
							}
							else
							{
								$final_price_normal = $final_price_day_normal * $days_left;
							}
						}
						
						if($normal_seasonal_cnt >= '7' && $normal_seasonal_cnt < '30')
						{
							if($normal_seasonal_cnt < '30' )
							{
								if($normal_seasonal_cnt >= '7')
								{
									$total_week = floor($normal_seasonal_cnt/7);
									$final_price_normal = $final_price_week_normal * $total_week;
									$remain_week = $normal_seasonal_cnt % 7;
									if($remain_week != '0')
									{
										$final_price_normal = $final_price_normal + ($final_price_day_normal * $remain_week);
									}
								}
							}
							else 
							{
								if( $final_price_month_normal != 0 ||  $final_price_month_normal != '') 
								{
									$final_price_normal +=  $final_price_month_normal;
								} 
								else
								{
									$final_price_normal +=  $final_price_day_normal;
								}
							}
						}	
						elseif($normal_seasonal_cnt < '7')
						{
							$final_price_normal += $final_price_day_normal;
						}
					}
				}
			}
			$newdate = strtotime ( '+1 day' , strtotime ( $check_in_date ) ) ;
			$check_in_date = date ( 'Y-m-d' , $newdate );
		}
		if($count_seasonal_price==1){
			$final_price_seasonal = $final_price_seasonal + $final_price_normal;	
		}
		//set the minimum stay to zero if its less than booking days.
		if($minimun_stay <= $days_between)
		{
			$minimun_stay = 0;
		}
		$seasonal = 1;
	}else{
		$price_per_day = $templatic_settings['templatic_booking_system_settings']['templatic_booking_manage_prices_settings']["price_for_$property_id"]["price_guests_$guests"]["price_per_day"];
		$price_per_week = $templatic_settings['templatic_booking_system_settings']['templatic_booking_manage_prices_settings']["price_for_$property_id"]["price_guests_$guests"]["price_per_week"];
		$price_per_month = $templatic_settings['templatic_booking_system_settings']['templatic_booking_manage_prices_settings']["price_for_$property_id"]["price_guests_$guests"]["price_per_month"];
		

		//Price calculation for person/day or person/week or person/month
		if($days_between >= '30') {
			if($days_between >= '30' && ($price_per_month != 0 || $price_per_month != '')) {
				$total_month = floor($days_between/30);
				$final_price_seasonal = $price_per_month * $total_month;
				$days_left = $days_between % 30;
				
				if($days_left != '0'){
					if($days_left >= '7'){
						$total_week = floor($days_left/7);
						$final_price_seasonal = $final_price_seasonal + ($price_per_week * $total_week);
						$month_remaining_days = $days_left % 7;
						if($month_remaining_days != '0'){
							$final_price_seasonal = $final_price_seasonal + ($price_per_day * $month_remaining_days);
						}
					} else {
						$final_price_seasonal = $final_price_seasonal + ($price_per_day * $days_left);
					}
				}
			} else {
				$final_price_seasonal = $price_per_day * $days_between;
			}
		}elseif($days_between >= '7' && $days_between < '30'){
			if($days_between < '30' ){
				if($days_between >= '7'){
					$total_week = floor($days_between/7);
					$final_price_seasonal = $price_per_week * $total_week;
					$remain_week = $days_between % 7;
					if($remain_week != '0'){
						$final_price_seasonal = $final_price_seasonal + ($price_per_day * $remain_week);
					}
				}
			}
		}elseif($days_between < '7'){
			$final_price_seasonal = $price_per_day * $days_between;
		}
	}
	
	
	if(isset($_REQUEST['hidden_no_of_rooms']) && $_REQUEST['hidden_no_of_rooms']!=""){
		$number_of_rooms = ($_REQUEST['hidden_no_of_rooms']) ? $_REQUEST['hidden_no_of_rooms'] : 1;
		$no_of_rooms_final_amount = $final_price_seasonal * $number_of_rooms;
	}else{
		$no_of_rooms_final_amount = $final_price_seasonal;
	}
	$sub_pric = $no_of_rooms_final_amount;
	//Price calculation for services
	if(isset($_REQUEST['booking_service_prices']) && $_REQUEST['booking_service_prices']!=""){
		$service_price = explode(",",$_REQUEST['booking_service_prices']);
		$cnt_services = count($service_price);
		$price = 0;
		$number_of_rooms = ($_REQUEST['hidden_no_of_rooms']) ? $_REQUEST['hidden_no_of_rooms'] : 1;
		for($j=0;$j<$cnt_services;$j++){ 
			if(get_option('service_type') == 'once' )
			{
				$price +=($number_of_rooms * $service_price[$j]);
			}
			else
			{
				$price +=($days_between * $number_of_rooms * $service_price[$j]);
			}
		}
		$final_amount = $no_of_rooms_final_amount + $price;
	}else{
		$final_amount = $no_of_rooms_final_amount;
	}

	//Price calculation for coupon code
	//$final_amount = get_payable_amount_with_coupon_plugin($final_amount,$coupon_code_text);
	global $wpdb;
	if($coupon_code_text!='' && $final_amount>0){
		$couponsql = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type='coupon_code'", $coupon_code_text ));
		$couponinfo = $couponsql;
		$discount_amt=0;
		$discount_type = get_post_meta($couponinfo,'coupondisc',true);
		$discount_amount = get_post_meta($couponinfo,'couponamt',true);
		$start_date = strtotime(get_post_meta($couponinfo,'startdate',true));
		$end_date 	= strtotime(get_post_meta($couponinfo,'enddate',true));
		$todays_date = strtotime(date("Y-m-d"));
		if ($end_date >= $todays_date){
			if($couponinfo){
				if($discount_type=='per'){
					$discount_amt = ($final_amount*$discount_amount)/100;
				}elseif($discount_type=='amt'){
					$discount_amt = $discount_amount;
				}
			}
		}
		if($discount_amt>0){
			$final_amount = $final_amount - $discount_amt;
		}else{
			$final_amount = $final_amount;
		}
	}else{
		$final_amount = $final_amount;
	}
}else{
	$final_amount = '';
}
echo $final_amount.'-'.$sub_pric.'-'.$minimun_stay.'-'.$seasonal;
?>
