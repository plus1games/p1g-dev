<?php 
$file = dirname(__FILE__);
$file = substr($file,0,stripos($file, "wp-content"));
require($file . "/wp-load.php");

$my_post_type = get_post_type($_REQUEST['property_id']);
$property_id = $_REQUEST['property_id'];
$check_in_date = $_REQUEST['checkindate'];
$check_out_date = $_REQUEST['checkoutdate'];
$time_check_in_date = strtotime($_REQUEST['checkindate']);
$time_check_out_date = strtotime($_REQUEST['checkoutdate']);
$days_between = ceil(abs($time_check_out_date - $time_check_in_date) / 86400);

$min_stay = get_post_meta($property_id,'minimum_stay',true);

$result = '';
if($min_stay > 0 && $min_stay != '' && $my_post_type == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE && $min_stay > $days_between && $days_between != '')
{
	$result = sprintf(__('You need to book for minimum %s nights.',BKG_DOMAIN),$min_stay);
}
echo $result;exit;

?>