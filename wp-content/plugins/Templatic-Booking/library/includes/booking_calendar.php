<?php
	global $post,$wp_query,$booking_info;
	?>	
	<h1><?php the_title();?></h1>
	<p><?php //echo get_the_content($post->ID); ?></p>
    <div class="post-content">
		<div class="templatic_settings">
			<div>
				
				<form name="house_room" id="house_room" action="" method="post" >
				<table>
				<tr>
					<th style="width:150px;text-align:center;vertical-align: middle;">
						<span style="font-size:16px;"><?php _e("Please Select",BKG_DOMAIN); ?></span>
					</th>
				<td>
						<?php
							$prepare_label_house = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_HOUSE);
							$label_house = $prepare_label_house->labels->name;
							$prepare_label_hotel = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_ROOM);
							$label_hotel = $prepare_label_hotel->labels->name;
							$set_price_for = @$_REQUEST['set_room_house'];
						?>
						<select name="set_room_house" onchange="this.form.submit()" id="set_room_house">
							<option value=""><?php _e("Please select",BKG_DOMAIN);?></option>
							
								<?php 
									global $wpdb,$wp_query;
									$args_house = array(
														'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
														'posts_per_page' => -1,
														'post_status' => 'publish',
														);
									$posts_houses = new WP_Query($args_house);
									if(count($posts_houses->posts)>0){
									?>
									<optgroup label="<?php echo $label_house;?>">
									<?php
										while($posts_houses->have_posts()){
											$posts_houses->the_post();
											global $post;
											if($set_price_for == $post->ID)
												$selected = "selected=selected";
											else
												$selected = '';
											echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
										}wp_reset_query();
									}
								?>	
							</optgroup>
							
								<?php 
									global $wpdb,$wp_query;
									$args_hotel = array(
														'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
														'posts_per_page' => -1,
														'post_status' => 'publish',
														);
									$posts_hotel = new WP_Query($args_hotel);
									if(count($posts_hotel->posts) > 0){
									?>
										<optgroup label="<?php _e($label_hotel.'s',BKG_DOMAIN);?>">
									<?php
										while($posts_hotel->have_posts()){
											$posts_hotel->the_post();
											global $post;
											if($set_price_for == $post->ID)
												$selected = "selected=selected";
											else
												$selected = '';
											echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
										}wp_reset_query();
									}
								?>
							</optgroup>
						</select>
					</td>
					</tr>
					</table>
					</form>
					
					<?php get_admin_event_calendar(@$_REQUEST['set_room_house']);?>

				<div class="display_availability"><span class="allow_booking"></span><b> <?php _e("Available",BKG_DOMAIN);?></b></div>
				<div class="display_availability"><span class="book_not_available"></span><b> <?php _e("Not Available",BKG_DOMAIN);?></b></div>
				<div class="display_availability"><span class="book_closed"></span><b> <?php _e("Booking Closed",BKG_DOMAIN);?></b></div>
				<div class="display_availability"><span class="rooms_left"></span><b> <?php _e("Number of rooms/house left",BKG_DOMAIN);?></b></div>
			</div>
		</div>
	</div>	
