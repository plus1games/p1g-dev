<?php
global $wpdb,$booking_seasonal_price_table;
$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";
$templatic_settings = get_option( "templatic_settings" );
$sql_custom_max_field = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_capacity' and $wpdb->posts.post_type = 'custom_fields'");
$GetPostMeta = get_post_meta($sql_custom_max_field,'option_values',true);
$max_adults_allowed = explode(",",$GetPostMeta);
$edit_id = ($_REQUEST['edit_id']) ? $_REQUEST['edit_id'] : '';
if($edit_id){
	$sql_default_value = $wpdb->get_row(("select seasonal_title,from_date,to_date,prices_per_persons,seasonal_minimum_stay,status,set_price_for from $booking_seasonal_price_table where seasonal_prices_id=".$edit_id));

	if(!empty($sql_default_value)){
		$default_from_date = date_i18n(get_option('date_format'), strtotime($sql_default_value->from_date));
		$default_to_date = date_i18n(get_option('date_format'), strtotime($sql_default_value->to_date));
		$default_status = $sql_default_value->status;
		$default_prices_per_persons = unserialize($sql_default_value->prices_per_persons);
		$set_price_for = $sql_default_value->set_price_for;
		$seasonal_title = $sql_default_value->seasonal_title;
		$seasonal_minimum_stay = $sql_default_value->seasonal_minimum_stay;
		$post_type_min_stay = get_post_type($set_price_for);;
	}
}
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#add_seasonal_price").submit(function(){
			var fromdate = jQuery("#booking_frmdate").val();
			var from_post_date = new Date(fromdate);
			var from_post_time = from_post_date.getTime();
			
			var from_current_date = new Date();
			var from_current_time = from_current_date.getTime();
			var todate = jQuery("#booking_todate").val();
			var to_post_date = new Date(todate);
			var to_post_time = to_post_date.getTime();
			var to_current_date = new Date();
			var to_current_time = to_current_date.getTime();
			var frm_time = from_post_date.getTime();
			var to_time = to_post_date.getTime();
			
			
			if(fromdate==""){
				jQuery("#seasonal_error").html("<?php _e('Please select From Date',BKG_DOMAIN);?>");
				jQuery("#booking_frmdate").bind('change',function(){
					jQuery("#seasonal_error").html("");
				});
				return false;
			}else if(todate==""){
				jQuery("#seasonal_error").html("<?php _e('Please select To Date',BKG_DOMAIN);?>");
				jQuery("#booking_frmdate").bind('change',function(){
					jQuery("#seasonal_error").html("");
				});
				return false;
			}else if(from_post_time > to_post_time){
				jQuery("#seasonal_error").html("<?php _e('To date is invalid. Please select valid date.',BKG_DOMAIN);?>");
				jQuery("#booking_frmdate").bind('change',function(){
					jQuery("#seasonal_error").html("");
				});
				return false;
			}else{
				return true;
			}
		});
	});
</script>
<div class="templatic_settings">
	<form class="form_style" action="" method="post" name="add_seasonal_price" id="add_seasonal_price">
		<p class="description"><?php _e("Here you can add a new seasonal price.",BKG_DOMAIN);?></p>
		<table style="width:100%">
			<tbody>
				<tr>
					<td colspan="2">
						<h3><?php _e("Add New Seasonal Price",BKG_DOMAIN);?>
							<a title="<?php _e("Back to Manage Seasonal Prices",BKG_DOMAIN);?>" class="add-new-h2" name="btnviewlisting" href="<?php echo admin_url().'admin.php?page=templatic_booking_system&tab=manage_prices';?>" id="add_edit_seasonal_prices">
								<?php _e("Back to Manage Seasonal Prices",BKG_DOMAIN);?>
							</a>
						</h3>
					</td>
				</tr>
				<tr>
					<th style="width:100px">
						<?php _e("Seasonal Title",BKG_DOMAIN); ?>
					</th>
					<td>
						<input type="text" name="seasonal_title" id="seasonal_title" value="<?php echo $seasonal_title; ?>"/>
					</td>
				</tr>
				<tr>
					<th style="width:120px"><label for="booking_todate"><?php _e("Seasonal Date",BKG_DOMAIN);?></label></th>
					<td>
						<div class="main_date_div">
							<div class="from_div">
								<input name="booking_frmdate" id="booking_frmdate" type="text" PLACEHOLDER="<?php _e('From Date',BKG_DOMAIN); ?>" class="clearfix" value="<?php echo ($default_from_date) ? $default_from_date : ''?>" />
							</div>
							<div class="to_div" for="booking_frmdate" >
								<input name="booking_todate" id="booking_todate" type="text" PLACEHOLDER="<?php _e('To Date',BKG_DOMAIN); ?>" class="clearfix" value="<?php echo ($default_to_date) ? $default_to_date : ''?>" />
							</div>
						</div>
						<script type="text/javascript">				
							var $date = jQuery.noConflict();
							$date(function(){
							var pickerOpts = {
								showOn: "both",
								buttonImage: "<?php echo TEMPL_PLUGIN_URL; ?>css/datepicker/images/cal.png",
								buttonText: "Show Datepicker",
							};	
							$date("#booking_todate").datepicker(pickerOpts);
							$date("#booking_frmdate").datepicker(pickerOpts);
						});
						</script>
						<label for="ilc_tag_class">
							<p class="description" style="clear:both"><?php _e("Seasonal price will be applied between the dates selected in this calendar",BKG_DOMAIN);?></p>
						</label>
						<span id="seasonal_error"></span>
					</td>
				</tr>
				<tr>
					<th>
						<?php _e("Select House/Room",BKG_DOMAIN); ?>
					</th>
					<td>
						<?php
							$prepare_label_house = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_HOUSE);
							$label_house = $prepare_label_house->labels->name;
							$prepare_label_hotel = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_ROOM);
							$label_hotel = $prepare_label_hotel->labels->name;
						?>
						<select name="set_price_for" onchange="return check_post_type(this.value);" id="set_price_for">
							<option value=""><?php _e("Please select",BKG_DOMAIN);?></option>
							<optgroup label="<?php echo $label_house;?>">
								<?php 
									global $wpdb,$wp_query;
									$args_house = array(
														'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
														'posts_per_page' => -1,
														'post_status' => 'publish',
														);
									$posts_houses = new WP_Query($args_house);
									if($posts_houses){
										while($posts_houses->have_posts()){
											$posts_houses->the_post();
											global $post;
											if($set_price_for == $post->ID)
												$selected = "selected=selected";
											else
												$selected = '';
											echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
										}wp_reset_query();
									}
								?>	
							</optgroup>
							<optgroup label="<?php echo $label_hotel;?>">
								<?php 
									global $wpdb,$wp_query;
									$args_hotel = array(
														'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
														'posts_per_page' => -1,
														'post_status' => 'publish',
														);
									$posts_hotel = new WP_Query($args_hotel);
									if($posts_hotel){
										while($posts_hotel->have_posts()){
											$posts_hotel->the_post();
											global $post;
											if($set_price_for == $post->ID)
												$selected = "selected=selected";
											else
												$selected = '';
											echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
										}wp_reset_query();
									}
								?>
							</optgroup>
						</select>
					</td>
				</tr>
				<tr id="tr_seasonal_minimum_stay" <?php if(@$post_type_min_stay == 'room'){?>style="display:none;"<?php } ?>>
					<th>
						<?php _e("Minimum Stay",BKG_DOMAIN); ?>
					</th>
					<td>
						<input type="text" name="seasonal_minimum_stay" id="seasonal_minimum_stay" value="<?php echo $seasonal_minimum_stay; ?>"/>
						<p class="description" style="clear:both"><?php _e('Mention the minimum number of days you want your guests to book.',BKG_DOMAIN); ?></p>
					</td>
				</tr>
				<tr>
					<th><?php _e("Status",BKG_DOMAIN);?></th>
					<td>
						<input type="radio" name="seasonal_status" id="seasonal_status_1" value="enable" checked="checked" <?php echo (strtolower($default_status) == strtolower("enable")) ? ' checked="checked"' : '';?>/>
						<label for="seasonal_status_1"><?php _e("Enable",BKG_DOMAIN);?></label>
						<input type="radio" name="seasonal_status" id="seasonal_status_2" value="disable" <?php echo (strtolower($default_status) == strtolower("disable")) ? ' checked="checked"' : '';?>/>
						<label for="seasonal_status_2"><?php _e("Disable",BKG_DOMAIN);?></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table style="width:35%" class="widefat post">
							<thead>
								<tr>
									<th><label for="no_of_persons" class="form-textfield-label"><?php _e("No. of persons",BKG_DOMAIN);?></label></th>
									<th><label for="price_day" class="form-textfield-label"><?php _e("Price/Day",BKG_DOMAIN);?></label></th>
									<th><label for="price_week" class="form-textfield-label"><?php _e("Price/Week",BKG_DOMAIN);?></label></th>
									<th><label for="price_month" class="form-textfield-label"><?php _e("Price/Month",BKG_DOMAIN);?></label></th>
								</tr>
							</thead>
							<tbody>
								<?php
									if(count($max_adults_allowed)>0){
										$cnt = 1;
										for($i=0;$i<count($max_adults_allowed);$i++){
											$price_per_day_seasonal = $default_prices_per_persons["price_per_day"][$i];
											$price_per_week_seasonal = $default_prices_per_persons["price_per_week"][$i];
											$price_per_month_seasonal = $default_prices_per_persons["price_per_month"][$i];
								?>
											<tr>
												<td><label class="form-textfield-label"><?php echo $cnt;?></label></td>
												<td><input type="text" style="height:40px;width:80px;" name="price_day_seasonal_<?php echo $cnt;?>" id="price_day_seasonal_<?php echo $cnt;?>" value="<?php echo $price_per_day_seasonal;?>"/></td>
												<td><input type="text" style="height:40px;width:80px;" name="price_week_seasonal_<?php echo $cnt;?>" id="price_week_seasonal_<?php echo $cnt;?>" value="<?php echo $price_per_week_seasonal;?>"/></td>
												<td><input type="text" style="height:40px;width:80px;" name="price_month_seasonal_<?php echo $cnt;?>" id="price_month_seasonal_<?php echo $cnt;?>" value="<?php echo $price_per_month_seasonal;?>"/></td>
											</tr>
								<?php
										$cnt++;}
									}else{
										echo '<tr><td colspan="3"><b>';	
										_e('Please set the Maximum Adults first from General Settings.',BKG_DOMAIN);
										echo '</b></label></td></tr>';
									}
								?>	
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>	
		</table>
		<p class="submit" style="clear: both;">
		  <input type="submit" name="Submit" class="button-primary" value="<?php _e('Save All Settings',BKG_DOMAIN);?>">
		  <input type="hidden" name="seasonal_price_submit" value="y">
		  <input type="hidden" name="edit_seasonal_id" value="<?php echo $edit_id;?>">
		</p>
	</form>
</div>
<?php
/*
 * name : check_post_type
 * description : ajax to hide  mimimum stay textbox for post type house
 * */
add_action('admin_footer','check_post_type');
function check_post_type()
{
		$site_url = get_bloginfo( 'wpurl' )."/wp-admin/admin-ajax.php" ;
	?>
	<script>
		function check_post_type(val)
		{
			var ajaxUrl = "<?php echo esc_js( $site_url); ?>";
			jQuery.ajax({
			url:ajaxUrl,
			type:'POST',
			data:'action=min_stay_room_type&pid=' + val,
			success:function(results) {
					if(results == 'house')
					{
						jQuery('#tr_seasonal_minimum_stay').css('display','');
					}
					else
					{
						jQuery('#tr_seasonal_minimum_stay').css('display','none');
					}
				}
			});
		}
	</script>
<?php
}
?>
