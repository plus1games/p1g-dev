<?php
global $wpdb;
$templatic_settings = get_option( "templatic_settings" );
if(isset($_POST) && @$_POST['booking_form_page']!="")
{
	update_option('booking_system_form',@$_POST['booking_form_page']);	
}
if(isset($_POST) && @$_POST['booking_cancel_page']!="")
{
	update_option('booking_system_cancel',@$_POST['booking_cancel_page']);	
}
if(isset($_POST) && @$_POST['booking_calendar_page']!="")
{
	update_option('booking_system_calendar',@$_POST['booking_calendar_page']);	
}	
?>
<div class="templatic_settings">
	<form class="form_style" action="" method="post">
		<p class="description"><?php _e("These settings will be applied to booking system.",BKG_DOMAIN);?></p>
		<table class="form-table">
			<tbody>
				<tr>
					<td colspan="2"><h3><?php _e("Tax Settings",BKG_DOMAIN);?></h3></td>
				</tr>
				<tr>
					<th><label><?php _e("Tax amount",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="booking_tax" class="input_wrap">
								<input type="text" name="tax_amount" class="input_select" id="tax_amount" value="<?php if(isset($templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_amount'])){echo $templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_amount'];}else{echo 10.00;}?>"/>
							</div>
						</div>
						<label for="ilc_tag_class">
							<p class="description"><?php _e("An additional surcharge on regular payment.",BKG_DOMAIN);?></p>
						</label>
					</td>
				</tr>
				<tr>
					<th><label><?php _e("Tax Type",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="tax_calculation" class="input_wrap">
								<select name="tax_type" class="input_select">
									<option value="percent" <?php if("percent" == $templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_type']){echo "selected=selected";}?>><?php _e("Percent",BKG_DOMAIN);echo '(%)';?></option>
									<option value="exact"<?php if("exact" == $templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_type']){echo "selected=selected";}?>><?php _e("Exact Amount",BKG_DOMAIN);?></option>
								</select>
							</div>
						</div>
						<label for="ilc_tag_class">
							<p class="description"><?php _e("There are two types of surcharge: Exact Amount (direct surcharge) or Percent (depends on the total payment amount). If you choose 'Percent', please do not enter 'Tax amount' more than 100.",BKG_DOMAIN);?></p>
						</label>
					</td>
				</tr>
				<tr>
					<td colspan="2"><h3><?php _e("Deposit Percent",BKG_DOMAIN);?></h3></td>
				</tr>
				<tr>
					<th><label><?php _e("Deposit Percent",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="booking_tax" class="input_wrap">
								<input type="text" name="deposite_percentage" id="deposite_percentage" value="<?php echo get_option('deposite_percentage');?>" />
							</div>
						</div>
						<label for="ilc_tag_class">
							<p class="description"><?php _e("An advance payment before the booking configuration.(Leave blank,if you dont want to charge advance payment)",BKG_DOMAIN);?></p>
						</label>
					</td>
				</tr>
                    <tr>
					<td colspan="2"><h3><?php _e("Page Settings",BKG_DOMAIN);?></h3></td>
				</tr>
                    <tr>
                    	<th><label><?php _e("Booking Form Page",BKG_DOMAIN);?></label></th>
                         <td>                         
                              <?php $pages = get_pages();?>
                              <select id="booking_form_page" name="booking_form_page">
                                   <?php
                                   if($pages) :
                                   $select_page=get_option('booking_system_form');
                                        foreach ( $pages as $page ) {
                                             $selected=($select_page==$page->ID)?'selected="selected"':'';
                                             $option = '<option value="' . $page->ID . '" ' . $selected . '>';
                                             $option .= $page->post_title;
                                             $option .= '</option>';
                                             echo $option;
                                        }
                                   else :
                                        echo '<option>' . __('No pages found', 'edd') . '</option>';
                                   endif;
                                   ?>
                              </select>
                         </td>
                    </tr>
                    <tr>
                    	<th><label><?php _e("Booking Cancel Page",BKG_DOMAIN);?></label></th>
                         <td>                         
                              <?php $pages = get_pages();?>
                              <select id="booking_cancel_page" name="booking_cancel_page">
                                   <?php
                                   if($pages) :
                                   $select_page=get_option('booking_system_cancel');
                                        foreach ( $pages as $page ) {
                                             $selected=($select_page==$page->ID)?'selected="selected"':'';
                                             $option = '<option value="' . $page->ID . '" ' . $selected . '>';
                                             $option .= $page->post_title;
                                             $option .= '</option>';
                                             echo $option;
                                        }
                                   else :
                                        echo '<option>' . __('No pages found', BKG_DOMAIN) . '</option>';
                                   endif;
                                   ?>
                              </select>
                         </td>
                    </tr>
                    <tr>
                    	<th><label><?php _e("Booking Calendar page",BKG_DOMAIN);?></label></th>
                         <td>                         
                              <?php $pages = get_pages();?>
                              <select id="booking_calendar_page" name="booking_calendar_page">
                                   <?php
                                   if($pages) :
                                   $select_page=get_option('booking_system_calendar');
                                        foreach ( $pages as $page ) {
                                             $selected=($select_page==$page->ID)?'selected="selected"':'';
                                             $option = '<option value="' . $page->ID . '" ' . $selected . '>';
                                             $option .= $page->post_title;
                                             $option .= '</option>';
                                             echo $option;
                                        }
                                   else :
                                        echo '<option>' . __('No pages found', BKG_DOMAIN) . '</option>';
                                   endif;
                                   ?>
                              </select>
                         </td>
                    </tr>
					 <tr>
						<td colspan="2"><h3><?php _e("Service Settings",BKG_DOMAIN);?></h3></td>
					</tr>
                    <tr>
                    	<th><label><?php _e("Charge Services",BKG_DOMAIN);?></label></th>
                         <td> 
							<div class="element">
								<label for="daily_serivce_type">
									<input type="radio" name="service_type" <?php if((get_option('service_type') == 'daily' ) || get_option('service_type') == ''){?>checked="checked" <?php } ?> id="daily_serivce_type" value="daily"><?php _e("Daily",BKG_DOMAIN);?>
								 </label>
								 <label for="once_serivce_type">
									<input type="radio" <?php if( get_option('service_type') == 'once' ){?>checked="checked" <?php } ?> name="service_type" id="once_serivce_type" value="once"><?php _e("Once",BKG_DOMAIN);?>
								 </label>
							 </div>
							 <label>
								<p class="description"><?php _e("Services should be charged daily or once.",BKG_DOMAIN);?></p>
							</label>
                         </td>
                    </tr>
                    <!-- added an option to show booking calendar on detail page -->
                     <tr>
                    	<th><label><?php _e("Show Booking Calendar On Detail Page",BKG_DOMAIN);?></label></th>
                         <td> 
							<div class="element">
								<label for="detail_show_cal">
									<input type="checkbox" name="detail_show_cal" <?php if(get_option('detail_show_cal')){?>checked="checked" <?php } ?> id="detail_show_cal" ><?php _e("Enable",BKG_DOMAIN);?>
								 </label>
							 </div>
                         </td>
                    </tr>
			</tbody>	
		</table>
		<p class="submit" style="clear: both;">
		  <input type="submit" name="Submit" class="button-primary" value="<?php _e('Save All Settings',BKG_DOMAIN);?>">
		  <input type="hidden" name="general_submit" value="y">
		</p>
	</form>
</div>
