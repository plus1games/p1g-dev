<?php
function booking_system_tariff($atts)
{
	ob_start();	
	global $post;
	if(get_option('booking_system_calendar')=='' || get_option('booking_system_calendar')!=$post->ID)
		update_option('booking_system_calendar',$post->ID);
	?>
      <div class="post-content">
		<div class="templatic_settings">
               <form name="house_room" id="house_room" action="" method="post" >
                    <table>
                         <tr>
                              <th style="width:150px;text-align:center;vertical-align: middle;">
                              	<span style="font-size:16px;"><?php _e("Please Select",BKG_DOMAIN); ?></span>
                              </th>
                              <td>
                              <?php
							$prepare_label_house = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_HOUSE);
							$label_house = $prepare_label_house->labels->name;
							$prepare_label_hotel = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_ROOM);
							$label_hotel = $prepare_label_hotel->labels->name;
							$set_price_for = $_REQUEST['set_room_house'];
							if(function_exists('icl_register_string')){
							$context = BKG_DOMAIN;
							
							$label_house = __('House',BKG_DOMAIN);
							
							$label_hotel = __('Room',BKG_DOMAIN);
							}	
							?>
							<select name="set_room_house" onchange="this.form.submit()" id="set_room_house">
                                        <option value=""><?php _e("Please select",BKG_DOMAIN);?></option>
                                        
									<?php 
                                             global $wpdb,$wp_query;
                                             $args_house = array(
                                             'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
                                             'posts_per_page' => -1,
                                             'post_status' => 'publish',
                                             );
                                             $posts_houses = new WP_Query($args_house);
                                             if(count($posts_houses->posts)>0){
                                             ?>
								<optgroup label="<?php echo sprintf(__('%ss',BKG_DOMAIN),$label_house); ?>">
                                             <?php
                                             while($posts_houses->have_posts()){
                                             $posts_houses->the_post();
                                             global $post;
                                             if($set_price_for == $post->ID)
                                             $selected = "selected=selected";
                                             else
                                             $selected = '';
                                             echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
                                             }wp_reset_query();
                                             }
                                             ?>	
                                        </optgroup>                                        
                                        <?php 
                                        global $wpdb,$wp_query;
                                        $args_hotel = array(
                                        'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
                                        'posts_per_page' => -1,
                                        'post_status' => 'publish',
                                        );
                                        $posts_hotel = new WP_Query($args_hotel);
                                        if(count($posts_hotel->posts) > 0){
                                        ?>
                                        <optgroup label="<?php echo sprintf(__('%ss',BKG_DOMAIN),$label_hotel); ?>">
									<?php
                                             while($posts_hotel->have_posts()){
                                             $posts_hotel->the_post();
                                             global $post;
                                             if($set_price_for == $post->ID)
                                             $selected = "selected=selected";
                                             else
                                             $selected = '';
                                             echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
                                             }wp_reset_query();
                                             }
                                             ?>
                                        </optgroup>
							</select>
                              </td>
                         </tr>
                    </table>
               </form>
               <?php if(isset($_REQUEST['set_room_house']) && $_REQUEST['set_room_house']!= '') {get_my_booking_prices();}?>
              
		</div>
	</div>
     
     <?php
	return ob_get_clean();

}
function booking_system_calendar($atts){	
	ob_start();	
	global $post;
	if(get_option('booking_system_calendar')=='' || get_option('booking_system_calendar')!=$post->ID)
		update_option('booking_system_calendar',$post->ID);
	?>
      <div class="post-content">
		<div class="templatic_settings">
               <form name="house_room" id="house_room" action="" method="post" >
                    <table>
                         <tr>
                              <th style="width:150px;text-align:center;vertical-align: middle;">
                              	<span style="font-size:16px;"><?php _e("Please Select",BKG_DOMAIN); ?></span>
                              </th>
                              <td>
                              <?php
							$prepare_label_house = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_HOUSE);
							$label_house = $prepare_label_house->labels->name;
							$prepare_label_hotel = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_ROOM);
							$label_hotel = $prepare_label_hotel->labels->name;
							$set_price_for = $_REQUEST['set_room_house'];
							if(function_exists('icl_register_string')){
							$context = BKG_DOMAIN;
							
							
							$label_house = __('House',BKG_DOMAIN);
							
							$label_hotel = __('Room',BKG_DOMAIN);
							}	
							?>
							<select name="set_room_house" onchange="this.form.submit()" id="set_room_house">
                                        <option value=""><?php _e("Please select",BKG_DOMAIN);?></option>
                                        
									<?php 
                                             global $wpdb,$wp_query;
                                             $args_house = array(
                                             'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
                                             'posts_per_page' => -1,
                                             'post_status' => 'publish',
                                             );
                                             $posts_houses = new WP_Query($args_house);
                                             if(count($posts_houses->posts)>0){
                                             ?>
								<optgroup label="<?php echo sprintf(__('%ss',BKG_DOMAIN),$label_house); ?>">
                                             <?php
                                             while($posts_houses->have_posts()){
                                             $posts_houses->the_post();
                                             global $post;
                                             if($set_price_for == $post->ID)
                                             $selected = "selected=selected";
                                             else
                                             $selected = '';
                                             echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
                                             }wp_reset_query();
                                             }
                                             ?>	
                                        </optgroup>                                        
                                        <?php 
                                        global $wpdb,$wp_query;
                                        $args_hotel = array(
                                        'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
                                        'posts_per_page' => -1,
                                        'post_status' => 'publish',
                                        );
                                        $posts_hotel = new WP_Query($args_hotel);
                                        if(count($posts_hotel->posts) > 0){
                                        ?>
                                        <optgroup label="<?php echo sprintf(__('%ss',BKG_DOMAIN),$label_hotel); ?>">
									<?php
                                             while($posts_hotel->have_posts()){
                                             $posts_hotel->the_post();
                                             global $post;
                                             if($set_price_for == $post->ID)
                                             $selected = "selected=selected";
                                             else
                                             $selected = '';
                                             echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
                                             }wp_reset_query();
                                             }
                                             ?>
                                        </optgroup>
							</select>
                              </td>
                         </tr>
                    </table>
               </form>
               <?php get_my_event_calendar();?>
               <!-- -->
               <div class="display_availability"><span class="allow_booking"></span><b> <?php _e("Available",BKG_DOMAIN);?></b></div>
               <div class="display_availability"><span class="book_not_available"></span><b> <?php _e("Not Available",BKG_DOMAIN);?></b></div>
               <div class="display_availability"><span class="book_closed"></span><b> <?php _e("Booking Closed",BKG_DOMAIN);?></b></div>
               <div class="display_availability"><span class="rooms_left"></span><b> <?php _e("Number of rooms/house left",BKG_DOMAIN);?></b></div>
			<!-- -->
		</div>
	</div>
     
     <?php
	return ob_get_clean();
}

/*
 * Function Name: get_my_event_calendar
 * Return: display booking calendar
 */
function get_my_event_calendar()
{
	$monthNames = array(__('January',BKG_DOMAIN), __('February',BKG_DOMAIN), __('March',BKG_DOMAIN), __('April',BKG_DOMAIN), __('May',BKG_DOMAIN), __('June',BKG_DOMAIN), __('July',BKG_DOMAIN), __('August',BKG_DOMAIN), __('September',BKG_DOMAIN), __('October',BKG_DOMAIN	), __('November',BKG_DOMAIN), __('December',BKG_DOMAIN));
	
	if (!isset($_REQUEST["mnth"])) $_REQUEST["mnth"] = date("n");
	if (!isset($_REQUEST["yr"])) $_REQUEST["yr"] = date("Y");
	
	$cMonth = $_REQUEST["mnth"];
	$cYear = $_REQUEST["yr"];
	
	$prev_year = $cYear;
	$next_year = $cYear;
	$prev_month = $cMonth-1;
	$next_month = $cMonth+1;
	
	if ($prev_month == 0 ) {
		$prev_month = 12;
		$prev_year = $cYear - 1;
	}
	if ($next_month == 13 ) {
		$next_month = 1;
		$next_year = $cYear + 1;
	}
	$mainlink = get_permalink(get_option('booking_system_calendar'));	
	if(strstr($_SERVER['REQUEST_URI'],'?mnth') && strstr($_SERVER['REQUEST_URI'],'&yr'))
	{
		$replacestr = "?mnth=".$_REQUEST['mnth'].'&yr='.$_REQUEST['yr']."&set_room_house=". $_REQUEST['set_room_house'];
	}elseif(strstr($_SERVER['REQUEST_URI'],'&mnth') && strstr($_SERVER['REQUEST_URI'],'&yr'))
	{
		$replacestr = "&mnth=".$_REQUEST['mnth'].'&yr='.$_REQUEST['yr']."&set_room_house=". $_REQUEST['set_room_house'];		
	}
	
	if(strstr($_SERVER['REQUEST_URI'],'?') && !strstr($_SERVER['REQUEST_URI'],'?mnth'))
	{
		$pre_link = $mainlink."&mnth=". $prev_month . "&yr=" . $prev_year ."&set_room_house=". $_REQUEST['set_room_house'];
		$next_link = $mainlink."&mnth=". $next_month . "&yr=" . $next_year ."&set_room_house=". $_REQUEST['set_room_house'];
	}else
	{
		$pre_link = $mainlink."?mnth=". $prev_month . "&yr=" . $prev_year ."&set_room_house=". $_REQUEST['set_room_house'];	
		$next_link = $mainlink."?mnth=". $next_month . "&yr=" . $next_year ."&set_room_house=". $_REQUEST['set_room_house'];
	}
	
	$id = '';
	global $wpdb;
	$icl_translations = $wpdb->prefix.'icl_translations';
	$post_type = get_post_type($_REQUEST['set_room_house']);
	if(function_exists('icl_register_string') && is_plugin_active('sitepress-multilingual-cms/sitepress.php')){
		global $sitepress;		
		$trid = $sitepress->get_element_trid($_REQUEST['set_room_house'],'post_'.$post_type);		
		$translation = $sitepress->get_element_translations($trid);	
		foreach($translation as $val){
			$id[] = $val->element_id;
		}
	}else{
		$id[] = $_REQUEST['set_room_house'];
	}
	?> 
    
     <div class="calendar_box">
          <table width="100%">
               <tr align="center">
                    <td >
                         <table width="100%">
                              <tr align="center" class="title">
                                   <td width="10%" class="title"> <a href="<?php echo $pre_link; ?>" > <img src="<?php echo TEMPLATIC_BOOKING_URL; ?>images/previous2.png" alt=""  /></a></td>
                                   <td class="title"><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></td>
                                   <td width="10%" class="title" style="border-right:1px solid #fff;"><a href="<?php echo $next_link; ?>" >  <img src="<?php echo TEMPLATIC_BOOKING_URL; ?>images/next2.png" alt=""  /></a> </td>
                              </tr>
                         </table>
                    </td>
			</tr>
               <tr>
                    <td align="center">
                    <?php 
                    if(isset($_REQUEST['set_room_house']) && $_REQUEST['set_room_house']!=""){ ?>
                         <table width="100%" border="0" cellpadding="2" cellspacing="2"  class="calendar_widget">
                              <tr>
                                   <td align="center" class="days"><?php _e('Sun',BKG_DOMAIN); ?></td>
                                   <td align="center" class="days"><?php _e('Mon',BKG_DOMAIN); ?></td>
                                   <td align="center" class="days"><?php _e('Tue',BKG_DOMAIN); ?></td>
                                   <td align="center" class="days"><?php _e('Wed',BKG_DOMAIN); ?></td>
                                   <td align="center" class="days"><?php _e('Thu',BKG_DOMAIN); ?></td>
                                   <td align="center" class="days"><?php _e('Fri',BKG_DOMAIN); ?></td>
                                   <td align="center" class="days"><?php _e('Sat',BKG_DOMAIN); ?></td>
                              </tr> 
                         <?php
                         $timestamp = mktime(0,0,0,$cMonth,1,$cYear);
                         $maxday = date("t",$timestamp);
                         $thismonth = getdate ($timestamp);
                         $startday = $thismonth['wday'];
                         
                         if($_GET['m'])
                         {
						$m = $_GET['m'];	
						$py=substr($m,0,4);
						$pm=substr($m,4,2);
						$pd=substr($m,6,2);
						$monthstdate = "$cYear-$cMonth-01";
						$monthenddate = "$cYear-$cMonth-$maxday";
                         }
                         global $wpdb,$closing_days_table;
                         for ($i=0; $i<($maxday+$startday); $i++) {
                        		if(($i % 7) == 0 ) echo "<tr>\n";
						if($i < $startday){
							echo "<td></td>\n";
						}
						else 
						{
							$cal_date = $i - $startday + 1;
							$calday = $cal_date;							
							if(strlen($cal_date)==1)
							{
								$calday=$cal_date;
							}
							$cMonth1 = $cMonth;
							if(strlen($cMonth)==1)
							{
								$cMonth1=$cMonth;
							}
							$urlddate = "$cYear$cMonth1$calday";
							$thelink = site_url()."/?s=cal_event&m=$urlddate";
							
							$the_cal_date = $cal_date;
							if(strlen($the_cal_date)==1){$the_cal_date = '0'.$the_cal_date;}
							$todaydate = "$cYear-$cMonth1-$the_cal_date";
							global $booking_master,$additional_price_table;
							
							$event_of_month_sql = "select * from $booking_master where check_in_date <= \"$todaydate\" and check_out_date >\"$todaydate\" and booking_status = 'confirmed'";
							
							$args=array(
									'post_type'      => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,	
									'posts_per_page' => -1,									
									'post_status'    => array('publish'),
									'meta_query'     => array(
														'relation' => 'AND',
															array(
																'key'     => 'status',
																'value'   => 'Approved',
																'compare' => '=',
															),
															array(
																'key' 	=> 'property_room_id',
																'value'	=> $id,
																'compare'	=> 'IN'
															),
															array(
																'key' 	=> 'checkin_date',
																'value'	=> $todaydate,
																'compare'	=> '<=',
																'type' => 'DATE'
															),
															array(
																'key' 	=> 'checkout_date',
																'value'	=> $todaydate,
																'compare'	=> '>',
																'type' => 'DATE'
															),
															
														),	
									'order'          => 'ASC'	
								);
							$post_calendar=new WP_Query($args);
							//and wp_9_posts.ID in (select trid from wp_9_icl_translations t where t.element_id = 233 AND t.element_type = 'post_room'  )
							$event_post_info_perday = 0;
							$booked_no_of_rooms = 0;
							$rooms_left = 0;
							if($post_calendar && isset($_REQUEST['set_room_house']) && $_REQUEST['set_room_house'] != '')
							{
								while ($post_calendar->have_posts())
								{
									$post_calendar->the_post();			
									$property_room_id=get_post_meta(get_the_ID(),'property_room_id',true);
									$no_of_rooms=get_post_meta($property_room_id,'no_of_rooms',true);
									$booked_no_of_rooms+=get_post_meta(get_the_ID(),'no_of_rooms',true);
									if($booked_no_of_rooms >= $no_of_rooms)
										$event_post_info_perday = $booked_no_of_rooms;
									else
										$rooms_left = $no_of_rooms - $booked_no_of_rooms;
									
									if(get_post_type($property_room_id)=='house')
									{ 
										$event_post_info_perday=1;
									}									
								}
								wp_reset_query();
							}
							wp_reset_query();							
							
							$caldate1 = mktime(0,0,0,$cYear,$cMonth,$calday);
							echo "<td align='center' valign='middle'>";
							$available_booking = date($cYear."-".$cMonth."-".$calday);
							$curdate = date('Y-m-d');	
							$closeing_date = check_calendar_closing_days($available_booking);						
							if($event_post_info_perday )
							{
								echo "<span class=\"booked\">". ($cal_date) . "</span>";
							}
							else if($closeing_date!=''){
								echo "<span class=\"closed\">". ($cal_date) . "</span>";
							}else if(strtotime($available_booking) <= strtotime($curdate)){
								echo "<span class=\"past\">". ($cal_date) . "</span>";
							}else if(strtotime($available_booking) > strtotime($curdate) && $rooms_left)
							{
								echo "<span class=\"no_event\" >". "<b class=\"room_left\" >".$rooms_left."</b>".($cal_date) . "</span>";
							}
							else if(strtotime($available_booking) > strtotime($curdate) )
							{
								echo "<span class=\"no_event\" >". "<b class=\"room_left\" >".get_post_meta($_REQUEST['set_room_house'],'no_of_rooms',true)."</b>".($cal_date) . "</span>";
							}
							echo "</td>\n";
						}
						
						if(($i % 7) == 6 ) echo "</tr>\n";
                         }
                         ?>
                         </table>
                    <?php
                    }else{
					echo '<table style="border:0;min-height:200px" width="100%" cellpadding="0" cellspacing="0">
					
					<tr><td  style="border:0;text-align:center"><b>';
					_e('Please select room/house first',BKG_DOMAIN);
					echo '</b></td></tr></table>';
                    }
                    ?>
                    </td>
               </tr>
          </table>
     </div>
<?php 
}

/*
 * Function Name: booking_system_form
 * Return: display the booking form fon front end
 */
function booking_system_form($atts)
{
	extract( shortcode_atts( array (
				'post_type'   =>'post',				
				), $atts ) 
			);
	ob_start();	
	remove_filter( 'the_content', 'wpautop' , 12);
	global $post,$current_user;
	if(get_option('booking_system_form')=='' || get_option('booking_system_form')!=$post->ID)
		update_option('booking_system_form',$post->ID);

	$templatic_custom_posts = get_option('templatic_custom_post');
	if((@$_REQUEST['id']) && @$_REQUEST['id']!=""){
		$retrived_post_id = @$_REQUEST['id'];
		$post_type = get_post_type($retrived_post_id);
	}elseif((@$_REQUEST['houses_rooms']) && @$_REQUEST['houses_rooms']!=""){
		$retrived_post_id = $_REQUEST['houses_rooms'];
		$post_type = get_post_type($retrived_post_id);
	}
	//Get post type for which booking form should be use.
	//$post_type = get_post_meta($post->ID,'template_post_type',true);
	$post_type_search = in_array($post_type,array_keys(get_option('templatic_custom_post')));
	if(!$post_type_search && $post_type !='post'){
		$post_type = '';
	}
	/* CONDOTION TO SHOW AN ERROR MSG IF USER'S IP IS BLOCKED */
	$ip = (function_exists('templ_fetch_ip'))?templ_fetch_ip():'';	
	if($ip == "")
	{
		wp_reset_query();
		global $post,$wp_query,$wpdb;		
		$page_id2 = get_option('booking_system_calendar');
		if(function_exists('icl_object_id')){
			$page_id2 = icl_object_id($page_id2, 'page', false,ICL_LANGUAGE_CODE);
		}
		$book_calendar_url = get_permalink($page_id2);		
	?>
		<div class="booking_frm_cont">
			<a class="button view-calendar-btn" href="<?php echo $book_calendar_url;?>" title="<?php _e("Check Availability",BKG_DOMAIN);?>" target="_blank"><?php _e("Check Availability",BKG_DOMAIN);?></a>
	    </div>
	    <?php
			if(isset($_REQUEST['id']) && $_REQUEST['id']!="" && $_REQUEST['id']!=0){
				$post_meta_data = get_post_custom($_REQUEST['id']);
				$data = $post_meta_data['include_tax'][0];
				$is_tax = $data;
			}
			elseif(isset($_REQUEST['houses_rooms']) && $_REQUEST['houses_rooms']!="" && $_REQUEST['houses_rooms']!=0){
				$post_meta_data = get_post_custom($_REQUEST['houses_rooms']);
				$data = $post_meta_data['include_tax'][0];
				echo $is_tax = $data;
			}
			if(isset($_REQUEST['ecptcha']) == 'captch'){
				$a = get_option("recaptcha_options");
				$blank_field = $a['no_response_error'];
				$incorrect_field = $a['incorrect_response_error'];
				echo '<div class="error_msg">'.$incorrect_field.'</div>';
			}
			if(isset($_REQUEST['invalid']) == 'playthru'){
		?> 
			<div class="error_msg"><?php _e("You need to play the game to submit post successfully",BKG_DOMAIN);?>.</div>
		<?php } ?>
		<?php
			global $submit_form_validation_id;
			$submit_form_validation_id = "submit_form";
			echo '<script type="text/javascript" src="'.TEMPLATIC_BOOKING_URL."js/datepicker.js".'"></script>';
			echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/base.css".'" rel="stylesheet">';
			echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/clean.css".'" rel="stylesheet">';
			echo '<link media="all" type="text/css" href="'.TEMPLATIC_BOOKING_URL."css/date_range.css".'" rel="stylesheet">';
			$booking_nonce = wp_create_nonce('booking_submit_nonce');
			$url = home_url().'/?page=paynow';
			if(function_exists('tmpl_get_ssl_normal_url'))
			{
				$url = tmpl_get_ssl_normal_url($url);
			}

		?>
		<form name="submit_form" id="submit_form" class="form_front_style" action="<?php echo $url; ?>" method="post" enctype="multipart/form-data">
		
			<span style="color:red;font-weight:bold;display:block;"><?php 
				if(isset($_REQUEST['paypalerror']) && $_REQUEST['paypalerror']=='yes'){
					echo $_SESSION['paypal_errors'];
				}
				if(isset($_REQUEST['eway_error']) && $_REQUEST['eway_error']=='yes'){
					echo $_SESSION['display_message'];
				}
				if(isset($_REQUEST['stripeerror']) && $_REQUEST['stripeerror']=='yes'){
					echo $_SESSION['stripe_errors'];
				}
				if(isset($_REQUEST['psigateerror']) && $_REQUEST['psigateerror']=='yes'){
					echo $_SESSION['psigate_errors'];
				}
				if(isset($_REQUEST['braintreeerror']) && $_REQUEST['braintreeerror']=='yes'){
					echo $_SESSION['braintree_errors'];
				}
				if(isset($_REQUEST['inspire_commerceerror']) && $_REQUEST['inspire_commerceerror']=='yes'){
					echo $_SESSION['inspire_commerce_errors'];
				}
			?></span>
		
			<?php 
				$request_property_id = "";
				if(@$_REQUEST['id']){
					$request_property_id = @$_REQUEST['id'];
				}elseif(@$_REQUEST['houses_rooms']){
					$request_property_id = @$_REQUEST['houses_rooms'];
				}else{
					$request_property_id = '';
				}
				if($request_property_id==""){?>
					<div>
							<label><?php _e("Select House/Room",BKG_DOMAIN); ?></label>
							<?php
								$prepare_label_house = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_HOUSE);
								$label_house = $prepare_label_house->labels->name.'s';
								$prepare_label_hotel = get_post_type_object(CUSTOM_POST_TYPE_TEMPLATIC_ROOM);
								$label_hotel = $prepare_label_hotel->labels->name.'s';
								
								if(function_exists('icl_register_string')){
									icl_register_string('',$label_house,$label_house);
								}
								
								if(function_exists('icl_t')){
									$label_house1 = icl_t('',$label_house,$label_house);
								}else{
									$label_house1 = sprintf(__('%s',''),$label_house);
								}
								
								if(function_exists('icl_register_string')){
									icl_register_string('',$label_hotel,$label_hotel);
								}
								
								if(function_exists('icl_t')){
									$label_hotel1 = icl_t('',$label_hotel,$label_hotel);
								}else{
									$label_hotel1 = sprintf(__('%s',''),$label_hotel);
								}
	
								
							?>
							<select name="set_price_for" id="set_price_for" onchange="get_custom_field_data(this.value)">
								<option value=""><?php _e("Please select",BKG_DOMAIN);?></option>
								      
									<?php 
                                             global $wpdb,$wp_query;
                                             $args_house = array(
                                             'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
                                             'posts_per_page' => -1,
                                             'post_status' => 'publish',
                                             );
                                             $posts_houses = new WP_Query($args_house);
                                             if(count($posts_houses->posts)>0){
                                             ?>
								<optgroup label="<?php echo $label_house1;?>">
									<?php 
										global $wpdb,$wp_query;
										$args_house = array(
															'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
															'posts_per_page' => -1,
															'post_status' => 'publish',
															);
										$posts_houses = new WP_Query($args_house);
										if($posts_houses){
											while($posts_houses->have_posts()){
												$posts_houses->the_post();
												global $post;
												if($set_price_for == $post->ID)
													$selected = "selected=selected";
												else
													$selected = '';
												echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
											}wp_reset_query();
										}
									
									?>	
								</optgroup>
								<?php } ?>
								 <?php 
                                        global $wpdb,$wp_query;
                                        $args_hotel = array(
                                        'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
                                        'posts_per_page' => -1,
                                        'post_status' => 'publish',
                                        );
                                        $posts_hotel = new WP_Query($args_hotel);
                                        if(count($posts_hotel->posts) > 0){
                                        ?>
								<optgroup label="<?php echo $label_hotel1;?>">
									<?php 
										global $wpdb,$wp_query;
										$args_hotel = array(
															'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
															'posts_per_page' => -1,
															'post_status' => 'publish',
															);
										$posts_hotel = new WP_Query($args_hotel);
										if($posts_hotel){
											while($posts_hotel->have_posts()){
												$posts_hotel->the_post();
												global $post;
												if($set_price_for == $post->ID)
													$selected = "selected=selected";
												else
													$selected = '';
												echo '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';
											}wp_reset_query();
										}
									?>
								</optgroup>
								<?php } ?>
							</select>
						</div>
					
			<?php	
				}
			?>
			<span class="message_error2" id="booking_common_error"></span>
			<input type="hidden" name="booking_page_id" id="booking_page_id" value="<?php echo $post->ID; ?>" />
			<input type="hidden" name="house_seasonal_min_stay" id="house_seasonal_min_stay" value="" />
			<input type="hidden" name="captcha_room_house_booking_id" id="captcha_room_house_booking_id" value="<?php echo @$_REQUEST['id']; ?>" />
		    <input type="hidden" name="property_id" id="property_id" value="<?php echo $request_property_id;?>"/>
		    <input type="hidden" name="coupon_code_text" id="coupon_code_text" value=""/>
		    <input type="hidden" name="hidden_no_of_rooms" id="hidden_no_of_rooms" value="<?php  if(isset($_REQUEST['number_of_rooms'])){echo $_REQUEST['number_of_rooms'];}elseif(isset($_REQUEST['no_of_rooms'])){echo @$_REQUEST['no_of_rooms'];}else{echo 1;} ?>"/>
		    <input type="hidden" name="booking_post_type" id="booking_post_type" value="<?php echo $post_type; ?>"/>
		    <input type="hidden" name="booking_service_ids" id="booking_service_ids" value=""/>
		    <input type="hidden" name="booking_service_prices" id="booking_service_prices" value=""/>
		    <input type="hidden" name="checkindate" id="checkindate" value="<?php if(isset($_REQUEST['check_in_date'])){echo @$_REQUEST['check_in_date'];}?>"/>
		    <input type="hidden" name="checkoutdate" id="checkoutdate" value="<?php if(isset($_REQUEST['check_out_date'])){echo @$_REQUEST['check_out_date'];}?>"/>
		    <input type="hidden" name="total_amount" id="total_amount" value=""/>
		    <input type="hidden" name="submit_booking_nonce" id="submit_booking_nonce" value="<?php echo @$booking_nonce;?>"/>
		    <input type="hidden" name="cur_post_taxonomy" id="cur_post_taxonomy" value="<?php echo @$taxonomy; ?>"  />
		    <input type="hidden" name="cur_post_id" value="<?php echo $post_id; ?>"  />
		    <input type="hidden" name="hidden_cost_subtotal" id="hidden_cost_subtotal" value=""  />
		    <input type="hidden" name="before_disc_amt" id="before_disc_amt" value=""  />
		    <input type="hidden" name="hidden_total" id="hidden_total" value=""  />
			<div class="booking_main">
			<?php if(@$retrived_post_id!=""){
					$property_post_title = get_the_title($retrived_post_id);?>
	
			<span><?php _e("You are Booking for",BKG_DOMAIN);?>: <b><?php echo sprintf(__('%s',BKG_DOMAIN),$property_post_title); ?></b></span>
			<?php } ?>
			<h3><?php _e("Review booking cost",BKG_DOMAIN);?>:</h3>
			<div class="booking_fields">
				<div class="form_row clearfix">
					<label><?php _e('Check-In/Check-Out date',BKG_DOMAIN);?><span class="required">*</span></label>
					<div id="section_calculation"></div>
					<div class="clearfix"></div>
					<div id="date-range">
					  <div id="date-range-field">
						<span></span>
						<a href="#">&#9660;</a>
					  </div>
					  <div id="datepicker-calendar">
						<span class="booking_calendar_link">
							<img id="close_cal1" class="close_cal1" src="<?php echo TEMPLATIC_BOOKING_URL.'images/close_btn.png'?>" alt="Close" title="Close"/>
						</span>
					  </div>
					  <span id="booking_check_in_date_error" class="message_error2"></span>
					</div>	
				</div>
		<?php
			//echo "<pre>";print_r($_REQUEST);echo "</pre>";
			global $post;
			$action = @$_REQUEST['action'];
			wp_reset_query();
			$button_text  = BOOK_NOW_BUTTON_TEXT;
			$templatic_settings = get_option( "templatic_settings" );
			$deposite_percentage = get_option('deposite_percentage');
			$custom_metaboxes = array();
			$heading_type = fetch_heading_per_post_type($post_type);
			if(count($heading_type) > 0){
				foreach($heading_type as $_heading_type){
					$custom_metaboxes[$_heading_type] = get_post_custom_fields_templ_plugin($post_type,'','',$_heading_type);//custom fields for custom post type..
				 }
			}else{
				 $custom_metaboxes[] = get_post_custom_fields_templ_plugin($post_type);//custom fields for custom post type..
			}
			if($templatic_settings['templatic-category_custom_fields'] == 'No'){
				display_custom_post_field_plugin($custom_metaboxes,'custom_fields',$post_type);//displaty custom fields html.
			}
			
			//templ_captcha_integrate('submit'); /* Display recaptcha in submit form */
			$currency = get_option("currency_symbol");
			$position = get_option("currency_pos");
			$tax_amount = $templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_amount'];
			$tax_type = $templatic_settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_type'];
			$services_price = '';
		?>	
					<?php
						$args = array(
							'type'        => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,			
							'orderby'     => 'name',
							'order'       => 'ASC',
							'hide_empty'  => 0,
							'taxonomy'    => CUSTOM_CATEGORY_TEMPLATIC_BOOKING,
							'pad_counts'  => false );
						if(!empty($args)){
							$booking_service_categories = get_categories( $args );
							if($booking_service_categories){
							$cnt = 1;?>
							<div class="form_row clearfix" id="booking_services_div">
									<label>
										<?php 
											$lbl_services = "Services";
											if(function_exists('icl_register_string')){
												icl_register_string(BKG_DOMAIN,$lbl_services,$lbl_services);
											}
											if(function_exists('icl_t')){
												$lbl_services1 = icl_t(BKG_DOMAIN,$lbl_services,$lbl_services);
											}else{
												$lbl_services1 = __($lbl_services,BKG_DOMAIN); 
											}
											echo $lbl_services1;
										?>
									</label>
						<?php	foreach($booking_service_categories as $service):
								//echo "<pre>";print_r($service);
								$price=$service->term_price!=""? $service->term_price:'0';							
								$price_vaue=fetch_currency_with_position($price);
		
								if(in_array($post_type,explode(',',$service->used_for))){
						  ?>
									<label>
										<input type="checkbox" name="booking_services" id="booking_services_<?php echo $cnt;?>" onclick="update_cost_with_services(this.id,this.value)" value="<?php echo $service->term_id."+".$price;?>" /> <?php echo $service->name." (".$price_vaue.")<br/>";?>
									</label>
						 <?php }
							$cnt++;
							endforeach; echo '</div>';}} ?>
			</div>
			<script type="text/javascript">
				var $date_picker = jQuery.noConflict();
				function update_cost_with_services(id,value){
					var services_ids = '';
					var split_services_id = 0;
					var service_prices = '';
					var len=document.getElementsByName('booking_services').length;				
					for (i = 0; i < len; i++){ 
						if (document.getElementsByName('booking_services')[i].checked) {
							var val=document.getElementsByName('booking_services')[i].value;
							split_services_id = document.getElementsByName('booking_services')[i].value.split('+');
							services_ids = services_ids+split_services_id[0]+',';
							service_prices = service_prices+split_services_id[1]+',';
						}
					}
					services_ids=services_ids.substring(0,services_ids.length - 1);
					service_prices=service_prices.substring(0,service_prices.length - 1);
					$date_picker('#booking_service_ids').val(services_ids);
					$date_picker('#booking_service_prices').val(service_prices);
					calculate_cost();
				}
				function get_custom_field_data(room_house_id){
					if(room_house_id!=""){
						$date_picker('#houses_rooms').val(room_house_id);
						document.get_id.submit();
					}
				}
				function booking_information()
				{
					jQuery('#booking_coupong_code').val('');
					jQuery('#booking_coupons_code_chk').attr('checked', false);
					jQuery('#coupon_code_div').hide();
					jQuery('#coupon_code_error').html('');
					var dataString = $date_picker('#submit_form').serialize();
					$date_picker.ajax({
						type: "POST",
						url: "<?php echo TEMPLATIC_BOOKING_URL.'library/includes/ajax_booking_information.php';?>",
						data: dataString,
						success: function(html){
							if(html!='')
								document.getElementById("booking_infomation_div").style.display = '';
							else
								document.getElementById("booking_infomation_div").style.display = 'none';
								document.getElementById("booking_infomation").innerHTML = html;
								if(<?php echo $position;?> == 1){
									total2display = '<?php echo $currency;?>'+jQuery('.bookling_information_currency').html();
								}else if(<?php echo $position;?> == 2){
									total2display = '<?php echo $currency." ";?>'+jQuery('.bookling_information_currency').html();
								}else if(<?php echo $position;?> == 3){
									total2display = jQuery('.bookling_information_currency').html()+'<?php echo $currency;?>';
								}else if(<?php echo $position;?> == 4){
									total2display = jQuery('.bookling_information_currency').html()+'<?php echo " ".$currency;?>';
								}
							jQuery('.bookling_information_currency').html(total2display);
						}
					});
				}
				
				function calculate_cost(){
				   booking_information();
					var dataString = $date_picker('#submit_form').serialize();
					$date_picker.ajax({
						type: "POST",
						url: "<?php echo TEMPLATIC_BOOKING_URL.'library/includes/calculate_cost.php';?>",
						data: dataString,
						success: function(html){
							var prices = html.split("-");
							//validation for minimum stay for seasonal booking.
							if(prices[2] > 0)
							{
								document.getElementById("house_seasonal_min_stay").value = 1;
								var message = '<?php _e('You need to book for minimum ',BKG_DOMAIN); ?>';
								var message_nights = '<?php _e(' nights',BKG_DOMAIN); ?>';
								$date_picker("#booknow").prop('disabled',true);
								$date_picker("#section_calculation").html("<span style='color:red;'>"+message+prices[2]+message_nights+"</span>");
								return true;
							}
							else if(prices[0]!=''){
								if(prices[3] > 0)
									document.getElementById("house_seasonal_min_stay").value = 1;
								else
									document.getElementById("house_seasonal_min_stay").value = 0;
								$date_picker("#section_calculation").html("");
								if(<?php echo $position;?> == 1){
									var html2display = '<?php echo $currency;?>'+prices[0];
								}else if(<?php echo $position;?> == 2){
									var html2display = '<?php echo $currency." ";?>'+prices[0];
								}else if(<?php echo $position;?> == 3){
									var html2display = prices[0]+'<?php echo $currency;?>';
								}else if(<?php echo $position;?> == 4){
									var html2display = prices[0]+'<?php echo " ".$currency;?>';
								}
								$date_picker("#cost_subtotal").text(html2display);
								$date_picker("#hidden_cost_subtotal").val(prices[0]);
								$date_picker("#hidden_total").val(prices[1]);
								var sub_tot=parseFloat(prices[0]);
								var total_costs=0;
								var total2display='';
								<?php if($is_tax){?>
										if('Yes' == '<?php echo $is_tax;?>'){
											var booking_fee=parseFloat(<?php echo $tax_amount;?>);
											if('<?php echo $tax_type;?>'=='exact'){
												total_costs=sub_tot+booking_fee;
												$date_picker('#tax_price').val(booking_fee);
											}else if('<?php echo $tax_type;?>'=='percent'){
												tax_costs=((sub_tot*booking_fee)/100);
												total_costs=sub_tot+tax_costs;
												$date_picker('#tax_price').val(tax_costs);
											}
										}else{
											total_costs = sub_tot;
										}
								<?php }else{?>
											total_costs = sub_tot;
								<?php }?>
								  <?php if($deposite_percentage){?>
									var deposite_percentage = <?php echo $deposite_percentage;?>;
									 total_costs = ((deposite_percentage * total_costs) / 100);
								  <?php } ?>
								  total_costs = parseFloat(total_costs).toFixed(2);
								if(<?php echo $position;?> == 1){
									total2display = '<?php echo $currency;?>'+total_costs;
								}else if(<?php echo $position;?> == 2){
									total2display = '<?php echo $currency." ";?>'+total_costs;
								}else if(<?php echo $position;?> == 3){
									total2display = total_costs+'<?php echo $currency;?>';
								}else if(<?php echo $position;?> == 4){
									total2display = total_costs+'<?php echo " ".$currency;?>';
								}
								$date_picker("#cost_total_cost").text(total2display);
								$date_picker("#total_amount").val(total2display);
								$date_picker("#booknow").prop('disabled',false);
						}
						else{
							$date_picker.ajax({
									type: "POST",
									url: "<?php echo TEMPLATIC_BOOKING_URL.'library/includes/ajax_manage_available_room.php';?>",
									data: dataString,
									success: function(no_room){
												if(no_room == 'allow')
												{
													$date_picker("#booknow").prop('disabled',false);
													$date_picker("#section_calculation").html("");
												}
												else if(no_room > 0)
												{
													$date_picker("#booknow").prop('disabled',true);
													$date_picker("#section_calculation").html("<span style='color:red;'>"+no_room+" <?php _e("room/room's available on this date",BKG_DOMAIN);?>.</span>");
												}
												else if(no_room == 'closed')
												{
													$date_picker("#booknow").prop('disabled',true);
													$date_picker("#section_calculation").html("<span><?php _e("Booking not available on this date.Choose another date",BKG_DOMAIN);?>.</span>");
												}
												else
												{
													$date_picker("#booknow").prop('disabled',true);
													$date_picker("#section_calculation").html("<span><?php _e("Booking not available on this date.Choose another date",BKG_DOMAIN);?>.</span>");
												}
										}
								
							});
						}
						if(document.getElementById('booking_post_type'))
						{
							var booking_type = document.getElementById('booking_post_type').value;
							var add_sesonal_price = document.getElementById("house_seasonal_min_stay").value;
							if(booking_type == 'house' && add_sesonal_price == 0 )
							{
									$date_picker.ajax({
												type: "POST",
												url: "<?php echo TEMPLATIC_BOOKING_URL.'library/includes/house_minimum_stay.php';?>",
												data: dataString,
												success: function(minimum_stay){
															if(minimum_stay !='' )
															{
																$date_picker("#booknow").prop('disabled',true);
																$date_picker("#section_calculation").html("<span style='color:red;'>"+minimum_stay+"</span>");
															}
															else
															{
																$date_picker("#booknow").prop('disabled',false);
															}
													}
								});
							}
						}
					}
				});
			}
			<?php 
				if(isset($_REQUEST['houses_rooms']) && @$_REQUEST['houses_rooms']!=""){?>calculate_cost();<?php } ?>
			$date_picker(document).ready(function() {
				var from = new Date();
				var to = new Date(from.getTime() + 200 * 60 * 60 * 24 * 14);
				$date_picker('#booking_coupong_code').keypress(function(){$date_picker('#coupon_code_error').html('');$date_picker('#coupon_code_success').html('');});
				$date_picker('#apply_code').click(function(){
					$date_picker('#coupon_code_text').val($date_picker('#booking_coupong_code').val());
					var dataString1 = $date_picker('#submit_form').serialize();
					$date_picker.ajax({
						type: "GET",
						url: "<?php echo TEMPLATIC_BOOKING_URL.'library/includes/check_coupon_code.php';?>",
						data: dataString1,
						success: function(html){
						
							var result = html.split("-");
							if(result[0]==""){
								$date_picker('#coupon_code_error').html('<?php _e("Invalid Coupon code!",BKG_DOMAIN);?>');
							}else{
								calculate_cost();
								var a = html.split("-");
								var b = a[1].split("<?php echo get_option('currency_symbol'); ?>");
								$date_picker('#booking_coupon_id').val(a[0]);
								$date_picker('#coupon_code_success').html('<?php _e("Coupon code successfully applied",BKG_DOMAIN);?>');
								$date_picker('#before_disc_amt').val(b[1]);
							}
						}
					});
					
				});
				<?php
					if(get_option('date_format') !=""){
						$date_format = get_option('date_format');
					}else{
						$date_format = 'F j, Y';
					}
					$checkin_date = date_i18n($date_format, strtotime($_REQUEST['check_in_date']));
					$checkout_date = date_i18n($date_format, strtotime($_REQUEST['check_out_date']));
					$time1 = (strtotime($_REQUEST['check_out_date']) - strtotime($_REQUEST['check_in_date']));
				
				if((isset($_REQUEST['check_in_date']) && $_REQUEST['check_in_date']!="")){
				?>
					var from1 = new Date(<?php if($_REQUEST['check_in_date']!=""){echo strtotime($_REQUEST['check_in_date']) * 1000;}?>);
					var to1 = new Date(<?php if($_REQUEST['check_in_date']!=""){echo strtotime($_REQUEST['check_out_date']) * 1000;}?>);	
				<?php }else{ ?>
					var from1 = new Date();
					var to1 = new Date();	
				<?php } ?>
				$date_picker('#datepicker-calendar').DatePicker({
					inline: true,
					date: [from1, to1],
					calendars: 2,
					mode: 'range',
					current: new Date(to.getFullYear(), to.getMonth() + 1, 1),
					onChange: function(dates,el) {
					  jQuery("#booking_check_in_date_error").html('');
					  $date_picker('#section_calculation').html('');
					  
					  var chk_in_month = dates[0].getMonth()+1;
					  var chk_out_month = dates[1].getMonth()+1;
					  var d = new Date();
					  var from_date_validation = d.setDate(d.getDate()-1);

					 var first_date = dates[0].getFullYear()+'-'+chk_in_month+'-'+dates[0].getDate();
					 var second_date = dates[1].getFullYear()+'-'+chk_out_month+'-'+dates[1].getDate();
					 if(first_date == second_date)
					 {
						$date_picker("#booknow").prop('disabled',true);
						$date_picker("#section_calculation").html("<span><?php _e("Booking cannot be done on same date.Start and End date must be different.",BKG_DOMAIN);?>.</span>");
						$date_picker('.booking_fields .form_row #date-range-field span').text('<?php _e("Invalid date selection",BKG_DOMAIN);?>');
						$date_picker('#checkindate').val('');
						$date_picker('#checkoutdate').val('');
					 }else if(dates[0] < from_date_validation || dates[1] < from_date_validation){
						$date_picker('.booking_fields .form_row #date-range-field span').text('<?php _e("Invalid date selection",BKG_DOMAIN);?>');
						$date_picker('#checkindate').val('');
						$date_picker('#checkoutdate').val('');
						$date_picker('#coupon_code_main_div').css('display','none');
					  }else{
						
						$date_picker('.booking_fields .form_row #date-range-field span').text(dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+dates[0].getFullYear()+' - '+
														dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear());
						$date_picker('#checkindate').val(dates[0].getFullYear()+'-'+chk_in_month+'-'+dates[0].getDate());
						$date_picker('#checkoutdate').val(dates[1].getFullYear()+'-'+chk_out_month+'-'+dates[1].getDate());
						$date_picker('#coupon_code_main_div').css('display','block');
						calculate_cost();
					  }								
					 }
				   });
				   <?php if($_REQUEST['check_in_date']!="" && $_REQUEST['check_out_date']!=""){?>
							$date_picker('.booking_fields .form_row #date-range-field span').text('<?php echo $checkin_date.' - '.$checkout_date;?>');
				   <?php }else{ ?>
							$date_picker('.booking_fields .form_row #date-range-field span').text('<?php _e("Select Check-In/Check-Out date",BKG_DOMAIN);?>');
				   <?php } ?>
					
				   
				   $date_picker('.booking_fields .form_row #date-range-field').bind('click', function(){
					 $date_picker('.booking_fields .form_row #datepicker-calendar').toggle();
					 if($date_picker('.booking_fields .form_row #date-range-field a').text().charCodeAt(0) == 9660) {
					 } else {
					 }
					 return false;
				   });
				   $date_picker('html').click(function() {
					 if($date_picker('.booking_fields .form_row #datepicker-calendar').is(":visible")) {
					   $date_picker('.booking_fields .form_row #datepicker-calendar').hide();
					 }
				   });
				   $date_picker('.booking_fields .form_row #datepicker-calendar').click(function(event){
					 event.stopPropagation();
				   });
				   $date_picker("#close_cal1").click(function(){
					   $date_picker('.booking_fields .form_row #datepicker-calendar').hide();
				   });
			 });
			 $date_picker("#home_capacity").bind('change',function(){
				calculate_cost();
			});
			$date_picker("#no_of_rooms").bind('change',function(){
				$date_picker('#hidden_no_of_rooms').val(this.value);
				calculate_cost();
			});
			function use_coupon_code(){
				if ($date_picker('#booking_coupons_code_chk').is(':checked')){
					$date_picker('#coupon_code_div').show('slow');
				}else{
					$date_picker('#coupon_code_div').hide('slow');
				}
			}
		</script>
			
			<div class="booking_calculation">
				<div class="form_row clearfix">
					<?php if((isset($_REQUEST['houses_rooms']) && $_REQUEST['houses_rooms']!="") || (isset($_REQUEST['id']) && $_REQUEST['id']!=""))
					{
						$post_title = get_post($_REQUEST['houses_rooms']);
						$post_title = $post_title->post_title;
					?>
					<div class="bk_row clearfix" id="booking_infomation_div" style="display:none;">
						<span id="booking_infomation"></span>
					</div>
					<?php } ?>
					<div class="bk_row clearfix">
						<?php
							echo "<span class='sub_total'>"; _e("Cost Subtotal",BKG_DOMAIN);echo ":</span> ";
							echo '<span id="cost_subtotal">'.fetch_currency_with_position('0').'</span>';
						?>
					</div>
					
					<?php if(@$is_tax!="" && @$is_tax=="Yes"){?>
					<input type="hidden" name="tax_price" value="" id="tax_price" />
					<div class="bk_row clearfix">
						<?php
							
							$tax_amtt = "+ Tax amount:";
							$tax_amt1 = "+ Tax:";
							
							if(function_exists('icl_register_string')){
								icl_register_string(BKG_DOMAIN,$tax_amtt,$tax_amtt);
								icl_register_string(BKG_DOMAIN,$tax_amt1,$tax_amt1);
							}
							if(function_exists('icl_t')){
								$tax_amnt = icl_t(BKG_DOMAIN,$tax_amtt,$tax_amtt);
								$tax_amnt1 = icl_t(BKG_DOMAIN,$tax_amt1,$tax_amt1);
							}else{
								$tax_amnt = __("+ Tax amount:",BKG_DOMAIN); 
								$tax_amnt1 = __("+ Tax:",BKG_DOMAIN); 
							}
							echo $tax_type.$wpdb->prefix;
							if($tax_type=='exact'){
								$tax_text = $tax_amnt;
								$tax_amt = fetch_currency_with_position($tax_amount);
							}elseif($tax_type=='percent'){
								$tax_text = $tax_amnt1;
								$tax_amt = $tax_amount.'%';
							}
							?>
							<span class='booking_service_fee'><?php echo $tax_text;?><br/><small><?php _e("(This is the additional fee charged for booking)",BKG_DOMAIN);?></small></span>
							<span id="cost_booking_service_fee">
								<?php  echo sprintf(__('%s',BKG_DOMAIN),$tax_amt); ?>
							</span>
					</div>
					<div id="coupon_success"></div>
					<?php }?>
					<?php if($deposite_percentage!=""){?>
					<div class="bk_row clearfix">
						<span class='total_cost'><?php	_e("Deposit ",BKG_DOMAIN);?>:</span>		
						<span id="deposite_percentage"><?php echo $deposite_percentage.__('%',BKG_DOMAIN);?></span>
					</div>
					<?php } ?>
					<div class="bk_row clearfix">
						<span class='total_cost'><?php	_e("Total cost ",BKG_DOMAIN);?>:</span>		
						<span id="cost_total_cost"><?php _e(fetch_currency_with_position(0.00),BKG_DOMAIN);?></span>
					</div>
				</div>
			  <div class="bk_row clearfix">
				 <div class="form_row clearfix" id="coupon_code_main_div">
					<label>
					    <input type="checkbox" name="booking_coupons_code_chk" id="booking_coupons_code_chk" onclick="use_coupon_code()"  /> <?php _e("Have Coupon Code",BKG_DOMAIN);?>?
					</label>
					<div class="coupon_code_div" id="coupon_code_div" style="display:none">
							<input type="hidden" name="booking_coupon_id" id="booking_coupon_id"  value="" />
					    <input type="text" name="booking_coupong_code" id="booking_coupong_code" placeholder="<?php _e("Enter Coupon Code",BKG_DOMAIN);?>" value="" />
					    <input type="button" name="apply_code" id="apply_code" value="<?php _e("Apply",BKG_DOMAIN);?>"/>
					    <span id="coupon_code_error" class="message_error2"></span>
					    <span id="coupon_code_success" style="color:green"></span>
					</div>
				 </div>
			  </div>
			</div>
			<div class="main_user_info" style="clear:both">
				<div class="user_info">
			<?php
				
				global $validation_info;
	
				$form_fields = array();
				$args=
					array( 
					'post_type' => 'custom_fields',
					'posts_per_page' => -1	,
					'post_status' => array('publish'),
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
							'value' => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
							'compare' => '=',
							'type'=> 'text'
						),
						array(
							'key' => 'show_on_page',
							'value' =>  array('user_side','both_side'),
							'compare' => 'IN',
							'type'=> 'text'
						),
						array(
							'key' => 'is_require',
							'value' =>  1,
							'compare' => '='
						),
						array(
							'key' => 'is_active',
							'value' =>  '1',
							'compare' => '='
						)
			
					),
					 
					 'meta_key' => 'sort_order',
					 'orderby' => 'meta_value_num',
					 'meta_value_num'=>'sort_order',
					);
					$extra_field_sql = null;
					add_filter('posts_join', 'custom_field_posts_where_filter');
					$extra_field_sql = new WP_Query($args);
					remove_filter('posts_join', 'custom_field_posts_where_filter');
					
					if($extra_field_sql->have_posts())
					 {
						while ($extra_field_sql->have_posts()) : $extra_field_sql->the_post();
							$title = get_post_meta($post->ID,'site_title',true);
							$name = get_post_meta($post->ID,'htmlvar_name',true);
							$type = get_post_meta($post->ID,'ctype',true);
							$require_msg = get_post_meta($post->ID,'field_require_desc',true);
							$is_require = get_post_meta($post->ID,'is_require',true);
							$validation_type = get_post_meta($post->ID,'validation_type',true);
							if($name != 'category')
							{
								$form_fields[$name] = array(
										   'title'	=> $title,
										   'name'	=> $name,
										   'espan'	=> $name.'_error',
										   'type'	=> $type,
										   'text'	=> $require_msg,
										   'is_require'	=> $is_require,
										   'validation_type' => $validation_type);
							}
						endwhile;
					  }
					
				$validation_info = array();
				foreach($form_fields as $key=>$val)
				{
					$str = ''; $fval = '';
					$field_val = $key.'_val';
					if(!isset($val['title']))
					   {
						 $val['title'] = '';
					   }
					$validation_info[] = array(
												   'title'	=> $val['title'],
												   'name'	=> $key,
												   'espan'	=> $key.'_error',
												   'type'	=> $val['type'],
												   'text'	=> $val['text'],
												   'is_require'	=> $val['is_require'],
												   'validation_type'	=> $val['validation_type']);
				}
			
				$custom_metaboxes = array();
				$heading_type = fetch_heading_per_post_type(CUSTOM_POST_TYPE_TEMPLATIC_BOOKING);
				if(count($heading_type) > 0){
					foreach($heading_type as $_heading_type){
						$custom_metaboxes[$_heading_type] = get_post_custom_fields_templ_plugin(CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,'','',$_heading_type);//custom fields for custom post type..
					 }
				}else{
					 $custom_metaboxes[] = get_post_custom_fields_templ_plugin(CUSTOM_POST_TYPE_TEMPLATIC_BOOKING);//custom fields for custom post type..
				}
				if($templatic_settings['templatic-category_custom_fields'] == 'No'){
					display_custom_post_field_plugin($custom_metaboxes,'custom_fields',CUSTOM_POST_TYPE_TEMPLATIC_BOOKING);//displaty custom fields html.
				}
			?>
				</div>
				<div class="payment_info">
					<?php if(function_exists('templatic_payment_option_preview_page')){?>
					<h3><?php _e("Make Your Payment",BKG_DOMAIN);?></h3>
					<?php templatic_payment_option_preview_page();}?>
				</div>
				<span id="payment_errors" style="color:red;font-weight:bold;display:block;"></span>
				<?php 
				templ_captcha_integrate('submit');
				 global $submit_button;
				 if(!isset($submit_button)){ $submit_button = ''; }
				 if(function_exists('tevolution_show_term_and_condition')){tevolution_show_term_and_condition(); }
				 ?>
				 
				<input type="submit" name="booknow" id="booknow" value="<?php _e("Book Now",BKG_DOMAIN);?>" class="normal_button" <?php echo $submit_button; ?> />
		   </div>	
			</div> 
		</form>
		<style>
			
			.payment_error {
				color: red;
				display: block;
				font-size: 12px;
			}
		</style>	
		<script type="text/javascript" src="<?php echo CUSTOM_FIELDS_URLPATH; ?>js/payment_gateway_validation.js"></script>
		 <form name="get_id" id="get_id" method="post" action="">
			<input type="hidden" name="houses_rooms" id="houses_rooms" value=""/>
		 </form>
		 <script type="text/javascript">
			jQuery.noConflict();
			jQuery("#submit_form").submit(function(){
			
				var checkindate = jQuery("#checkindate").val();
				var checkoutdate = jQuery("#checkoutdate").val();
				var selected = jQuery('input[name=paymentmethod]:checked').val();
				var no_of_rooms = '';
				if( document.getElementById("no_of_rooms_error"))
						no_of_rooms = jQuery("#no_of_rooms").val();
				
				var home_capacity = jQuery("#home_capacity").val();
				var set_price_for = jQuery("#set_price_for").val();
				
				if(checkindate=="" || checkoutdate=="" ){
					jQuery("#booking_check_in_date_error").html('<?php _e("Please select valid Check-in/Check-Out date",BKG_DOMAIN);?>');
					jQuery("#booking_check_in_date_error").css('line-height','inherit');
					jQuery("#date-range-field span").focus();
					return false;
				}else if(home_capacity == ""){
					jQuery("#home_capacity_error").html('<?php _e("Please select total guests",BKG_DOMAIN);?>');
					jQuery("#home_capacity").bind('change',function(){
						jQuery("#home_capacity_error").html('');
					});
					return false;
				}
				else if(no_of_rooms == "" && document.getElementById("no_of_rooms_error")){
					jQuery("#no_of_rooms_error").html('<?php _e("Please select total number of rooms.",BKG_DOMAIN);?>');
					jQuery("#no_of_rooms").bind('change',function(){
						jQuery("#no_of_rooms_error").html('');
					});
					return false;
				}
				else if(set_price_for == ""){
					jQuery("#booking_common_error").html('<?php _e("Please select Room/house",BKG_DOMAIN);?>');
					jQuery("#booking_common_error").bind('change',function(){
						jQuery("#booking_common_error").html('');
					});
					return false;
				}else if(selected != ""){
				
					if(selected=="authorizedotnet"){
						if(validate_authorizedotnet()){return true;}else{return false;}
					}else if(selected=="eway"){
						if(validate_eway()){return true;}else{return false;}
					}else if(selected=="paypal_pro"){
						if(validate_paypal_pro()){return true;}else{return false;}
					}
					else if(selected=="psigate"){
						if(validate_psigate()){return true;}else{return false;}
					}else if(selected=="stripe"){
						if(validate_stripe()){ return true; }else{return false;}
					}
					else if(selected=="Braintree"){
						if(validate_braintree()){return true;}else{return false;}
					}
					else if(selected=="inspirecommerce"){
						if(validate_inspire_commerce()){return true;}else{return false;}
					}
				
				}else{
					
					return true;
				}
				
			});
		</script>
		<?php include_once(TEMPLATIC_BOOKING_PATH.'/js/submition_validation.php');
		
	}
	else
	{ ?>
	    <div class="error_msg">
	    <?php _e(IP_BLOCK,BKG_DOMAIN); ?>
	    </div>
<?php }
	$content = str_replace( array( '<p></p>' ), '', @$content );  
	return ob_get_clean();
}

function booking_system_cancel($atts){
	ob_start();
	global $post,$current_user;
	if(get_option('booking_system_cancel')=='' || get_option('booking_system_cancel')!=$post->ID)
		update_option('booking_system_cancel',$post->ID);
		
	$postid = $_REQUEST['booking_id'];
	$get_post = get_post( $postid );
	$submit_booking_nonce = get_post_meta($postid,'submit_booking_nonce',true);
	if($postid && ( isset($_REQUEST['submit_booking_nonce']) && $submit_booking_nonce == $_REQUEST['submit_booking_nonce'] )){
		global $wpdb;
		echo '<h4>'.BOOKING_CANCEL_MSG.'</h4>';?>
		<center><?php
		$fromEmail = get_site_emailId_plugin();
		$fromEmailName = get_site_emailName_plugin();
		$toEmail = get_post_meta($postid,'booking_email',true);
		$toEmailName = get_the_title($postid);
		$store_name = get_option('blogname');
		
		$templatic_settings = get_option('templatic_settings');
		$email_content = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_msg'];
		$email_subject = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_sub'];
		if(!$email_subject){
			$email_subject = __('Your Booking is Cancelled','');	
		}
		if(!$email_content){
			$email_content = '<p>'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('You successfully cancelled your booking. To book another date feel free to visit our site at any time.',BKG_DOMAIN).'<br /><br />'.__('Kind regards',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>';
		}
		
		$property_email = get_post_meta($postid,'property_room_id',true);
		$email_info = get_post_meta($property_email,'home_contact_mail',true);//Fetch email id from house's email meta information   
		$BookingPostType = get_post_meta($postid,'booking_post_type',true);
		
		if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
			$hotel_name = get_the_title($property_email);
			$addresses = get_post_meta($property_email,'address',true);
			$address_exclude = explode(',',$addresses);
			$home_street = $address_exclude[0];
			$home_state = $address_exclude[1];
			$home_country = $address_exclude[2];
		}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
			$hotel_name = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
			$home_street = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
			$home_state = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
			$home_country = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
		}
		
		$regard_content = '<strong>'.$hotel_name.'</strong><br />'.$home_street.'<br />'.$home_state;
		if($home_country!=""){
			$regard_content .= ', '.$home_country;
		}
		
		$serach_array = array('[USER_NAME]','[ADMIN_NAME]');
		$replace_array = array($toEmailName, $regard_content);
		$mail_content = str_replace($serach_array,$replace_array,$email_content);
		templ_send_email($fromEmail,$fromEmailName,$toEmail,$toEmailName,$email_subject,stripslashes($mail_content),$extra='');//to client email 
		//delete booking
		/*wp_delete_post($postid);
		$tbl_transcation=$wpdb->prefix."transactions";
		$wpdb->query("delete from $tbl_transcation where post_id=$postid");*/
		$my_post['post_status'] = 'Draft';
		$my_post['ID'] = $postid;
		wp_update_post( $my_post );
		$UpdatedStatus = 'Cancelled';
		update_post_meta($postid,'status',$UpdatedStatus);
		$booking_availability_table = $wpdb->prefix."booking_availability_check";
		$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET status='$UpdatedStatus' where booking_post_id=".$postid);
		//finish the delete process
		
		/*$mypost = array();
		$mypost['ID'] = $postid;
		$mypost['post_status'] = 'draft';
		wp_update_post($mypost);
		update_post_meta($postid,'status','Cancelled');*/
		//$transaction_table = $wpdb->prefix."transactions";
		//$wpdb->query("update $transaction_table set status = 0 where post_id=$postid");
	}else
	{
		echo 'zz';
		echo '<h4>'.__("You can't cancel this booking.",BKG_DOMAIN).'</h4>';
	}
	return ob_get_clean();
}
function get_my_booking_prices()
{
		global $wpdb;
		$templatic_settings = get_option( "templatic_settings" );
		$property_id = $_REQUEST['set_room_house'];
		//$sql_custom_max_field = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_capacity' and $wpdb->posts.post_type = 'custom_fields'");
		//$GetPostMeta = get_post_meta($sql_custom_max_field,'option_values',true);
		//$max_adults_allowed = explode(",",$GetPostMeta);
		$total_adults = get_post_meta($property_id,'home_capacity',true);
		$no_person = __("No. of persons",BKG_DOMAIN);
		$pric_per_day = __("Price/Day",BKG_DOMAIN);
		$price_per_week = __("Price/Week",BKG_DOMAIN);
		$price_per_month = __("Price/Month",BKG_DOMAIN);
			$html='
		<table class="widefat post" id="set_price_tbl">
			<thead>
				<tr>
					<th><label for="no_of_persons" class="form-textfield-label">'.$no_person.'</label></th>
					<th><label for="price_day" class="form-textfield-label">'.$pric_per_day.'</label></th>
					<th><label for="price_week" class="form-textfield-label">'.$price_per_week.'</label></th>
					<th><label for="price_month" class="form-textfield-label">'.$price_per_month.'</label></th>
				</tr>
			</thead>
			<tbody>';
			
				if($total_adults>0){
						for($i=1;$i<=$total_adults;$i++){
							$price_per_day = $templatic_settings["templatic_booking_system_settings"]["templatic_booking_manage_prices_settings"]["price_for_$property_id"]["price_guests_$i"]["price_per_day"];
							$price_per_week = $templatic_settings["templatic_booking_system_settings"]["templatic_booking_manage_prices_settings"]["price_for_$property_id"]["price_guests_$i"]["price_per_week"];
							$price_per_month = $templatic_settings["templatic_booking_system_settings"]["templatic_booking_manage_prices_settings"]["price_for_$property_id"]["price_guests_$i"]["price_per_month"];
							
							$html .= '<tr>
								<td><label class="form-textfield-label">'.$i.'</label></td>
								<td><span style="height:40px;width:80px;" name="price_day_'.$i.'" id="price_day_'.$i.'" >'.fetch_currency_with_position($price_per_day).'</span></td>
								<td><span style="height:40px;width:80px;" name="price_week_'.$i.'" id="price_week_'.$i.'" >'.fetch_currency_with_position($price_per_week).'</span></td>
								<td><span style="height:40px;width:80px;" name="price_month_'.$i.'" id="price_month_'.$i.'">'.fetch_currency_with_position($price_per_month).'</span></td>
							</tr>';
						}
				}else{	
					$html .= '<tr><td colspan="3"><b>Please set the Maximum Adults first from General Settings.</b></label></td></tr>';
				}
			
$html .='   </tbody>
		</table>';
		
		$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";
		$booking_seasonal_prices=$wpdb->get_results("select * from $booking_seasonal_price_table where set_price_for = ".$property_id);
		$seasonal_title = __("Seasonal Title",BKG_DOMAIN);
		$no_person = __("No. of persons",BKG_DOMAIN);
		$pric_per_day1 = __("Price/Day",BKG_DOMAIN);
		$price_per_week1 = __("Price/Week",BKG_DOMAIN);
		$price_per_month1 = __("Price/Month",BKG_DOMAIN);
		$dates = __("Date :",BKG_DOMAIN);
	
		for($k=0;$k<count($booking_seasonal_prices);$k++)
		{
			$format = get_option('date_format');
			$frmdate = date($format,strtotime($booking_seasonal_prices[$k]->from_date));
			$todate = date($format,strtotime($booking_seasonal_prices[$k]->to_date));
			$html.='
		<table class="widefat post" id="set_price_tbl">
			<thead>
				<tr>
					<th colspan="4">
						<label for="no_of_persons" >'.$booking_seasonal_prices[$k]->seasonal_title.'</label>
					</th>
				</tr>
				<tr>
					<th colspan="4">
						<label for="date" >'.$dates.' '.$frmdate.' '.__('To',BKG_DOMAIN).' '.$todate.'</label>
					</th>
				</tr>
				<tr>
					<th><label for="no_of_persons" class="form-textfield-label">'.$no_person.'</label></th>
					<th><label for="price_day" class="form-textfield-label">'.$pric_per_day1.'</label></th>
					<th><label for="price_week" class="form-textfield-label">'.$price_per_week1.'</label></th>
					<th><label for="price_month" class="form-textfield-label">'.$price_per_month1.'</label></th>
				</tr>
			</thead>
			<tbody>';
			$a = unserialize($booking_seasonal_prices[$k]->prices_per_persons);
			for($i=0;$i<$total_adults;$i++){
			
				$price_per_day = $a['price_per_day'][$i];
				$price_per_week = $a['price_per_week'][$i];
				$price_per_month = $a['price_per_month'][$i];
				
				$html .= '<tr>
					<td><label class="form-textfield-label">'.($i+1).'</label></td>
					<td><span style="height:40px;width:80px;" name="price_day_'.$i.'" id="price_day_'.$i.'" >'.fetch_currency_with_position($price_per_day).'</span></td>
					<td><span style="height:40px;width:80px;" name="price_week_'.$i.'" id="price_week_'.$i.'" >'.fetch_currency_with_position($price_per_week).'</span></td>
					<td><span style="height:40px;width:80px;" name="price_month_'.$i.'" id="price_month_'.$i.'">'.fetch_currency_with_position($price_per_month).'</span></td>
				</tr>';
					
			}
		}
		$html .='   </tbody>
		</table>';
	_e($html);
}

/**
 * Shortcode creation
 **/
add_shortcode('booking_calendar', 'booking_system_calendar');
add_shortcode('booking_cancel', 'booking_system_cancel');
add_shortcode('booking_submit_form', 'booking_system_form');
add_shortcode('booking_tariff', 'booking_system_tariff');
?>
