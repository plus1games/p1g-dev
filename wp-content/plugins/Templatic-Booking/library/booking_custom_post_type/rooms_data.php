<?php 
ini_set('set_time_limit', 0);ini_set('max_execution_time', 0);
global $wpdb;
//Add some categories in "room" post type.
update_option('no_alive_days',1);
//CREATED CATEGORY FOR ROOM TAXONOMY FINISH.
// rooms entry 1 //
$post_info=array();
$image_array = array();
$post_meta = array();
$image_array[] = "dummy/img1.jpg" ;
$image_array[] = "dummy/img2.jpg" ;
$image_array[] = "dummy/img3.jpg" ;
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "no_of_rooms"  => 5,
				   "booking_dummy_content"	  => '1',
				);
$post_info[] = array(
					"post_title" =>	'A Toast for our Hotel',
					"post_content" =>	'Its our Fifth anniversary, and you are invited. Thanks to you all, we have progressed much in this time. Lets have a toast for this. 
					
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent aliquam,  justo convallis luctus rutrum, erat nulla fermentum diam, at nonummy quam  ante ac quam. Maecenas urna purus, fermentum id, molestie in, commodo  porttitor, felis. 
					
Edgar Allan Poe, author of "Annabel Lee" and "The Fall of the House of Usher" , is easily recognized as one of the foremost masters of horror and the macabre. His works have inspired terror and anxiety in many individuals, primarily through the use of heavy psychological tones, as opposed to the gore and blood themes used and abused by writers of his time. Poes collected works easily counts as some of the most frightening material ever written, especially now, in an age where horror movies are relegated to two hours of bloodshed and senseless violence, lacking any true horror and relying solely on shock value to appear "scary." Poe also stands out as being among the few who can make even the most mundane things seem utterly terrifying, a feat emulated by Stephen King and several Japanese horror authors, but never truly duplicated.

In a completely different vein of horror from his predecessors, and arguably creating a sub-genre of horror through his works, H. P. Lovecraft also stands out. His works, while lacking in humanity, are difficult to see as anything but terrifying, particularly because of the apparent lack of humanity in them. In contrast to writers of previous generations, Lovecraft focused more on the truly monstrous, ignoring the human element that most horror writers tended to insert into their works since the days of the Gothic era. His stories were littered with monsters that knew neither morality nor mercy, seeing humanity as insignificant insects and, in Lovecrafts malignant world of ancient races and Elder Gods, humanity was insignificant. He also brought back something from the Gothic horror era, showing his readers that knowledge, even just a little knowledge, can lead to the most terrifying of discoveries. This is perhaps best exemplified by the so-called "Cthulhu Mythos," a collection of stories that centered around Lovecrafts anti-mythological beings.

<h3>Frankenstein</h3>
<ol>
	<li>Among the most enduring horror classics in the world is that of Shelleys "Frankenstein," which combines the elements of horror with the intrinsic questions that plagued morality and philosophy at the time. </li>
	<li>In some ways, the story is one that puts a new spin on the old ghost story, in that the "ghost" is inevitably caused by the actions of mortal men who meddled in things they were not meant to. </li>
	<li>The story, aside from being a genuine tale of terror, also took on the role of a lesson in morality and the limits to just how far medical science could go.</li>
	<li>Prolonging life is one thing, but bringing back the dead is another thing entirely, which is one of the subtle messages of the novel.</li>
	<li>The underlying question of whether or not Frankensteins creature is the monster, or if it is Frankenstein himself, also contributes to making the story a memorable, chilling tale.</li>
</ol> 

However, very few stories can truly stand up against the pure terror and the subtle anxiety and dread caused by Bram Stokers infamous novel, "Dracula." The novel is a hallmark of the Gothic horror era, presenting a villain of potentially epic scope in the guise of a remarkable gentleman and nobleman. It deviated from other vampire stories of the time in that the vampire, Dracula, was not monstrous in appearance. He looked every inch a master and nobleman, establishing the "lord of the night" archetype that would be a stock image of vampire characters in literature for centuries to come. It also had all the elements necessary to both frighten readers and keep them coming back for more, marking it as the most enduring horror novel in history.

',
					"post_category" =>	'exclusive-room',
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  CUSTOM_POST_TYPE_TEMPLATIC_ROOM
					);
////post end///

// rooms entry 2 start //
$image_array = array();
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "no_of_rooms"  => 5,
				   "booking_dummy_content"	  => '1',
				);
$image_array[] = "dummy/img4.jpg" ;
$image_array[] = "dummy/img5.jpg" ;
$image_array[] = "dummy/img6.jpg" ;
$post_info[] = array(
					"post_title" =>	'The Beautiful Rooms',

					"post_content" =>	'Famous Paintings have been greatly admired in art history. Famous art paintings are invaluable and of great historic importance. Famous artists have been successful in creating famous artwork paintings. Canvas oil paintings make the most the popular form of the most famous Oil paintings. Famous Oil Paintings are of various styles. These include famous landscape paintings, famous still life paintings, famous fruit paintings, famous seascape paintings, famous contemporary paintings.
					
Famous artists paintings have earned world wide recognition in different periods of times. Famous painters paintings truly an asset for fine arts. There have been a great number of famous painters in different parts of the world in different periods of times. These include Marc Chagall, Salvador Dali, Leonardo Da Vinci, Paul Klee, Henri Matisse,Claude Monet, Pablo Picasso,Pierre Auguste Renoir,Henri Rousseau,Henri de Toulouse-Lautrec,Vincent Van Gogh,Andy Warhol.

<h3>Yo Picasso</h3>
<ol>
	<li>Famous abstract paintings present the fine art at the highest level. </li>
	<li>Famous abstract artists have been gratly greatly appreciated for their famous abstract oil paintings. </li>
	<li>Picasso is one of the most famous abstract painter. Picasso became very famous because he work in multiple styles.</li>
	<li>Famous paintings of Picasso are Guernica ,Three Musicians,The Three Dancers and Self Portrait: Yo Picasso.</li>
	<li>Picasso famous paintings have earned him worldwide recognition.</li>
</ol> 

Many famous flower paintings have been created by the outstanding flower painters. Famous Floral Oil Paintings are in wide range of styles. Famous floral fine art paintings are exquisite. Famous landscape paintings are the master pieces of fine art. Famous Landscape painters have created a great number of famous landscape paintings. Famous Landscape art has greatly been admired in all the periods of times. Famous contemporary landscape painters have successfully attained the mastery in the landscape art.

Still life fruit paintings and fruit bowl paintings make the famous fruit paintings. The highly skilled artists have also created the most famous paintings of rotting fruit. The modern famous artists are successful creating the masterpieces of still fruit oil paintings and oil pastel fruit paintings.

Famous still Life art depicts drinking glasses, foodstuffs, pipes, books and so on. Famous Still life paintings are indeed the master pieces of fine art. Woman portrait paintings make the famous portrait paintings. There are also famous portrait paintings of men. Famous portrait paintings of Oscar dela hova have been greatly appreciated. Japanese women portrait paintings are very popular in Japanese culture. In addition to women portrait paintings and portrait paintings of men, there are many famous pet portrait paintings and famous portrait paintings of houses and famous paintings of sports cars.

Famous Islamic paintings of holy places and the famous islamic calligraphy of the holy verses have been truly represent the muslim art. Famous muslim artists have developed mastery of Islamic art calligraphy. The famous Islamic paintings include the paintings of the Holy places such as Khana kaaba, Masjid-e-Nabvi and other famous mosques and shrines. Famous Islamic art is fascinating and has always been appreciated. The famous Islamic art galleries have produced a great number of famous muslim painters and famous muslim calligraphist.

Famous modern galleries have produced the famous contemporary artists who have created many famous contemporary paintings. Famous oil paintings reproduction are also created in these famous galleries.

In addition to above styles, there are many famous paintings of other subjects. These include famous war paintings, famous paintings of jesus, famous figure paintings, religious famous paintings, famous paintings romantic, famous battle paintings, famous military paintings, famous sunset paintings, famous paintings of women, famous paintings of love, famous water paintings, famous acrylic paintings, famous paintings of buildings, famous dance paintings, famous dragon paintings, famous black paintings, famous paintings in the fall, famous paintings of cats, famous paintings of children, famous paintings of friends, famous paintings of christinaity, famous paintings of jesus and famous paintings of humanity. There are also famous native American paintings and famous Spanish paintings.

',
					"post_category" =>	'exclusive-room',
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  CUSTOM_POST_TYPE_TEMPLATIC_ROOM
					);
// rooms entry 1 end //

// rooms entry 2 start //
$image_array = array();
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "no_of_rooms"  => 5,
				   "booking_dummy_content"	  => '1',
				);
$image_array[] = "dummy/img7.jpg" ;
$image_array[] = "dummy/img8.jpg" ;
$image_array[] = "dummy/img9.jpg" ;
$post_info[] = array(
					"post_title" =>	'The Delux Room',

					"post_content" =>	'Canvas oil paintings make the most the popular form of the most famous Oil paintings. Famous Oil Paintings are of various styles. These include famous landscape paintings, famous still life paintings, famous fruit paintings, famous seascape paintings, famous contemporary paintings.
					
Famous artists paintings have earned world wide recognition in different periods of times. Famous painters paintings truly an asset for fine arts. There have been a great number of famous painters in different parts of the world in different periods of times. These include Marc Chagall, Salvador Dali, Leonardo Da Vinci, Paul Klee, Henri Matisse,Claude Monet, Pablo Picasso,Pierre Auguste Renoir,Henri Rousseau,Henri de Toulouse-Lautrec,Vincent Van Gogh,Andy Warhol.

<h3>Yo Picasso</h3>
<ol>
	<li>Famous abstract paintings present the fine art at the highest level. </li>
	<li>Famous abstract artists have been gratly greatly appreciated for their famous abstract oil paintings. </li>
	<li>Picasso is one of the most famous abstract painter. Picasso became very famous because he work in multiple styles.</li>
	<li>Famous paintings of Picasso are Guernica ,Three Musicians,The Three Dancers and Self Portrait: Yo Picasso.</li>
	<li>Picasso famous paintings have earned him worldwide recognition.</li>
</ol> 

Many famous flower paintings have been created by the outstanding flower painters. Famous Floral Oil Paintings are in wide range of styles. Famous floral fine art paintings are exquisite. Famous landscape paintings are the master pieces of fine art. Famous Landscape painters have created a great number of famous landscape paintings. Famous Landscape art has greatly been admired in all the periods of times. Famous contemporary landscape painters have successfully attained the mastery in the landscape art.

Still life fruit paintings and fruit bowl paintings make the famous fruit paintings. The highly skilled artists have also created the most famous paintings of rotting fruit. The modern famous artists are successful creating the masterpieces of still fruit oil paintings and oil pastel fruit paintings.

Famous still Life art depicts drinking glasses, foodstuffs, pipes, books and so on. Famous Still life paintings are indeed the master pieces of fine art. Woman portrait paintings make the famous portrait paintings. There are also famous portrait paintings of men. Famous portrait paintings of Oscar dela hova have been greatly appreciated. Japanese women portrait paintings are very popular in Japanese culture. In addition to women portrait paintings and portrait paintings of men, there are many famous pet portrait paintings and famous portrait paintings of houses and famous paintings of sports cars.

Famous Islamic paintings of holy places and the famous islamic calligraphy of the holy verses have been truly represent the muslim art. Famous muslim artists have developed mastery of Islamic art calligraphy. The famous Islamic paintings include the paintings of the Holy places such as Khana kaaba, Masjid-e-Nabvi and other famous mosques and shrines. Famous Islamic art is fascinating and has always been appreciated. The famous Islamic art galleries have produced a great number of famous muslim painters and famous muslim calligraphist.

Famous modern galleries have produced the famous contemporary artists who have created many famous contemporary paintings. Famous oil paintings reproduction are also created in these famous galleries.

In addition to above styles, there are many famous paintings of other subjects. These include famous war paintings, famous paintings of jesus, famous figure paintings, religious famous paintings, famous paintings romantic, famous battle paintings, famous military paintings, famous sunset paintings, famous paintings of women, famous paintings of love, famous water paintings, famous acrylic paintings, famous paintings of buildings, famous dance paintings, famous dragon paintings, famous black paintings, famous paintings in the fall, famous paintings of cats, famous paintings of children, famous paintings of friends, famous paintings of christinaity, famous paintings of jesus and famous paintings of humanity. There are also famous native American paintings and famous Spanish paintings.

',
					"post_category" =>	'delux-room',
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  CUSTOM_POST_TYPE_TEMPLATIC_ROOM
					);
// rooms entry 1 end //

// rooms entry 2 start //
$image_array = array();
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "no_of_rooms"  => 5,
				   "booking_dummy_content"	  => '1',
				);
$image_array[] = "dummy/img10.jpg" ;
$image_array[] = "dummy/room1.jpg" ;
$image_array[] = "dummy/room2.jpg" ;
$post_info[] = array(
					"post_title" =>	'Luxury Room',

					"post_content" =>	'There have been a great number of famous painters in different parts of the world in different periods of times. These include Marc Chagall, Salvador Dali, Leonardo Da Vinci, Paul Klee, Henri Matisse,Claude Monet, Pablo Picasso,Pierre Auguste Renoir,Henri Rousseau,Henri de Toulouse-Lautrec,Vincent Van Gogh,Andy Warhol.

<h3>Yo Picasso</h3>
<ol>
	<li>Famous abstract paintings present the fine art at the highest level. </li>
	<li>Famous abstract artists have been gratly greatly appreciated for their famous abstract oil paintings. </li>
	<li>Picasso is one of the most famous abstract painter. Picasso became very famous because he work in multiple styles.</li>
	<li>Famous paintings of Picasso are Guernica ,Three Musicians,The Three Dancers and Self Portrait: Yo Picasso.</li>
	<li>Picasso famous paintings have earned him worldwide recognition.</li>
</ol> 

Many famous flower paintings have been created by the outstanding flower painters. Famous Floral Oil Paintings are in wide range of styles. Famous floral fine art paintings are exquisite. Famous landscape paintings are the master pieces of fine art. Famous Landscape painters have created a great number of famous landscape paintings. Famous Landscape art has greatly been admired in all the periods of times. Famous contemporary landscape painters have successfully attained the mastery in the landscape art.

Still life fruit paintings and fruit bowl paintings make the famous fruit paintings. The highly skilled artists have also created the most famous paintings of rotting fruit. The modern famous artists are successful creating the masterpieces of still fruit oil paintings and oil pastel fruit paintings.

Famous still Life art depicts drinking glasses, foodstuffs, pipes, books and so on. Famous Still life paintings are indeed the master pieces of fine art. Woman portrait paintings make the famous portrait paintings. There are also famous portrait paintings of men. Famous portrait paintings of Oscar dela hova have been greatly appreciated. Japanese women portrait paintings are very popular in Japanese culture. In addition to women portrait paintings and portrait paintings of men, there are many famous pet portrait paintings and famous portrait paintings of houses and famous paintings of sports cars.

Famous Islamic paintings of holy places and the famous islamic calligraphy of the holy verses have been truly represent the muslim art. Famous muslim artists have developed mastery of Islamic art calligraphy. The famous Islamic paintings include the paintings of the Holy places such as Khana kaaba, Masjid-e-Nabvi and other famous mosques and shrines. Famous Islamic art is fascinating and has always been appreciated. The famous Islamic art galleries have produced a great number of famous muslim painters and famous muslim calligraphist.

Famous modern galleries have produced the famous contemporary artists who have created many famous contemporary paintings. Famous oil paintings reproduction are also created in these famous galleries.

In addition to above styles, there are many famous paintings of other subjects. These include famous war paintings, famous paintings of jesus, famous figure paintings, religious famous paintings, famous paintings romantic, famous battle paintings, famous military paintings, famous sunset paintings, famous paintings of women, famous paintings of love, famous water paintings, famous acrylic paintings, famous paintings of buildings, famous dance paintings, famous dragon paintings, famous black paintings, famous paintings in the fall, famous paintings of cats, famous paintings of children, famous paintings of friends, famous paintings of christinaity, famous paintings of jesus and famous paintings of humanity. There are also famous native American paintings and famous Spanish paintings.

',
					"post_category" =>	'delux-room',
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  CUSTOM_POST_TYPE_TEMPLATIC_ROOM
					);
// rooms entry 1 end //

// rooms entry 2 start //
$image_array = array();
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "no_of_rooms"  => 5,
				   "booking_dummy_content"	  => '1',
				);
$image_array[] = "dummy/room3.jpg" ;
$image_array[] = "dummy/room4.jpg" ;
$image_array[] = "dummy/room5.jpg" ;
$post_info[] = array(
					"post_title" =>	'Executive Room',

					"post_content" =>	'<h3>Yo Picasso</h3>
<ol>
	<li>Famous abstract paintings present the fine art at the highest level. </li>
	<li>Famous abstract artists have been gratly greatly appreciated for their famous abstract oil paintings. </li>
	<li>Picasso is one of the most famous abstract painter. Picasso became very famous because he work in multiple styles.</li>
	<li>Famous paintings of Picasso are Guernica ,Three Musicians,The Three Dancers and Self Portrait: Yo Picasso.</li>
	<li>Picasso famous paintings have earned him worldwide recognition.</li>
</ol> 

Many famous flower paintings have been created by the outstanding flower painters. Famous Floral Oil Paintings are in wide range of styles. Famous floral fine art paintings are exquisite. Famous landscape paintings are the master pieces of fine art. Famous Landscape painters have created a great number of famous landscape paintings. Famous Landscape art has greatly been admired in all the periods of times. Famous contemporary landscape painters have successfully attained the mastery in the landscape art.

Still life fruit paintings and fruit bowl paintings make the famous fruit paintings. The highly skilled artists have also created the most famous paintings of rotting fruit. The modern famous artists are successful creating the masterpieces of still fruit oil paintings and oil pastel fruit paintings.

Famous still Life art depicts drinking glasses, foodstuffs, pipes, books and so on. Famous Still life paintings are indeed the master pieces of fine art. Woman portrait paintings make the famous portrait paintings. There are also famous portrait paintings of men. Famous portrait paintings of Oscar dela hova have been greatly appreciated. Japanese women portrait paintings are very popular in Japanese culture. In addition to women portrait paintings and portrait paintings of men, there are many famous pet portrait paintings and famous portrait paintings of houses and famous paintings of sports cars.

Famous Islamic paintings of holy places and the famous islamic calligraphy of the holy verses have been truly represent the muslim art. Famous muslim artists have developed mastery of Islamic art calligraphy. The famous Islamic paintings include the paintings of the Holy places such as Khana kaaba, Masjid-e-Nabvi and other famous mosques and shrines. Famous Islamic art is fascinating and has always been appreciated. The famous Islamic art galleries have produced a great number of famous muslim painters and famous muslim calligraphist.

Famous modern galleries have produced the famous contemporary artists who have created many famous contemporary paintings. Famous oil paintings reproduction are also created in these famous galleries.

In addition to above styles, there are many famous paintings of other subjects. These include famous war paintings, famous paintings of jesus, famous figure paintings, religious famous paintings, famous paintings romantic, famous battle paintings, famous military paintings, famous sunset paintings, famous paintings of women, famous paintings of love, famous water paintings, famous acrylic paintings, famous paintings of buildings, famous dance paintings, famous dragon paintings, famous black paintings, famous paintings in the fall, famous paintings of cats, famous paintings of children, famous paintings of friends, famous paintings of christinaity, famous paintings of jesus and famous paintings of humanity. There are also famous native American paintings and famous Spanish paintings.

',
					"post_category" =>	'delux-room',
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  CUSTOM_POST_TYPE_TEMPLATIC_ROOM
					);

					
insert_posts($post_info);
function insert_posts($post_info)
{
	global $wpdb,$current_user;
	for($i=0;$i<count($post_info);$i++)
	{
		$post_title = $post_info[$i]['post_title'];
		$post_count = $wpdb->get_var("SELECT count(ID) FROM $wpdb->posts where post_title like \"$post_title\" and post_type='".CUSTOM_POST_TYPE_TEMPLATIC_ROOM."' and post_status in ('publish','draft')");
		if(!$post_count)
		{
			$post_info_arr = array();
			$catids_arr = array();
			$my_post = array();
			$post_info_arr = $post_info[$i];
			if($post_info_arr['post_category'])
			{
				for($c=0;$c<count($post_info_arr['post_category']);$c++)
				{
					$catids_arr[] = get_cat_ID($post_info_arr['post_category'][$c]);
				}
			}else
			{
				$catids_arr[] = 1;
			}
			$my_post['post_title'] = @$post_info_arr['post_title'];
			$my_post['post_content'] = @$post_info_arr['post_content'];
			$my_post['post_type'] = @$post_info_arr['post_type'];
			if(@$post_info_arr['post_author'])
			{
				$my_post['post_author'] = $post_info_arr['post_author'];
			}else
			{
				$my_post['post_author'] = 1;
			}
			$my_post['post_status'] = 'publish';
			$my_post['post_category'] = $catids_arr;
			$my_post['tags_input'] = $post_info_arr['post_tags'];
			$last_postid = wp_insert_post( $my_post );
			
			$cat1 = get_term_by( 'slug', $post_info_arr['post_category'], CUSTOM_CATEGORY_TEMPLATIC_ROOM );
			$cats_id1 = $cat1->term_id;
			wp_set_post_terms( $last_postid, $cats_id1, CUSTOM_CATEGORY_TEMPLATIC_ROOM,false );
			
			$tag1 = get_term_by( 'slug', 'room', CUSTOM_TAG_TYPE_TEMPLATIC_TAG);
			$tag_id1 = $tag1->slug;
			wp_set_post_terms( $last_postid, $tag_id1, CUSTOM_TAG_TYPE_TEMPLATIC_TAG,false );
			
			$post_meta = $post_info_arr['post_meta'];
			update_post_meta($last_postid, 'tl_dummy_content',1);
			if($post_meta)
			{
				foreach($post_meta as $mkey=>$mval)
				{
					update_post_meta($last_postid, $mkey, $mval);
				}
			}
			
			$post_image = $post_info_arr['post_image'];
			if($post_image)
			{
				for($m=0;$m<count($post_image);$m++)
				{
					$menu_order = $m+1;
					$image_name_arr = explode('/',$post_image[$m]);
					$img_name = $image_name_arr[count($image_name_arr)-1];
					$img_name_arr = explode('.',$img_name);
					$post_img = array();
					$post_img['post_title'] = $img_name_arr[0];
					$post_img['post_status'] = 'inherit';
					$post_img['post_parent'] = $last_postid;
					$post_img['post_type'] = 'attachment';
					$post_img['post_mime_type'] = 'image/jpeg';
					$post_img['menu_order'] = $menu_order;
					$last_postimage_id = wp_insert_post( $post_img );
					update_post_meta($last_postimage_id, '_wp_attached_file', $post_image[$m]);					
					$post_attach_arr = array(
										"width"	=>	580,
										"height" =>	480,
										"hwstring_small"=> "height='150' width='150'",
										"file"	=> $post_image[$m],
										//"sizes"=> $sizes_info_array,
										);
					wp_update_attachment_metadata( $last_postimage_id, $post_attach_arr );
				}
			}
		}
	}
}
//====================================================================================//

// HOUSE ENTRY START
$post_info=array();
$image_array = array();
$post_meta = array();
$image_array[] = "dummy/house1.jpg" ;
$image_array[] = "dummy/house2.jpg" ;
$image_array[] = "dummy/house3.jpg" ;
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "home_contact_mail"  => 'mymail@mydomain.com',
				   "address" => '30 Mortensen Avenue, Salinas, CA',
				   "geo_latitude"		=> '36.66560281182198',		
				   "geo_longitude"		=> '-121.61211759999998',		
				   "home_contact1"  => '(831) 758-7214',
				   "booking_dummy_content"	  => '1',
				);
$post_info[] = array(
					"post_title" =>	'The Old Post Office',
					"post_content" =>	'Its our Fifth anniversary, and you are invited. Thanks to you all, we have progressed much in this time. Lets have a toast for this. 
					
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent aliquam,  justo convallis luctus rutrum, erat nulla fermentum diam, at nonummy quam  ante ac quam. Maecenas urna purus, fermentum id, molestie in, commodo  porttitor, felis. 
					
Edgar Allan Poe, author of "Annabel Lee" and "The Fall of the House of Usher" , is easily recognized as one of the foremost masters of horror and the macabre. His works have inspired terror and anxiety in many individuals, primarily through the use of heavy psychological tones, as opposed to the gore and blood themes used and abused by writers of his time. Poes collected works easily counts as some of the most frightening material ever written, especially now, in an age where horror movies are relegated to two hours of bloodshed and senseless violence, lacking any true horror and relying solely on shock value to appear "scary." Poe also stands out as being among the few who can make even the most mundane things seem utterly terrifying, a feat emulated by Stephen King and several Japanese horror authors, but never truly duplicated.

In a completely different vein of horror from his predecessors, and arguably creating a sub-genre of horror through his works, H. P. Lovecraft also stands out. His works, while lacking in humanity, are difficult to see as anything but terrifying, particularly because of the apparent lack of humanity in them. In contrast to writers of previous generations, Lovecraft focused more on the truly monstrous, ignoring the human element that most horror writers tended to insert into their works since the days of the Gothic era. His stories were littered with monsters that knew neither morality nor mercy, seeing humanity as insignificant insects and, in Lovecrafts malignant world of ancient races and Elder Gods, humanity was insignificant. He also brought back something from the Gothic horror era, showing his readers that knowledge, even just a little knowledge, can lead to the most terrifying of discoveries. This is perhaps best exemplified by the so-called "Cthulhu Mythos," a collection of stories that centered around Lovecrafts anti-mythological beings.

<h3>Frankenstein</h3>
<ol>
	<li>Among the most enduring horror classics in the world is that of Shelleys "Frankenstein," which combines the elements of horror with the intrinsic questions that plagued morality and philosophy at the time. </li>
	<li>In some ways, the story is one that puts a new spin on the old ghost story, in that the "ghost" is inevitably caused by the actions of mortal men who meddled in things they were not meant to. </li>
	<li>The story, aside from being a genuine tale of terror, also took on the role of a lesson in morality and the limits to just how far medical science could go.</li>
	<li>Prolonging life is one thing, but bringing back the dead is another thing entirely, which is one of the subtle messages of the novel.</li>
	<li>The underlying question of whether or not Frankensteins creature is the monster, or if it is Frankenstein himself, also contributes to making the story a memorable, chilling tale.</li>
</ol> 

However, very few stories can truly stand up against the pure terror and the subtle anxiety and dread caused by Bram Stokers infamous novel, "Dracula." The novel is a hallmark of the Gothic horror era, presenting a villain of potentially epic scope in the guise of a remarkable gentleman and nobleman. It deviated from other vampire stories of the time in that the vampire, Dracula, was not monstrous in appearance. He looked every inch a master and nobleman, establishing the "lord of the night" archetype that would be a stock image of vampire characters in literature for centuries to come. It also had all the elements necessary to both frighten readers and keep them coming back for more, marking it as the most enduring horror novel in history.

',
					"post_category" =>	array('houses'),
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  'house'
					);
					
					
$image_array = array();
$post_meta = array();
$image_array[] = "dummy/house4.jpg" ;
$image_array[] = "dummy/house5.jpg" ;
$image_array[] = "dummy/house1.jpg" ;
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "home_contact_mail"  => 'mymail@mydomain.com',
				   "address" => '30 Mortensen Avenue, Salinas, CA',
				   "geo_latitude"		=> '36.66560281182198',		
				   "geo_longitude"		=> '-121.61211759999998',		
				   "home_contact1"  => '(831) 758-7214',
				   "booking_dummy_content"	  => '1',
				);
$post_info[] = array(
					"post_title" =>	'The Old School',
					"post_content" =>	'Its our Fifth anniversary, and you are invited. Thanks to you all, we have progressed much in this time. Lets have a toast for this. 
					
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent aliquam,  justo convallis luctus rutrum, erat nulla fermentum diam, at nonummy quam  ante ac quam. Maecenas urna purus, fermentum id, molestie in, commodo  porttitor, felis. 
					
Edgar Allan Poe, author of "Annabel Lee" and "The Fall of the House of Usher" , is easily recognized as one of the foremost masters of horror and the macabre. His works have inspired terror and anxiety in many individuals, primarily through the use of heavy psychological tones, as opposed to the gore and blood themes used and abused by writers of his time. Poes collected works easily counts as some of the most frightening material ever written, especially now, in an age where horror movies are relegated to two hours of bloodshed and senseless violence, lacking any true horror and relying solely on shock value to appear "scary." Poe also stands out as being among the few who can make even the most mundane things seem utterly terrifying, a feat emulated by Stephen King and several Japanese horror authors, but never truly duplicated.

In a completely different vein of horror from his predecessors, and arguably creating a sub-genre of horror through his works, H. P. Lovecraft also stands out. His works, while lacking in humanity, are difficult to see as anything but terrifying, particularly because of the apparent lack of humanity in them. In contrast to writers of previous generations, Lovecraft focused more on the truly monstrous, ignoring the human element that most horror writers tended to insert into their works since the days of the Gothic era. His stories were littered with monsters that knew neither morality nor mercy, seeing humanity as insignificant insects and, in Lovecrafts malignant world of ancient races and Elder Gods, humanity was insignificant. He also brought back something from the Gothic horror era, showing his readers that knowledge, even just a little knowledge, can lead to the most terrifying of discoveries. This is perhaps best exemplified by the so-called "Cthulhu Mythos," a collection of stories that centered around Lovecrafts anti-mythological beings.

<h3>Frankenstein</h3>
<ol>
	<li>Among the most enduring horror classics in the world is that of Shelleys "Frankenstein," which combines the elements of horror with the intrinsic questions that plagued morality and philosophy at the time. </li>
	<li>In some ways, the story is one that puts a new spin on the old ghost story, in that the "ghost" is inevitably caused by the actions of mortal men who meddled in things they were not meant to. </li>
	<li>The story, aside from being a genuine tale of terror, also took on the role of a lesson in morality and the limits to just how far medical science could go.</li>
	<li>Prolonging life is one thing, but bringing back the dead is another thing entirely, which is one of the subtle messages of the novel.</li>
	<li>The underlying question of whether or not Frankensteins creature is the monster, or if it is Frankenstein himself, also contributes to making the story a memorable, chilling tale.</li>
</ol> 

However, very few stories can truly stand up against the pure terror and the subtle anxiety and dread caused by Bram Stokers infamous novel, "Dracula." The novel is a hallmark of the Gothic horror era, presenting a villain of potentially epic scope in the guise of a remarkable gentleman and nobleman. It deviated from other vampire stories of the time in that the vampire, Dracula, was not monstrous in appearance. He looked every inch a master and nobleman, establishing the "lord of the night" archetype that would be a stock image of vampire characters in literature for centuries to come. It also had all the elements necessary to both frighten readers and keep them coming back for more, marking it as the most enduring horror novel in history.

',
					"post_category" =>	array('houses'),
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  'house'
					);

$image_array = array();
$post_meta = array();
$image_array[] = "dummy/house2.jpg" ;
$image_array[] = "dummy/house3.jpg" ;
$image_array[] = "dummy/house4.jpg" ;
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "home_contact_mail"  => 'mymail@mydomain.com',
				   "address" => '30 Mortensen Avenue, Salinas, CA',
				   "geo_latitude"		=> '36.66560281182198',		
				   "geo_longitude"		=> '-121.61211759999998',		
				   "home_contact1"  => '(831) 758-7214',
				   "booking_dummy_content"	  => '1',
				);
$post_info[] = array(
					"post_title" =>	'Holly Cottage',
					"post_content" =>	'Its our Fifth anniversary, and you are invited. Thanks to you all, we have progressed much in this time. Lets have a toast for this. 
					
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent aliquam,  justo convallis luctus rutrum, erat nulla fermentum diam, at nonummy quam  ante ac quam. Maecenas urna purus, fermentum id, molestie in, commodo  porttitor, felis. 
					
Edgar Allan Poe, author of "Annabel Lee" and "The Fall of the House of Usher" , is easily recognized as one of the foremost masters of horror and the macabre. His works have inspired terror and anxiety in many individuals, primarily through the use of heavy psychological tones, as opposed to the gore and blood themes used and abused by writers of his time. Poes collected works easily counts as some of the most frightening material ever written, especially now, in an age where horror movies are relegated to two hours of bloodshed and senseless violence, lacking any true horror and relying solely on shock value to appear "scary." Poe also stands out as being among the few who can make even the most mundane things seem utterly terrifying, a feat emulated by Stephen King and several Japanese horror authors, but never truly duplicated.

In a completely different vein of horror from his predecessors, and arguably creating a sub-genre of horror through his works, H. P. Lovecraft also stands out. His works, while lacking in humanity, are difficult to see as anything but terrifying, particularly because of the apparent lack of humanity in them. In contrast to writers of previous generations, Lovecraft focused more on the truly monstrous, ignoring the human element that most horror writers tended to insert into their works since the days of the Gothic era. His stories were littered with monsters that knew neither morality nor mercy, seeing humanity as insignificant insects and, in Lovecrafts malignant world of ancient races and Elder Gods, humanity was insignificant. He also brought back something from the Gothic horror era, showing his readers that knowledge, even just a little knowledge, can lead to the most terrifying of discoveries. This is perhaps best exemplified by the so-called "Cthulhu Mythos," a collection of stories that centered around Lovecrafts anti-mythological beings.

<h3>Frankenstein</h3>
<ol>
	<li>Among the most enduring horror classics in the world is that of Shelleys "Frankenstein," which combines the elements of horror with the intrinsic questions that plagued morality and philosophy at the time. </li>
	<li>In some ways, the story is one that puts a new spin on the old ghost story, in that the "ghost" is inevitably caused by the actions of mortal men who meddled in things they were not meant to. </li>
	<li>The story, aside from being a genuine tale of terror, also took on the role of a lesson in morality and the limits to just how far medical science could go.</li>
	<li>Prolonging life is one thing, but bringing back the dead is another thing entirely, which is one of the subtle messages of the novel.</li>
	<li>The underlying question of whether or not Frankensteins creature is the monster, or if it is Frankenstein himself, also contributes to making the story a memorable, chilling tale.</li>
</ol> 

However, very few stories can truly stand up against the pure terror and the subtle anxiety and dread caused by Bram Stokers infamous novel, "Dracula." The novel is a hallmark of the Gothic horror era, presenting a villain of potentially epic scope in the guise of a remarkable gentleman and nobleman. It deviated from other vampire stories of the time in that the vampire, Dracula, was not monstrous in appearance. He looked every inch a master and nobleman, establishing the "lord of the night" archetype that would be a stock image of vampire characters in literature for centuries to come. It also had all the elements necessary to both frighten readers and keep them coming back for more, marking it as the most enduring horror novel in history.

',
					"post_category" =>	array('houses'),
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  'house'
					);
					
					
$image_array = array();
$post_meta = array();
$image_array[] = "dummy/house5.jpg" ;
$image_array[] = "dummy/house1.jpg" ;
$image_array[] = "dummy/house2.jpg" ;
$post_meta = array(
				   "include_tax" =>'Yes',
				   "home_capacity"	  => 4,
				   "home_contact_mail"  => 'mymail@mydomain.com',
				   "address" => '30 Mortensen Avenue, Salinas, CA',
				   "geo_latitude"		=> '36.66560281182198',		
				   "geo_longitude"		=> '-121.61211759999998',		
				   "home_contact1"  => '(831) 758-7214',
				   "booking_dummy_content"	  => '1',
				);
$post_info[] = array(
					"post_title" =>	'Willow Cottage',
					"post_content" =>	'Its our Fifth anniversary, and you are invited. Thanks to you all, we have progressed much in this time. Lets have a toast for this. 
					
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent aliquam,  justo convallis luctus rutrum, erat nulla fermentum diam, at nonummy quam  ante ac quam. Maecenas urna purus, fermentum id, molestie in, commodo  porttitor, felis. 
					
Edgar Allan Poe, author of "Annabel Lee" and "The Fall of the House of Usher" , is easily recognized as one of the foremost masters of horror and the macabre. His works have inspired terror and anxiety in many individuals, primarily through the use of heavy psychological tones, as opposed to the gore and blood themes used and abused by writers of his time. Poes collected works easily counts as some of the most frightening material ever written, especially now, in an age where horror movies are relegated to two hours of bloodshed and senseless violence, lacking any true horror and relying solely on shock value to appear "scary." Poe also stands out as being among the few who can make even the most mundane things seem utterly terrifying, a feat emulated by Stephen King and several Japanese horror authors, but never truly duplicated.

In a completely different vein of horror from his predecessors, and arguably creating a sub-genre of horror through his works, H. P. Lovecraft also stands out. His works, while lacking in humanity, are difficult to see as anything but terrifying, particularly because of the apparent lack of humanity in them. In contrast to writers of previous generations, Lovecraft focused more on the truly monstrous, ignoring the human element that most horror writers tended to insert into their works since the days of the Gothic era. His stories were littered with monsters that knew neither morality nor mercy, seeing humanity as insignificant insects and, in Lovecrafts malignant world of ancient races and Elder Gods, humanity was insignificant. He also brought back something from the Gothic horror era, showing his readers that knowledge, even just a little knowledge, can lead to the most terrifying of discoveries. This is perhaps best exemplified by the so-called "Cthulhu Mythos," a collection of stories that centered around Lovecrafts anti-mythological beings.

<h3>Frankenstein</h3>
<ol>
	<li>Among the most enduring horror classics in the world is that of Shelleys "Frankenstein," which combines the elements of horror with the intrinsic questions that plagued morality and philosophy at the time. </li>
	<li>In some ways, the story is one that puts a new spin on the old ghost story, in that the "ghost" is inevitably caused by the actions of mortal men who meddled in things they were not meant to. </li>
	<li>The story, aside from being a genuine tale of terror, also took on the role of a lesson in morality and the limits to just how far medical science could go.</li>
	<li>Prolonging life is one thing, but bringing back the dead is another thing entirely, which is one of the subtle messages of the novel.</li>
	<li>The underlying question of whether or not Frankensteins creature is the monster, or if it is Frankenstein himself, also contributes to making the story a memorable, chilling tale.</li>
</ol> 

However, very few stories can truly stand up against the pure terror and the subtle anxiety and dread caused by Bram Stokers infamous novel, "Dracula." The novel is a hallmark of the Gothic horror era, presenting a villain of potentially epic scope in the guise of a remarkable gentleman and nobleman. It deviated from other vampire stories of the time in that the vampire, Dracula, was not monstrous in appearance. He looked every inch a master and nobleman, establishing the "lord of the night" archetype that would be a stock image of vampire characters in literature for centuries to come. It also had all the elements necessary to both frighten readers and keep them coming back for more, marking it as the most enduring horror novel in history.

',
					"post_category" =>	'houses',
					"post_meta" =>	$post_meta,
					"post_image" =>	$image_array,
					"post_tags" =>	array('Tags','Sample Tags'),
					"post_type"	=>  'house'
					);


					
insert_posts1($post_info);
function insert_posts1($post_info)
{
	global $wpdb,$current_user;
	for($i=0;$i<count($post_info);$i++)
	{
		$post_title = $post_info[$i]['post_title'];
		$post_count = $wpdb->get_var("SELECT count(ID) FROM $wpdb->posts where post_title like \"$post_title\" and post_type='house' and post_status in ('publish','draft')");
		if(!$post_count)
		{
			$post_info_arr = array();
			$my_post = array();
			$post_info_arr = $post_info[$i];
			if($post_info_arr['post_category'])
			{
				//$catids_arr = get_term_by( 'name', $post_info_arr['post_category'], 'house_category' );
			}else
			{
				$catids_arr = 1;
			}
			//print_r($catids_arr);exit;
			$my_post['post_title'] = $post_info_arr['post_title'];
			$my_post['post_content'] = $post_info_arr['post_content'];
			$my_post['post_type'] = $post_info_arr['post_type'];
			if(@$post_info_arr['post_author'])
			{
				$my_post['post_author'] = $post_info_arr['post_author'];
			}else
			{
				$my_post['post_author'] = 1;
			}
			$my_post['post_status'] = 'publish';
			
			$term_id = @$term->term_id;
			$my_post['tags_input'] = @$post_info_arr['post_tags'];
			$last_postid = wp_insert_post( $my_post );
			$cat = get_term_by( 'slug', 'houses', 'house_category' );
			$cats_id = $cat->term_id;
			wp_set_post_terms( $last_postid, $cats_id, 'house_category',false );
			
			$tag1 = get_term_by( 'slug', 'house', 'house_tag' );
			$tag_id1 = $tag1->slug;
			wp_set_post_terms( $last_postid, $tag_id1, 'house_tag',false );
			
			$post_meta = $post_info_arr['post_meta'];
			if($post_meta)
			{
				foreach($post_meta as $mkey=>$mval)
				{
					update_post_meta($last_postid, $mkey, $mval);
				}
			}
			
			$post_image = $post_info_arr['post_image'];
			if($post_image)
			{
				for($m=0;$m<count($post_image);$m++)
				{
					$menu_order = $m+1;
					$image_name_arr = explode('/',$post_image[$m]);
					$img_name = $image_name_arr[count($image_name_arr)-1];
					$img_name_arr = explode('.',$img_name);
					$post_img = array();
					$post_img['post_title'] = $img_name_arr[0];
					$post_img['post_status'] = 'inherit';
					$post_img['post_parent'] = $last_postid;
					$post_img['post_type'] = 'attachment';
					$post_img['post_mime_type'] = 'image/jpeg';
					$post_img['menu_order'] = $menu_order;
					$last_postimage_id = wp_insert_post( $post_img );
					update_post_meta($last_postimage_id, '_wp_attached_file', $post_image[$m]);					
					$post_attach_arr = array(
										"width"	=>	580,
										"height" =>	480,
										"hwstring_small"=> "height='150' width='150'",
										"file"	=> $post_image[$m],
										//"sizes"=> $sizes_info_array,
										);
					wp_update_attachment_metadata( $last_postimage_id, $post_attach_arr );
				}
			}
		}
	}
}//HOUSE ENTRY FINISH.

//Set default permalink to postname start
global $wp_rewrite;
if($wp_rewrite){
	$wp_rewrite->set_permalink_structure( '/%postname%/' );
	$wp_rewrite->flush_rules();
	if(function_exists('flush_rewrite_rules')){
		flush_rewrite_rules(true);  
	}
}
//Set default permalink to postname end
?>
