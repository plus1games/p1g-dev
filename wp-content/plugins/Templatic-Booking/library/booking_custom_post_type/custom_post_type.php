<?php
//----------------------------------------------------------------------//
// Initiate the plugin to add custom post type
//----------------------------------------------------------------------//

//register custom post type on initialization when plugin active 
function templatic_booking_system_custom_post_type(){
//===============Booking system Listing SECTION START================
	$custom_post_type = CUSTOM_POST_TYPE_TEMPLATIC_ROOM;
	$custom_cat_type = CUSTOM_CATEGORY_TEMPLATIC_ROOM;
	$custom_tag_type = CUSTOM_TAG_TYPE_TEMPLATIC_TAG;

	//update the "Room" custom post type to the default tevolution custom post type options
	$post_arr_merge[$custom_post_type] = 
				array(	'label' 			=> CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
						'labels' 			=> array(	'name' 					=> 	CUSTOM_MENU_SIGULAR_NAME_ROOM,
														'singular_name' 		=> 	CUSTOM_MENU_SIGULAR_NAME_ROOM,
														'add_new' 				=>  CUSTOM_MENU_ADD_NEW_ROOM,
														'add_new_item' 			=>  CUSTOM_MENU_ADD_NEW_ITEM_ROOM,
														'edit' 					=>  CUSTOM_MENU_EDIT_ROOM,
														'edit_item' 			=>  CUSTOM_MENU_EDIT_ITEM_ROOM,
														'new_item' 				=>  CUSTOM_MENU_NEW_ROOM,
														'view_item'				=>  CUSTOM_MENU_VIEW_ROOM,
														'search_items' 			=>  CUSTOM_MENU_SEARCH_ROOM,
														'not_found' 			=>  CUSTOM_MENU_NOT_FOUND_ROOM,
														'not_found_in_trash' 	=>  CUSTOM_MENU_NOT_FOUND_TRASH_ROOM	),
						'public' 			=> true,
						'can_export'		=> true,
						'show_ui' 			=> true, // SHOW UI IN ADMIN PANEL /
						'_builtin' 			=> false, // IT IS A CUSTOM POST TYPE NOT BUILT IN //
						'_edit_link' 		=> 'post.php?post=%d',
						'capability_type' 	=> 'post',
						'menu_icon' 		=> TEMPLATIC_BOOKING_URL.'/images/favicon-active.png',
						'hierarchical' 		=> false,
						'rewrite' 			=> array("slug" => "$custom_post_type"), // PERMALINKS TO EVENT POST TYPE /
						'query_var' 		=> "$custom_post_type", // THIS GOES TO WPQUERY SCHEMA //
						'supports' 			=> array(	'title',
														'author', 
														'excerpt',
														'thumbnail',
														'comments',
														'editor', 
														'trackbacks',
														'custom-fields',
														'revisions') ,
						'show_in_nav_menus'	=> true ,
						'slugs'				=> array("$custom_cat_type","$custom_tag_type"),
						'taxonomies'		=> array(CUSTOM_MENU_SIGULAR_CAT_ROOM,CUSTOM_MENU_TAG_LABEL_ROOM)
					
				);
				$original = get_option('templatic_custom_post');
				if($original)
				{
					$post_arr_merge = array_merge($original,$post_arr_merge);
				}
				
				ksort($post_arr_merge);
				update_option('templatic_custom_post',$post_arr_merge);

	//update the room category options to the default tevolution custom taxonomy options
	$original = array();
	$taxonomy_arr_merge[$custom_cat_type] = 
				 
				array (	"hierarchical" 		=> true, 
						"label" 			=> CUSTOM_MENU_CAT_LABEL_ROOM, 
						"post_type"			=> $custom_post_type,
						"post_slug"			=> $custom_post_type,
						'labels' 			=> array(	'name' 				=>  CUSTOM_MENU_CAT_TITLE_ROOM,
														'singular_name' 	=>  $custom_cat_type,
														'search_items' 		=>  CUSTOM_MENU_CAT_SEARCH_ROOM,
														'popular_items' 	=>  CUSTOM_MENU_CAT_POPULAR_ROOM,
														'all_items' 		=>  CUSTOM_MENU_CAT_ALL_ROOM,
														'parent_item' 		=>  CUSTOM_MENU_CAT_PARENT_ROOM_CATEGORY,
														'parent_item_colon' =>  CUSTOM_MENU_CAT_PARENT_ROOM_CATEGORY_COL,
														'edit_item' 		=>  CUSTOM_MENU_CAT_EDIT_ROOM,
														'update_item'		=>  CUSTOM_MENU_CAT_UPDATE_ROOM,
														'add_new_item' 		=>  CUSTOM_MENU_CAT_ADDNEW_ROOM,
														'new_item_name' 	=>  CUSTOM_MENU_CAT_NEW_NAME_ROOM,	), 
						'public' 			=> true,
						'show_ui' 			=> true,
						"rewrite" 			=> true	
				);
				$original = get_option('templatic_custom_taxonomy');
				if($original)
				{
					$taxonomy_arr_merge = array_merge($original,$taxonomy_arr_merge);
				}
				//register_taxonomy($custom_cat_type,array($custom_post_type),$taxonomy_arr_merge[$custom_cat_type]);
				ksort($taxonomy_arr_merge);
				update_option('templatic_custom_taxonomy',$taxonomy_arr_merge);


	//update the room tags options to the default tevolution custom tags options
	$tag_arr_merge = array();
	$tag_arr_merge[$custom_tag_type] =
				array(	"hierarchical" 		=> false, 
						"label" 			=> CUSTOM_MENU_TAG_LABEL_ROOM, 
						"post_type"			=> $custom_post_type,
						"post_slug"			=> $custom_post_type,
						'labels' 			=> array(	'name' 				=>  CUSTOM_MENU_TAG_TITLE_ROOM,
														'singular_name' 	=>  $custom_tag_type,
														'search_items' 		=>  CUSTOM_MENU_TAG_SEARCH_ROOM,
														'popular_items' 	=>  CUSTOM_MENU_TAG_POPULAR_ROOM,
														'all_items' 		=>  CUSTOM_MENU_TAG_ALL_ROOM,
														'parent_item' 		=>  CUSTOM_MENU_TAG_PARENT_ROOM,
														'parent_item_colon' =>  CUSTOM_MENU_TAG_PARENT_COL_ROOM,
														'edit_item' 		=>  CUSTOM_MENU_TAG_EDIT_ROOM,
														'update_item'		=>  CUSTOM_MENU_TAG_UPDATE_ROOM,
														'add_new_item' 		=>  CUSTOM_MENU_TAG_ADD_NEW_ROOM,
														'new_item_name' 	=>  CUSTOM_MENU_TAG_NEW_NAME_ROOM,	),  
						'public' 			=> true,
						'show_ui' 			=> true,
						"rewrite" 			=> true	
				);
				$original = get_option('templatic_custom_tags');
				if($original)
				{
					$tag_arr_merge = array_merge($original,$tag_arr_merge);
				}
				ksort($tag_arr_merge);
				update_option('templatic_custom_tags',$tag_arr_merge);
				

	add_action( 'init', 'create_custom_taxonomy_room' );
	function create_custom_taxonomy_room() {
	
		$args = get_option('templatic_custom_taxonomy');
		if($args): 
			foreach($args as $key=> $_args)
			{
				//register_taxonomy($_args['labels']['singular_name'],array($_args['post_type']),$args[$key]);
			}
		endif;
	}			

	
	// CREATED HOUSE TAXONOMY
	$custom_post_type1 = CUSTOM_POST_TYPE_TEMPLATIC_HOUSE;
	$custom_cat_type1 = CUSTOM_CATEGORY_TEMPLATIC_HOUSE;
	$custom_tag_type1 = CUSTOM_TAG_TYPE_TEMPLATIC_TAG_HOUSE;

	//update the "Room" custom post type to the default tevolution custom post type options
	$post_arr_merge[$custom_post_type1] = 
				array(	'label' 			=> CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
						'labels' 			=> array(	'name' 					=> 	CUSTOM_MENU_SIGULAR_NAME_HOUSE,
														'singular_name' 		=> 	CUSTOM_MENU_SIGULAR_NAME_HOUSE,
														'add_new' 				=>  CUSTOM_MENU_ADD_NEW_HOUSE,
														'add_new_item' 			=>  CUSTOM_MENU_ADD_NEW_ITEM_HOUSE,
														'edit' 					=>  CUSTOM_MENU_EDIT_HOUSE,
														'edit_item' 			=>  CUSTOM_MENU_EDIT_ITEM_HOUSE,
														'new_item' 				=>  CUSTOM_MENU_NEW_HOUSE,
														'view_item'				=>  CUSTOM_MENU_VIEW_HOUSE,
														'search_items' 			=>  CUSTOM_MENU_SEARCH_HOUSE,
														'not_found' 			=>  CUSTOM_MENU_NOT_FOUND_HOUSE,
														'not_found_in_trash' 	=>  CUSTOM_MENU_NOT_FOUND_TRASH_HOUSE	),
						'public' 			=> true,
						'can_export'		=> true,
						'show_ui' 			=> true, // SHOW UI IN ADMIN PANEL /
						'_builtin' 			=> false, // IT IS A CUSTOM POST TYPE NOT BUILT IN //
						'_edit_link' 		=> 'post.php?post=%d',
						'capability_type' 	=> 'post',
						'menu_icon' 		=> TEMPLATIC_BOOKING_URL.'/images/favicon-active.png',
						'hierarchical' 		=> false,
						'rewrite' 			=> array("slug" => "$custom_post_type1"), // PERMALINKS TO EVENT POST TYPE /
						'query_var' 		=> "$custom_post_type1", // THIS GOES TO WPQUERY SCHEMA //
						'supports' 			=> array(	'title',
														'author', 
														'excerpt',
														'thumbnail',
														'comments',
														'editor', 
														'trackbacks',
														'custom-fields',
														'revisions') ,
						'show_in_nav_menus'	=> true ,
						'slugs'				=> array("$custom_cat_type1","$custom_tag_type1"),
						'taxonomies'		=> array(CUSTOM_MENU_SIGULAR_CAT_HOUSE,CUSTOM_MENU_TAG_LABEL_HOUSE)
					
				);
				$original = get_option('templatic_custom_post');
				if($original)
				{
					$post_arr_merge = array_merge($original,$post_arr_merge);
				}
				
				ksort($post_arr_merge);
				update_option('templatic_custom_post',$post_arr_merge);

	//update the room category options to the default tevolution custom taxonomy options
	$original = array();
	$taxonomy_arr_merge[$custom_cat_type1] = 
				 
				array (	"hierarchical" 		=> true, 
						"label" 			=> CUSTOM_MENU_CAT_LABEL_HOUSE, 
						"post_type"			=> $custom_post_type1,
						"post_slug"			=> $custom_post_type1,
						'labels' 			=> array(	'name' 				=>  CUSTOM_MENU_CAT_TITLE_HOUSE,
														'singular_name' 	=>  $custom_cat_type1,
														'search_items' 		=>  CUSTOM_MENU_CAT_SEARCH_HOUSE,
														'popular_items' 	=>  CUSTOM_MENU_CAT_POPULAR_HOUSE,
														'all_items' 		=>  CUSTOM_MENU_CAT_ALL_HOUSE,
														'parent_item' 		=>  CUSTOM_MENU_CAT_PARENT_HOUSE_CATEGORY,
														'parent_item_colon' =>  CUSTOM_MENU_CAT_PARENT_HOUSE_CATEGORY_COL,
														'edit_item' 		=>  CUSTOM_MENU_CAT_EDIT_HOUSE,
														'update_item'		=>  CUSTOM_MENU_CAT_UPDATE_HOUSE,
														'add_new_item' 		=>  CUSTOM_MENU_CAT_ADDNEW_HOUSE,
														'new_item_name' 	=>  CUSTOM_MENU_CAT_NEW_NAME_HOUSE,	), 
						'public' 			=> true,
						'show_ui' 			=> true,
						"rewrite" 			=> true	
				);
				$original = get_option('templatic_custom_taxonomy');
				if($original)
				{
					$taxonomy_arr_merge = array_merge($original,$taxonomy_arr_merge);
				}
				//register_taxonomy($custom_cat_type1,array($custom_post_type1),$taxonomy_arr_merge[$custom_cat_type1]);
				ksort($taxonomy_arr_merge);
				update_option('templatic_custom_taxonomy',$taxonomy_arr_merge);


	//update the room tags options to the default tevolution custom tags options
	$tag_arr_merge = array();
	$tag_arr_merge[$custom_tag_type1] =
				array(	"hierarchical" 		=> false, 
						"label" 			=> CUSTOM_MENU_TAG_LABEL_HOUSE, 
						"post_type"			=> $custom_post_type1,
						"post_slug"			=> $custom_post_type1,
						'labels' 			=> array(	'name' 				=>  CUSTOM_MENU_TAG_TITLE_HOUSE,
														'singular_name' 	=>  $custom_tag_type1,
														'search_items' 		=>  CUSTOM_MENU_TAG_SEARCH_HOUSE,
														'popular_items' 	=>  CUSTOM_MENU_TAG_POPULAR_HOUSE,
														'all_items' 		=>  CUSTOM_MENU_TAG_ALL_HOUSE,
														'parent_item' 		=>  CUSTOM_MENU_TAG_PARENT_HOUSE,
														'parent_item_colon' =>  CUSTOM_MENU_TAG_PARENT_COL_HOUSE,
														'edit_item' 		=>  CUSTOM_MENU_TAG_EDIT_HOUSE,
														'update_item'		=>  CUSTOM_MENU_TAG_UPDATE_HOUSE,
														'add_new_item' 		=>  CUSTOM_MENU_TAG_ADD_NEW_HOUSE,
														'new_item_name' 	=>  CUSTOM_MENU_TAG_NEW_NAME_HOUSE,	),  
						'public' 			=> true,
						'show_ui' 			=> true,
						"rewrite" 			=> true	
				);
				$original = get_option('templatic_custom_tags');
				if($original)
				{
					$tag_arr_merge = array_merge($original,$tag_arr_merge);
				}
				ksort($tag_arr_merge);
				update_option('templatic_custom_tags',$tag_arr_merge);
				

	add_action( 'init', 'create_custom_taxonomy_houses' );
	function create_custom_taxonomy_houses() {
	
		$args = get_option('templatic_custom_taxonomy');
		if($args): 
			foreach($args as $key=> $_args)
			{
				//register_taxonomy($_args['labels']['singular_name'],array($_args['post_type']),$args[$key]);
			}
		endif;
	}			

	
	// CREATED BOOKING TAXONOMY
	$custom_post_type2 = CUSTOM_POST_TYPE_TEMPLATIC_BOOKING;
	$custom_cat_type2 = CUSTOM_CATEGORY_TEMPLATIC_BOOKING;
	$custom_tag_type2 = CUSTOM_TAG_TEMPLATIC_BOOKING;
	
	//update the "Room" custom post type to the default tevolution custom post type options
	$post_arr_merge[$custom_post_type2] = 
				array(	'label' 			=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
						'labels' 			=> array(	'name' 					=> 	CUSTOM_MENU_SIGULAR_NAME_BOOKING,
														'singular_name' 		=> 	CUSTOM_MENU_SIGULAR_NAME_BOOKING,
														'add_new' 				=>  CUSTOM_MENU_ADD_NEW_BOOKING,
														'add_new_item' 			=>  CUSTOM_MENU_ADD_NEW_ITEM_BOOKING,
														'edit' 					=>  CUSTOM_MENU_EDIT_BOOKING,
														'edit_item' 			=>  CUSTOM_MENU_EDIT_ITEM_BOOKING,
														'new_item' 				=>  CUSTOM_MENU_NEW_BOOKING,
														'view_item'				=>  CUSTOM_MENU_VIEW_BOOKING,
														'search_items' 			=>  CUSTOM_MENU_SEARCH_BOOKING,
														'not_found' 			=>  CUSTOM_MENU_NOT_FOUND_BOOKING,
														'not_found_in_trash' 	=>  CUSTOM_MENU_NOT_FOUND_TRASH_BOOKING	),
						'public' 			=> true,
						'can_export'		=> true,
						'show_ui' 			=> true, // SHOW UI IN ADMIN PANEL /
						'_builtin' 			=> false, // IT IS A CUSTOM POST TYPE NOT BUILT IN //
						'_edit_link' 		=> 'post.php?post=%d',
						'capability_type' 	=> 'post',
						'menu_icon' 		=> TEMPLATIC_BOOKING_URL.'/images/favicon-active.png',
						'hierarchical' 		=> false,
						'rewrite' 			=> array("slug" => "$custom_post_type2"), // PERMALINKS TO EVENT POST TYPE /
						'query_var' 		=> "$custom_post_type2", // THIS GOES TO WPQUERY SCHEMA //
						'supports' 			=> array(	'title',
														'author', 
														'excerpt',
														'thumbnail',
														'comments',
														'editor', 
														'trackbacks',
														'custom-fields',
														'revisions') ,
						'show_in_nav_menus'	=> true ,
						'slugs'				=> array("$custom_cat_type2","$custom_tag_type2"),
						'taxonomies'		=> array(CUSTOM_MENU_SIGULAR_CAT_BOOKING,CUSTOM_MENU_TAG_TITLE_BOOKING)
					
				);
				$original = get_option('templatic_custom_post');
				if($original)
				{
					$post_arr_merge = array_merge($original,$post_arr_merge);
				}
				
				ksort($post_arr_merge);
				update_option('templatic_custom_post',$post_arr_merge);

	//update the room category options to the default tevolution custom taxonomy options
	$original = array();
	$taxonomy_arr_merge[$custom_cat_type2] = 
				 
				array (	"hierarchical" 		=> true, 
						"label" 			=> CUSTOM_MENU_CAT_LABEL_BOOKING, 
						"post_type"			=> $custom_post_type2,
						"post_slug"			=> $custom_post_type2,
						'labels' 			=> array(	'name' 				=>  CUSTOM_MENU_CAT_TITLE_BOOKING,
														'singular_name' 	=>  $custom_cat_type2,
														'search_items' 		=>  CUSTOM_MENU_CAT_SEARCH_BOOKING,
														'popular_items' 	=>  CUSTOM_MENU_CAT_POPULAR_BOOKING,
														'all_items' 		=>  CUSTOM_MENU_CAT_ALL_BOOKING,
														'parent_item' 		=>  CUSTOM_MENU_CAT_PARENT_BOOKING_CATEGORY,
														'parent_item_colon' =>  CUSTOM_MENU_CAT_PARENT_BOOKING_CATEGORY_COL,
														'edit_item' 		=>  CUSTOM_MENU_CAT_EDIT_BOOKING,
														'update_item'		=>  CUSTOM_MENU_CAT_UPDATE_BOOKING,
														'add_new_item' 		=>  CUSTOM_MENU_CAT_ADDNEW_BOOKING,
														'new_item_name' 	=>  CUSTOM_MENU_CAT_NEW_NAME_BOOKING,	), 
						'public' 			=> true,
						'show_ui' 			=> true,
						"rewrite" 			=> true	
				);
				$original = get_option('templatic_custom_taxonomy');
				if($original)
				{
					$taxonomy_arr_merge = array_merge($original,$taxonomy_arr_merge);
				}
				//register_taxonomy($custom_cat_type1,array($custom_post_type1),$taxonomy_arr_merge[$custom_cat_type1]);
				ksort($taxonomy_arr_merge);
				update_option('templatic_custom_taxonomy',$taxonomy_arr_merge);

	


	$tag_arr_merge = array();
		$tag_arr_merge[$custom_tag_type2] =
					array(	"hierarchical" 		=> false, 
							"label" 			=> CUSTOM_MENU_TAG_LABEL_BOOKING, 
							"post_type"			=> $custom_post_type2,
							"post_slug"			=> $custom_post_type2,
							'labels' 			=> array(	'name' 				=>  CUSTOM_MENU_TAG_TITLE_BOOKING,
															'singular_name' 	=>  $custom_tag_type2,
															'search_items' 		=>  CUSTOM_MENU_TAG_SEARCH_BOOKING,
															'popular_items' 	=>  CUSTOM_MENU_TAG_POPULAR_BOOKING,
															'all_items' 		=>  CUSTOM_MENU_TAG_ALL_BOOKING,
															'parent_item' 		=>  CUSTOM_MENU_TAG_PARENT_BOOKING,
															'parent_item_colon' =>  CUSTOM_MENU_TAG_PARENT_COL_BOOKING,
															'edit_item' 		=>  CUSTOM_MENU_TAG_EDIT_BOOKING,
															'update_item'		=>  CUSTOM_MENU_TAG_UPDATE_BOOKING,
															'add_new_item' 		=>  CUSTOM_MENU_TAG_ADD_NEW_BOOKING,
															'new_item_name' 	=>  CUSTOM_MENU_TAG_NEW_NAME_BOOKING,	),  
							'public' 			=> true,
							'show_ui' 			=> true,
							"rewrite" 			=> true	
					);
					$original = get_option('templatic_custom_tags');
					if($original)
					{
						$tag_arr_merge = array_merge($original,$tag_arr_merge);
					}
					ksort($tag_arr_merge);
					update_option('templatic_custom_tags',$tag_arr_merge);

				
				
				

	add_action( 'init', 'create_custom_taxonomy_booking' );
	function create_custom_taxonomy_booking() {
	
		$args = get_option('templatic_custom_taxonomy');
		if($args): 
			foreach($args as $key=> $_args)
			{
				//register_taxonomy($_args['labels']['singular_name'],array($_args['post_type']),$args[$key]);
			}
		endif;
	}			
	
	/* remove_filter( 'manage_edit-'.$CUSTOM_POST_TYPE_TEMPLATIC_BOOKING.'_columns', 'templatic_edit_taxonomy_columns') ;
	//add_action( 'manage_'.$post_type.'_posts_custom_column', 'templatic_manage_taxonomy_columns', 10, 2 );
	remove_action('manage_posts_custom_column','templatic_manage_taxonomy_columns'); */
	
	
}
if(!function_exists('templatic_booking_remove')){
function booking_column()
{
	//Custom Columns to be display in the manage room list page
	add_filter("manage_edit-".CUSTOM_POST_TYPE_TEMPLATIC_BOOKING."_columns", "modify_templatic_booking_booking_post_table_row", 12,2);
	
	function modify_templatic_booking_booking_post_table_row($columns){
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Customer Name',BKG_DOMAIN ),
			'room_house' => __( 'Room/House',BKG_DOMAIN ),
			'service_name' => __( 'Services',BKG_DOMAIN ),
			'check_in_date' => __( 'Check-In Date'.BKG_DOMAIN ),
			'check_out_date' => __( 'Check-Out Date',BKG_DOMAIN ),
			'booking_status' => __( 'Status',BKG_DOMAIN ),
		);
	  return $columns;
		
	}
	
	add_filter("manage_edit-".CUSTOM_CATEGORY_TEMPLATIC_BOOKING."_columns", "modify_templatic_booking_services_post_table_row");
		function modify_templatic_booking_services_post_table_row($columns) {
			$columns = array(
				'cb' => '<input type="checkbox" />',
				'name' => __('Service'),
				'price' =>  __('Price'),
				'description' => __('Description'),
				'slug' => __('Slug'),
				'posts' => __('Bookings')
				);
			return $columns;
		}
	//Get the values of particular columns
	add_action('manage_posts_custom_column','templatic_booking_booking_custom_columns',10,2);
	function templatic_booking_booking_custom_columns($column){
		global $post,$wpdb;
		if(isset($_REQUEST['post_ID']))
			$post_id=$_REQUEST['post_ID'];
		$taxonomy ='';
		$post_type = @$_REQUEST['post_type'];
		$custom_post_types_args = array();  
		$custom_post_types = get_post_types($custom_post_types_args,'objects');
		if  ($custom_post_types) {
			 foreach ($custom_post_types as $content_type) {
			 
				if($content_type->name == $post_type){
				$taxonomy = @$content_type->slugs[0];
				$tags = @$content_type->slugs[1]; break;
				}
			
		  }
		} 
		switch ($column){
			case 'room_house':
				$room_house_id = get_post_meta($post->ID,'property_room_id',true);
				echo get_the_title($room_house_id);
			break;
			case 'service_name':
				$templ_events = get_the_terms($post_id,$taxonomy);
				if (is_array($templ_events)) {
					foreach($templ_events as $key => $templ_event) {
						$edit_link = home_url()."/wp-admin/edit.php?".$taxonomy."=".$templ_event->slug."&post_type=".$post_type;
						$templ_events[$key] = '<a href="'.$edit_link.'">' . $templ_event->name . '</a>';
						}
					echo implode(' , ',$templ_events);
				}else {
					_e( 'Uncategorized',BKG_DOMAIN );
				}
			break;
			case 'check_in_date':
				echo date_i18n(get_option('date_format'),strtotime(get_post_meta($post->ID,'checkin_date',true)));
				break;
			case 'check_out_date':
				echo date_i18n(get_option('date_format'),strtotime(get_post_meta($post->ID,'checkout_date',true)));
				break;
			case 'booking_status':
				echo get_post_meta($post->ID,'status',true);
				break;
			default :
				break;
		}
		
	}
	
	
	//HOUSE CATEGORY COLUMNS START.
		add_filter("manage_edit-".CUSTOM_CATEGORY_TEMPLATIC_HOUSE."_columns", "modify_templatic_house_category");
			function modify_templatic_house_category($columns) {
				$columns = array(
					'cb' => '<input type="checkbox" />',
					'name' => __('Name'),
					'description' => __('Description'),
					'slug' => __('Slug'),
					'posts' => __('Houses')
					);
				return $columns;
			}
		//Get the values of particular columns
	//HOUSE CATEGORY COLUMNS FINISH.
	remove_action('house_category_edit_form_fields','category_custom_fields_Edit');
	remove_action('room_category_edit_form_fields','category_custom_fields_Edit');
}
}


/*
 * Function Name: room_manage_edit_columns
 * Remove price package and transaction status on room and house post type column
 */
add_filter('tevolution_change_edit-room_columns','room_manage_edit_columns');
add_filter('tevolution_change_edit-house_columns','room_manage_edit_columns');
function room_manage_edit_columns($column){
	
	unset($column['price_package']);
	unset($column['tran_status']);
	return $column;
}

/*
 * Function Name: digitaldownload_transaction_column_fields
 * return: unset the price package column and expire date on digital download plugin
 */

add_filter('transaction_column_fields','booking_system_transaction_column_fields');
function booking_system_transaction_column_fields($columns){
	unset($columns['package']);
	unset($columns['exp_date']);
	return $columns;
}
?>