<?php
require_once(TEMPLATIC_BOOKING_PATH."/library/includes/booking_category_custom_field.php");
require_once(TEMPLATIC_BOOKING_PATH."/library/includes/booking_custom_fields_function.php");
require_once(TEMPLATIC_BOOKING_PATH."/library/includes/shortcodes_booking_system.php");
include_once(TEMPLATIC_BOOKING_PATH.'/templatic_booking_system_main.php');

add_action('widgets_init', 'register_booking_widget');
if(!function_exists('register_booking_widget')){
function register_booking_widget() {
	include(TEMPLATIC_BOOKING_PATH."/library/includes/templatic_booking_widget.php");
}
}
/**-- condition for activate booking system --**/
if(@$_REQUEST['activated'] == 'templatic_booking_system' && @$_REQUEST['true']==1){
	if(!is_plugin_active('Templatic-Booking/templatic_booking.php'))
	{		
		$current = get_option( 'active_plugins' );
	     $plugin = plugin_basename( trim( 'Templatic-Booking/templatic_booking.php ') );	
	     if ( !in_array( $plugin, $current ) ) {
		   $current[] = $plugin;
		   sort( $current );		  
		   update_option( 'active_plugins', $current );		  
	     }
	}
	
	update_option('templatic_booking_system','Active');
	update_option('monetization','Active');
}elseif( @$_REQUEST['deactivate'] == 'templatic_booking_system' && @$_REQUEST['true']==0 ){
	delete_option('templatic_booking_system');
	if(is_plugin_active('Templatic-Booking/templatic_booking.php')){
		deactivate_plugins( 'Templatic-Booking/templatic_booking.php' );
	}
}
/*
 * Add The custom categories fields
 */


$taxnow = 'services';
if(isset($taxnow) && $taxnow== 'services') 
{
	add_action($taxnow.'_edit_form_fields','booking_category_custom_fields_EditFields');
	add_action($taxnow.'_add_form_fields','booking_category_custom_fields_AddFieldsAction');
	add_action('edited_term','booking_category_custom_fields_AlterFields');
	add_action('created_'.$taxnow,'booking_category_custom_fields_AlterFields');		
	
	//manage service price columns
	add_filter('manage_edit-'.$taxnow.'_columns', 'booking_service_categories_columns');	
	add_filter('manage_'.$taxnow.'_custom_column', 'booking_manage_service_columns', 20, 7);
}
if(!function_exists('booking_system_setup')){
	function booking_system_setup(){
		global $wp_query,$wpdb;
		//currency and currency symbol
		
		if(!get_option('currency_symbol') || !get_option('currency_code')  || !get_option('currency_pos'))
		{
			update_option('currency_symbol','$');
			update_option('currency_code', 'USD');	
			update_option('currency_pos',1);	
		}
		/*$arg=array(
			'post_type'  => 'custom_fields',			
		);
		query_posts($arg);*/
		
		
		$settings = get_option( "templatic_settings" );
		
		//SET THE DEFAULT CATEGORY WISE CUSTOM FILED OPTION TO NO START.
			$settings = get_option( "templatic_settings" );
			//$settings['templatic-category_custom_fields'] = "No";
			//update_option('templatic_settings',$settings);
		//SET THE DEFAULT CATEGORY WISE CUSTOM FILED OPTION TO NO FINISH.
		
		//General Settings Start
		if($settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_amount']){
			$tax_amount = $settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_amount'];
		}else{
			$tax_amount = 10.00;
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_type']){
			$tax_type = $settings['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_type'];
		}else{
			$tax_type = "percent";
		}
		$general_settings_array = array();
		$general_settings_array = array(
										  "tax_amount" => $tax_amount,
										  "tax_type" => $tax_type,
									  );
		$settings["templatic_booking_system_settings"]['templatic_booking_general_settings'] = $general_settings_array;
		update_option('templatic_settings',$settings);
		
		//Notification Settings Start
		if($settings['templatic_booking_system_settings']['templatic_booking_notification_settings']['templatic_booking_success_subject']){
			$templatic_booking_success_subject = $settings['templatic_booking_system_settings']['templatic_booking_notification_settings']['templatic_booking_success_subject'];
		}else{
			$templatic_booking_success_subject = BOOKING_SUCCESS_SUBJECT;
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_notification_settings']['templatic_booking_success_message']){
			$templatic_booking_success_message = $settings['templatic_booking_system_settings']['templatic_booking_notification_settings']['templatic_booking_success_message'];
		}else{
			$templatic_booking_success_message = '<p>'.__('Congratulations, your booking request is confirmed.',BKG_DOMAIN).'<p><p>'.__('In case you have any queries, please email us at',BKG_DOMAIN).' [CONTACT_MAIL]'.__(' by quoting the transaction number. We will be glad to assist you.',BKG_DOMAIN).'</p>';
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_notification_settings']['prebank_message']){
			$prebank_message = $settings['templatic_booking_system_settings']['templatic_booking_notification_settings']['prebank_message'];
		}else{
			$prebank_message = PRE_BANK_PAYMENT_MSG;
		}
		$notification_settings_array = array();
		$notification_settings_array = array("templatic_booking_success_subject" => $templatic_booking_success_subject,
										"templatic_booking_success_message" => $templatic_booking_success_message,
										"prebank_message" => $prebank_message,
									  );
		$settings["templatic_booking_system_settings"]['templatic_booking_notification_settings'] = $notification_settings_array;
		update_option('templatic_settings',$settings);
		
		
		//Email Settings Start
		if($settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_request_sent_subject']){
			$booking_request_sent_subject = $settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_request_sent_subject'];
		}else{
			$booking_request_sent_subject = BOOKING_REQUEST_SENT_SUBJECT;
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_request_sent_msg']){
			$booking_request_sent_msg = $settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_request_sent_msg'];
		}else{
			$booking_request_sent_msg = BOOKING_REQUEST_SENT_MSG;
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_sub']){
			$booking_confirm_success_sub = $settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_sub'];
		}else{
			$booking_confirm_success_sub = BOOKING_REQUEST_CONFIRM_SUCCESS_SUBJECT;
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_msg']){
			$booking_confirm_success_msg = $settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_msg'];
		}else{
			$booking_confirm_success_msg = BOOKING_REQUEST_CONFIRM_SUCCESS_MSG;
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_sub']){
			$booking_reject_success_sub = $settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_sub'];
		}else{
			$booking_reject_success_sub = BOOKING_REQUEST_REJECT_SUBJECT;
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_msg']){
			$booking_reject_success_msg = $settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_msg'];
		}else{
			$booking_reject_success_msg = BOOKING_REQUEST_REJECT_MSG;
		}
		
		
		if($settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_sub']){
			$booking_cancel_success_sub = $settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_sub'];
		}else{
			$booking_cancel_success_sub = BOOKING_CANCEL_MSG_MAIL;
		}
		if($settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_msg']){
			$booking_cancel_success_msg = $settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_msg'];
		}else{
			$booking_cancel_success_msg = BOOK_PROCESS_CANCEL_MAIL;
		}
		
		$email_settings_array = array();
		$email_settings_array = array("booking_request_sent_subject" => $booking_request_sent_subject,
										"booking_request_sent_msg" => $booking_request_sent_msg,
										"booking_confirm_success_sub" => $booking_confirm_success_sub,
										"booking_confirm_success_msg" => $booking_confirm_success_msg,
										"booking_reject_success_sub" => $booking_reject_success_sub,
										"booking_reject_success_msg" => $booking_reject_success_msg,
										"booking_cancel_success_sub" => $booking_cancel_success_sub,
										"booking_cancel_success_msg" => $booking_cancel_success_msg,
									  );
		$settings["templatic_booking_system_settings"]['templatic_booking_email_settings'] = $email_settings_array;
		update_option('templatic_settings',$settings);				
		
		/* Insert Post heading type into posts */
		 $taxonomy_name = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_title = '[#taxonomy_name#]' and $wpdb->posts.post_type = 'custom_fields'");
		 if(count($taxonomy_name) != 0)
		 {
			 $post_type=get_post_meta($taxonomy_name->ID, 'post_type',true );
				if(!strstr($post_type,CUSTOM_POST_TYPE_TEMPLATIC_ROOM))
					update_post_meta($taxonomy_name->ID, 'post_type',$post_type.','.CUSTOM_POST_TYPE_TEMPLATIC_ROOM.','.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE.','.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING );
						
				update_post_meta($taxonomy_name->ID, 'post_type_room',CUSTOM_POST_TYPE_TEMPLATIC_ROOM );
				update_post_meta($taxonomy_name->ID, 'taxonomy_type_room_category',CUSTOM_CATEGORY_TEMPLATIC_ROOM );
				
				update_post_meta($taxonomy_name->ID, 'post_type_house',CUSTOM_POST_TYPE_TEMPLATIC_HOUSE);
				update_post_meta($taxonomy_name->ID, 'taxonomy_type_house_category',CUSTOM_CATEGORY_TEMPLATIC_HOUSE );
				
				update_post_meta($taxonomy_name->ID, 'post_type_services','services' );
				update_post_meta($taxonomy_name->ID, 'taxonomy_type_services','services' );
			 
		 }
		 
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'include_tax' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) == 0){
			
			
			$my_post = array(
				 'post_title' => 'Charge Tax?',
				 'post_content' => 'If enabled &rsquo;Yes&rsquo;, tax amount will be added in the final payable amount at booking page.',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'include_tax',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/				
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			
			$field_post_type = CUSTOM_POST_TYPE_TEMPLATIC_ROOM.",".CUSTOM_POST_TYPE_TEMPLATIC_HOUSE;
			$post_meta = array(
				'post_type'=> $field_post_type,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_ROOM => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'ctype'=>'radio',
				'option_values'=>'Yes,No',
				'htmlvar_name'=>'include_tax',
				'sort_order' => '1',
				'is_active' => '1',
				'is_require' => '0',
				'show_on_page' => 'admin_side',
				'show_in_column' => '1',
				'validation_type' => '',
				'field_require_desc' => '',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '1',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '1',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_ROOM => CUSTOM_CATEGORY_TEMPLATIC_ROOM,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_ROOM => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
			
		
		$option_values_rooms = '1,2,3,4,5,6,7,8,9,10';
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'no_of_rooms' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Number Of Rooms',
				 'post_content' => 'Number of rooms',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'no_of_rooms',
				 'post_type' => "custom_fields",
				);
				
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
				'post_type_room'=> CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
				'ctype'=>'select',
				'option_title'=>$option_values_rooms,
				'option_values'=>$option_values_rooms,
				'htmlvar_name'=>'no_of_rooms',
				'sort_order' => '3',
				'is_active' => '1',
				'is_require' => '0',
				'show_on_page' => 'both_side',
				'show_in_column' => '1',
				'validation_type' => '',
				'field_require_desc' => '',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '1',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '1',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_ROOM => CUSTOM_CATEGORY_TEMPLATIC_ROOM,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_ROOM => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		 
		
		 
		$option_values1 = '1,2,3,4,5';
		$post_content = $wpdb->get_row("SELECT ID,post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_capacity' and $wpdb->posts.post_type = 'custom_fields'");
		$field_post_type = CUSTOM_POST_TYPE_TEMPLATIC_ROOM.",".CUSTOM_POST_TYPE_TEMPLATIC_HOUSE;
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Total Guests',
				 'post_content' => 'Select number of guests per room',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'home_capacity',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> $field_post_type,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_ROOM => CUSTOM_POST_TYPE_TEMPLATIC_ROOM,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'ctype'=>'select',
				'option_title'=>$option_values1,
				'option_values'=>$option_values1,
				'htmlvar_name'=>'home_capacity',
				'sort_order' => '2',
				'is_active' => '1',
				'is_require' => '1',
				'show_on_page' => 'both_side',
				'show_in_column' => '1',
				'validation_type' => 'require',
				'field_require_desc' => 'Please select Total Guest.',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '1',
				'is_search'=>'0',
				'show_on_success'=>'0',
				'show_in_email'=>'0',
				'is_delete' => '1',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_HOUSE => CUSTOM_CATEGORY_TEMPLATIC_HOUSE,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_contact_mail' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Contact Mail',
				 'post_content' => 'Metion your Email ID',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'home_contact_mail',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'post_type_house'=> CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'ctype'=>'text',
				'htmlvar_name'=>'home_contact_mail',
				'sort_order' => '4',
				'is_active' => '1',
				'is_require' => '0',
				'show_on_page' => 'admin_side',
				'show_in_column' => '0',
				'validation_type' => '',
				'field_require_desc' => '',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '1',
				'is_search'=>'0',
				'show_on_success'=>'0',
				'show_in_email'=>'0',
				'is_delete' => '1',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_HOUSE => CUSTOM_CATEGORY_TEMPLATIC_HOUSE,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'address' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'House Address',
				 'post_content' => 'Add your addres (comma saperated Street address, state, and country)',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'address',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'post_type_house'=> CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'ctype'=>'geo_map',
				'htmlvar_name'=>'address',
				'sort_order' => '5',
				'is_active' => '1',
				'is_require' => '0',
				'show_on_page' => 'admin_side',
				'show_in_column' => '0',
				'validation_type' => '',
				'field_require_desc' => '',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '1',
				'is_search'=>'0',
				'show_on_success'=>'0',
				'show_in_email'=>'0',
				'is_delete' => '1',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_HOUSE => CUSTOM_CATEGORY_TEMPLATIC_HOUSE,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		
		
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_contact1' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Home Contact',
				 'post_content' => 'Mention your phone number',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'home_contact1',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'post_type_house'=> CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'ctype'=>'text',
				'htmlvar_name'=>'home_contact1',
				'sort_order' => '8',
				'is_active' => '1',
				'is_require' => '0',
				'show_on_page' => 'admin_side',
				'show_in_column' => '0',
				'validation_type' => '',
				'field_require_desc' => '',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '1',
				'is_search'=>'0',
				'show_on_success'=>'0',
				'show_in_email'=>'0',
				'is_delete' => '1',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_HOUSE => CUSTOM_CATEGORY_TEMPLATIC_HOUSE,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		 
		 $post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'minimum_stay' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Minimum Stay',
				 'post_content' => 'Mention the minimum number of days you want your guests to book.',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'minimum_stay',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$field_post_type = CUSTOM_POST_TYPE_TEMPLATIC_HOUSE;
			$post_meta = array(
				'post_type'=> $field_post_type,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'ctype'=>'text',
				'htmlvar_name'=>'minimum_stay',
				'sort_order' => '11',
				'is_active' => '1',
				'is_require' => '0',
				'show_on_page' => 'admin_side',
				'show_in_column' => '0',
				'validation_type' => '',
				'field_require_desc' => '',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '1',
				'is_search'=>'0',
				'show_on_success'=>'0',
				'show_in_email'=>'0',
				'is_delete' => '1',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_HOUSE => CUSTOM_CATEGORY_TEMPLATIC_HOUSE,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_HOUSE => CUSTOM_POST_TYPE_TEMPLATIC_HOUSE,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		//Booking Post type custom fields
		
		
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'booking_first_name' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'First Name',
				 'post_content' => '',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'booking_first_name',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'post_type_booking'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'ctype'=>'text',
				'htmlvar_name'=>'booking_first_name',
				'default_value'=>'',
				'sort_order' => '12',
				'is_active' => '1',
				'is_require' => '1',
				'show_on_page' => 'user_side',
				'show_in_column' => '0',
				'validation_type' => 'require',
				'field_require_desc' => 'Please enter first name.',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '0',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '0',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_BOOKING => CUSTOM_CATEGORY_TEMPLATIC_BOOKING,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		 
		 
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'booking_last_name' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Last Name',
				 'post_content' => '',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'booking_last_name',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'post_type_booking'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'ctype'=>'text',
				'htmlvar_name'=>'booking_last_name',
				'default_value'=>'',
				'sort_order' => '13',
				'is_active' => '1',
				'is_require' => '1',
				'show_on_page' => 'user_side',
				'show_in_column' => '0',
				'validation_type' => 'require',
				'field_require_desc' => 'Please enter last name.',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '0',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '0',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_BOOKING => CUSTOM_CATEGORY_TEMPLATIC_BOOKING,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		
		 
		 
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'booking_email' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Email',
				 'post_content' => '',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'booking_email',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'post_type_booking'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'ctype'=>'text',
				'htmlvar_name'=>'booking_email',
				'default_value'=>'',
				'sort_order' => '14',
				'is_active' => '1',
				'is_require' => '1',
				'show_on_page' => 'both_side',
				'show_in_column' => '0',
				'validation_type' => 'email',
				'field_require_desc' => '',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '0',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '0',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_BOOKING => CUSTOM_CATEGORY_TEMPLATIC_BOOKING,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		 
		
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'booking_phone' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Phone',
				 'post_content' => '',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'booking_phone',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'post_type_booking'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'ctype'=>'text',
				'htmlvar_name'=>'booking_phone',
				'default_value'=>'',
				'sort_order' => '15',
				'is_active' => '1',
				'is_require' => '1',
				'show_on_page' => 'both_side',
				'show_in_column' => '0',
				'validation_type' => 'phone_no',
				'field_require_desc' => 'Please enter valid phone number',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '0',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '0',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_BOOKING => CUSTOM_CATEGORY_TEMPLATIC_BOOKING,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
	

		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'booking_street' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Street',
				 'post_content' => '',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'booking_street',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'post_type_booking'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'ctype'=>'text',
				'htmlvar_name'=>'booking_street',
				'default_value'=>'',
				'sort_order' => '16',
				'is_active' => '1',
				'is_require' => '1',
				'show_on_page' => 'both_side',
				'show_in_column' => '0',
				'validation_type' => 'require',
				'field_require_desc' => 'Please enter street address',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '0',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '0',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_BOOKING => CUSTOM_CATEGORY_TEMPLATIC_BOOKING,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
		
		 
		
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'booking_city_state' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'City/State',
				 'post_content' => '',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'booking_city_state',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'post_type_booking'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'ctype'=>'text',
				'htmlvar_name'=>'booking_city_state',
				'default_value'=>'',
				'sort_order' => '17',
				'is_active' => '1',
				'is_require' => '1',
				'show_on_page' => 'both_side',
				'show_in_column' => '0',
				'validation_type' => 'require',
				'field_require_desc' => 'Please enter city/state',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '0',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '0',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_BOOKING => CUSTOM_CATEGORY_TEMPLATIC_BOOKING,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			
			
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		}
		 
		$option_values_countries = 'Afghanistan, Albania, Algeria, American Samoa, Andorra, Angola, Anguilla, Antarctica, Antigua and Barbuda, Argentina, Armenia, Aruba, Australia, Austria, Azerbaijan, Bahamas, Bahrain, Bangladesh, Barbados, Belarus, Belgium, Belize, Benin, Bermuda, Bhutan, Bolivia, Bosnia and Herzegovina, Botswana, Brazil, Brunei Darussalam, Bulgaria, Burkina Faso, Burundi, Cambodia, Cameroon, Canada, Cape Verde, Cayman Islands, Central African Republic, Chad, Chile, China, Christmas Island, Cocos (Keeling) Islands, Colombia, Comoros, Democratic Republic of the Congo (Kinshasa), Congo, Republic of (Brazzaville), Cook Islands, Costa Rica, Ivory Coast, Croatia, Cuba, Cyprus, Czech Republic, Denmark, Djibouti, Dominica, Dominican Republic, East Timor, Ecuador, Egypt, El Salvador, Equatorial Guinea, Eritrea, Estonia, Ethiopia, Falkland Islands, Faroe Islands, Fiji, Finland, France, French Guiana, French Polynesia, French Southern Territories, Gabon, Gambia, Georgia, Germany, Ghana, Gibraltar, Greece, Greenland, Grenada, Guadeloupe, Guam, Guatemala, Guinea, Guinea-Bissau, Guyana, Haiti, Holy See, Honduras, Holy See, Honduras, Hong Kong, Hungary, Iceland, India, Indonesia, Iran (Islamic Republic of), Iraq, Ireland, Israel, Italy, Jamaica, Japan, Jordan, Kazakhstan, Kenya, Kiribati, Korea, Democratic People&acute;s Rep. (North Korea), Korea, Republic of (South Korea), Kosovo, Kuwait, Kyrgyzstan, Lao, People&acute;s Democratic Republic, Latvia, Lebanon, Lesotho, Liberia, Libya, Liechtenstein, Lithuania, Luxembourg, Macau, Macedonia, Rep. of, Madagascar, Malawi, Malaysia, Maldives, Mali, Malta, Marshall Islands, Martinique, Mauritania, Mauritius, Mayotte, Mexico, Micronesia, Federal States of, Moldova, Republic of, Monaco, Mongolia, Montenegro, Montserrat, Morocco, Mozambique, Myanmar, Burma, Namibia, Nauru, Nepal, Netherlands, Netherlands Antilles, New Caledonia, New Zealand, Nicaragua, Niger, Nigeria, Niue, Northern Mariana Islands, Norway, Oman, Pakistan, Palestinian Territories, Panama, Papua New Guinea, Paraguay, Peru, Philippines, Pitcairn Island, Poland, Portugal, Puerto Rico, Qatar, Reunion Island, Romania, Russian Federation, Rwanda, Saint Kitts and Nevis, Saint Lucia, Saint Vincent and the Grenadines, Samoa, San Marino, Sao Tome and Principe, Saudi Arabia, Senegal, Serbia, Seychelles, Sierra Leone, Singapore, Slovakia (Slovak Republic), Slovenia, Solomon Islands, Somalia, South Africa, South Sudan, Spain, Sri Lanka, Sudan, Suriname, Swaziland, Sweden, Switzerland, Syrian Arab Republic, Taiwan , Tanzania United Republic of , Thailand, Tibet, Timor-Leste (East Timor), Togo, Tokelau, Tonga, Trinidad and Tobago, Tunisia, Turkey, Turkmenistan, Turks and Caicos Islands, Tuvalu, Uganda, Ukraine, United Arab Emirates, United Kingdom, United States, Uruguay, Uzbekistan, Vanuatu, Vatican City State (Holy See), Venezuela, Vietnam, Virgin Islands (British), Virgin Islands (U.S.), Wallis and Futuna Islands, Western Sahara, Yemen, Zambia, Zimbabwe';
		$post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'booking_country' and $wpdb->posts.post_type = 'custom_fields'"); 	 
		if(count($post_content) == 0){
			$my_post = array(
				 'post_title' => 'Country',
				 'post_content' => '',
				 'post_status' => 'publish',
				 'post_author' => 1,
				 'post_name' => 'booking_country',
				 'post_type' => "custom_fields",
				);
			$post_id = wp_insert_post( $my_post );	
			if(is_plugin_active('wpml-translation-management/plugin.php')){
				global $sitepress;
				$current_lang_code= ICL_LANGUAGE_CODE;
				$default_language = $sitepress->get_default_language();	
				/* Insert wpml  icl_translations table*/
				$sitepress->set_element_language_details($post_id, $el_type='post_custom_fields', $post_id, $current_lang_code, $default_language );
				if(function_exists('wpml_insert_templ_post'))
					wpml_insert_templ_post($post_id,'custom_fields'); /* insert post in language */
			}
			$post_meta = array(
				'post_type'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'post_type_booking'=> CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'ctype'=>'select',
				'option_values'=>$option_values_countries,
				'htmlvar_name'=>'booking_country',
				'default_value'=>'',
				'sort_order' => '18',
				'is_active' => '1',
				'is_require' => '1',
				'show_on_page' => 'both_side',
				'show_in_column' => '0',
				'validation_type' => 'require',
				'field_require_desc' => 'Please enter country',
				'show_on_listing' => '0',
				'is_edit' => 'true',
				'show_on_detail' => '0',
				'is_search'=>'0',
				'show_on_success'=>'1',
				'show_in_email'=>'0',
				'is_delete' => '0',
				'style_class' => '',
				'extra_parameter' => '',
				'taxonomy_type_'.CUSTOM_CATEGORY_TEMPLATIC_BOOKING => CUSTOM_CATEGORY_TEMPLATIC_BOOKING,
				'post_type_'.CUSTOM_POST_TYPE_TEMPLATIC_BOOKING => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
				'heading_type' => '[#taxonomy_name#]',
				'field_id' => $post_id,
				);
			//wp_set_post_terms($post_id,'1','category',true);
			foreach($post_meta as $key=> $_post_meta){
				add_post_meta($post_id, $key, $_post_meta);
			 }
		 }
	
	}
}

if(function_exists('is_active_addons') && is_active_addons('templatic_booking_system')){
	add_action('admin_menu', 'add_booking_system_admin_menu');
	if(!function_exists('add_booking_system_admin_menu')){
	function add_booking_system_admin_menu(){
		if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'templatic_booking_system'){
			$templatic_booking_system_icon = TEMPLATIC_BOOKING_URL.'/images/favicon-active.png';
		}else{
			$templatic_booking_system_icon = TEMPLATIC_BOOKING_URL.'/images/favicon-active.png';
		}
		add_menu_page('Templatic Booking', 'Booking System', 'administrator', 'templatic_booking_system', 'templatic_booking_system_settings', $templatic_booking_system_icon, 64);
	}
	}
	global $wpdb;
	$page_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts where post_name like 'booking-form' and post_type='page' and post_status='publish'");
	if(!$page_id){
		$sql = "insert into ".$wpdb->posts." set post_author=1, post_title='Booking Form',comment_status='closed',ping_status='closed',post_content='[booking_submit_form]', post_name='booking-form',post_type='page'";
		$wpdb->query($sql);
		$last_post_id = $wpdb->get_var("SELECT max(ID) FROM $wpdb->posts");
		$guid = home_url()."/?p=$last_post_id";
		$guid_sql = "update $wpdb->posts set guid=\"$guid\" where ID=\"$last_post_id\"";
		$wpdb->query($guid_sql);				
		update_post_meta($last_post_id,'booking_dummy_content','1');
		update_option('booking_system_form',$last_post_id);
	}
	
	$page_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts where post_name like 'booking-cancel' and post_type='page' and post_status='publish'");
	if(!$page_id){
		$sql = "insert into ".$wpdb->posts." set post_author=1, post_title='Booking Cancel',comment_status='closed',ping_status='closed',post_content='[booking_cancel]', post_name='booking-cancel',post_type='page'";
		$wpdb->query($sql);
		$last_post_id = $wpdb->get_var("SELECT max(ID) FROM $wpdb->posts");
		$guid = home_url()."/?p=$last_post_id";
		$guid_sql = "update $wpdb->posts set guid=\"$guid\" where ID=\"$last_post_id\"";
		$wpdb->query($guid_sql);		
		update_post_meta($last_post_id,'booking_dummy_content','1');
		update_option('booking_system_cancel',$last_post_id);
	}
	
	$page_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts where post_name like 'booking-tariff' and post_type='page' and post_status='publish'");
	if(!$page_id){
		$sql = "insert into ".$wpdb->posts." set post_author=1, post_title='Booking Tariff',comment_status='closed',ping_status='closed',post_content='Booking Tariff[booking_tariff]', post_name='booking-tariff',post_type='page'";
		$wpdb->query($sql);
		$last_post_id = $wpdb->get_var("SELECT max(ID) FROM $wpdb->posts");
		$guid = home_url()."/?p=$last_post_id";
		$guid_sql = "update $wpdb->posts set guid=\"$guid\" where ID=\"$last_post_id\"";
		$wpdb->query($guid_sql);		
		update_post_meta($last_post_id,'booking_dummy_content','1');
		update_option('booking_system_tariff',$last_post_id);
	}	
	
	$page_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts where post_name like 'booking-calendar' and post_type='page' and post_status='publish'");
	if(!$page_id){
		$sql = "insert into ".$wpdb->posts." set post_author=1, post_title='Booking Calendar',comment_status='closed',ping_status='closed',post_content='Booking Calendar[booking_calendar]', post_name='booking-calendar',post_type='page'";
		$wpdb->query($sql);
		$last_post_id = $wpdb->get_var("SELECT max(ID) FROM $wpdb->posts");
		$guid = home_url()."/?p=$last_post_id";
		$guid_sql = "update $wpdb->posts set guid=\"$guid\" where ID=\"$last_post_id\"";
		$wpdb->query($guid_sql);		
		update_post_meta($last_post_id,'booking_dummy_content','1');
		update_option('booking_system_calendar',$last_post_id);
	}
	
	add_action('admin_init','booking_system_setup');
//	booking_system_setup();
}



?>