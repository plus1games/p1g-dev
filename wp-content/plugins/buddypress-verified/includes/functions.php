<?php
/**
 * BuddyVerified Functions
 *
 * @package BuddyVerifiedFunctions
 * @author modemlooper
 * @since 1.0.0
 */

/**
 * Bp_show_verified_badge function.
 *
 * @param mixed $object
 * @return string
 */
function bp_show_verified_badge( $object ) {
    global $bp;

    $img = $object;

    $is_verified = get_user_meta( $bp->displayed_user->id, 'bp-verified', true );

    if ( ! empty( $is_verified ) ) {
    	if ( 'yes' === $is_verified['profile'] ) {
    		if ( null === $is_verified['image'] ) {
    			$img = '<div id="avatar-wrap">' . $object . '<span id="bp-verified-header"><img id="verified-badge" src="' . esc_url( VERIFIED_URL ) . '/images/1.png"></span></div>';
    		} else {
    			$img = '<div id="avatar-wrap">' . $object . '<span id="bp-verified-header"><img id="verified-badge" src="' . esc_url( VERIFIED_URL ) . '/images/' . $is_verified['image'] . '.png"></span></div>';
    		}
    	}
    }

    return $img;
}
add_filter( 'bp_get_displayed_user_avatar', 'bp_show_verified_badge' );


/**
 * Bp_show_verified_badge_activity function.
 *
 * @access public
 * @param mixed $object
 * @return string
 */
function bp_show_verified_badge_activity( $object ) {
	global $bp, $activities_template;

    $img = $object;

	if ( isset( $activities_template->activity->current_comment ) && 'activity_comment' === $activities_template->activity->current_comment->type ) { return $object; }

	$comments = isset( $activities_template->activity->current_comment ) ? $activities_template->activity->current_comment->user_id : (int) $activities_template->activity->user_id;
	$is_verified = get_user_meta( $comments, 'bp-verified', true );

	if ( ! empty( $is_verified ) ) {
		if ( 'yes' === $is_verified['profile'] ) {
			if (  null === $is_verified['image'] ) {
				$img = '<div class="avatar-wrap">' . $object . '<span class="bp-verified"><img src="' . esc_url( VERIFIED_URL ) . '/images/1.png"></span></div>';
			} else {
				$img = '<div class="avatar-wrap">' . $object . '<span class="bp-verified"><img src="' . esc_url( VERIFIED_URL ) . '/images/' . $is_verified['image'] . '.png"></span></div>';
			}
	  	}
	}

	return $img;
}
add_filter( 'bp_get_activity_avatar', 'bp_show_verified_badge_activity' );


function bp_show_verified_badge_activity_comment( $object ) {
	global $bp, $activities_template;

    $img = $object;

	$comments = isset( $activities_template->activity->current_comment ) ? $activities_template->activity->current_comment->user_id : (int) $activities_template->activity->user_id;
	$is_verified = get_user_meta( $comments, 'bp-verified', true );

	if ( ! empty( $is_verified ) ) {
		if ( 'yes' === $is_verified['profile'] ) {
			if (  null === $is_verified['image'] ) {
				$img = '<span class="buddyverified-comment"><img src="' . esc_url( VERIFIED_URL ) . '/images/1.png"></span>' . $object;
			} else {
				$img = '<span class="buddyverified-comment"><img src="' . esc_url( VERIFIED_URL ) . '/images/' . $is_verified['image'] . '.png"></span>' . $object;
			}
	  	}
	}

	return $img;
}
add_filter( 'bp_activity_comment_name', 'bp_show_verified_badge_activity_comment' );


/**
 * Bp_show_verified_badge_members function.
 *
 * @param mixed $object
 * @return string
 */
function bp_show_verified_badge_members( $object ) {
	global $bp, $members_template;

	$comments = isset( $members_template->members ) ? (int) $members_template->member->id : '';
	$is_verified = get_user_meta( $comments, 'bp-verified', true );

	if ( ! empty( $is_verified ) ) {
		if ( 'yes' === $is_verified['profile'] ) {
			if (  null === $is_verified['image'] ) {
				$object .= '<span id="bp-verified"><img src="' . esc_url( VERIFIED_URL ) . '/images/1.png"></span>';
			} else {
				$object .= '<span id="bp-verified"><img src="' . esc_url( VERIFIED_URL ) . '/images/' . $is_verified['image'] . '.png"></span>';
			}
	  	}
	}

	return $object;
}
//add_filter( 'bp_get_member_avatar', 'bp_show_verified_badge_members' );


/**
 * Bp_show_verified_badge_avatar function.
 *
 * @param mixed $object
 * @return string
 */
function bp_show_verified_badge_avatar( $object ) {
	global $bp, $members_template;

	$comments = isset( $members_template->members ) ? (int) $members_template->member->id : '';
	$is_verified = get_user_meta( $comments, 'bp-verified', true );

	if ( ! empty( $is_verified ) ) {
		if ( 'yes' === $is_verified['profile'] ) {
			if (  null === $is_verified['image'] ) {
				$object .= '<span id="bp-verified"><img src="' . esc_url( VERIFIED_URL ) . '/images/1.png"></span>';
			} else {
				$object .= '<span id="bp-verified"><img src="' . esc_url( VERIFIED_URL ) . '/images/' . $is_verified['image'] . '.png"></span>';
			}
	  	}
	}

	return $object;
}
//add_action( 'bp_get_member_avatar', 'bp_show_verified_badge_members' );
//add_action( 'bp_get_group_member_avatar_thumb', 'bp_show_verified_badge_members' );

/**
 * [bp_verified_text description]
 *
 * @return void
 */
function bp_verified_text() {
	global $bp, $members_template;

	$is_verified = get_user_meta( $bp->displayed_user->id, 'bp-verified', true );

	$text = ! empty( $is_verified['text'] ) ? $is_verified['text'] : 'Verified User';

	if ( $is_verified ) {
		echo '<div id="bp-verified-text">' . esc_html( $text ) . '</div>';
	}

}
add_action( 'bp_profile_header_meta', 'bp_verified_text' );

/**
 * [buddyverified_css description]
 *
 * @return void
 */
function buddyverified_css() {

	$buddyverified_value = bp_get_option( 'buddyverified' );

	if ( ! empty( $buddyverified_value ) ) {
		?>
		<style>
			<?php echo esc_html( $buddyverified_value ); ?>
		</style>
		<?php

	}
}
add_action( 'wp_head', 'buddyverified_css' );

/**
 * [buddyverified_add_theme_body_class description]
 *
 * @param array $classes
 * @return array
 */
function buddyverified_add_theme_body_class( $classes ) {
	return array_merge( $classes, array( get_template() ) );
}
add_filter( 'body_class', 'buddyverified_add_theme_body_class' );

/**
 * Buddyverified_scripts_enqueue function.
 *
 * @access public
 * @return void
 */
function buddyverified_scripts_enqueue() {
	wp_enqueue_style( 'verified-style', plugins_url( '/css/verified.css' , __FILE__ ) );
	//wp_enqueue_script( 'verified-script', plugins_url( '/js/verified.js' , __FILE__ ) );
}

add_action( 'wp_enqueue_scripts', 'buddyverified_scripts_enqueue' );
