<?php 
/*
Plugin Name: Rss Feed
Version: 1.0
*/

add_filter( 'cron_schedules', 'event_add_a_cron_schedule' );

function event_add_a_cron_schedule( $schedules ) {

    $schedules['min'] = array(

        'interval' => 60, // Every minute

        'display'  => __( 'Every minute' ),
    );

    return $schedules;
}

     ///////Schedule an action if it's not already scheduled

if ( ! wp_next_scheduled( 'event_curl_cron_action' ) ) {

    wp_schedule_event( time(), 'min', 'event_curl_cron_action' );
}

///Hook into that action that'll fire sixhour
add_action( 'event_curl_cron_action', 'update_event123' );


function update_event123() {
    // retrieve the previous date from database
        $time = get_lastpostdate( $timezone );
        //read the feed
        $uri = 'http://www.bikeiowa.com/RSS/Event/All/All';
        $feed = fetch_feed($uri);
		$i=0;
        if($feed) {
			if($i<5){
            foreach ($feed->get_items() as $item){
                $titlepost = $item->get_title();
                $content = $item->get_content();
                $description = $item->get_description();
                $itemdate = $item->get_date();
                $media_group = $item->get_item_tags('', 'enclosure');
                $img = $media_group[0]['attribs']['']['url'];
                $width = $media_group[0]['attribs']['']['width'];           
                $latestItemDate = $feed->get_item()->get_date();


                // if the date is < than the date we have in database, get out of the loop
                if( $itemdate <= $time) break;


                // prepare values for inserting

                $post_information = array(
                    'post_title' => $titlepost,
                    'post_content' => $description,
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'post_date' => date('Y-m-d H:i:s')
                );

                wp_insert_post( $post_information );
			}
			$i++;
           }
        }
        // update the new date in database to the date of the first item in the loop        
        update_option( 'latestpostdate', $feed->get_item()->get_date() );
}
?>