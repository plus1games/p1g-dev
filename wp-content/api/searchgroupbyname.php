<?php
header('Content-type: application/json');
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;
if($_REQUEST['name'] !=''){
	$args = array (
		'post_type'              => 'listing',
		'post_status'            => 'publish',
		's'                      => $_REQUEST['name'],
		'posts_per_page'		 => '-1',
		'tax_query' => array(array(
										'taxonomy' => 'listingcategory', 
										'field' => 'id',
										'terms' => 406, 
										'include_children' => true,
								)),
	);

	$query = new WP_Query( $args );

	foreach($query as $key=>$val){
		if($key == 'posts'){
			$group =array();
			$group['result']= 'success';
			$group['total_sport_groups']=0;
			$group['groups']=array();
			$i=0;
			foreach($val as $k=>$v){
				
				$v = get_object_vars($v);
				if (stripos($v['post_title'],$_REQUEST['name']) !== false) {
					$group['total_sport_groups']= $i+1;
					$group['groups'][$v['ID']]=$v;
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $v['ID'] ) );
					$group['groups'][$v['ID']]['featured_image']=$image[0];
					$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
					$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
					$resultimages=$wpdb->get_results($sqlimage);
					$group['groups'][$v['ID']]['images']=array();
					foreach($resultimages as $img){
						if($img->ID !=$featuredimg_id){
							$group['groups'][$v['ID']]['images'][]=$img->guid;
						}
					}
					$group['groups'][$v['ID']]['meta']=get_post_meta($v['ID']);
					$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
					foreach($terminfo as $t){
						$t = get_object_vars($t);
						$group['groups'][$v['ID']]['term'][]=$t;
					}
					$i++;
				}
			}
		}
	}
}
if(empty($group['groups'])){
	$group['groups']=array();
	$group['groups']='No Sport Groups Found';
}
echo json_encode($group);

?>