<?php
/*******************************************/
//Server variable configuration
ini_set('safe_mode', 'Off');
ini_set('max_file_uploads', '400M');
ini_set('max_execution_time', '3000');
ini_set('max_input_time', '3000');
ini_set('memory_limit', '-1');
ini_set('output_buffering', '4096');

error_reporting( E_ALL );
ini_set("display_errors", 1);
/*******************************************/

$absPath= __FILE__;
$getPath=explode('wp-content',$absPath);
$path=$getPath[0].'wp-load.php';
include($path);
global $wpdb, $current_cityinfo;
$searchString = $_REQUEST['search_string'];
$searchRadius = $_REQUEST['radius'];
$lat = $_REQUEST['latitude'];
$long = $_REQUEST['longitude'];
$milesRange=explode('-',$searchRadius);
$toMiles = trim($milesRange[0]);
$miles = trim($milesRange[1]);
if($miles ==''){ $miles = '1000'; }	

if(empty($lat) && empty($long)){
	if(is_plugin_active('Tevolution-LocationManager/location-manager')){
		$ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
		$url = "http://freegeoip.net/json/$ip";
		$data=wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );
		if ($data) {
			$location = json_decode($data['body']);				
			$lat = $location->latitude;
			$long = $location->longitude;
		}else{
			$lat =$current_cityinfo['lat'];
			$long = $current_cityinfo['lng'];
		}
	}else{ /* if location manager is deactiv then get latitude and longitude from map setting and find results from that location */

		$cityGooglemapSetting = get_option('city_googlemap_setting');
		$lat =$cityGooglemapSetting['map_city_latitude'];
		$long = $cityGooglemapSetting['map_city_longitude'];
		
		if(empty($lat) || empty($long)){
		
			$ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
			$url = "http://freegeoip.net/json/$ip";
			$data=wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );
			
			if ($data) {
				$location = json_decode($data['body']);				
				$lat = $location->latitude;
				$long = $location->longitude;
			}
		}
	}
}

$result = array();
$result['events'] = array();
$result['parks'] = array();
$result['groups'] = array();
$result['goods'] = array();

if(!empty($searchString) && !empty($searchRadius)) {

	/*******************************************************************************/
	//Code is for events with search string
	$eventSearchQuery = "SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) as redious, wp_em_events.* FROM wp_posts INNER JOIN wp_em_locations ON (wp_em_locations.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$toMiles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND wp_posts.post_status = 'publish' AND ((meta_key='_location_town' AND meta_value = '".$searchString."') OR (meta_key='_location_state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."' OR (meta_key='_location_town' AND meta_value like '%".$searchString."%') OR (meta_key='_location_state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_em_events ON (wp_posts.ID = wp_em_events.post_id) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories') ORDER BY redious";

	$eventSearchResult = $wpdb->get_results($eventSearchQuery);
	$result['events']['result']= 'success';
	$result['events']['total_events'] = count($eventSearchResult);
	$result['events']['records'] = array();
	if(!empty($eventSearchResult)){
		foreach ($eventSearchResult as $event) {
			$event->post_content = trim(strip_tags($event->post_content));
			$event->post_title = trim(strip_tags($event->post_title));
			$eventPostmeta = get_post_meta( $event->id );
			$eventId = $event->event_id;
			$result['events']['records'][$eventId]= $event;
			$result['events']['records'][$eventId]->location=array();
			$result['events']['records'][$eventId]->location['location_name']=$eventPostmeta['_location_name'][0];
			$result['events']['records'][$eventId]->location['address']=$eventPostmeta['_location_address'][0];
			$result['events']['records'][$eventId]->location['city']=$eventPostmeta['_location_town'][0];
			$result['events']['records'][$eventId]->location['state']=$eventPostmeta['_location_state'][0];
			$result['events']['records'][$eventId]->location['latitude']=$eventPostmeta['_location_latitude'][0];
			$result['events']['records'][$eventId]->location['longitude']=$eventPostmeta['_location_longitude'][0];
			
		}
	} else $result['events']['records']= 'No event found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for places(parks) with search string
	$parksSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$toMiles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."' OR (meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345) ORDER BY redious";

	$parksSearchResult = $wpdb->get_results($parksSearchQuery);
	$result['parks']['result']= 'success';
	$result['parks']['total_parks'] = count($parksSearchResult);
	$result['parks']['records'] = array();
	if(!empty($parksSearchResult)){
			
		foreach($parksSearchResult as $park){
			$park->post_content = trim(strip_tags($park->post_content));
			$park->post_title = trim(strip_tags($park->post_title));
			$park = get_object_vars($park);
			
			$result['parks']['records'][$park['ID']]=$park;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $park['ID'] ) );
			if(!empty($image))
				$result['parks']['records'][$park['ID']]['featured_image']=$image[0];

			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$park['ID']." AND post_type='attachment'");
			$result['parks']['records'][$park['ID']]['images']=array();
			if(!empty($resultImages))
				$result['parks']['records'][$park['ID']]['images']=$resultImages;

			$result['parks']['records'][$park['ID']]['meta']=get_post_meta($park['ID']);
			$termInfo = wp_get_post_terms($park['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['parks']['records'][$park['ID']]['term']=$termInfo;
		}

	} else $result['parks']['records']='No Park Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport groups with search string
	$groupsSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$toMiles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."' OR (meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406) ORDER BY redious";

	$groupsSearchResult = $wpdb->get_results($groupsSearchQuery);
	$result['groups']['result']= 'success';
	$result['groups']['total_groups'] = count($groupsSearchResult);
	$result['groups']['records'] = array();
	if(!empty($groupsSearchResult)){
			
		foreach($groupsSearchResult as $group){
			$group->post_content = trim(strip_tags($group->post_content));
			$group->post_title = trim(strip_tags($group->post_title));
			$group = get_object_vars($group);
					
			$result['groups']['records'][$group['ID']]=$group;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $group['ID'] ) );
			if(!empty($image))
				$result['groups']['records'][$group['ID']]['featured_image']=$image[0];
			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$group['ID']." AND post_type='attachment'");
			$result['groups']['records'][$group['ID']]['images']=array();
			if(!empty($resultImages))
				$result['groups']['records'][$group['ID']]['images']=$resultImages;

			$result['groups']['records'][$group['ID']]['meta']=get_post_meta($group['ID']);
			$termInfo = wp_get_post_terms($group['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['groups']['records'][$group['ID']]['term']=$termInfo;
		}

	} else $result['groups']['records']='No Sport Groups Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport goods with search string
	$goodsSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$toMiles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."' OR (meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400) ORDER BY redious";

	$goodsSearchResult = $wpdb->get_results($goodsSearchQuery);
	$result['goods']['result']= 'success';
	$result['goods']['total_goods'] = count($goodsSearchResult);
	$result['goods']['records'] = array();
	if(!empty($goodsSearchResult)){
			
		foreach($goodsSearchResult as $good){
			$good->post_content = trim(strip_tags($good->post_content));
			$good->post_title = trim(strip_tags($good->post_title));
			$good = get_object_vars($good);
					
			$result['goods']['records'][$good['ID']]=$good;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $good['ID'] ) );
			if(!empty($image))
				$result['goods']['records'][$good['ID']]['featured_image']=$image[0];
			
			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$good['ID']." AND post_type='attachment'");
			$result['goods']['records'][$good['ID']]['images']=array();
			if(!empty($resultImages))
				$result['goods']['records'][$good['ID']]['images']=$resultImages;
			
			$result['goods']['records'][$good['ID']]['meta']=get_post_meta($good['ID']);
			$termInfo = wp_get_post_terms($good['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['goods']['records'][$good['ID']]['term']=$termInfo;
		}

	} else $result['goods']['records']='No Sporting Goods Found';
	/*******************************************************************************/
	
	$result['result']= 'success';
	echo htmlspecialchars(json_encode($result), ENT_QUOTES, 'UTF-8');

} elseif (!empty($searchString) && empty($searchRadius)) {

	/*******************************************************************************/
	//Code is for events with search string
	$eventSearchQuery = "SELECT DISTINCT id, wp_em_events.* FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND wp_posts.post_status = 'publish' AND ((meta_key='_location_town' AND meta_value = '".$searchString."') OR (meta_key='_location_state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."' OR (meta_key='_location_town' AND meta_value like '%".$searchString."%') OR (meta_key='_location_state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_em_events ON (wp_posts.ID = wp_em_events.post_id) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories')";

	$eventSearchResult = $wpdb->get_results($eventSearchQuery);
	$result['events']['result']= 'success';
	$result['events']['total_events'] = count($eventSearchResult);
	$result['events']['records'] = array();
	if(!empty($eventSearchResult)){
		foreach ($eventSearchResult as $event) {
			$event->post_content = trim(strip_tags($event->post_content));
			$event->post_title = trim(strip_tags($event->post_title));
			$eventPostmeta = get_post_meta( $event->id );
			$eventId = $event->event_id;
			$result['events']['records'][$eventId]= $event;
			$result['events']['records'][$eventId]->location=array();
			$result['events']['records'][$eventId]->location['location_name']=$eventPostmeta['_location_name'][0];
			$result['events']['records'][$eventId]->location['address']=$eventPostmeta['_location_address'][0];
			$result['events']['records'][$eventId]->location['city']=$eventPostmeta['_location_town'][0];
			$result['events']['records'][$eventId]->location['state']=$eventPostmeta['_location_state'][0];
			$result['events']['records'][$eventId]->location['latitude']=$eventPostmeta['_location_latitude'][0];
			$result['events']['records'][$eventId]->location['longitude']=$eventPostmeta['_location_longitude'][0];
			
		}
	} else $result['events']['records']= 'No event found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for places(parks) with search string
	$parksSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."' OR (meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345)";

	$parksSearchResult = $wpdb->get_results($parksSearchQuery);
	$result['parks']['result']= 'success';
	$result['parks']['total_parks'] = count($parksSearchResult);
	$result['parks']['records'] = array();
	if(!empty($parksSearchResult)){
		
		foreach($parksSearchResult as $park){
			$park->post_content = trim(strip_tags($park->post_content));
			$park->post_title = trim(strip_tags($park->post_title));
			$park = get_object_vars($park);
			
			$result['parks']['records'][$park['ID']]=$park;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $park['ID'] ) );
			if(!empty($image))
				$result['parks']['records'][$park['ID']]['featured_image']=$image[0];

			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$park['ID']." AND post_type='attachment'");
			$result['parks']['records'][$park['ID']]['images']=array();
			if(!empty($resultImages))
				$result['parks']['records'][$park['ID']]['images']=$resultImages;
			
			$result['parks']['records'][$park['ID']]['meta']=get_post_meta($park['ID']);
			$termInfo = wp_get_post_terms($park['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['parks']['records'][$park['ID']]['term']=$termInfo;
		}

	} else $result['parks']['records']='No Park Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport groups with search string
	$groupsSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."' OR (meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406)";

	$groupsSearchResult = $wpdb->get_results($groupsSearchQuery);
	$result['groups']['result']= 'success';
	$result['groups']['total_groups'] = count($groupsSearchResult);
	$result['groups']['records'] = array();
	if(!empty($groupsSearchResult)){
			
		foreach($groupsSearchResult as $group){
			$group->post_content = trim(strip_tags($group->post_content));
			$group->post_title = trim(strip_tags($group->post_title));
			$group = get_object_vars($group);
					
			$result['groups']['records'][$group['ID']]=$group;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $group['ID'] ) );
			if(!empty($image))
				$result['groups']['records'][$group['ID']]['featured_image']=$image[0];
			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$group['ID']." AND post_type='attachment'");
			$result['groups']['records'][$group['ID']]['images']=array();
			if(!empty($resultImages))
				$result['groups']['records'][$group['ID']]['images']=$resultImages;
			
			$result['groups']['records'][$group['ID']]['meta']=get_post_meta($group['ID']);
			$termInfo = wp_get_post_terms($group['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['groups']['records'][$group['ID']]['term']=$termInfo;
		}

	} else $result['groups']['records']='No Sport Groups Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport goods with search string
	$goodsSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."' OR (meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400)";

	$goodsSearchResult = $wpdb->get_results($goodsSearchQuery);
	$result['goods']['result']= 'success';
	$result['goods']['total_goods'] = count($goodsSearchResult);
	$result['goods']['records'] = array();
	if(!empty($goodsSearchResult)){
		
		foreach($goodsSearchResult as $good){
			$good->post_content = trim(strip_tags($good->post_content));
			$good->post_title = trim(strip_tags($good->post_title));
			$good = get_object_vars($good);
					
			$result['goods']['records'][$good['ID']]=$good;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $good['ID'] ) );
			if(!empty($image))
				$result['goods']['records'][$good['ID']]['featured_image']=$image[0];
			
			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$good['ID']." AND post_type='attachment'");
			$result['goods']['records'][$good['ID']]['images']=array();
			if(!empty($resultImages))
				$result['goods']['records'][$good['ID']]['images']=$resultImages;
			
			$result['goods']['records'][$good['ID']]['meta']=get_post_meta($good['ID']);
			$termInfo = wp_get_post_terms($good['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['goods']['records'][$good['ID']]['term']=$termInfo;
		}

	} else $result['goods']['records']='No Sporting Goods Found';
	/*******************************************************************************/
	
	$result['result']= 'radius, latitude, longitude parameters are empty';
	echo htmlspecialchars(json_encode($result), ENT_QUOTES, 'UTF-8');

} elseif (empty($searchString) && !empty($searchRadius)) {

	/*******************************************************************************/
	//Code is for events with search string
	$eventSearchQuery = "SELECT DISTINCT id, wp_em_events.* FROM wp_posts INNER JOIN wp_em_locations ON (wp_em_locations.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$toMiles.") INNER JOIN wp_em_events ON (wp_posts.ID = wp_em_events.post_id) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories')";

	$eventSearchResult = $wpdb->get_results($eventSearchQuery);
	$result['events']['result']= 'success';
	$result['events']['total_events'] = count($eventSearchResult);
	$result['events']['records'] = array();
	if(!empty($eventSearchResult)){
		foreach ($eventSearchResult as $event) {
			$event->post_content = trim(strip_tags($event->post_content));
			$event->post_title = trim(strip_tags($event->post_title));
			$eventPostmeta = get_post_meta( $event->id );
			$eventId = $event->event_id;
			$result['events']['records'][$eventId]= $event;
			$result['events']['records'][$eventId]->location=array();
			$result['events']['records'][$eventId]->location['location_name']=$eventPostmeta['_location_name'][0];
			$result['events']['records'][$eventId]->location['address']=$eventPostmeta['_location_address'][0];
			$result['events']['records'][$eventId]->location['city']=$eventPostmeta['_location_town'][0];
			$result['events']['records'][$eventId]->location['state']=$eventPostmeta['_location_state'][0];
			$result['events']['records'][$eventId]->location['latitude']=$eventPostmeta['_location_latitude'][0];
			$result['events']['records'][$eventId]->location['longitude']=$eventPostmeta['_location_longitude'][0];
			
		}
	} else $result['events']['records']= 'No event found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for places(parks) with search string
	$parksSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$toMiles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345) ORDER BY redious";

	$parksSearchResult = $wpdb->get_results($parksSearchQuery);
	$result['parks']['result']= 'success';
	$result['parks']['total_parks'] = count($parksSearchResult);
	$result['parks']['records'] = array();
	if(!empty($parksSearchResult)){
			
		foreach($parksSearchResult as $park){
			$park->post_content = trim(strip_tags($park->post_content));
			$park->post_title = trim(strip_tags($park->post_title));
			$park = get_object_vars($park);
			
			$result['parks']['records'][$park['ID']]=$park;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $park['ID'] ) );
			if(!empty($image))
				$result['parks']['records'][$park['ID']]['featured_image']=$image[0];

			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$park['ID']." AND post_type='attachment'");
			$result['parks']['records'][$park['ID']]['images']=array();
			if(!empty($resultImages))
				$result['parks']['records'][$park['ID']]['images']=$resultImages;
			
			$result['parks']['records'][$park['ID']]['meta']=get_post_meta($park['ID']);
			$termInfo = wp_get_post_terms($park['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['parks']['records'][$park['ID']]['term']=$termInfo;
		}

	} else $result['parks']['records']='No Park Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport groups with search string
	$groupsSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$toMiles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406) ORDER BY redious";

	$groupsSearchResult = $wpdb->get_results($groupsSearchQuery);
	$result['groups']['result']= 'success';
	$result['groups']['total_groups'] = count($groupsSearchResult);
	$result['groups']['records'] = array();
	if(!empty($groupsSearchResult)){
		
		foreach($groupsSearchResult as $group){
			$group->post_content = trim(strip_tags($group->post_content));
			$group->post_title = trim(strip_tags($group->post_title));
			$group = get_object_vars($group);
					
			$result['groups']['records'][$group['ID']]=$group;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $group['ID'] ) );
			if(!empty($image))
				$result['groups']['records'][$group['ID']]['featured_image']=$image[0];
			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$group['ID']." AND post_type='attachment'");
			$result['groups']['records'][$group['ID']]['images']=array();
			if(!empty($resultImages))
				$result['groups']['records'][$group['ID']]['images']=$resultImages;
			
			$result['groups']['records'][$group['ID']]['meta']=get_post_meta($group['ID']);
			$termInfo = wp_get_post_terms($group['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['groups']['records'][$group['ID']]['term']=$termInfo;
		}

	} else $result['groups']['records']='No Sport Groups Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport goods with search string
	$goodsSearchQuery = "SELECT DISTINCT ID, post_author, post_date, post_content, post_title, post_excerpt, post_status, post_name, post_modified, post_parent, guid, wp_posts.post_type, comment_count, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$toMiles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400) ORDER BY redious";

	$goodsSearchResult = $wpdb->get_results($goodsSearchQuery);
	$result['goods']['result']= 'success';
	$result['goods']['total_goods'] = count($goodsSearchResult);
	$result['goods']['records'] = array();
	if(!empty($goodsSearchResult)){
		
		foreach($goodsSearchResult as $good){
			$good->post_content = trim(strip_tags($good->post_content));
			$good->post_title = trim(strip_tags($good->post_title));
			$good = get_object_vars($good);
					
			$result['goods']['records'][$good['ID']]=$good;
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $good['ID'] ) );
			if(!empty($image))
				$result['goods']['records'][$good['ID']]['featured_image']=$image[0];
			
			$resultImages=$wpdb->get_results("SELECT ID, guid FROM wp_posts WHERE post_parent=".$good['ID']." AND post_type='attachment'");
			$result['goods']['records'][$good['ID']]['images']=array();
			if(!empty($resultImages))
				$result['goods']['records'][$good['ID']]['images']=$resultImages;
			
			$result['goods']['records'][$good['ID']]['meta']=get_post_meta($good['ID']);
			$termInfo = wp_get_post_terms($good['ID'], 'listingcategory');
			if(!empty($termInfo))
				$result['goods']['records'][$good['ID']]['term']=$termInfo;
		}

	} else $result['goods']['records']='No Sporting Goods Found';
	/*******************************************************************************/
	
	$result['result']= 'search_string parameter is empty';
	echo htmlspecialchars(json_encode($result), ENT_QUOTES, 'UTF-8');

} elseif (empty($searchString) && empty($searchRadius)) {

	$result['result']= 'search_string, radius parameters are empty';

	$result['events']['result']= 'unsuccess';
	$result['events']['total_events'] = 0;
	$result['events']['records']= 'No event found';


	$result['parks']['result']= 'unsuccess';
	$result['parks']['total_parks'] = 0;
	$result['parks']['records']='No Park Found';


	$result['groups']['result']= 'unsuccess';
	$result['groups']['total_groups'] = 0;
	$result['groups']['records']='No Sport Groups Found';


	$result['goods']['result']= 'unsuccess';
	$result['goods']['total_goods'] = 0;
	$result['goods']['records']='No Sporting Goods Found';

	echo htmlspecialchars(json_encode($result), ENT_QUOTES, 'UTF-8');
}
?>