<?php
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;

$_REQUEST['action'] = 'add';
$_REQUEST['user_id'] = '1';
$_REQUEST['game_category'] = '624';
$_REQUEST['game_level'] = 'advance';
$_REQUEST['game_position'] ="playersss1234456";
/*1 for on ,2 for off */
$_REQUEST['game_notification'] ="0";
$_REQUEST['zip']="34212";
$_REQUEST['radius'] = "9";
$_REQUEST['sex'] = 'female';
$_REQUEST['event_age'] = "grade";
$_REQUEST['game_profile_id']= "149";


$game_profile_id = $_REQUEST['game_profile_id'];
$user_id = $_REQUEST['user_id'];
$game_category = $_REQUEST['game_category'];
$game_level = $_REQUEST['game_level'];
$game_position = $_REQUEST['game_position'];
$game_notification = $_REQUEST['game_notification'];
$dis_type =  $_REQUEST['distance_type'];
$zip = $_REQUEST['zip'];
$radius = $_REQUEST['radius'];
$phno = $_REQUEST['phno'];
$sex = $_REQUEST['sex'];
$event_age= $_REQUEST['event_age'];
$event_min_age= $_REQUEST['min_age'];
$event_max_age= $_REQUEST['max_age'];
$event_from_grade =$_REQUEST['from_grade'];
$event_to_grade= $_REQUEST['to_grade'];

if($event_age == "18"){
	$event_age_group = 2;
}
elseif($event_age == "21"){
	$event_age_group = 3;
}
elseif($event_age == "grade"){
	$event_age_group = 4;
}
elseif($event_age == "custom"){
	$event_age_group = 5;
}
else{
	$event_age_group = "";
}

if($dis_type == 'km')
{
  $radius = $radius*0.62;
}  

if($_REQUEST['action'] == 'add'){
	$field1 =  "user_id,
				game_category,
				game_level,
				game_position,
				game_notification,
				zip,
				radius,
				phno,
				sex,
				event_age,
				event_min_age,
				event_max_age,
				event_from_grade,
				event_to_grade"; 


	$value1 = "'".$user_id."',
				'".$game_category."',
				'".$game_level."',
				'".	$game_position."',
				'".$game_notification."',	
				'".$zip."',
				'".$radius."',
				'".$phno."',
				'".$sex."',
				'".$event_age_group."',
				'".$event_min_age."',
				'".$event_max_age."',
				'".$event_from_grade."',
				'".$event_to_grade."'";

	$result = array();
	if($user_id != ""){
		if(!empty($game_category)){
			$sql = "SELECT * FROM game_profile WHERE user_id=".$user_id." AND game_category=".$game_category;
			$resultsql = $wpdb->get_results($sql);
			if(empty($resultsql)){
				$insertresult= $wpdb->query("INSERT INTO game_profile (".$field1 .") VALUES (". $value1.")");

				if($insertresult){
					$result['result']='success';
				} else {
					$result['result']='failed';
				}
			}
			else{
				$result['result']='This game is already added as game favourite';
			}
		}
		else{
			$result['result']='failed';
			$result['error'] = "game category id should not be empty";
		}
	}
	else{
		$result['result']='failed';
		$result['error'] = "user id should not be empty";
	}
	echo json_encode($result);
}
if($_REQUEST['action'] == 'edit'){
	$result = array();
	if($user_id != ""){
		$sql = "SELECT * FROM game_profile WHERE user_id=".$user_id." AND id=".$game_profile_id;
		$resultsql = $wpdb->get_results($sql);
		if(!empty($resultsql)){
			$updateresult= $wpdb->query("UPDATE game_profile SET game_category = '".$game_category."',
																game_level='".$game_level."',
																game_position='".$game_position."',
																game_notification='".$game_notification."',
																zip='".$zip."',
																radius='".$radius."',
																phno='".$phno."',
																sex='".$sex."',
																event_age='".$event_age_group."',
																event_min_age='".$event_min_age."',
																event_max_age='".$event_max_age."',
																event_from_grade='".$event_from_grade."',
																event_to_grade='".$event_to_grade."'
											WHERE id=".$game_profile_id);

			if($updateresult){
				$result['result']='success';
			} else {
				$result['result']='failed';
			}
		}
		else{
			$result['result']='failed';
		}
	}
	else{
		$result['result']='failed';
		$result['error'] = "user id should not be empty";
	}
	echo json_encode($result);
}
if($_REQUEST['action'] == 'delete'){
	$result = array();
	if($user_id != ""){
		$sql = "SELECT * FROM game_profile WHERE user_id=".$user_id." AND id=".$game_profile_id;
		$resultsql = $wpdb->get_results($sql);
		if(!empty($resultsql)){
			$deleteresult = $wpdb->query("DELETE FROM game_profile WHERE id=".$game_profile_id);
			if($deleteresult){
				$result['result']='success';
			} else {
				$result['result']='failed';
			}
		}
		else{
			$result['result']='failed';
		}
	}
	else{
		$result['result']='failed';
		$result['error'] = "user id should not be empty";
	}
	echo json_encode($result);
}
if($_REQUEST['action'] == 'list'){
	$result = array();
	if($user_id != ""){
		$sql = "SELECT * FROM game_profile WHERE user_id=".$user_id;
		$resultsql = $wpdb->get_results($sql);
		$result['result']='success';
		$i=0;
		$result['total_count']=0;
		if(!empty($resultsql)){
			foreach($resultsql as $v){
				$result['total_count'] = $i++;
				if($v->event_age == "2"){
					$event_age_group = '18+';
				}
				elseif($v->event_age == "3"){
					$event_age_group = "21+";
				}
				elseif($v->event_age == "4"){
					$event_age_group = "grade";
				}
				elseif($v->event_age == "5"){
					$event_age_group = "custom";
				}
				else{
					$event_age_group = "";
				}

				$result['game_favourite'][$v->id]=get_object_vars($v);
				$result['game_favourite'][$v->id][event_age] = $event_age_group; 
			}
		}
		else{		
			$result['game_favourite'] = '';
		}
	}
	else{
			$result['result']='failed';
			$result['error'] = "user id should not be empty";
		}
	echo json_encode($result);
}