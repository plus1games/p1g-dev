<?php
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;

/*$_REQUEST['action']='list';
$_REQUEST['user_id']= '231';
$_REQUEST['post_id'] = '43148';
$_REQUEST['type'] == 'parks';*/
$user_id = $_REQUEST['user_id'];
$post_id = $_REQUEST['post_id'] ;
$listing_type = $_REQUEST['type'] ;
$addresult =array();
$removeresult = array();
$userresult = array();
$listresult = array();
$parks = array();
$chkuser = "SELECT * FROM `wp_users` WHERE `ID`=".$user_id;
$result = $wpdb->get_results($chkuser);
if(!empty($result)){
	/*****  add favorite park   ******/
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='add'){
		$user_meta_data = array();
		$user_meta_data = get_user_meta($user_id,'user_favourite_post',false);
		$user_meta_data = $user_meta_data[0];
		if(!(in_array($post_id,$user_meta_data))){
			$user_meta_data[]=$post_id;
			if(update_user_meta($user_id, 'user_favourite_post', $user_meta_data) == true)
			{
				$addresult['result']='successfully added';
			}
			else{
				$addresult['result']='failed';
			}
		}
		else{
			$addresult['result']='This post is already added to user favorite';
		}
		echo json_encode($addresult);
	}

	/********* remove favorite park ***********/
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='remove'){
		$user_meta_data = array();
		$user_meta_data = get_user_meta($user_id,'user_favourite_post',false);
		$user_meta_data = $user_meta_data[0];
		if(!empty($user_meta_data )){
			if(in_array($post_id,$user_meta_data))
			{
				$user_new_data = array();
				foreach($user_meta_data as $key => $value)
				{
					if($post_id == $value)
					{
						$value= '';
					}else{
						$user_new_data[] = $value;
					}
				}	
				$user_meta_data	= $user_new_data;
			}
			if(update_user_meta($user_id, 'user_favourite_post', $user_meta_data) == true){
				$removeresult['result'] = "successfully removed";
				$removeresult['error'] = "";
			}
			else{
				$removeresult['result'] = "failed";
				$removeresult['error'] = "This post is not in user favorite";
			}
			
		}
		else{
			$removeresult['result'] = 'failed';
			$removeresult['error'] = 'This post is not in user favorite';
		}
		echo json_encode($removeresult);
	}
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='list'){
		$parks=array();
		$parks['result']= 'success';
		if($listing_type == 'parks')
		{
			$term_id = '345';
			$listtype= 'parks';
			$totallist = 'total_favorite_parks';
		}
		elseif($listing_type == 'groups'){
			$term_id = '406';
			$listtype= 'groups';
			$totallist = 'total_favorite_sports_group';
		}
		elseif($listing_type == 'goods'){
			$term_id = '400';
			$listtype= 'goods';
			$totallist = 'total_favorite_sporting_goods';
		}
		$parks[$totallist] = '0';
		$user_meta_data = get_user_meta($user_id,'user_favourite_post',false);
		$user_meta_data = $user_meta_data[0];
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'				 =>	$user_meta_data,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
											'taxonomy' => 'listingcategory', 
											'field' => 'id',
											'terms' => $term_id, 
											'include_children' => true,
									)),
		);

		$query = new WP_Query( $args );
		
		foreach($query as $key=>$val){
			if($key == 'posts'){
				$i=0;
				foreach($val as $v){
					$parks[$totallist]= $i+1;
					$v = get_object_vars($v);
					$parks[$listtype][$v['ID']]=$v;
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $v['ID'] ) );
					$parks[$listtype][$v['ID']]['featured_image']=$image[0];
					$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
					$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
					$resultimages=$wpdb->get_results($sqlimage);
					$parks[$listtype][$v['ID']]['images']=array();
					foreach($resultimages as $img){
						if($img->ID != $featuredimg_id){
							$parks[$listtype][$v['ID']]['images'][]=$img->guid;
						}
					}
					$parks[$listtype][$v['ID']]['meta']=get_post_meta($v['ID']);
					$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						foreach($terminfo as $t){
							$t = get_object_vars($t);
							$parks[$listtype][$v['ID']]['term'][]=$t;
						}
					$i++;
				}
			}
		}
		if(empty($parks[$listtype])){
		$parks[$listtype]=array();
		$parks[$listtype]='No '.$listtype.' Found';
	}
		echo json_encode($parks);
	}
}
else{
	$userresult['result'] ='failed';
	$userresult['error'] ='User doesnot exist';
	echo json_encode($userresult);
}
?>