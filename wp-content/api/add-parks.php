<?php
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;

/*******    Sample data **************/
/*$_REQUEST['post_author'] = '1';
$_REQUEST['post_date'] = date();
$_REQUEST['post_content'] = 'This is testing park description';
$_REQUEST['post_name'] = 'simponi park';
$_REQUEST['post_status'] = 'publish';
$_REQUEST['address']= '545 Francisco St, San Francisco, CA 94133, USA';
$_REQUEST['city']= 'San Francisco';
$_REQUEST['state']='CA';
$_REQUEST['zip'] = '94133';
$img_logo = file_get_contents($get_path[0].'wp-content/uploads/bulk/Koala.jpg');
$logo_data = base64_encode($img_logo);
$_REQUEST['listing_logo'] =  $logo_data ;
$_REQUEST['contact_person'] = 'admin';
$_REQUEST['phone'] = '9898567654';
$_REQUEST['email'] = 'plus1games.com';
$_REQUEST['website'] = 'http://p1g.co';
$_REQUEST['acres'] = '22';
$_REQUEST['location_public'] = 'private';
$_REQUEST['amenities'] = array('Electricity','Wi-Fi','Fields Lights','Concession Stand','Vending Machines','Water Fountains Available Seasonally','Water Fountain','Restrooms','Bar','Restaurant','Wading Pool','Splash Ground','PondLagoon','Lake,Grills','Picnic Tables','Fountains','Offstreet Parking');
$_REQUEST['authorized_representative'] = 'yes';
$_REQUEST['catname'] = array('374','352','405');
$_REQUEST['post_tag'] = '';
$image_fdata = file_get_contents($get_path[0].'wp-content/uploads/bulk/Chrysanthemum.jpg');
$featured_img = base64_encode($image_fdata);
$_REQUEST['featured_image'] = $featured_img ;
$img1 = $get_path[0].'wp-content/uploads/bulk/parks.png';
$img2 = $get_path[0].'wp-content/uploads/bulk/Groups.png';
$image = array($img1,$img2);
$imgbdata =array();
foreach($image as $bdata){
	$image_data = file_get_contents($bdata);
	$park_img = base64_encode($image_data);
	$imgbdata[] = $park_img;
}
$_REQUEST['listingimages'] = $imgbdata;
$_REQUEST['field_array'] = array(array('field_category'=>'234','field_name'=>'bad','field_indoor'=>'indoor','field_public'=>'private','field_desc'=>'','field_size'=>'','field_type'=>''),array('field_category'=>'434','field_name'=>'hoc','field_indoor'=>'outdoor','field_public'=>'public','field_desc'=>'','field_size'=>'','field_type'=>''));*/

/****** End sample data *********/

$listing_parks_images = array();
$listing_park_featuredimg = array();
$listing_park_logo = '';
$post_author = $_REQUEST['post_author'];
$post_date = $_REQUEST['post_date'];
$post_content = $_REQUEST['post_content'];
$post_name = $_REQUEST['post_name'];
$post_status = $_REQUEST['post_status'];
$post_type = 'listing';
$pkg_id = '33549';
$pkg_type = '1';
$user_pkg_select = '1';
$cat_fields ='1';
$address= $_REQUEST['address'];
$zooming_factor ='13';
$field_array = $_REQUEST['field_array'];

/* Find lat long */
$v = str_replace(' ','+',convert_chars(addslashes(iconv('', 'utf-8',$address))));
$newarr=array(
				  'query'=>$address,
				  'key'=>'An5eSI8b07vN08rUnPjscV6-weAMK3o3VSSZK_tlyGd4MfpQ1P7O8GJTpMSX9cch'
			 );
			   $extended_part= http_build_query($newarr);
			     $url = "http://dev.virtualearth.net/REST/v1/Locations?".$extended_part; 
				 $ch = curl_init();
				  curl_setopt($ch, CURLOPT_URL, $url);
				   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
					 curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					 $geocode = curl_exec($ch);
					curl_close($ch); 
				$output= json_decode($geocode);
				$lat = $output->resourceSets[0]->resources[0]->point->coordinates[0];
				$long = $output->resourceSets[0]->resources[0]->point->coordinates[1];

$geo_latitude = $lat;
$geo_longitude = $long;
$map_view = 'Road Map';
$city =$_REQUEST['city'];
$state = $_REQUEST['state'];
$zip=$_REQUEST['zip'];
$listing_logo = $_REQUEST['listing_logo'];
	imageupload($listing_logo,'logo');
$contact_person = $_REQUEST['contact_person'];
$phone = $_REQUEST['phone'];
$email =  $_REQUEST['email'];
$website =  $_REQUEST['website'];
$twitter = $_REQUEST['twitter'];
$facebook = $_REQUEST['facebook'];
$youtube = $_REQUEST['youtube'];
$google = $_REQUEST['google'];
$acres =  $_REQUEST['acres'];
$location_public =  $_REQUEST['location_public'];
$amenities =  $_REQUEST['amenities'];
$authorized_representative = $_REQUEST['authorized_representative'];
$paymentmethod ='Free';
$payable_amount ='0';
$paid_amount = '0';
$package_select = '33549';
$alive_days ='1825';
$featured_h ='n';
$featured_c ='n';
$featured_type ='none';
$remote_ip='';
$catname =  $_REQUEST['catname'];
array_push($catname , '345');
$post_tag = $_REQUEST['post_tag'];
$featured_image = $_REQUEST['featured_image'];
	imageupload($featured_image,'featured' );
$listingimages = $_REQUEST['listingimages'];
	foreach($listingimages as $pimg){
		imageupload($pimg ,'listing_images');
	}
	
function imageupload($data1,$imgtype) {
	global $listing_parks_images,$listing_park_featuredimg,$listing_park_logo;
	$data1 = base64_decode($data1);
		$image_name = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 10);
		$upload_path=wp_upload_dir();
		$image_upload_path=$upload_path['path'].'/'.$image_name .'.jpg';
		$fp = @fopen($image_upload_path , 'w');
		fwrite($fp , $data1);
		fclose($fp);
		$imagepath=$upload_path['url'].'/'.$image_name .'.jpg';
		if($imgtype == 'listing_images'){
			$listing_parks_images[] = $imagepath;
		}
		elseif($imgtype == 'featured'){
			$listing_park_featuredimg[] = $imagepath;
		}
		elseif($imgtype == 'logo'){
			$listing_park_logo = $imagepath;
		}
}

$postid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_type='".$post_type."' and  post_title = '".convert_chars($post_name)."'" );		$fulladdress = convert_chars($address);
if($postid!=""){
	$addresschk = get_post_meta($postid,'address');
}
			$new_post = array(
			'post_title'   => convert_chars($post_name),
			'post_content' => wpautop(convert_chars($post_content)),
			'post_status'  => $post_status,	
			'post_type'    => $post_type,
			'post_date'    => $post_date,
			'post_excerpt' => "",
			'post_author'  =>$post_author, 		
			//'post_parent'  => $_SESSION['csv_data'][$i]['templatic_post_parent'],
			//'tags_input' => $_SESSION['csv_data'][$i]['templatic_post_tags'],
		 );
		$last_postid = wp_insert_post( $new_post );

		$sqlpostcode = "INSERT INTO `wp_postcodes`(`post_id`, `post_type`, `address`, `latitude`, `longitude`) VALUES('".$last_postid."','".$post_type."','".$address."','".$geo_latitude."','".$geo_longitude."')";
		$wpdb->query($sqlpostcode);

		foreach($field_array as $value){
			$sqllocationfield = "INSERT INTO `wp_em_location_fields`(`post_id`, `field_category`, `field_name`, `field_indoor`, `field_public`,`field_desc`,`field_size`,`field_type`,`field_location_status`) 
			VALUES('".$last_postid."','".$value['field_category']."','".$value['field_name']."','".$value['field_indoor']."','".$value['field_public']."','".$value['field_desc']."','".$value['field_size']."','".$value['field_type']."','1')";
			$wpdb->query($sqllocationfield);
		}

		$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type,'public'   => true, '_builtin' => true ));
		if(!empty($catname))
				{	$category_name = array_map('intval', $catname);
					wp_set_object_terms($last_postid,$category_name, $taxonomies[0]);
				}
		if($post_tag!="")
				{
					wp_set_post_terms($last_postid,$post_tag,$taxonomies[1]);
				}
		$meta_array=array('pkg_id'=>$pkg_id,'pkg_type'=>$pkg_type,'is_user_select_subscription_pkg'=>$user_pkg_select,'cat_fields'=>$cat_fields,'address'=>$address,'zooming_factor'=>$zooming_factor,'geo_latitude'=>$geo_latitude,'geo_longitude'=>$geo_longitude,'map_view'=>$map_view,'city'=>$city,'state'=>$state,'zip'=>$zip,'listing_logo'=>$listing_park_logo,'contact_person'=>$contact_person,'phone'=>$phone,'email'=>$email,'website'=>$website,'twitter'=>$twitter,'facebook'=>$facebook,'youtube'=>$youtube,'google'=>$google,'acres'=>$acres,'location_public'=>$location_public,'amenities'=>$amenities,'authorized_representative'=>$authorized_representative,'paymentmethod'=>$paymentmethod,'payable_amount'=>$payable_amount,'paid_amount'=>$paid_amount,'package_select'=>$package_select,'alive_days'=>$alive_days,'featured_h'=>$featured_h,'featured_c'=>$featured_c,'featured_type'=>$featured_type,'remote_ip'=>$remote_ip);		
			foreach($meta_array as $metakey=>$meta_value){
				add_post_meta($last_postid, $metakey, $meta_value);
			}
		upload_templatic_images($last_postid,$listing_parks_images,'listing_images');
		upload_templatic_images($last_postid,$listing_park_featuredimg,'featured');
		$result = array();
		if($last_postid !=''){
			$result['result']='success';
			$result['park_id']=$last_postid;
		}
		else{
			$result['result']='failed';
		}
		echo json_encode($result);
	function upload_templatic_images($last_postid,$data,$imagetype)
	{	$dirinfo = wp_upload_dir();		
		$path = $dirinfo['path'];
		$url = $dirinfo['url'];
		$subdir = $dirinfo['subdir'];
		$basedir = $dirinfo['basedir'];
		$baseurl = $dirinfo['baseurl'];	
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		foreach ($data as $k=>$v) {
			
				$image_name=$v;// image name
				$imgname =explode($url.'/',$v);
				$wp_filetype = wp_check_filetype(basename($imgname[1]), null );
				$attachment = array('guid' => $v,
									'post_mime_type' => $wp_filetype['type'],
									'post_title' => preg_replace('/\.[^.]+$/', '', $imgname[1]),
									'post_content' => '',
									'post_status' => 'inherit'
								);
				$img_attachment=$path.'/'.$imgname[1];
				$attach_id = wp_insert_attachment( $attachment, $img_attachment, $last_postid );
				$upload_img_path=$path.'/'.$imgname[1];
				$attach_data = wp_generate_attachment_metadata( $attach_id, $upload_img_path );
				wp_update_attachment_metadata( $attach_id, $attach_data );
				if($imagetype=='featured'){
				add_post_meta($last_postid,'_thumbnail_id',$attach_id);
				}
				
				//finish foreach loop
			//finish the templatic_img preg_match condition
		} 
	} 