<?php
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb;
$term = $_REQUEST['term'];
$zip = $_REQUEST['zip'];
$radius =$_REQUEST['radius'];

$event_array = array();
$result_array=array();
if(isset($_REQUEST['term'])&& $_REQUEST['term']!=""){
	$query = "SELECT * FROM wp_em_events WHERE event_name like '%$term%'";
	$result = $wpdb->get_results($query,ARRAY_A);
	$event_array['result']= 'success';
	$event_array['total_events'] = count($result);
	$event_array['events'] = array();
	if(!empty($result)) {			
		foreach($result as $v){
			$event_id= $v['event_id'];
			$event_array['events'][$event_id]= $v;
			$post = "SELECT post_title FROM wp_posts WHERE id='".$v['location_id']."'";
			$result1 = $wpdb->get_results($post,ARRAY_A);
			$event_array['events'][$event_id]['location']=array();
			$event_array['events'][$event_id]['location']['location_name']=$result1[0]['post_title'];
			$postcity= get_post_meta ($v['location_id']);
			$event_array['events'][$event_id]['location']['address']= $postcity['address'][0];
			$event_array['events'][$event_id]['location']['city']=$postcity['city'][0];
			$event_array['events'][$event_id]['location']['state']=$postcity['state'][0];
			$event_array['events'][$event_id]['location']['latitude']=$postcity['geo_latitude'][0];
			$event_array['events'][$event_id]['location']['longitude']=$postcity['geo_longitude'][0];
		}
	}
	else{
		$event_array['events']= 'No event found';
	}
	echo json_encode($event_array);
}


if(isset($zip)&&$zip!=""){
	$sqlzip = "SELECT * FROM zips WHERE zip='".$zip."'";
	$rs=$wpdb->get_results($sqlzip,ARRAY_A);
	$latitude=$rs[0]['lat'];
	$longitude=$rs[0]['lng'];
	$R = 6371;
	$lat = $latitude;
    $lon = $longitude;
	$r= $radius;
	$maxLAT = $lat + rad2deg($r/$R);
	$minLAT = $lat - rad2deg($r/$R);

	$maxLNG = $lon + rad2deg($r/$R/cos(deg2rad($lat)));
	$minLNG = $lon - rad2deg($r/$R/cos(deg2rad($lat)));
	$sqlpostid = "SELECT DISTINCT(post_id) FROM wp_postcodes WHERE latitude BETWEEN ".$minLAT." AND ".$maxLAT." AND longitude BETWEEN ".$minLNG." AND ".$maxLNG;  
	$restpostid= $wpdb->get_results($sqlpostid,ARRAY_N);
	$result_array['result']= 'success';
	$totalcount = 0;
	$result_array['total_count'] = $totalcount;
	$result_array['events'] = array();
	foreach($restpostid as $pid){
		
		$query = "SELECT event_id,post_id,event_slug,event_owner,event_owner,event_status,event_name,event_start_time,event_end_time,event_start_date,event_end_date,post_content,event_category_id,invite,email_invite,email_invite_friendslist,email_invite_public,event_level,event_sex,event_age,event_min_age,event_max_age,event_from_grade,event_to_grade,event_game_type FROM wp_em_events WHERE location_id=".$pid[0];
		$result = $wpdb->get_results($query,ARRAY_A);
		$event_counts=count($result);
		//$result_array['total_events'] = count($result);		
		if(!empty($result)){
			foreach($result as $v){
				$totalcount++;
				$event_id = $v['event_id'];
				$result_array['events'][$event_id]=$v;
				$loc_name = "SELECT post_title FROM wp_posts WHERE ID=".$pid[0];
				$resloc_name =$wpdb->get_results($loc_name,ARRAY_A);
				$result_array['events'][$event_id]['location'] = array();
				$result_array['events'][$event_id]['location']['location_name']=$resloc_name[0]['post_title'];
				$postcity= get_post_meta ($pid[0]);
				$result_array['events'][$event_id]['location']['address']= $postcity['address'][0];
				$result_array['events'][$event_id]['location']['city']= $postcity['city'][0];
				$result_array['events'][$event_id]['location']['state']= $postcity['state'][0];
				$result_array['events'][$event_id]['location']['latitude']= $postcity['geo_latitude'][0];
				$result_array['events'][$event_id]['location']['longitude']= $postcity['geo_longitude'][0];
			}			
		}
	}
	if(empty($result_array['events'])){
		$result_array['events'] = "No event found";
	}
	$result_array['total_count']= $totalcount;
	echo json_encode($result_array);
}
?>

