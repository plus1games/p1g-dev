<?php
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;

$args = array (
		'post_type'              => 'listing',
		'post_status'            => 'publish',
		'posts_per_page'		 => '-1',
		'tax_query' => array(array(
										'taxonomy' => 'listingcategory', 
										'field' => 'id',
										'terms' => 345, 
										'include_children' => true,
								)),
	);
	$query = new WP_Query( $args );
	$parks=array();
	foreach($query as $key=>$value){
		if($key =='posts'){
			foreach($value as $k=>$v){
				$v = get_object_vars($v);
				$fiedsarr=array('ID','post_content','post_title','post_status','post_author');
				foreach($v as $t=>$tv){
					if(in_array($t,$fiedsarr)){
						$parks[$v['ID']][$t] = $tv;
					}
				}
				//$parks[$v['ID']] = $v;
				$post_meta = get_post_meta($v['ID']);
				$metafieldarr=array('address','geo_latitude','geo_longitude','phone','email','website','location_public','amenities','state','city','acres');
				$metaarr=array();
				foreach($post_meta as $mk=>$mv){
					if(in_array($mk,$metafieldarr)) {
						if($mk =='amenities' && is_array($mv)){
							$ament = unserialize($mv[0]);
							$metaarr[$mk]=implode(',',$ament);
						//	pr($metaarr);
						}
						else{
					$metaarr[$mk]=$mv[0];}
				}}
				$parks[$v['ID']] = array_merge($parks[$v['ID']],$metaarr);
				$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
				$term = array();
				foreach($terminfo as $t){
					$t = get_object_vars($t);
					if($t['name'] !='Places'){
					$parks[$v['ID']]['term'][]=$t['name'];
					}
				}
				$parks[$v['ID']]['term']= implode(',',$parks[$v['ID']]['term']);
			}
		}
	}
	//pr($parks);

	$fp = fopen('park_report.xls', 'w');
	$csvtitle= array('ID','post_author','post_content','post_title','post_status','address','city','state','geo_latitude','geo_longitude','phone','email','website','location_public','acres','amenities','fields');
			fputcsv($fp, $csvtitle, "\t", '"');
			foreach ($parks as $k=>$fields) {
				$newarr['ID']=$fields['ID'];
				$newarr['post_author']=$fields['post_author'];
				$newarr['post_content']=$fields['post_content'];
				$newarr['post_title']=$fields['post_title'];
				$newarr['post_status']=$fields['post_status'];
				$newarr['address']=$fields['address'];
				$newarr['city']=$fields['city'];
				$newarr['state']=$fields['state'];
				$newarr['geo_latitude']=$fields['geo_latitude'];
				$newarr['geo_longitude']=$fields['geo_longitude'];
				$newarr['phone']=$fields['phone'];
				$newarr['email']=$fields['email'];
				$newarr['website']=$fields['website'];
				$newarr['location_public']=$fields['location_public'];
				$newarr['acres']=$fields['acres'];
				$newarr['amenities']=$fields['amenities'];
				$newarr['fields']=$fields['term'];
				fputcsv($fp, $newarr, "\t", '"');
				
			}
			fclose($fp);
?>